package com.szoldapps.tvtrackr.service;

import com.szoldapps.tvtrackr.db.model.Episode;
import com.szoldapps.tvtrackr.db.model.Show;
import com.szoldapps.tvtrackr.exception.RestException;
import com.szoldapps.tvtrackr.service.listener.ServiceResponseListener;





public interface CommentsService {

	/**
	 * Get Show Comments from Trakt
	 * @param show
	 * @param listener
	 * @throws RestException
	 */
	public void getShowComments(Show show, ServiceResponseListener listener);

	/**
	 * Send Show Comment to Trakt
	 * @param show
	 * @param msg
	 * @param spoiler
	 * @param listener
	 */
	public void sendComment(Show show, String msg, boolean spoiler, ServiceResponseListener listener);

	/**
	 * Get Episode Comments from Trakt
	 * @param episode
	 * @param listener
	 * @throws RestException
	 */
	public void getEpisodeComments(Episode episode, ServiceResponseListener listener);

	/**
	 * Send Episode Comment to Trakt
	 * @param episode
	 * @param msg
	 * @param spoiler
	 * @param listener
	 */
	public void sendComment(Episode episode, String msg, boolean spoiler, ServiceResponseListener listener);



}
