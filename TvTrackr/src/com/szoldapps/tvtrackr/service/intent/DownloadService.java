package com.szoldapps.tvtrackr.service.intent;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;

import org.apache.http.client.HttpResponseException;

import android.app.Activity;
import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.text.format.DateUtils;
import android.util.Log;

import com.szoldapps.tvtrackr.db.model.Episode;
import com.szoldapps.tvtrackr.db.model.OfflineShowToUnfollow;
import com.szoldapps.tvtrackr.db.model.Show;
import com.szoldapps.tvtrackr.db.model.Show.ShowDownloadStatus;
import com.szoldapps.tvtrackr.db.model.User;
import com.szoldapps.tvtrackr.dto.internal.InternalShowDownloadResponseDTO;
import com.szoldapps.tvtrackr.exception.EpisodeUserNotFoundException;
import com.szoldapps.tvtrackr.exception.NoValidDownloadRequestTypeException;
import com.szoldapps.tvtrackr.exception.WrongObjectReturnedException;
import com.szoldapps.tvtrackr.helper.Constants;
import com.szoldapps.tvtrackr.helper.DateHelper;
import com.szoldapps.tvtrackr.helper.LogCatHelper;
import com.szoldapps.tvtrackr.helper.Constants.DownloadErrorType;
import com.szoldapps.tvtrackr.helper.Constants.DownloadRequestType;
import com.szoldapps.tvtrackr.helper.Constants.DownloadResponseType;
import com.szoldapps.tvtrackr.service.EpisodeService;
import com.szoldapps.tvtrackr.service.OfflineService;
import com.szoldapps.tvtrackr.service.ShowService;
import com.szoldapps.tvtrackr.service.UserService;
import com.szoldapps.tvtrackr.service.impl.EpisodeServiceImpl;
import com.szoldapps.tvtrackr.service.impl.OfflineServiceImpl;
import com.szoldapps.tvtrackr.service.impl.ShowServiceImpl;
import com.szoldapps.tvtrackr.service.impl.UserServiceImpl;
import com.szoldapps.tvtrackr.service.listener.ServiceResponseListener;

public class DownloadService extends IntentService {

	public static final String LOGTAG = LogCatHelper.getAppTag()+DownloadService.class.getSimpleName().toString();
	public static final String DS_ID = "++ ";

	private ShowService mShowService;
	private UserService mUserService;
	private OfflineService mOfflineService;
	private EpisodeService mEpisodeService;

	private List<Show> updateList = new ArrayList<Show>();
	protected boolean mCredentialsAreOK;

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// ----- REQUEST_SERVICE_START ---------------------------------------------------------------------------------- //

	/**
	 * Starts {@link DownloadService} with the following Extras:
	 * <ul>
	 * 		<li>{@link Constants#EXTRA_REQUEST_TYPE} = {@link DownloadRequestType#COMPLETE_UPDATE_INCL_SHOWLIST_REFRESH}</li>
	 * </ul>
	 * 
	 * @param tvdbId
	 * @param context
	 * @category REQUEST_SERVICE_START
	 */
	public static void requestCompleteUpdateInclShowRefresh(Context context) {
		Intent intent = new Intent(context, DownloadService.class);
		intent.putExtra(Constants.EXTRA_REQUEST_TYPE, DownloadRequestType.COMPLETE_UPDATE_INCL_SHOWLIST_REFRESH);
		context.startService(intent);
	}
	
	/**
	 * Starts {@link DownloadService} with the following Extras:
	 * <ul>
	 * 		<li>{@link Constants#EXTRA_REQUEST_TYPE} = {@link DownloadRequestType#SHOW_DOWNLOAD}</li>
	 * 		<li>{@link Constants#EXTRA_SHOW_TVDB_ID} = <code>tvdbId</code></li>
	 * </ul>
	 * 
	 * @param tvdbId
	 * @param context
	 * @category REQUEST_SERVICE_START
	 */
	public static void requestShowDownload(int tvdbId, Context context) {
		Intent intent = new Intent(context, DownloadService.class);
		intent.putExtra(Constants.EXTRA_REQUEST_TYPE, DownloadRequestType.SHOW_DOWNLOAD);
		intent.putExtra(Constants.EXTRA_SHOW_TVDB_ID, tvdbId);
		context.startService(intent);
	}

	/**
	 * Starts {@link DownloadService} with the following Extras:
	 * <ul>
	 * 		<li>{@link Constants#EXTRA_REQUEST_TYPE} = {@link DownloadRequestType#LOAD_SHOW}</li>
	 * 		<li>{@link Constants#EXTRA_SHOW_TVDB_ID} = <code>tvdbId</code></li>
	 * </ul>
	 * 
	 * @param tvdbId
	 * @param forceDownload 
	 * @param context
	 * @category REQUEST_SERVICE_START
	 */
	public static void requestLoadShow(int tvdbId, boolean forceDownload, Context context) {
		Intent intent = new Intent(context, DownloadService.class);
		intent.putExtra(Constants.EXTRA_REQUEST_TYPE, DownloadRequestType.LOAD_SHOW);
		intent.putExtra(Constants.EXTRA_SHOW_TVDB_ID, tvdbId);
		intent.putExtra(Constants.EXTRA_FORCE_DOWNLOAD, forceDownload);
		context.startService(intent);
	}

	public DownloadService() {
		super("DownloadServiceSetup");
	}

	@Override
	public void onCreate() {
		super.onCreate();
		Log.d(LOGTAG, "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n" + 
				"++++++++++++++++++++++++++++++ DOWNLOADSERVICE STARTED ++++++++++++++++++++++++++++++");
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		Log.d(LOGTAG, "++++++++++++++++++++++++++++++ DOWNLOADSERVICE ENDED ++++++++++++++++++++++++++++++\n" + 
				"+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
	}


	// will be called asynchronously by Android
	@Override
	protected void onHandleIntent(Intent intent) {
		// Load Intent Extras
		DownloadRequestType request = (DownloadRequestType) intent.getSerializableExtra(Constants.EXTRA_REQUEST_TYPE);
		if(request != null) {
			try {
				mShowService = ShowServiceImpl.getService(getApplicationContext());
				mUserService = UserServiceImpl.getService(getApplicationContext());
				mOfflineService = OfflineServiceImpl.getService(getApplicationContext());
				mEpisodeService = EpisodeServiceImpl.getService(getApplicationContext());

				Log.d(LOGTAG, DS_ID + "HANDLING request "+request.toString());
				int tvdbId = -1;

				switch (request) {
				case COMPLETE_UPDATE_INCL_SHOWLIST_REFRESH:
					checkCredentials();
					if(mCredentialsAreOK) {
						offlineUploads();
						completeUpdateInclShowlistRefresh();
					}
					break;
				case SHOW_DOWNLOAD:
					tvdbId = intent.getIntExtra(Constants.EXTRA_SHOW_TVDB_ID, -1);
					if(tvdbId != -1) {
						offlineUploads();
						downloadSingleShow(tvdbId, true, -1);
					}
					break;
				case LOAD_SHOW:
					tvdbId = intent.getIntExtra(Constants.EXTRA_SHOW_TVDB_ID, -1);
					boolean forceDownload = intent.getBooleanExtra(Constants.EXTRA_FORCE_DOWNLOAD, false);
					if(tvdbId != -1) {
						offlineUploads();
						loadShow(tvdbId, forceDownload);
					}
					break;
				default:
					Log.d(LOGTAG, DS_ID + "Nothing to HANDLE, no valid DownloadRequestType");
					break;
				}
			} catch (SQLException e) {
				publishError(e);
				return;
			} catch (EpisodeUserNotFoundException e) {
				publishError(e);
				return;
			}
		} else {
			publishError(new NoValidDownloadRequestTypeException("Nothing to HANDLE, no valid DownloadRequestType"));
			Log.d(LOGTAG, DS_ID + "Nothing to HANDLE, no valid DownloadRequestType");
		}

	}

	/**
	 * Checks whether current User credentials are still valid.
	 * if true, nothing happens
	 * if false, {@link DownloadErrorType#WRONG_CREDENTIALS_ERROR} is published and Service is stopped
	 */
	private void checkCredentials() {
		mCredentialsAreOK = false;
		mUserService.checkCredentialsOfCurrentUser(new ServiceResponseListener() {
			@Override
			public void onProgress(int bytesWritten, int totalSize) { }

			@Override
			public void onSuccess(Object o) {
				if(o instanceof User) {
					// ALL GOOD, current User credentials are still valid
					Log.d(LOGTAG, DS_ID + "ALL GOOD, current User credentials are still valid");
					mCredentialsAreOK = true;
				} else {
					onFailure(new WrongObjectReturnedException(User.class));
				}
			}

			@Override
			public void onFailure(Throwable throwable) {
				throwable.printStackTrace();
				publishError(throwable);
			}
		});
	}

	private void completeUpdateInclShowlistRefresh() throws SQLException, EpisodeUserNotFoundException {

		// Download new Showlist from trakt and sync it up with local list
		// Shows are added or removed locally (Trakt-List always wins)
		fullyDownloadShowsOrAddToUpdateList();
		// Update ShowData of Shows in updateList
		updateShowData();
		// Update ShowUserData of Shows in updateList
		updateShowUserData();
		// Publish that completeUpdate Process is done
		publishUpdateAllShowsDone();
	}

	private Throwable mThrowable = null;

	private void offlineUploads() throws SQLException, EpisodeUserNotFoundException {
		mThrowable = null;
		// Upload offline unfollowed Shows (and delete locally)
		List<OfflineShowToUnfollow> showsToDelete = mOfflineService.getAllOfflineShowsToUnfollow();
		for (OfflineShowToUnfollow oShow : showsToDelete) {
			final int tvdbId = oShow.getTvdbID();
			mShowService.uploadUnfollow(oShow, new ServiceResponseListener() {
				@Override
				public void onProgress(int bytesWritten, int totalSize) { }

				@Override
				public void onSuccess(Object o) {
					if(o instanceof Boolean) {
						if((Boolean)o) {
							// Remove OfflineShowToUnfollow from DB because its job is done
							try {
								mOfflineService.deleteOfflineShowToUnfollowById(tvdbId);
								return;
							} catch (SQLException e) {
								mThrowable = e;
								return;
							}
						}
					}
					mThrowable = new WrongObjectReturnedException(Boolean.class);
				}

				@Override
				public void onFailure(Throwable throwable) {
					mThrowable = throwable;
				}
			});
			if(mThrowable != null) {
				publishError(mThrowable);
				return;
			}
		}

		// Upload offline marked Episodes (of Shows that weren't offline unfollowed)
		for (Entry<Show, List<Episode>> entry : mOfflineService.getAllOfflineMarkedEpisodesShowMap().entrySet()) {
			Show show = entry.getKey();
			List<Episode> episodes = entry.getValue();
			if(episodes == null || episodes.isEmpty()) { continue; }

			mShowService.uploadEpisodesSeenOrUnseen(mEpisodeService.getEpisodeUser(episodes.get(0)).isWatched(), show, episodes, new ServiceResponseListener() {
				@Override
				public void onProgress(int bytesWritten, int totalSize) { }

				@Override
				public void onSuccess(Object o) {
					if(o instanceof Boolean) {
						if((Boolean)o) {
							// ALL GOOD
							return;
						}
					}
					mThrowable = new WrongObjectReturnedException(Boolean.class);
				}

				@Override
				public void onFailure(Throwable throwable) {
					mThrowable = throwable;
				}
			});

			if(mThrowable != null) {
				publishError(mThrowable);
				return;
			}
		}

	}

	private void fullyDownloadShowsOrAddToUpdateList() {

		mShowService.downloadShowlistFromTraktAndSaveToDB(new ServiceResponseListener() {
			@Override public void onProgress(int bytesWritten, int totalSize) { }

			@Override @SuppressWarnings({ "unchecked" })
			public void onSuccess(Object o) {
				try {
					if(o != null) {
						List<Show> allShows = (List<Show>) o;
						publishCompleteUpdateInterimResult();

						// reset updateList
						updateList = new ArrayList<Show>();
						if(allShows.isEmpty()) {
							return;
						}

						// download Shows which are not yet fully downloaded
						for (Show show : allShows) {
							fullyDownloadShowOrAddToUpdateList(show);
						}
						Log.w(LOGTAG, DS_ID + "------ ALL SHOWS => FULLY DOWNLOADED (updateList.size="+updateList.size()+") --------------------------");
					}
				} catch (SQLException e) {
					onFailure(e);
				}
			}

			@Override
			public void onFailure(Throwable throwable) {
				publishError(throwable);
			}
		});
	}

	/**
	 * downloads complete Showdetails or adds show to updateList
	 * @param tvdbId
	 * @param show
	 * @throws SQLException 
	 */
	private void fullyDownloadShowOrAddToUpdateList(Show show) throws SQLException {

		if(show == null) {
			return;
		}
		int tvdbId = show.getTvdbID();

		if(show.isFullyDownloaded()) {
			// Just a quick publish to tell if show isfullyDownloaded, show could be displayed
			Log.d(LOGTAG, DS_ID + "Show ("+show.getTitle()+") is fully downloaded. -> UpdateList.ADD");
			// add to UpdateList
			updateList.add(show);
		} else {
			Log.d(LOGTAG, DS_ID + "Show ("+show.getTitle()+") is NOT fully downloaded. -> DOWNLOADSINGLESHOW");
			downloadSingleShow(tvdbId, true, -1);
		}
	}

	private void downloadSingleShow(final int tvdbId, final boolean forcePublish, final long lastUpdated) throws SQLException {
		final boolean oldFollowedStatus = mShowService.isShowFollowedByCurrentUser(tvdbId);
		publishShowStatusUpdates(tvdbId, ShowDownloadStatus.DOWNLOADING);
		Log.d(LOGTAG, DS_ID + "DOWNLOADING Show="+tvdbId);
		mShowService.downloadShowFromTraktAndSaveToDB(tvdbId, new ServiceResponseListener() {
			@Override public void onProgress(int bytesWritten, int totalSize) {
				if(bytesWritten == -1 && totalSize == -1) {
					try {
						publishShowStatusUpdates(tvdbId, ShowDownloadStatus.SAVING);
					} catch (SQLException e) {
						onFailure(e);
					}
				}
			}

			@Override
			public void onSuccess(Object o) {
				if(o instanceof InternalShowDownloadResponseDTO) {
					try {
						InternalShowDownloadResponseDTO resultDTO = (InternalShowDownloadResponseDTO) o;
						Show show = resultDTO.getShow();
						if(show != null) {
							if(forcePublish || mShowService.hasFollowedStatusChanged(oldFollowedStatus, tvdbId)) {
								publishShowStatusUpdates(tvdbId, show.getDownloadStatus());
							} else {
								if(lastUpdated > -1) {
									if(lastUpdated == show.getLastUpdated()) {
										Log.w(LOGTAG, DS_ID + "Nothing new, so no update");
										return;
									}
								}
								publishShowStatusUpdates(tvdbId, show.getDownloadStatus());
							}
						}
					} catch (SQLException e) {
						publishError(e);
					}
				}
			}

			@Override
			public void onFailure(Throwable throwable) {
				publishError(throwable);
			}
		});

	}


	private void updateShowData() throws SQLException {
		if(!updateList.isEmpty()) {
			mShowService.updateShowData(updateList, new ServiceResponseListener() {

				@Override public void onProgress(int bytesWritten, int totalSize) { }

				@Override @SuppressWarnings("unchecked")
				public void onSuccess(Object o) {
					if(o != null) {
						try {
							List<Show> showsThatShouldBeUpdated = (List<Show>) o;
							for (Show show : showsThatShouldBeUpdated) {
								updateList.remove(show);
								Log.w(LOGTAG, DS_ID + show.getTitle()+" isFullyDownloaded="+show.isFullyDownloaded());
								downloadSingleShow(show.getTvdbID(), true, show.getLastUpdated());
							}
						} catch (SQLException e) {
							onFailure(e);
						}
					}
				}

				@Override
				public void onFailure(Throwable throwable) {
					publishError(throwable);
				}
			});
		}

	}

	/**
	 * updates all ShowUser data (marks shows or episodes as seen or unseen, NO Episode-Ratings are updated)
	 * @throws SQLException 
	 */
	private void updateShowUserData() throws SQLException {
		if(!updateList.isEmpty()) {
			for (Show show : updateList) {
				publishShowStatusUpdates(show.getTvdbID(), ShowDownloadStatus.UPDATING);
			}
			mShowService.updateAllShowUserData(new ServiceResponseListener() {

				@Override public void onProgress(int bytesWritten, int totalSize) { }

				@Override
				public void onSuccess(Object o) {
					try {
						for (Show show : updateList) {
							publishShowStatusUpdates(show.getTvdbID(), ShowDownloadStatus.DONE);
						}
						updateList.clear();
					} catch (SQLException e) {
						onFailure(e);
					}
				}

				@Override
				public void onFailure(Throwable throwable) {
					publishError(throwable);
				}
			});
		}
	}


	private void loadShow(int tvdbId, boolean forceDownload) {
		try {
			if(forceDownload) {
				downloadSingleShow(tvdbId, false, -1);
			} else {

				Show show = mShowService.getShowById(tvdbId);
				if(show != null) {
					Log.d(LOGTAG, DS_ID + "Show ("+tvdbId+") is already in DB");
					try {
						publishShowStatusUpdates(tvdbId, ShowDownloadStatus.DONE);

						// CHECK whether Show was updated within the last 5 minutes, IF true, do not update
						long difference = DateHelper.getCurrentTime() - show.getLastDBSave();
						long minDiffToUpdate = 5*DateUtils.MINUTE_IN_MILLIS;	// DEBUG
						if(difference < minDiffToUpdate) {
							Log.w(LOGTAG, DS_ID + "difference ("+difference+") is under 5min (300.000), so no update");
							publishLoadShowDownloadProcessDone(tvdbId, true);
							return;
						}

						downloadSingleShow(tvdbId, false, show.getLastUpdated());

					} catch (SQLException e) {
						publishError(e);
					}
				} else {
					downloadSingleShow(tvdbId, false, -1);
				}

			}
		} catch (SQLException e) {
			publishError(e);
		}

		publishLoadShowDownloadProcessDone(tvdbId, true);
	}


	/** -------------------------------------------------------- **/
	/** ------------------ PUBLISHING RESULTS ------------------ **/

	private void publishError(Throwable throwable) {
		Log.e(LOGTAG, DS_ID + "published an Error");
		Intent intent = new Intent(Constants.EXTRA_DOWNLOAD_SERVICE_NOTIFICATION);
		intent.putExtra(Constants.EXTRA_RESULT, Activity.RESULT_CANCELED);

		DownloadErrorType errorType = DownloadErrorType.UNKNOWN_ERROR;
		if(throwable instanceof NoValidDownloadRequestTypeException) {
			errorType = DownloadErrorType.NO_VALID_REQUEST_TYPE_ERROR;
		} else if(throwable instanceof SQLException) {
			errorType = DownloadErrorType.SQL_ERROR;
		} else if (throwable instanceof HttpResponseException) {
			errorType = DownloadErrorType.WRONG_CREDENTIALS_ERROR;
		} else if (throwable instanceof IOException) {
			errorType = DownloadErrorType.NO_INTERNET_ERROR;
		} 
		intent.putExtra(Constants.EXTRA_ERROR_TYPE, errorType);
		sendBroadcast(intent);
	}

	private void publishShowStatusUpdates(int tvdbId, ShowDownloadStatus showDownloadStatus) throws SQLException {
		mShowService.setDownloadStatus(tvdbId, showDownloadStatus);
		Intent intent = new Intent(Constants.EXTRA_DOWNLOAD_SERVICE_NOTIFICATION);
		intent.putExtra(Constants.EXTRA_RESULT, Activity.RESULT_OK);
		intent.putExtra(Constants.EXTRA_RESPONSE_TYPE, DownloadResponseType.ONE_SHOW_UPDATE);
		intent.putExtra(Constants.EXTRA_SHOW_TVDB_ID, tvdbId);
		intent.putExtra(Constants.EXTRA_SHOW_DOWNLOAD_STATUS, showDownloadStatus);
		sendBroadcast(intent);
	}

	private void publishLoadShowDownloadProcessDone(int tvdbId, boolean up2dateStatus) {
		Intent intent = new Intent(Constants.EXTRA_DOWNLOAD_SERVICE_NOTIFICATION);
		intent.putExtra(Constants.EXTRA_RESULT, Activity.RESULT_OK);
		intent.putExtra(Constants.EXTRA_RESPONSE_TYPE, DownloadResponseType.ONE_SHOW_UPDATE);
		intent.putExtra(Constants.EXTRA_SHOW_TVDB_ID, tvdbId);
		intent.putExtra(Constants.EXTRA_UP2DATE_STATUS, up2dateStatus);
		sendBroadcast(intent);
	}

	private void publishCompleteUpdateInterimResult() throws SQLException {
		mUserService.updateLastCompleteUpdate();
		Intent intent = new Intent(Constants.EXTRA_DOWNLOAD_SERVICE_NOTIFICATION);
		intent.putExtra(Constants.EXTRA_RESULT, Activity.RESULT_OK);
		intent.putExtra(Constants.EXTRA_RESPONSE_TYPE, DownloadResponseType.COMPLETE_UPDATE_INTERIM_RESULT);
		sendBroadcast(intent);
	}

	private void publishUpdateAllShowsDone() throws SQLException {
		mUserService.updateLastCompleteUpdate();
		Intent intent = new Intent(Constants.EXTRA_DOWNLOAD_SERVICE_NOTIFICATION);
		intent.putExtra(Constants.EXTRA_RESULT, Activity.RESULT_OK);
		intent.putExtra(Constants.EXTRA_RESPONSE_TYPE, DownloadResponseType.COMPLETE_UPDATE_FINAL_RESULT);
		sendBroadcast(intent);
	}
} 

