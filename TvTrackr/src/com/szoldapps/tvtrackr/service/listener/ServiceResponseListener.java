package com.szoldapps.tvtrackr.service.listener;

public interface ServiceResponseListener {
    public void onSuccess(Object o);
    public void onFailure(Throwable throwable);
    public void onProgress(int bytesWritten, int totalSize);
}