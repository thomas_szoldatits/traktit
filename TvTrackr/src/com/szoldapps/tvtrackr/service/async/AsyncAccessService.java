package com.szoldapps.tvtrackr.service.async;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.sql.SQLException;
import java.util.List;

import org.apache.http.client.HttpResponseException;
import org.apache.http.entity.StringEntity;

import com.szoldapps.tvtrackr.activity.main.recommendations.RecommendationsListArrayAdapter;
import com.szoldapps.tvtrackr.db.model.Actor;
import com.szoldapps.tvtrackr.db.model.Episode;
import com.szoldapps.tvtrackr.db.model.EpisodeUser;
import com.szoldapps.tvtrackr.db.model.OfflineShowToUnfollow;
import com.szoldapps.tvtrackr.db.model.Rating;
import com.szoldapps.tvtrackr.db.model.Season;
import com.szoldapps.tvtrackr.db.model.Show;
import com.szoldapps.tvtrackr.db.model.ShowUser;
import com.szoldapps.tvtrackr.db.model.User;
import com.szoldapps.tvtrackr.dto.internal.InternalEpisodeInfosDTO;
import com.szoldapps.tvtrackr.dto.internal.InternalProgressDTO;
import com.szoldapps.tvtrackr.dto.internal.InternalSeasonsFragmentDTO;
import com.szoldapps.tvtrackr.dto.internal.InternalToWatchDTO;
import com.szoldapps.tvtrackr.dto.response.ResponseStatsDTO;
import com.szoldapps.tvtrackr.dto.show.MediumShowDTO;
import com.szoldapps.tvtrackr.dto.show.SearchShowDTO;
import com.szoldapps.tvtrackr.exception.FailedAuthentificationException;
import com.szoldapps.tvtrackr.exception.UserNotFoundException;
import com.szoldapps.tvtrackr.service.intent.DownloadService;
import com.szoldapps.tvtrackr.service.listener.ServiceResponseListener;

/**
 * Method naming convention:
 * <ul>
 * 		<li>upload/download ... uploads/downloads data to trakt and persists changes to local DB</li>
 * 		<li>get/set ... data is get/set only locally</li>
 * </ul>
 * @author Thomas Szoldatits
 *
 */
public interface AsyncAccessService {

	/**
	 * <p>
	 * 1) Watchlists <code>medShow</code> on trakt.tv. <br>
	 * 2) Stores <code>medShow</code> in local DB. <br>
	 * 3) Requests complete Download from {@link DownloadService}
	 * TODO: JAVADOC: Description of what happens, when no Inet Access
	 * </p>
	 * <b>Returns (via <code>listener</code>) either:</b> <br>
	 * {@link ServiceResponseListener#onSuccess(Object)}
	 * <ul>
	 * 		<li>{@link Boolean} ... <code>TRUE</code> (if everything worked) or <code>FALSE</code> (if no No Internet)</li>
	 * </ul>
	 * or {@link ServiceResponseListener#onFailure(Throwable)}
	 * <ul>
	 * 		<li>{@link SQLException} ... on any SQL Problem (e.g. Access-Problem)</li>
	 * 		<li>{@link HttpResponseException} ... Wrong Login Credentials</li>
	 * 		<li>{@link UnsupportedEncodingException} ... Problem during {@link StringEntity} creation</li>
	 * </ul>
	 * 
	 * @param medShow
	 * @param listener
	 */
	public void uploadFollowAndSaveShow(MediumShowDTO medShow, ServiceResponseListener listener);

	/**
	 * <p>
	 * 1) Watchlists <code>show</code> on trakt.tv. <br>
	 * 2) Adds {@link ShowUser} & {@link EpisodeUser} relations to already fullyDownloaded <code>show</code>. <br>
	 * TODO: JAVADOC: Description of what happens, when no Inet Access
	 * </p>
	 * <b>Returns (via <code>listener</code>) either:</b> <br>
	 * {@link ServiceResponseListener#onSuccess(Object)}
	 * <ul>
	 * 		<li>{@link Boolean} ... <code>TRUE</code> (if everything worked) or <code>FALSE</code> (if no No Internet)</li>
	 * </ul>
	 * or {@link ServiceResponseListener#onFailure(Throwable)}
	 * <ul>
	 * 		<li>{@link SQLException} ... on any SQL Problem (e.g. Access-Problem)</li>
	 * 		<li>{@link HttpResponseException} ... Wrong Login Credentials</li>
	 * 		<li>{@link UnsupportedEncodingException} ... Problem during {@link StringEntity} creation</li>
	 * </ul>
	 * 
	 * @param show
	 * @param listener
	 */
	public void uploadFollowAndSaveShow(Show show, ServiceResponseListener listener);

	/**
	 * <p>Unfollows (Unwatchlist & unsee) {@link Show} on Trakt and removes Show from DB. <br>
	 * Definitely removes every ShowUser and EpisodeUser Relations.<br>
	 * If <code>preventCompleteRemoval</code> = FALSE AND no other User follows this Show -> the complete Show is removed.<br>
	 * If <code>preventCompleteRemoval</code> = TRUE OR any other User follows this Show -> no further actions<br>
	 * If No Internet -> {@link OfflineShowToUnfollow} is created in DB but <code>show</code> is still deleted in DB.
	 * </p>
	 * <b>Returns (via <code>listener</code>) either</b><br>
	 * {@link ServiceResponseListener#onSuccess(Object)}
	 * <ul>
	 * 		<li>{@link Boolean} ... <code>TRUE</code> (if everything worked) or <code>FALSE</code> (if no No Internet)</li>
	 * </ul>
	 * or {@link ServiceResponseListener#onFailure(Throwable)}
	 * <ul>
	 * 		<li>{@link SQLException} ... on any SQL Problem (e.g. Access-Problem)</li>
	 * 		<li>{@link HttpResponseException} ... Wrong Login Credentials</li>
	 * 		<li>{@link UnsupportedEncodingException} ... Problem during {@link StringEntity} creation</li>
	 * </ul>
	 * 
	 * @param show
	 * @param preventCompleteRemoval
	 * @param listener
	 */
	public void uploadUnfollowShow(Show show, boolean preventCompleteRemoval, ServiceResponseListener listener);

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// ----- SEEN_UNSEEN -------------------------------------------------------------------------------------------- //
	/**
	 * <p>If <code>markAsSeen</code>=TRUE -> Marks all <b>aired</b> Episodes of <code>show</code> as seen for 
	 * currentUser.<br>
	 * If <code>markAsSeen</code>=FALSE -> Marks all Episodes of <code>show</code> as unseen for currentUser and checks
	 * whether all Episodes are already marked unseen. If that is TRUE, show is watchlisted.<br>
	 * If there is no Internet connection, Episodes are markedOffline.</p>
	 * Returns (via <code>listener</code>) either:<br> 
	 * {@link ServiceResponseListener#onSuccess(Object)}
	 * <ul>
	 * 		<li>{@link Boolean} ... TRUE if everything worked, FALSE if there was no Internet Connection 
	 * 			-> Episodes marked Offline</li>
	 * </ul>
	 * or {@link ServiceResponseListener#onFailure(Throwable)}
	 * <ul>
	 * 		<li>{@link HttpResponseException} ... invalid user credentials</li>
	 * 		<li>{@link UnsupportedEncodingException} ... Problem during {@link StringEntity} creation</li>
	 * 		<li>{@link SQLException} ... on any SQL Problem (e.g. Access-Problem)</li>
	 * </ul>
	 * 
	 * @param markAsSeen
	 * @param season
	 * @param listener
	 * @category SEEN_UNSEEN
	 */
	public void uploadShowSeenOrUnseen(boolean markAsSeen, Show show, ServiceResponseListener listener);

	/**
	 * <p>If <code>markAsSeen</code>=TRUE -> Marks all <b>aired</b> Episodes of <code>season</code> as seen for 
	 * currentUser.<br>
	 * If <code>markAsSeen</code>=FALSE -> Marks all Episodes of <code>season</code> as unseen for currentUser and 
	 * checks whether all Episodes are already marked unseen. If that is TRUE, show is watchlisted.<br>
	 * If there is no Internet connection, Episodes are markedOffline.</p>
	 * Returns (via <code>listener</code>) either:<br> 
	 * {@link ServiceResponseListener#onSuccess(Object)}
	 * <ul>
	 * 		<li>{@link Boolean} ... TRUE if everything worked, FALSE if there was no Internet Connection 
	 * 			-> Episodes marked Offline</li>
	 * </ul>
	 * or {@link ServiceResponseListener#onFailure(Throwable)}
	 * <ul>
	 * 		<li>{@link HttpResponseException} ... invalid user credentials</li>
	 * 		<li>{@link UnsupportedEncodingException} ... Problem during {@link StringEntity} creation</li>
	 * 		<li>{@link SQLException} ... on any SQL Problem (e.g. Access-Problem)</li>
	 * </ul>
	 * 
	 * @param markAsSeen
	 * @param episodes
	 * @param listener
	 * @category SEEN_UNSEEN
	 */
	public void uploadSeasonSeenOrUnseen(boolean markAsSeen, Season season, ServiceResponseListener listener);

	/**
	 * <p>Marks <code>episode</code> as seen/unseen for currentUser (depending on <code>markAsSeen</code>) and 
	 * checks whether all Episodes are already marked unseen. If that is TRUE, show is watchlisted.<br>
	 * If there is no Internet connection, Episode is markedOffline.</p>
	 * Returns (via <code>listener</code>) either<br>
	 * {@link ServiceResponseListener#onSuccess(Object)}
	 * <ul>
	 * 		<li>{@link Boolean} ... TRUE if everything worked, FALSE if there was no Internet Connection 
	 * 			-> Episodes marked Offline</li>
	 * </ul>
	 * or {@link ServiceResponseListener#onFailure(Throwable)}
	 * <ul>
	 * 		<li>{@link SQLException} ... on any SQL Problem (e.g. Access-Problem)</li>
	 * 		<li>{@link UnsupportedEncodingException} ... Problem during {@link StringEntity} creation</li>
	 * 		<li>{@link HttpResponseException} ... Wrong Login Credentials</li>
	 * </ul>
	 * 
	 * @param episode
	 * @param listener
	 * @category SEEN_UNSEEN
	 */
	public void uploadEpisodeSeenToggle(Episode episode, ServiceResponseListener listener);

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// ----- EPISODE ------------------------------------------------------------------------------------------------ //

	/**
	 * <p>Checks (in local DB) whether the episode is seen or not seen by the current User.</p>
	 * <b>Returns (via <code>listener</code>) either</b><br>
	 * {@link ServiceResponseListener#onSuccess(Object)}
	 * <ul>
	 * 		<li>{@link Boolean} ... <code>true</code> if seen, <code>false</code> if unseen or no EpisodeUser Relation</li>
	 * </ul>
	 * or {@link ServiceResponseListener#onFailure(Throwable)}
	 * <ul>
	 * 		<li>{@link SQLException} ... on any SQL Problem (e.g. Access-Problem)</li>
	 * </ul>
	 * 
	 * @param episode
	 * @param listener
	 */
	public void isEpisodeSeen(Episode episode, ServiceResponseListener listener);

	/**
	 * <p>Gets Episode infos (like episodeCount, position or tabNames) from local DB for given <code>tvdbId</code>.<br>
	 * Remark: {@link InternalEpisodeInfosDTO#setPosition(int)} is only checked if <code>episodeID</code> != -1</p>
	 * <b>Returns (via <code>listener</code>) either</b><br>
	 * {@link ServiceResponseListener#onSuccess(Object)}
	 * <ul>
	 * 		<li>{@link InternalEpisodeInfosDTO}</li>
	 * </ul>
	 * or {@link ServiceResponseListener#onFailure(Throwable)}
	 * <ul>
	 * 		<li>{@link SQLException} ... on any SQL Problem (e.g. Access-Problem)</li>
	 * </ul>
	 * 
	 * @param tvdbId
	 * @param episodeId
	 * @param listener
	 */
	public void getEpisodeInfosOfShow(int tvdbId, int episodeId, ServiceResponseListener listener);

	/**
	 * <p>Gets {@link Episode} of {@link Show} with <code>showTvdbId</code> by <code>position</code><br>
	 * Returns (via <code>listener</code>) either</p>
	 * {@link ServiceResponseListener#onSuccess(Object)}
	 * <ul>
	 * <li>{@link Episode}</li>
	 * </ul>
	 * or {@link ServiceResponseListener#onFailure(Throwable)}
	 * <ul>
	 * <li>{@link SQLException} ... on any SQL Problem (e.g. Access-Problem)</li>
	 * </ul>
	 * 
	 * @param position
	 * @param showTvdbId
	 * @param listener
	 */
	public void getEpisodeByPosition(int position, int showTvdbId, ServiceResponseListener listener);


	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// ----- SHOW --------------------------------------------------------------------------------------------------- //
	/**
	 * <p>Gets Show from DB. Returns (via <code>listener</code>) either</p>
	 * {@link ServiceResponseListener#onSuccess(Object)}
	 * <ul>
	 * <li>{@link Show}</li>
	 * </ul>
	 * or {@link ServiceResponseListener#onFailure(Throwable)}
	 * <ul>
	 * <li>{@link SQLException} ... on any SQL Problem (e.g. Access-Problem)</li>
	 * @param mTvdbId
	 * @param listener
	 * @category SHOW
	 */
	public void getShowById(int mTvdbId, ServiceResponseListener listener);

	/**
	 * <p>Gets all {@link Show}s of the current User (from DB)
	 * <br> Returns (via <code>listener</code>) either</p>
	 * {@link ServiceResponseListener#onSuccess(Object)}
	 * <ul>
	 * <li>{@link List}<{@link Show}></li>
	 * </ul>
	 * or {@link ServiceResponseListener#onFailure(Throwable)}
	 * <ul>
	 * <li>{@link SQLException} ... on any SQL Problem (e.g. Access-Problem)</li>
	 * 
	 * @param listener
	 * @category SHOW
	 */
	public void getAllShowsOfUser(ServiceResponseListener listener);

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// ----- PROGRESS ----------------------------------------------------------------------------------------------- //
	/**
	 * <p>Calculates watchedEpisodeCount and episodeCount of Show and current User.</p>
	 * <b>Returns (via <code>listener</code>) either</b><br>
	 * {@link ServiceResponseListener#onSuccess(Object)}
	 * <ul>
	 * 		<li>{@link InternalProgressDTO}</li>
	 * </ul>
	 * or {@link ServiceResponseListener#onFailure(Throwable)}
	 * <ul>
	 * 		<li>{@link SQLException} ... on any SQL Problem (e.g. Access-Problem)</li>
	 * </ul>
	 * 
	 * @param show
	 * @param listener
	 * @category PROGRESS
	 */
	public void getProgressOfAiredEpisodes(Show show, ServiceResponseListener listener);

	/**
	 * <p>Calculates watchedEpisodeCount and episodeCount of Season and current User.</p>
	 * <b>Returns (via <code>listener</code>) either</b><br>
	 * {@link ServiceResponseListener#onSuccess(Object)}
	 * <ul>
	 * 		<li>{@link InternalProgressDTO}</li>
	 * </ul>
	 * or {@link ServiceResponseListener#onFailure(Throwable)}
	 * <ul>
	 * 		<li>{@link SQLException} ... on any SQL Problem (e.g. Access-Problem)</li>
	 * </ul>
	 * 
	 * @param season
	 * @param listener
	 * @category PROGRESS
	 */
	public void getProgressOfAiredEpisodes(Season season, ServiceResponseListener listener);




	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// ----- USER ------------------------------------------------------------------------------------------- //

	/**
	 * <p>Tries to log in {@link User} <code>user</code>. </br>Returns (via <code>listener</code>) either </p>
	 * {@link ServiceResponseListener#onSuccess(Object)}
	 * <ul>
	 * <li>{@link User} object ... which is also set as the current User</li>
	 * </ul>
	 * or {@link ServiceResponseListener#onFailure(Throwable)}
	 * <ul>
	 * <li>{@link FailedAuthentificationException} ... invalid user credentials</li>
	 * <li>{@link SQLException} ... on any SQL Problem (e.g. Access-Problem)</li>
	 * <li>{@link IOException} ... No Internet Access (or URL wrong)</li>
	 * </ul>
	 * @param user
	 * @param listener
	 * @category USER
	 */
	public void login(User user, ServiceResponseListener listener);

	/**
	 * <p>Logs out the current {@link User}. </br>Returns (via <code>listener</code>) either </p>
	 * {@link ServiceResponseListener#onSuccess(Object)}
	 * <ul>
	 * <li>{@link User} ... which is then NOT set as the current User</li>
	 * <li><code>null</code> ... if no currentUser was set</li>
	 * </ul>
	 * or {@link ServiceResponseListener#onFailure(Throwable)}
	 * <ul>
	 * <li>{@link SQLException} ... on any SQL Problem (e.g. Access-Problem)</li>
	 * </ul>
	 * @param listener
	 * @category USER
	 */
	public void logout(ServiceResponseListener listener);

	/**
	 * TODO: JAVADOC
	 * 
	 * @param listener
	 * @category USER
	 */
	public void getLastCompleteUpdateString(ServiceResponseListener listener);

	/**
	 * TODO: JAVADOC
	 * 
	 * @param forceDownload
	 * @param listener
	 * @category USER
	 */
	public void getCurrentUser(boolean forceDownload, ServiceResponseListener listener);

	/**
	 * TODO: JAVADOC
	 * 
	 * @param user
	 * @param listener
	 * @category USER
	 */
	public void register(User user, ServiceResponseListener listener);


	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// ----- SEARCH ------------------------------------------------------------------------------------------------- //

	/**
	 * <p>Searches for Shows. Returns (via <code>listener</code>) either</p>
	 * <b>Returns (via <code>listener</code>) either</b><br>
	 * {@link ServiceResponseListener#onSuccess(Object)}
	 * <ul>
	 * 		<li>List<{@link SearchShowDTO}></li>
	 * </ul>
	 * or {@link ServiceResponseListener#onFailure(Throwable)}
	 * <ul>
	 * 		<li>{@link SQLException} ... on any SQL Problem (e.g. Access-Problem)</li>
	 * 		<li>{@link IOException} ... No Internet Access (or URL wrong)</li>
	 * </ul>
	 * 
	 * @param query
	 * @param limit
	 * @param listener
	 */
	public void search(String query, int limit, ServiceResponseListener listener);

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// ----- CALENDAR ----------------------------------------------------------------------------------------------- //
	/**
	 * <p>Gets {@link List} of {@link Episode}s of the current Day plus minus the offset <br>
	 * <i>(e.g. yesterday has an offset of -1, tomorrow +1)</i></p>
	 * 
	 * <b>Returns (via <code>listener</code>) either</b><br>
	 * {@link ServiceResponseListener#onSuccess(Object)}
	 * <ul>
	 * 		<li>List<{@link SearchShowDTO}></li>
	 * </ul>
	 * or {@link ServiceResponseListener#onFailure(Throwable)}
	 * <ul>
	 * 		<li>{@link SQLException} ... on any SQL Problem (e.g. Access-Problem)</li>
	 * 		<li>{@link IOException} ... No Internet Access (or URL wrong)</li>
	 * </ul>
	 * 
	 * @param offset
	 * @param listener
	 * @category CALENDAR
	 */
	public void getEpisodesOfDay(int offset, ServiceResponseListener listener);

	/**
	 * <p>Returns a count of all unwatched Episodes of the day <br>
	 * <i>(offset=0 equals today, offset=1 equals tomorrow, etc.)</i></p>
	 * 
	 * <b>Returns (via <code>listener</code>) either</b><br>
	 * {@link ServiceResponseListener#onSuccess(Object)}
	 * <ul>
	 * 		<li>{@link Long} ... unwatched episode count of requested day</li>
	 * </ul>
	 * or {@link ServiceResponseListener#onFailure(Throwable)}
	 * <ul>
	 * 		<li>{@link SQLException} ... on any SQL Problem (e.g. Access-Problem)</li>
	 * </ul>
	 * 
	 * @param offset
	 * @param listener
	 * @category CALENDAR
	 */
	public void getUnwatchedEpisodeCountOfDay(int offset, ServiceResponseListener listener);


	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// ----- TO_WATCH ----------------------------------------------------------------------------------------------- //

	/**
	 * <p>Gets {@link List} of {@link Show}s that has unseen Episodes</p>
	 * <b>Returns (via <code>listener</code>) either</b><br>
	 * {@link ServiceResponseListener#onSuccess(Object)}
	 * <ul>
	 * 		<li> {@link List}<{@link Show}></li>
	 * </ul>
	 * or {@link ServiceResponseListener#onFailure(Throwable)}
	 * <ul>
	 * 		<li>{@link SQLException} ... on any SQL Problem (e.g. Access-Problem)</li>
	 * </ul>
	 * 
	 * @param listener
	 * @category TO_WATCH
	 */
	public void getToWatchShows(ServiceResponseListener listener);

	/**
	 * <p>Fills <code>toWatchDTO</code> with episode and episodesLeft.</p>
	 * <b>Returns (via <code>listener</code>) either</b><br>
	 * {@link ServiceResponseListener#onSuccess(Object)}
	 * <ul>
	 * 		<li>{@link InternalToWatchDTO}</li>
	 * </ul>
	 * or {@link ServiceResponseListener#onFailure(Throwable)}
	 * <ul>
	 * 		<li>{@link SQLException} ... on any SQL Problem (e.g. Access-Problem)</li>
	 * </ul>
	 * 
	 * @param toWatchDTO
	 * @param showTvdbId
	 * @param listener
	 * @category TO_WATCH
	 */
	public void getNextEpisodeToWatchAndLeftEpisodeCount(InternalToWatchDTO toWatchDTO, int showTvdbId, ServiceResponseListener listener);

	/**
	 * <p>Calculates {@link Long} count of all Episodes left to watch for currentUser</p>
	 * <b>Returns (via <code>listener</code>) either</b><br>
	 * {@link ServiceResponseListener#onSuccess(Object)}
	 * <ul>
	 * 		<li>{@link Long} ... count of all Episodes left to watch for currentUser</li>
	 * </ul>
	 * or {@link ServiceResponseListener#onFailure(Throwable)}
	 * <ul>
	 * 		<li>{@link SQLException} ... on any SQL Problem (e.g. Access-Problem)</li>
	 * </ul>
	 * 
	 * @param listener
	 * @category TO_WATCH
	 */
	public void getAllEpisodesToWatchCount(ServiceResponseListener listener);

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// ----- RECOMMENDATIONS ---------------------------------------------------------------------------------------- //

	/**
	 * TODO: JAVADOC
	 * 
	 * @param listener
	 */
	public void getGenreStringList(ServiceResponseListener listener);

	/**
	 * TODO: JAVADOC
	 * 
	 * @param showTvdbId
	 * @param listener
	 */
	public void dismissRecommendation(int showTvdbId, ServiceResponseListener listener);

	/**
	 * TODO: JAVADOC
	 * 
	 * @param genre
	 * @param startYear
	 * @param endYear
	 * @param listener
	 */
	public void getRecommendations(String genre, Integer startYear, Integer endYear, ServiceResponseListener listener);

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// ----- TRENDING_SHOWS ----------------------------------------------------------------------------------------- //

	/**
	 * TODO: JAVADOC
	 * returns List< TrendingShowDTO >
	 * @param listener
	 */
	public void getTrendingShows(ServiceResponseListener listener);


	// ################################################################################################### //
	// --------------------------------------------- Stats ----------------------------------------------- //
	/**
	 * <p>Downloads Stats of <code>show</code> from trakt and saves them locally in the DB (in the {@link Show} object)
	 * <br>Returns (via <code>listener</code>) either</p>
	 * {@link ServiceResponseListener#onSuccess(Object)}
	 * <ul>
	 * <li>{@link ResponseStatsDTO}</li>
	 * </ul>
	 * or {@link ServiceResponseListener#onFailure(Throwable)}
	 * <ul>
	 * <li>{@link SQLException} ... on any SQL Problem (e.g. Access-Problem)</li>
	 * <li>{@link HttpResponseException} ... invalid user credentials</li>
	 * <li>{@link IOException} ... No Internet Access (or URL wrong)</li>
	 * </ul>
	 * 
	 * @param show
	 * @param listener
	 */
	public void downloadStats(Show show, ServiceResponseListener listener);

	/**
	 * <p>Downloads Stats of <code>episode</code> from trakt and saves them locally in the DB (in the {@link Episode} object)
	 * <br>Returns (via <code>listener</code>) either</p>
	 * {@link ServiceResponseListener#onSuccess(Object)}
	 * <ul>
	 * <li>{@link ResponseStatsDTO}</li>
	 * </ul>
	 * or {@link ServiceResponseListener#onFailure(Throwable)}
	 * <ul>
	 * <li>{@link SQLException} ... on any SQL Problem (e.g. Access-Problem)</li>
	 * <li>{@link HttpResponseException} ... invalid user credentials</li>
	 * <li>{@link IOException} ... No Internet Access (or URL wrong)</li>
	 * </ul>
	 * 
	 * @param show
	 * @param listener
	 */
	public void downloadStats(Episode episode, ServiceResponseListener listener);

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// ----- RATING ------------------------------------------------------------------------------------------------- //

	/**
	 * <p>Gets Rating of <code>show</code> from local DB.</p>
	 * <b>Returns (via <code>listener</code>) either</b><br>
	 * {@link ServiceResponseListener#onSuccess(Object)}
	 * <ul>
	 * 		<li>{@link Rating}</li>
	 * </ul>
	 * or {@link ServiceResponseListener#onFailure(Throwable)}
	 * <ul>
	 * 		<li>{@link SQLException} ... on any SQL Problem (e.g. Access-Problem)</li>
	 * </ul>
	 * 
	 * @param show
	 * @param listener
	 * @category RATING
	 */
	public void getRating(Show show, ServiceResponseListener listener);

	/**
	 * <p>Gets Rating of <code>episode</code> from local DB.</p>
	 * <b>Returns (via <code>listener</code>) either</b><br>
	 * {@link ServiceResponseListener#onSuccess(Object)}
	 * <ul>
	 * 		<li>{@link Rating}</li>
	 * </ul>
	 * or {@link ServiceResponseListener#onFailure(Throwable)}
	 * <ul>
	 * 		<li>{@link SQLException} ... on any SQL Problem (e.g. Access-Problem)</li>
	 * </ul>
	 * 
	 * @param episode
	 * @param listener
	 * @category RATING
	 */
	public void getRating(Episode episode, ServiceResponseListener listener);

	/**
	 * First, uploads the current User's <code>show</code> <code>rating</code> to trakt. After trakt-response -> updates {@link Show} ratings in DB
	 * <br>Returns (via <code>listener</code>) either</p>
	 * {@link ServiceResponseListener#onSuccess(Object)}
	 * <ul>
	 * <li><{@link Show}></li>
	 * </ul>
	 * or {@link ServiceResponseListener#onFailure(Throwable)}
	 * <ul>
	 * <li>{@link UnsupportedEncodingException} ... Problem StringEntity creation</li>
	 * <li>{@link NumberFormatException} ... Problem during parseInt</li>
	 * <li>{@link SQLException} ... on any SQL Problem (e.g. Access-Problem)</li>
	 * <li>{@link HttpResponseException} ... invalid user credentials</li>
	 * <li>{@link IOException} ... No Internet Access (or URL wrong)</li>
	 * </ul>
	 * 
	 * @param show
	 * @param rating
	 * @param listener
	 */
	public void uploadRating(Show show, int rating, ServiceResponseListener listener);

	/**
	 * First, uploads the current User's <code>episode</code> <code>rating</code> to trakt. After trakt-response -> updates {@link Episode} ratings in DB
	 * <br>Returns (via <code>listener</code>) either</p>
	 * {@link ServiceResponseListener#onSuccess(Object)}
	 * <ul>
	 * <li><{@link Episode}></li>
	 * </ul>
	 * or {@link ServiceResponseListener#onFailure(Throwable)}
	 * <ul>
	 * <li>{@link UnsupportedEncodingException} ... Problem StringEntity creation</li>
	 * <li>{@link NumberFormatException} ... Problem during parseInt</li>
	 * <li>{@link SQLException} ... on any SQL Problem (e.g. Access-Problem)</li>
	 * <li>{@link HttpResponseException} ... invalid user credentials</li>
	 * <li>{@link IOException} ... No Internet Access (or URL wrong)</li>
	 * </ul>
	 * 
	 * @param episode
	 * @param rating
	 * @param listener
	 */
	public void uploadRating(Episode episode, int rating, ServiceResponseListener listener);


	// ################################################################################################### //
	// --------------------------------------------- COMMENTS -------------------------------------------- //

	/**
	 * TODO: JAVADOC
	 * 
	 * @param show
	 * @param msg
	 * @param spoiler
	 * @param listener
	 */
	public void sendComment(Show show, String msg, boolean spoiler, ServiceResponseListener listener);

	/**
	 * TODO: JAVADOC
	 * 
	 * @param episode
	 * @param msg
	 * @param spoiler
	 * @param listener
	 */
	public void sendComment(Episode episode, String msg, boolean spoiler, ServiceResponseListener listener);

	/**
	 * TODO: JAVADOC
	 * 
	 * @param show
	 * @param listener
	 */
	public void getShowComments(Show show, ServiceResponseListener listener);

	/**
	 * TODO: JAVADOC
	 * 
	 * @param episode
	 * @param listener
	 */
	public void getEpisodeComments(Episode episode, ServiceResponseListener listener);

	// ################################################################################################### //
	// --------------------------------------------- ACTOR ----------------------------------------------- //

	/**
	 * Gets Actor from local DB by <code>slug</code>
	 * <br>Returns (via <code>listener</code>) either</p>
	 * {@link ServiceResponseListener#onSuccess(Object)}
	 * <ul>
	 * <li>{@link Actor}</li>
	 * <li><code>null</code> ... if not found</li>
	 * </ul>
	 * or {@link ServiceResponseListener#onFailure(Throwable)}
	 * <ul>
	 * <li>{@link SQLException} ... on any SQL Problem (e.g. Access-Problem)</li>
	 * </ul>
	 * @param slug
	 * @param listener
	 */
	public void getActorBySlug(String slug, ServiceResponseListener listener);

	/**
	 * <p>Downloads {@link Actor} details from trakt and saves him/her locally in the DB (in the {@link Actor} object)
	 * <br>Returns (via <code>listener</code>) either</p>
	 * {@link ServiceResponseListener#onSuccess(Object)}
	 * <ul>
	 * <li>{@link Actor}</li>
	 * <li><code>null</code> ... if not found</li>
	 * </ul>
	 * or {@link ServiceResponseListener#onFailure(Throwable)}
	 * <ul>
	 * <li>{@link SQLException} ... on any SQL Problem (e.g. Access-Problem)</li>
	 * <li>{@link IOException} ... No Internet Access (or URL wrong)</li>
	 * </ul>
	 * 
	 * @param actor
	 * @param listener
	 */
	public void downloadActorDetails(Actor actor, ServiceResponseListener listener);


	/**
	 * <p>Sets {@link User#setFirstTimeUserOnDevice(boolean)} with the provided value <code>b</code>
	 * <br>Returns (via <code>listener</code>) either</p>
	 * {@link ServiceResponseListener#onSuccess(Object)}
	 * <ul>
	 * <li>{@link Boolean} ... the same as <code>b</code></li>
	 * <li><code>null</code> ... if not found</li>
	 * </ul>
	 * or {@link ServiceResponseListener#onFailure(Throwable)}
	 * <ul>
	 * <li>{@link SQLException} ... on any SQL Problem (e.g. Access-Problem)</li>
	 * <li>{@link UserNotFoundException} ... if not current User is set</li>
	 * </ul>
	 * 
	 * @param b
	 * @param listener
	 */
	public void setFirstTimeUserOnDevice(boolean b, ServiceResponseListener listener);

	/**
	 * <p>Gets corresponding {@link Show} and creates <code>headerList</code> and <code>childList</code>
	 * <br>Returns (via <code>listener</code>) either</p>
	 * {@link ServiceResponseListener#onSuccess(Object)}
	 * <ul>
	 * 		<li>{@link InternalSeasonsFragmentDTO} with {@link InternalSeasonsFragmentDTO#isFullyDownloaded()}=TRUE ... containing <code>headerList</code> and <code>childList</code></li>
	 * 		<li>{@link InternalSeasonsFragmentDTO} with {@link InternalSeasonsFragmentDTO#isFullyDownloaded()}=FALSE ... nothing else in the object</li>
	 * 		<li><code>null</code> ... if not found</li>
	 * </ul>
	 * or {@link ServiceResponseListener#onFailure(Throwable)}
	 * <ul>
	 * 		<li>{@link SQLException} ... on any SQL Problem (e.g. Access-Problem)</li>
	 * </ul>
	 * 
	 * @param tvdbId
	 * @param serviceResponseListener
	 */
	public void getSeasonsFragmentDTO(int tvdbId, ServiceResponseListener serviceResponseListener);

	/**
	 * <p>Checks whether <code>show</code> is followed by current {@link User}
	 * <br>Returns (via <code>listener</code>) either</p>
	 * {@link ServiceResponseListener#onSuccess(Object)}
	 * <ul>
	 * 		<li>{@link Boolean} ... <code>TRUE</code> or <code>FALSE</code></li>
	 * </ul>
	 * or {@link ServiceResponseListener#onFailure(Throwable)}
	 * <ul>
	 * 		<li>{@link SQLException} ... on any SQL Problem (e.g. Access-Problem)</li>
	 * </ul>
	 * 
	 * @param show
	 * @param listener
	 */
	public void isShowFollowedByCurrentUser(Show show, ServiceResponseListener listener);

	/**
	 * Simply calls {@link #isShowFollowedByCurrentUser(Show, ServiceResponseListener)}.
	 * TODO: JAVADOC
	 * 
	 * @param tvdbId
	 * @param listener
	 */
	public void isShowFollowedByCurrentUser(int tvdbId, ServiceResponseListener listener);

	/**
	 * CURRENTLY NOT IN USE. Planned for {@link RecommendationsListArrayAdapter}
	 * 
	 * @param tvdbId
	 * @param listener
	 */
	public void getRecShowStatusData(int tvdbId, ServiceResponseListener listener);

	/**
	 * <p>Checks whether <code>episode</code> has been aired already.</p>
	 * Returns (via <code>listener</code>) either<br>
	 * {@link ServiceResponseListener#onSuccess(Object)}
	 * <ul>
	 * 		<li>{@link Boolean} ... <code>TRUE</code> or <code>FALSE</code></li>
	 * </ul>
	 * or {@link ServiceResponseListener#onFailure(Throwable)}
	 * <ul>
	 * 		<li>{@link SQLException} ... on any SQL Problem (e.g. Access-Problem)</li>
	 * </ul>
	 * 
	 * @param episode
	 * @param listener
	 */
	public void hasEpisodeBeenAired(Episode episode, ServiceResponseListener listener);



	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// ----- PHOTO_CACHE -------------------------------------------------------------------------------------------- //

	/**
	 * <p>Clears Photo cache (located in context.getFilesDir()).</p>
	 * Returns (via <code>listener</code>)<br>
	 * {@link ServiceResponseListener#onSuccess(Object)}
	 * <ul>
	 * 		<li>{@link Boolean} ... <code>TRUE</code> to show success</li>
	 * </ul>
	 * 
	 * @param listener
	 * @category PHOTO_CACHE
	 */
	public void clearPhotoDiscCache(ServiceResponseListener listener);


}
