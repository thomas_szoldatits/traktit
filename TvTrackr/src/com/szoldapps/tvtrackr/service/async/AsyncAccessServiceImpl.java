package com.szoldapps.tvtrackr.service.async;

import java.io.UnsupportedEncodingException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.szoldapps.tvtrackr.db.model.Actor;
import com.szoldapps.tvtrackr.db.model.Episode;
import com.szoldapps.tvtrackr.db.model.Rating;
import com.szoldapps.tvtrackr.db.model.Season;
import com.szoldapps.tvtrackr.db.model.Show;
import com.szoldapps.tvtrackr.db.model.Show.ShowDownloadStatus;
import com.szoldapps.tvtrackr.db.model.User;
import com.szoldapps.tvtrackr.dto.internal.InternalCommentsDTO;
import com.szoldapps.tvtrackr.dto.internal.InternalEpisodeInfosDTO;
import com.szoldapps.tvtrackr.dto.internal.InternalEpisodeWatchedDTO;
import com.szoldapps.tvtrackr.dto.internal.InternalProgressDTO;
import com.szoldapps.tvtrackr.dto.internal.InternalRecommendationStatusDTO;
import com.szoldapps.tvtrackr.dto.internal.InternalSeasonsFragmentDTO;
import com.szoldapps.tvtrackr.dto.internal.InternalToWatchDTO;
import com.szoldapps.tvtrackr.dto.response.ResponseStatsDTO;
import com.szoldapps.tvtrackr.dto.show.MediumShowDTO;
import com.szoldapps.tvtrackr.dto.show.RecommendationShowDTO;
import com.szoldapps.tvtrackr.dto.show.SearchShowDTO;
import com.szoldapps.tvtrackr.dto.show.TrendingShowDTO;
import com.szoldapps.tvtrackr.exception.EpisodeUserNotFoundException;
import com.szoldapps.tvtrackr.exception.UserNotFoundException;
import com.szoldapps.tvtrackr.exception.WrongObjectReturnedException;
import com.szoldapps.tvtrackr.helper.DateHelper;
import com.szoldapps.tvtrackr.helper.DialogHelper;
import com.szoldapps.tvtrackr.helper.ImageLoaderHelper;
import com.szoldapps.tvtrackr.helper.LogCatHelper;
import com.szoldapps.tvtrackr.service.ActorService;
import com.szoldapps.tvtrackr.service.CommentsService;
import com.szoldapps.tvtrackr.service.EpisodeService;
import com.szoldapps.tvtrackr.service.OfflineService;
import com.szoldapps.tvtrackr.service.ShowService;
import com.szoldapps.tvtrackr.service.UserService;
import com.szoldapps.tvtrackr.service.impl.ActorServiceImpl;
import com.szoldapps.tvtrackr.service.impl.CommentsServiceImpl;
import com.szoldapps.tvtrackr.service.impl.EpisodeServiceImpl;
import com.szoldapps.tvtrackr.service.impl.OfflineServiceImpl;
import com.szoldapps.tvtrackr.service.impl.ShowServiceImpl;
import com.szoldapps.tvtrackr.service.impl.UserServiceImpl;
import com.szoldapps.tvtrackr.service.intent.DownloadService;
import com.szoldapps.tvtrackr.service.listener.ServiceResponseListener;

public class AsyncAccessServiceImpl implements AsyncAccessService {

	// LogTag
	public static final String LOGTAG = LogCatHelper.getAppTag()+AsyncAccessServiceImpl.class.getSimpleName().toString();

	// Services
	private static boolean sAllServicesLoaded = false;
	private static AsyncAccessServiceImpl sAsyncAccessServiceImpl = null;
	private static UserService sUserService;
	private static ShowService sShowService;
	private static EpisodeService sEpisodeService;
	private static CommentsService sCommentsService;
	private static ActorService sActorService;
	private static OfflineService sOfflineService;


	private Context mContext;


	public static AsyncAccessServiceImpl getInstance(Context context) {
		if(sAsyncAccessServiceImpl == null) {
			sAsyncAccessServiceImpl = new AsyncAccessServiceImpl(context);
		}
		return sAsyncAccessServiceImpl;
	}

	private AsyncAccessServiceImpl(Context context) {
		mContext = context;
	}

	private void loadAllServices() throws SQLException {
		if(!sAllServicesLoaded) {
			sUserService = UserServiceImpl.getService(mContext);
			sShowService = ShowServiceImpl.getService(mContext);
			sEpisodeService = EpisodeServiceImpl.getService(mContext);
			sCommentsService = CommentsServiceImpl.getService(mContext);
			sActorService = ActorServiceImpl.getService(mContext);
			sOfflineService = OfflineServiceImpl.getService(mContext);
		}
	}

	private void defaultOnPostExecute(Throwable throwable, Object o, ServiceResponseListener listener) {
		if(throwable != null) {
			listener.onFailure(throwable);
		} else {
			listener.onSuccess(o);
		}
	}

	@Override
	public void uploadFollowAndSaveShow(final MediumShowDTO medShow, final ServiceResponseListener listener) {
		new AsyncTask<Void, Void, Void>() {
			Throwable mThrowable = null;
			Boolean mSuccess;
			@Override
			protected Void doInBackground(Void... params) {
				try {
					loadAllServices();
					// 1) Watchlists <code>medShow</code> on trakt.tv.
					sShowService.uploadWatchlist(medShow.getTvdb_id(), new ServiceResponseListener() {
						@Override
						public void onProgress(int bytesWritten, int totalSize) { }

						@Override
						public void onSuccess(Object o) {
							if(o instanceof Boolean) {
								mSuccess = (Boolean) o;
							} else {
								onFailure(new WrongObjectReturnedException(Boolean.class));
							}
						}

						@Override
						public void onFailure(Throwable throwable) {
							if(DialogHelper.isInternetError(throwable)) {	// if no Internet -> save for later
								mSuccess = false;
							} else {
								mThrowable = throwable;
							}

						}
					});
					// Check whether Error happened
					if(mThrowable != null) { return null; }
					
					// Only persist follow, if watchlist was successful
					if(mSuccess) {
						// 2) Stores <code>medShow</code> in local DB.
						sShowService.saveShow(medShow);
					}
				} catch (SQLException e) {
					mThrowable = e;
					return null;
				}
				return null;
			}
			@Override
			protected void onPostExecute(Void result) {
				// 3) Requests complete Download from {@link DownloadService}
				DownloadService.requestShowDownload(medShow.getTvdb_id(), mContext);
				defaultOnPostExecute(mThrowable, mSuccess, listener);
			}
		}.execute(new Void[0]);
	}

	@Override
	public void uploadFollowAndSaveShow(final Show show, final ServiceResponseListener listener) {
		new AsyncTask<Void, Void, Void>() {
			Throwable mThrowable = null;
			Boolean mSuccess;
			@Override
			protected Void doInBackground(Void... params) {
				try {
					loadAllServices();
					// 1) Watchlists <code>medShow</code> on trakt.tv.
					sShowService.uploadWatchlist(show.getTvdbID(), new ServiceResponseListener() {
						@Override
						public void onProgress(int bytesWritten, int totalSize) { }

						@Override
						public void onSuccess(Object o) {
							if(o instanceof Boolean) {
								mSuccess = (Boolean) o;
							} else {
								onFailure(new WrongObjectReturnedException(Boolean.class));
							}
						}

						@Override
						public void onFailure(Throwable throwable) {
							if(DialogHelper.isInternetError(throwable)) {	// if no Internet -> return false
								mSuccess = false;
							} else {
								mThrowable = throwable;
							}

						}
					});
					// Check whether Error happened
					if(mThrowable != null) { return null; }

					// Only persist follow, if watchlist was successful
					if(mSuccess) {
						// Adds {@link ShowUser} & {@link EpisodeUser} relations to already fullyDownloaded show
						sShowService.createShowUserAndEpisodeUserData(show, true);
					}

				} catch (SQLException e) {
					mThrowable = e;
					return null;
				}
				return null;
			}
			@Override
			protected void onPostExecute(Void result) {
				defaultOnPostExecute(mThrowable, mSuccess, listener);
			}
		}.execute(new Void[0]);
	}

	@Override
	public void uploadUnfollowShow(final Show show, final boolean preventCompleteRemoval, final ServiceResponseListener listener) {
		new AsyncTask<Void, Void, Void>() {
			Throwable mThrowable = null;
			boolean mSuccess;
			@Override
			protected Void doInBackground(Void... params) {
				try {
					loadAllServices();

					// UnFollow Show on Trakt
					sShowService.uploadUnfollow(show, new ServiceResponseListener() {
						@Override public void onProgress(int bytesWritten, int totalSize) { }

						@Override
						public void onSuccess(Object o) {
							if(o instanceof Boolean) {
								mSuccess = (Boolean) o;
							}
						}

						@Override
						public void onFailure(Throwable throwable) {
							// if no Internet -> save for later
							if(DialogHelper.isInternetError(throwable)) {
								try {
									sOfflineService.createIfNotExistOfflineShowToUnfollow(show);
									mSuccess = false;
								} catch (SQLException e) {
									mThrowable = throwable;
									return;
								}
							} else {
								mThrowable = throwable;
							}
						}
					});
					if(mThrowable != null) { return null; }

					// delete from local DB (even if an there is no Internet)
					sShowService.deleteShowForUser(show.getTvdbID(), preventCompleteRemoval);

				} catch (SQLException e) {
					mThrowable = e;
					return null;
				}
				return null;
			}
			@Override
			protected void onPostExecute(Void result) {
				defaultOnPostExecute(mThrowable, mSuccess, listener);
			}
		}.execute(new Void[0]);
	}

	@Override
	public void uploadShowSeenOrUnseen(final boolean markAsSeen, final Show show, 
			final ServiceResponseListener listener) {

		new AsyncTask<Void, Void, Void>() {
			Throwable mThrowable = null;
			Boolean mSuccess;
			@Override
			protected Void doInBackground(Void... params) {
				try {
					loadAllServices();
					// mark season as seen/unseen
					sShowService.uploadShowSeenOrUnseen(markAsSeen, show, new ServiceResponseListener() {
						@Override 
						public void onProgress(int bytesWritten, int totalSize) { }

						@Override
						public void onSuccess(Object o) {
							if(o instanceof Boolean) {
								mSuccess = (Boolean) o;
							}
						}

						@Override
						public void onFailure(Throwable throwable) {
							mThrowable = throwable;
						}
					});
				} catch (SQLException e) {
					mThrowable = e;
					return null;
				}
				return null;
			}
			@Override
			protected void onPostExecute(Void result) {
				defaultOnPostExecute(mThrowable, mSuccess, listener);
			}
		}.execute(new Void[0]);
	}

	@Override
	public void uploadSeasonSeenOrUnseen(final boolean markAsSeen, final Season season, final ServiceResponseListener listener) {
		new AsyncTask<Void, Void, Void>() {
			Throwable mThrowable = null;
			Boolean mSuccess;
			@Override
			protected Void doInBackground(Void... params) {
				try {
					loadAllServices();
					// mark season as seen/unseen
					sShowService.uploadSeasonSeenOrUnseen(markAsSeen, season, new ServiceResponseListener() {

						@Override public void onProgress(int bytesWritten, int totalSize) { }

						@Override
						public void onSuccess(Object o) {
							if(o instanceof Boolean) {
								mSuccess = (Boolean) o;
							}
						}

						@Override
						public void onFailure(Throwable throwable) {
							mThrowable = throwable;
						}
					});
					if(mThrowable != null) { return null; }

					// check whether Show was automatically marked unseen by trakt (as there are no more seen Episodes left)
					// only necessary if season marked as unseen
					if(!markAsSeen) {
						Show show = season.getShow();
						long seenEpisodesCount = sEpisodeService.getEpisodeSeenCountOfShow(show);
						if(seenEpisodesCount == 0) {
							sShowService.uploadWatchlist(show.getTvdbID(), new ServiceResponseListener() {
								@Override public void onProgress(int bytesWritten, int totalSize) { }

								@Override
								public void onSuccess(Object o) {
									if(!(o instanceof Boolean)) {
										mThrowable = new WrongObjectReturnedException(Boolean.class);
									}
								}

								@Override
								public void onFailure(Throwable throwable) {
									mThrowable = throwable;
								}
							});
						}
					}
				} catch (SQLException e) {
					mThrowable = e;
					return null;
				}
				return null;
			}
			@Override
			protected void onPostExecute(Void result) {
				defaultOnPostExecute(mThrowable, mSuccess, listener);
			}
		}.execute(new Void[0]);
	}

	@Override
	public void uploadEpisodeSeenToggle(final Episode episode, final ServiceResponseListener listener) {
		new AsyncTask<Void, Void, Void>() {
			Throwable mThrowable = null;
			InternalEpisodeWatchedDTO mDTO;
			@Override
			protected Void doInBackground(Void... params) {
				try {
					loadAllServices();
					
					Show show = episode.getSeason().getShow();
					if(!sShowService.isShowFollowedByCurrentUser(show.getTvdbID())) {	
						sShowService.createShowUserAndEpisodeUserData(show, false);
					}
					final boolean markAsSeen = !sEpisodeService.getEpisodeUser(episode).isWatched();
					
					sShowService.uploadEpisodeSeenOrUnseen(markAsSeen, episode, new ServiceResponseListener() {
						@Override public void onProgress(int bytesWritten, int totalSize) { }

						@Override
						public void onSuccess(Object o) {
							if(o instanceof Boolean) {	
								// o = false if offlineMarked, TRUE if not
								mDTO = new InternalEpisodeWatchedDTO(markAsSeen, !((Boolean) o));
								return;
							} else {
								mThrowable = new WrongObjectReturnedException(Boolean.class);
							}
						}

						@Override
						public void onFailure(Throwable throwable) {
							mThrowable = throwable;
						}
					});

					if(mThrowable != null) { return null; }

					// check whether Show was automatically marked unseen by trakt (as there are no more seen Episodes left)
					// only necessary if season marked as unseen
					if(!markAsSeen) {
						long seenEpisodesCount = sEpisodeService.getEpisodeSeenCountOfShow(show);
						if(seenEpisodesCount == 0) {
							sShowService.uploadWatchlist(show.getTvdbID(), new ServiceResponseListener() {
								@Override public void onProgress(int bytesWritten, int totalSize) { }

								@Override
								public void onSuccess(Object o) {
									if(!(o instanceof Boolean)) {
										mThrowable = new WrongObjectReturnedException(Boolean.class);
									}
								}

								@Override
								public void onFailure(Throwable throwable) {
									mThrowable = throwable;
								}
							});
						}
					}
				} catch (SQLException e) {
					mThrowable = e;
					return null;
				} catch (EpisodeUserNotFoundException e) {
					mThrowable = e;
					return null;
				}
				return null;
			}
			@Override
			protected void onPostExecute(Void result) {
				defaultOnPostExecute(mThrowable, mDTO, listener);
			}
		}.execute(new Void[0]);
	}

	// ######################################################################################################## //
	// --------------------------------------------- SHOW ----------------------------------------------------- //
	public void getShowById(final int tvdbId, final ServiceResponseListener listener) {
		new AsyncTask<Void, Void, Void>() {
			Throwable mThrowable = null;
			Show mShow = null;
			@Override
			protected Void doInBackground(Void... params) {
				try {
					loadAllServices();
					mShow = sShowService.getShowById(tvdbId);
				} catch (SQLException e) {
					mThrowable = e;
					return null;
				}
				return null;
			}
			@Override
			protected void onPostExecute(Void result) {
				defaultOnPostExecute(mThrowable, mShow, listener);
			}
		}.execute(new Void[0]);
	}

	@Override
	public void getAllShowsOfUser(final ServiceResponseListener listener) {
		new AsyncTask<Void, Void, Void>() {
			Throwable mThrowable = null;
			List<Show> mShows = null;
			@Override
			protected Void doInBackground(Void... params) {
				try {
					loadAllServices();
					mShows = sShowService.getAllShowsOfUser();
				} catch (SQLException e) {
					mThrowable = e;
					return null;
				}
				return null;
			}
			@Override
			protected void onPostExecute(Void result) {
				defaultOnPostExecute(mThrowable, mShows, listener);
			}
		}.execute(new Void[0]);
	}

	@Override
	public void getProgressOfAiredEpisodes(final Show show, final ServiceResponseListener listener) {
		new AsyncTask<Void, Void, Void>() {
			Throwable mThrowable = null;
			InternalProgressDTO mProgressDTO;
			@Override
			protected Void doInBackground(Void... params) {
				try {
					loadAllServices();
					mProgressDTO = sShowService.getProgressOfAiredEpisodes(show);
				} catch (SQLException e) {
					mThrowable = e;
					return null;
				}
				return null;
			}
			@Override
			protected void onPostExecute(Void result) {
				defaultOnPostExecute(mThrowable, mProgressDTO, listener);
			}
		}.execute(new Void[0]);
	}

	public void getProgressOfAiredEpisodes(final Season season, final ServiceResponseListener listener) {
		new AsyncTask<Void, Void, Void>() {
			Throwable mThrowable = null;
			InternalProgressDTO mDTO;
			@Override
			protected Void doInBackground(Void... params) {
				try {
					loadAllServices();
					mDTO = sShowService.getProgressOfAiredEpisodes(season);
				} catch (SQLException e) {
					mThrowable = e;
					return null;
				}
				return null;
			}
			@Override
			protected void onPostExecute(Void result) {
				defaultOnPostExecute(mThrowable, mDTO, listener);
			}
		}.execute(new Void[0]);
	}




	// ######################################################################################################## //
	// --------------------------------------------- USER RELATED --------------------------------------------- //

	@Override
	public void login(final User user, final ServiceResponseListener listener) {
		new AsyncTask<Void, Void, Void>() {
			Throwable mThrowable = null;
			User mUser;
			@Override
			protected Void doInBackground(Void... params) {
				try {
					loadAllServices();
					sUserService.login(user, new ServiceResponseListener() {
						@Override public void onProgress(int bytesWritten, int totalSize) { }

						@Override
						public void onSuccess(Object o) {
							if(o instanceof User) {
								mUser = (User) o;
							}
						}

						@Override
						public void onFailure(Throwable throwable) {
							mThrowable = throwable;
						}
					});
				} catch (Exception e) {
					mThrowable = e;
					return null; 
				}
				return null;
			}
			@Override
			protected void onPostExecute(Void result) {
				defaultOnPostExecute(mThrowable, mUser, listener);
			}
		}.execute(new Void[0]);
	}

	@Override
	public void logout(final ServiceResponseListener listener) {
		new AsyncTask<Void, Void, Void>() {
			Throwable mThrowable = null;
			User mUser;
			@Override
			protected Void doInBackground(Void... params) {
				try {
					loadAllServices();
					mUser = sUserService.logout();
				} catch (SQLException e) {
					mThrowable = e;
					return null;
				}
				return null;
			}
			@Override
			protected void onPostExecute(Void result) {
				defaultOnPostExecute(mThrowable, mUser, listener);
			}
		}.execute(new Void[0]);
	}

	@Override
	public void getCurrentUser(final boolean forceDownload, final ServiceResponseListener listener) {
		new AsyncTask<Void, Void, Void>() {
			Throwable mThrowable = null;
			User mUser;
			@Override
			protected Void doInBackground(Void... params) {
				try {
					loadAllServices();

					mUser = sUserService.getCurrentUser();
					if(mUser != null) {
						if(!mUser.isFullyLoaded() || (mUser.isFullyLoaded() && forceDownload)) {
							// Unsee Show on Trakt
							sUserService.downloadCurrentUserDetails(new ServiceResponseListener() {
								@Override public void onProgress(int bytesWritten, int totalSize) { }
								@Override
								public void onSuccess(Object o) {
									if(o instanceof User) {
										mUser = (User) o;
									}
								}
								@Override
								public void onFailure(Throwable throwable) {
									mThrowable = throwable;
								}
							});
						}
					} else {	// there is no current User set
						mThrowable = new UserNotFoundException("No current User set in DB");
					}

				} catch (SQLException e) {
					mThrowable = e;
					return null;
				}  catch (UnsupportedEncodingException e) {
					mThrowable = e;
					return null;
				}
				return null;
			}
			@Override
			protected void onPostExecute(Void result) {
				defaultOnPostExecute(mThrowable, mUser, listener);
			}
		}.execute(new Void[0]);
	}

	@Override
	public void getLastCompleteUpdateString(final ServiceResponseListener listener) {
		new AsyncTask<Void, Void, Void>() {
			Throwable mThrowable = null;
			String mLastCompleteUpdateString;
			@Override
			protected Void doInBackground(Void... params) {
				try {
					loadAllServices();
					mLastCompleteUpdateString = sUserService.getLastCompleteUpdateString();
				} catch (SQLException e) {
					mThrowable = e;
					return null;
				}
				return null;
			}
			@Override
			protected void onPostExecute(Void result) {
				defaultOnPostExecute(mThrowable, mLastCompleteUpdateString, listener);
			}
		}.execute(new Void[0]);
	}

	public void register(final User user, final ServiceResponseListener listener) {
		new AsyncTask<Void, Void, Void>() {
			Throwable mThrowable = null;
			User mUser;
			@Override
			protected Void doInBackground(Void... params) {
				try {
					loadAllServices();
					sUserService.register(user, new ServiceResponseListener() {
						@Override
						public void onProgress(int bytesWritten, int totalSize) { }

						@Override
						public void onSuccess(Object o) {
							if(o instanceof User) {
								mUser = (User) o;
							}
						}

						@Override
						public void onFailure(Throwable throwable) {
							mThrowable = throwable;
						}
					});
				} catch (SQLException e) {
					mThrowable = e;
					return null;
				}
				return null;
			}
			@Override
			protected void onPostExecute(Void result) {
				defaultOnPostExecute(mThrowable, mUser, listener);
			}
		}.execute(new Void[0]);
	}

	// ################################################################################################## //
	// --------------------------------------------- SEARCH --------------------------------------------- //

	@Override
	public void search(final String query, final int limit, final ServiceResponseListener listener) {
		new AsyncTask<Void, Void, Void>() {
			Throwable mThrowable = null;
			List<SearchShowDTO> mSearchResults;
			@Override
			protected Void doInBackground(Void... params) {
				try {
					loadAllServices();
					sShowService.search(query, limit, new ServiceResponseListener() {
						@Override public void onProgress(int bytesWritten, int totalSize) { }

						@Override @SuppressWarnings("unchecked")
						public void onSuccess(Object o) {
							if(o != null) {
								mSearchResults = (List<SearchShowDTO>) o;
							}
						}

						@Override
						public void onFailure(Throwable throwable) {
							mThrowable = throwable;
						}
					});
				} catch (SQLException e) {
					mThrowable = e;
					return null;
				}
				return null;
			}
			@Override
			protected void onPostExecute(Void result) {
				defaultOnPostExecute(mThrowable, mSearchResults, listener);
			}
		}.execute(new Void[0]);

	}


	// #################################################################################################### //
	// --------------------------------------------- CALENDAR --------------------------------------------- //

	@Override
	public void getEpisodesOfDay(final int offset, final ServiceResponseListener listener) {
		new AsyncTask<Void, Void, Void>() {
			Throwable mThrowable = null;
			List<Episode> mEpisodes;
			@Override
			protected Void doInBackground(Void... params) {
				try {
					loadAllServices();
					mEpisodes = sEpisodeService.getEpisodesOfDay(offset);
				} catch (SQLException e) {
					mThrowable = e;
					return null;
				}
				return null;
			}
			@Override
			protected void onPostExecute(Void result) {
				defaultOnPostExecute(mThrowable, mEpisodes, listener);
			}
		}.execute(new Void[0]);
	}

	@Override
	public void getUnwatchedEpisodeCountOfDay(final int offset, final ServiceResponseListener listener) {
		new AsyncTask<Void, Void, Void>() {
			Throwable mThrowable = null;
			Long mCount;
			@Override
			protected Void doInBackground(Void... params) {
				try {
					loadAllServices();
					mCount = sEpisodeService.getUnwatchedEpisodeCountOfDay(offset);
				} catch (SQLException e) {
					mThrowable = e;
					return null;
				}
				return null;
			}
			@Override
			protected void onPostExecute(Void result) {
				defaultOnPostExecute(mThrowable, mCount, listener);
			}
		}.execute(new Void[0]);
	}

	// #################################################################################################### //
	// --------------------------------------------- EPISODE --------------------------------------------- //
	@Override
	public void isEpisodeSeen(final Episode episode, final ServiceResponseListener listener) {
		new AsyncTask<Void, Void, Void>() {
			Throwable mThrowable = null;
			Boolean mIsSeen = false;
			@Override
			protected Void doInBackground(Void... params) {
				try {
					loadAllServices();
					mIsSeen = sEpisodeService.isEpisodeSeen(episode);
				} catch (SQLException e) {
					mThrowable = e;
					return null;
				}
				return null;
			}
			@Override
			protected void onPostExecute(Void result) {
				defaultOnPostExecute(mThrowable, mIsSeen, listener);
			}
		}.execute(new Void[0]);
	}

	@Override
	public void getEpisodeInfosOfShow(final int tvdbId, final int episodeId, final ServiceResponseListener listener) {
		new AsyncTask<Void, Void, Void>() {
			Throwable mThrowable = null;
			InternalEpisodeInfosDTO mDTO;
			@Override
			protected Void doInBackground(Void... params) {
				try {
					loadAllServices();
					List<String> tabNameList = sShowService.getEpisodesTabNameList(tvdbId);
					int position = (episodeId == -1) ? -1 : sShowService.getPositionByEpisodeId(episodeId, tvdbId);
					mDTO = new InternalEpisodeInfosDTO(tabNameList, position);
				} catch (SQLException e) {
					mThrowable = e;
					return null;
				}
				return null;
			}
			@Override
			protected void onPostExecute(Void result) {
				defaultOnPostExecute(mThrowable, mDTO, listener);
			}
		}.execute(new Void[0]);
	}

	@Override
	public void getEpisodeByPosition(final int position, final int showTvdbId, final ServiceResponseListener listener) {
		new AsyncTask<Void, Void, Void>() {
			Throwable mThrowable = null;
			Episode mEpisode;
			@Override
			protected Void doInBackground(Void... params) {
				try {
					loadAllServices();
					mEpisode = sEpisodeService.getEpisodeByPosition(position, showTvdbId);
				} catch (SQLException e) {
					mThrowable = e;
					return null;
				}
				return null;
			}
			@Override
			protected void onPostExecute(Void result) {
				defaultOnPostExecute(mThrowable, mEpisode, listener);
			}
		}.execute(new Void[0]);
	}

	// ################################################################################################### //
	// --------------------------------------------- ToWatch --------------------------------------------- //

	@Override
	public void getToWatchShows(final ServiceResponseListener listener) {
		new AsyncTask<Void, Void, Void>() {
			Throwable mThrowable = null;
			List<Show> mShows;
			@Override
			protected Void doInBackground(Void... params) {
				try {
					loadAllServices();
					mShows = sEpisodeService.getToWatchShows();
				} catch (SQLException e) {
					mThrowable = e;
					return null;
				}
				return null;
			}
			@Override
			protected void onPostExecute(Void result) {
				defaultOnPostExecute(mThrowable, mShows, listener);
			}
		}.execute(new Void[0]);
	}

	@Override
	public void getNextEpisodeToWatchAndLeftEpisodeCount(final InternalToWatchDTO toWatchDTO, final int showTvdbId, final ServiceResponseListener listener) {
		new AsyncTask<Void, Void, Void>() {
			Throwable mThrowable = null;
			@Override
			protected Void doInBackground(Void... params) {
				try {
					loadAllServices();
					Episode episode = sEpisodeService.getNextEpisodeToWatch(showTvdbId);
					Integer episodesLeft = sEpisodeService.getEpisodesToWatchCountOfShow(showTvdbId);
					toWatchDTO.setEpisode(episode);
					toWatchDTO.setEpisodesLeft(episodesLeft);
				} catch (SQLException e) {
					mThrowable = e;
					return null;
				}
				return null;
			}
			@Override
			protected void onPostExecute(Void result) {
				defaultOnPostExecute(mThrowable, toWatchDTO, listener);
			}
		}.execute(new Void[0]);
	}

	@Override
	public void getAllEpisodesToWatchCount(final ServiceResponseListener listener) {
		new AsyncTask<Void, Void, Void>() {
			Throwable mThrowable = null;
			Long mCount;
			@Override
			protected Void doInBackground(Void... params) {
				try {
					loadAllServices();
					mCount = sEpisodeService.getAllEpisodesToWatchCount();
				} catch (SQLException e) {
					mThrowable = e;
					return null;
				}
				return null;
			}
			@Override
			protected void onPostExecute(Void result) {
				defaultOnPostExecute(mThrowable, mCount, listener);
			}
		}.execute(new Void[0]);
	}

	// ################################################################################################### //
	// --------------------------------------------- Recommendations ------------------------------------- //

	public void getGenreStringList(final ServiceResponseListener listener) {
		new AsyncTask<Void, Void, Void>() {
			Throwable mThrowable = null;
			List<String> mGenres;
			@Override
			protected Void doInBackground(Void... params) {
				try {
					loadAllServices();
					sShowService.getGenreStringList(new ServiceResponseListener() {

						@Override public void onProgress(int bytesWritten, int totalSize) { }

						@Override @SuppressWarnings("unchecked")
						public void onSuccess(Object o) {
							if(o != null) {
								mGenres = (List<String>) o;
							}
						}

						@Override
						public void onFailure(Throwable throwable) {
							mThrowable = throwable;

						}
					});
				} catch (SQLException e) {
					mThrowable = e;
					return null;
				}
				return null;
			}
			@Override
			protected void onPostExecute(Void result) {
				defaultOnPostExecute(mThrowable, mGenres, listener);
			}
		}.execute(new Void[0]);
	}

	@Override
	public void dismissRecommendation(final int showTvdbId, final ServiceResponseListener listener) {
		new AsyncTask<Void, Void, Void>() {
			Throwable mThrowable = null;
			String mSuccessMsg;
			@Override
			protected Void doInBackground(Void... params) {
				try {
					loadAllServices();
					sShowService.dismissRecommendation(showTvdbId, new ServiceResponseListener() {

						@Override public void onProgress(int bytesWritten, int totalSize) { }

						@Override
						public void onSuccess(Object o) {
							if(o instanceof String) {
								mSuccessMsg = (String) o;
							}
						}

						@Override
						public void onFailure(Throwable throwable) {
							mThrowable = throwable;
						}
					});
				} catch (SQLException e) {
					mThrowable = e;
					return null;
				} catch (UnsupportedEncodingException e) {
					mThrowable = e;
					return null;
				}
				return null;
			}
			@Override
			protected void onPostExecute(Void result) {
				defaultOnPostExecute(mThrowable, mSuccessMsg, listener);
			}
		}.execute(new Void[0]);
	}

	@Override
	public void getRecommendations(final String genre, final Integer startYear, final Integer endYear, final ServiceResponseListener listener) {
		new AsyncTask<Void, Void, Void>() {
			Throwable mThrowable = null;
			List<RecommendationShowDTO> mRecShowDTOs;
			@Override
			protected Void doInBackground(Void... params) {
				try {
					loadAllServices();
					sShowService.getRecommendations(genre, startYear, endYear, new ServiceResponseListener() {

						@Override public void onProgress(int bytesWritten, int totalSize) { }

						@Override @SuppressWarnings("unchecked")
						public void onSuccess(Object o) {
							if(o != null) {
								mRecShowDTOs = (List<RecommendationShowDTO>) o;
							}
						}

						@Override
						public void onFailure(Throwable throwable) {
							mThrowable = throwable;

						}
					});
				} catch (SQLException e) {
					mThrowable = e;
					return null;
				} catch (UnsupportedEncodingException e) {
					mThrowable = e;
					return null;
				}
				return null;
			}
			@Override
			protected void onPostExecute(Void result) {
				defaultOnPostExecute(mThrowable, mRecShowDTOs, listener);
			}
		}.execute(new Void[0]);
	}


	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// ----- TRENDING_SHOWS ----------------------------------------------------------------------------------------- //
	@Override
	public void getTrendingShows(final ServiceResponseListener listener) {
		new AsyncTask<Void, Void, Void>() {
			Throwable mThrowable = null;
			List<TrendingShowDTO> mShows;
			@Override
			protected Void doInBackground(Void... params) {
				try {
					loadAllServices();
					sShowService.getTrendingShows(new ServiceResponseListener() {

						@Override public void onProgress(int bytesWritten, int totalSize) { }

						@Override @SuppressWarnings("unchecked")
						public void onSuccess(Object o) {
							if(o != null) {
								mShows = (List<TrendingShowDTO>) o;
							}
						}

						@Override
						public void onFailure(Throwable throwable) {
							mThrowable = throwable;

						}
					});
				} catch (SQLException e) {
					mThrowable = e;
					return null;
				}
				return null;
			}
			@Override
			protected void onPostExecute(Void result) {
				defaultOnPostExecute(mThrowable, mShows, listener);
			}
		}.execute(new Void[0]);
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// ----- STATS -------------------------------------------------------------------------------------------------- //
	@Override
	public void downloadStats(final Show show, final ServiceResponseListener listener) {
		new AsyncTask<Void, Void, Void>() {
			Throwable mThrowable = null;
			ResponseStatsDTO mDTO;
			@Override
			protected Void doInBackground(Void... params) {
				try {
					loadAllServices();
					sShowService.downloadStats(show, new ServiceResponseListener() {

						@Override public void onProgress(int bytesWritten, int totalSize) { }

						@Override
						public void onSuccess(Object o) {
							if(o instanceof ResponseStatsDTO) {
								mDTO = (ResponseStatsDTO) o;
							}
						}

						@Override
						public void onFailure(Throwable throwable) {
							mThrowable = throwable;
						}
					});
				} catch (SQLException e) {
					mThrowable = e;
					return null;
				}
				return null;
			}
			@Override
			protected void onPostExecute(Void result) {
				defaultOnPostExecute(mThrowable, mDTO, listener);
			}
		}.execute(new Void[0]);
	}

	@Override
	public void downloadStats(final Episode episode, final ServiceResponseListener listener) {
		new AsyncTask<Void, Void, Void>() {
			Throwable mThrowable = null;
			ResponseStatsDTO mDTO;
			@Override
			protected Void doInBackground(Void... params) {
				try {
					loadAllServices();
					sEpisodeService.downloadStats(episode, new ServiceResponseListener() {

						@Override public void onProgress(int bytesWritten, int totalSize) { }

						@Override
						public void onSuccess(Object o) {
							if(o instanceof ResponseStatsDTO) {
								mDTO = (ResponseStatsDTO) o;
							}
						}

						@Override
						public void onFailure(Throwable throwable) {
							mThrowable = throwable;
						}
					});
				} catch (SQLException e) {
					mThrowable = e;
					return null;
				}
				return null;
			}
			@Override
			protected void onPostExecute(Void result) {
				defaultOnPostExecute(mThrowable, mDTO, listener);
			}
		}.execute(new Void[0]);
	}

	// ################################################################################################### //
	// --------------------------------------------- Rating ---------------------------------------------- //

	@Override
	public void getRating(final Show show, final ServiceResponseListener listener) {
		new AsyncTask<Void, Void, Void>() {
			Throwable mThrowable = null;
			Rating mRating;
			@Override
			protected Void doInBackground(Void... params) {
				try {
					loadAllServices();
					mRating = sShowService.getRating(show);
				} catch (SQLException e) {
					mThrowable = e;
					return null;
				}
				return null;
			}
			@Override
			protected void onPostExecute(Void result) {
				defaultOnPostExecute(mThrowable, mRating, listener);
			}
		}.execute(new Void[0]);
	}

	@Override
	public void getRating(final Episode episode, final ServiceResponseListener listener) {
		new AsyncTask<Void, Void, Void>() {
			Throwable mThrowable = null;
			Rating mRating;
			@Override
			protected Void doInBackground(Void... params) {
				try {
					loadAllServices();
					mRating = sEpisodeService.getRating(episode);
				} catch (SQLException e) {
					mThrowable = e;
					return null;
				}
				return null;
			}
			@Override
			protected void onPostExecute(Void result) {
				defaultOnPostExecute(mThrowable, mRating, listener);
			}
		}.execute(new Void[0]);
	}

	@Override
	public void uploadRating(final Show show, final int rating, final ServiceResponseListener listener) {
		new AsyncTask<Void, Void, Void>() {
			Throwable mThrowable = null;
			Show mShow;
			@Override
			protected Void doInBackground(Void... params) {
				try {
					loadAllServices();
					sShowService.uploadRating(show, rating, new ServiceResponseListener() {
						@Override
						public void onProgress(int bytesWritten, int totalSize) { }

						@Override
						public void onSuccess(Object o) {
							if(o instanceof Show) {
								mShow = (Show) o;
							}
						}

						@Override
						public void onFailure(Throwable throwable) {
							mThrowable = throwable;
						}
					});
				} catch (SQLException e) {
					mThrowable = e;
					return null;
				} catch (UnsupportedEncodingException e) {
					mThrowable = e;
					return null;
				}
				return null;
			}
			@Override
			protected void onPostExecute(Void result) {
				defaultOnPostExecute(mThrowable, mShow, listener);
			}
		}.execute(new Void[0]);
	}

	@Override
	public void uploadRating(final Episode episode, final int rating, final ServiceResponseListener listener) {
		new AsyncTask<Void, Void, Void>() {
			Throwable mThrowable = null;
			Episode mEpisode;
			@Override
			protected Void doInBackground(Void... params) {
				try {
					loadAllServices();
					sEpisodeService.saveRating(episode, rating, new ServiceResponseListener() {
						@Override
						public void onProgress(int bytesWritten, int totalSize) { }

						@Override
						public void onSuccess(Object o) {
							if(o instanceof Episode) {
								mEpisode = (Episode) o;
							}
						}

						@Override
						public void onFailure(Throwable throwable) {
							mThrowable = throwable;
						}
					});
				} catch (SQLException e) {
					mThrowable = e;
					return null;
				} catch (UnsupportedEncodingException e) {
					mThrowable = e;
					return null;
				}
				return null;
			}
			@Override
			protected void onPostExecute(Void result) {
				defaultOnPostExecute(mThrowable, mEpisode, listener);
			}
		}.execute(new Void[0]);
	}

	// ################################################################################################### //
	// --------------------------------------------- COMMENTS -------------------------------------------- //

	@Override
	public void sendComment(final Show show, final String msg, final boolean spoiler, final ServiceResponseListener listener) {
		new AsyncTask<Void, Void, Void>() {
			Throwable mThrowable = null;
			Boolean mSuccess = false;
			@Override
			protected Void doInBackground(Void... params) {
				try {
					loadAllServices();
					sCommentsService.sendComment(show, msg, spoiler, new ServiceResponseListener() {

						@Override public void onProgress(int bytesWritten, int totalSize) { }

						@Override
						public void onSuccess(Object o) {
							if(o instanceof Boolean) {
								mSuccess = (Boolean) o;
							}
						}

						@Override
						public void onFailure(Throwable throwable) {
							mThrowable = throwable;
						}
					});
				} catch (SQLException e) {
					mThrowable = e;
					return null;
				}
				return null;
			}
			@Override
			protected void onPostExecute(Void result) {
				defaultOnPostExecute(mThrowable, mSuccess, listener);
			}
		}.execute(new Void[0]);
	}

	@Override
	public void sendComment(final Episode episode, final String msg, final boolean spoiler, final ServiceResponseListener listener) {
		new AsyncTask<Void, Void, Void>() {
			Throwable mThrowable = null;
			Boolean mSuccess = false;
			@Override
			protected Void doInBackground(Void... params) {
				try {
					loadAllServices();
					sCommentsService.sendComment(episode, msg, spoiler, new ServiceResponseListener() {

						@Override public void onProgress(int bytesWritten, int totalSize) { }

						@Override
						public void onSuccess(Object o) {
							if(o instanceof Boolean) {
								mSuccess = (Boolean) o;
							}
						}

						@Override
						public void onFailure(Throwable throwable) {
							mThrowable = throwable;
						}
					});
				} catch (SQLException e) {
					mThrowable = e;
					return null;
				}
				return null;
			}
			@Override
			protected void onPostExecute(Void result) {
				defaultOnPostExecute(mThrowable, mSuccess, listener);
			}
		}.execute(new Void[0]);
	}

	@Override
	public void getShowComments(final Show show, final ServiceResponseListener listener) {
		new AsyncTask<Void, Void, Void>() {
			Throwable mThrowable = null;
			InternalCommentsDTO mDTO;
			@Override
			protected Void doInBackground(Void... params) {
				try {
					loadAllServices();
					sCommentsService.getShowComments(show, new ServiceResponseListener() {

						@Override public void onProgress(int bytesWritten, int totalSize) { }

						@Override
						public void onSuccess(Object o) {
							if(o instanceof InternalCommentsDTO) {
								mDTO = (InternalCommentsDTO) o;
							}
						}

						@Override
						public void onFailure(Throwable throwable) {
							mThrowable = throwable;
						}
					});
				} catch (SQLException e) {
					mThrowable = e;
					return null;
				}
				return null;
			}
			@Override
			protected void onPostExecute(Void result) {
				defaultOnPostExecute(mThrowable, mDTO, listener);
			}
		}.execute(new Void[0]);
	}

	@Override
	public void getEpisodeComments(final Episode episode, final ServiceResponseListener listener) {
		new AsyncTask<Void, Void, Void>() {
			Throwable mThrowable = null;
			InternalCommentsDTO mDTO;
			@Override
			protected Void doInBackground(Void... params) {
				try {
					loadAllServices();
					sCommentsService.getEpisodeComments(episode, new ServiceResponseListener() {

						@Override public void onProgress(int bytesWritten, int totalSize) { }

						@Override
						public void onSuccess(Object o) {
							if(o instanceof InternalCommentsDTO) {
								mDTO = (InternalCommentsDTO) o;
							}
						}

						@Override
						public void onFailure(Throwable throwable) {
							mThrowable = throwable;
						}
					});
				} catch (SQLException e) {
					mThrowable = e;
					return null;
				}
				return null;
			}
			@Override
			protected void onPostExecute(Void result) {
				defaultOnPostExecute(mThrowable, mDTO, listener);
			}
		}.execute(new Void[0]);
	}

	// ################################################################################################### //
	// --------------------------------------------- ACTOR ----------------------------------------------- //

	@Override
	public void getActorBySlug(final String slug, final ServiceResponseListener listener) {
		new AsyncTask<Void, Void, Void>() {
			Throwable mThrowable = null;
			Actor mActor;
			@Override
			protected Void doInBackground(Void... params) {
				try {
					loadAllServices();
					mActor = sActorService.getActorBySlug(slug);
				} catch (SQLException e) {
					mThrowable = e;
					return null;
				}
				return null;
			}
			@Override
			protected void onPostExecute(Void result) {
				defaultOnPostExecute(mThrowable, mActor, listener);
			}
		}.execute(new Void[0]);
	}

	@Override
	public void downloadActorDetails(final Actor actor, final ServiceResponseListener listener) {
		new AsyncTask<Void, Void, Void>() {
			Throwable mThrowable = null;
			Actor mActor;
			@Override
			protected Void doInBackground(Void... params) {
				try {
					loadAllServices();
					sActorService.downloadActorDetails(actor, new ServiceResponseListener() {
						@Override
						public void onProgress(int bytesWritten, int totalSize) { }

						@Override
						public void onSuccess(Object o) {
							if(o instanceof Actor) {
								mActor = (Actor) o;
							}
						}

						@Override
						public void onFailure(Throwable throwable) {
							mThrowable = throwable;
						}
					});
				} catch (SQLException e) {
					mThrowable = e;
					return null;
				}
				return null;
			}
			@Override
			protected void onPostExecute(Void result) {
				defaultOnPostExecute(mThrowable, mActor, listener);
			}
		}.execute(new Void[0]);
	}



	@Override
	public void setFirstTimeUserOnDevice(final boolean b, final ServiceResponseListener listener) {
		new AsyncTask<Void, Void, Void>() {
			Throwable mThrowable = null;
			@Override
			protected Void doInBackground(Void... params) {
				try {
					loadAllServices();
					sUserService.setFirstTimeUserOnDevice(b);
				} catch (SQLException e) {
					mThrowable = e;
					return null;
				} catch (UserNotFoundException e) {
					mThrowable = e;
					return null;
				}
				return null;
			}
			@Override
			protected void onPostExecute(Void result) {
				defaultOnPostExecute(mThrowable, b, listener);
			}
		}.execute(new Void[0]);
	}


	@Override
	public void getSeasonsFragmentDTO(final int tvdbId, final ServiceResponseListener listener) {
		new AsyncTask<Void, Void, Void>() {
			Throwable mThrowable = null;
			InternalSeasonsFragmentDTO mDTO;
			@Override
			protected Void doInBackground(Void... params) {
				try {
					loadAllServices();
					Show show = sShowService.getShowById(tvdbId);
					if(show == null) { return null; }
					ArrayList<Season> headerList = new ArrayList<Season>();
					HashMap<Season, List<Episode>> childMap = new HashMap<Season, List<Episode>>();
					if(show.isFullyDownloaded() && show.getSeasons()!=null) {
						for (Season season : show.getSeasons()) {
							headerList.add(season);
							List<Episode> episodes = new ArrayList<Episode>();
							episodes.addAll(season.getEpisodes());
							childMap.put(season, episodes);
						}
						mDTO = new InternalSeasonsFragmentDTO(headerList, childMap);
					} else {
						// NOT ALL
						Log.w(LOGTAG, show.getTitle()+" NOT yet fullyloaded!");
						mDTO = new InternalSeasonsFragmentDTO(false);
					}
				} catch (SQLException e) {
					mThrowable = e;
					return null;
				}
				return null;
			}
			@Override
			protected void onPostExecute(Void result) {
				defaultOnPostExecute(mThrowable, mDTO, listener);
			}
		}.execute(new Void[0]);
	}

	@Override
	public void isShowFollowedByCurrentUser(final Show show, final ServiceResponseListener listener) {
		new AsyncTask<Void, Void, Void>() {
			Throwable mThrowable = null;
			Boolean mShowFollowed;
			@Override
			protected Void doInBackground(Void... params) {
				try {
					loadAllServices();
					mShowFollowed = sShowService.isShowFollowedByCurrentUser(show.getTvdbID());
				} catch (SQLException e) {
					mThrowable = e;
					return null;
				}
				return null;
			}
			@Override
			protected void onPostExecute(Void result) {
				defaultOnPostExecute(mThrowable, mShowFollowed, listener);
			}
		}.execute(new Void[0]);
	}

	@Override
	public void isShowFollowedByCurrentUser(int tvdbId, ServiceResponseListener listener) {
		isShowFollowedByCurrentUser(new Show(tvdbId), listener);
	}

	@Override
	public void getRecShowStatusData(final int tvdbId, final ServiceResponseListener listener) {
		new AsyncTask<Void, Void, Void>() {
			Throwable mThrowable = null;
			InternalRecommendationStatusDTO mDto = new InternalRecommendationStatusDTO();
			@Override
			protected Void doInBackground(Void... params) {
				try {
					loadAllServices();
					mDto.setFollowed(sShowService.isShowFollowedByCurrentUser(tvdbId));
					Show show = sShowService.getShowById(tvdbId);
					if(show != null) {
						ShowDownloadStatus status = show.getDownloadStatus();
						mDto.setStatus(status);
					}
				} catch (SQLException e) {
					mThrowable = e;
					return null;
				}
				return null;
			}
			@Override
			protected void onPostExecute(Void result) {
				defaultOnPostExecute(mThrowable, mDto, listener);
			}
		}.execute(new Void[0]);
	}

	@Override
	public void hasEpisodeBeenAired(final Episode episode, final ServiceResponseListener listener) {
		new AsyncTask<Void, Void, Void>() {
			Throwable mThrowable = null;
			Boolean mHasAired;
			@Override
			protected Void doInBackground(Void... params) {
				try {
					loadAllServices();
					mHasAired = DateHelper.hasBeenAired(episode.getFirstAiredUTC());
				} catch (SQLException e) {
					mThrowable = e;
					return null;
				}
				return null;
			}
			@Override
			protected void onPostExecute(Void result) {
				defaultOnPostExecute(mThrowable, mHasAired, listener);
			}
		}.execute(new Void[0]);
	}


	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// ----- PHOTO_CACHE -------------------------------------------------------------------------------------------- //

	@Override
	public void clearPhotoDiscCache(final ServiceResponseListener listener) {
		new AsyncTask<Void, Void, Void>() {
			Boolean mSuccess;
			@Override
			protected void onProgressUpdate(Void... values) {
				listener.onProgress(-1, -1);
			}

			@Override
			protected Void doInBackground(Void... params) {
				ImageLoaderHelper.clearDiskCache(mContext);
				mSuccess = true;
				publishProgress(new Void[0]);
				try {
					// This is just here, to show the loading
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				return null;
			}

			@Override
			protected void onPostExecute(Void result) {
				listener.onSuccess(mSuccess);
			}
		}.execute(new Void[0]);
	}


}
