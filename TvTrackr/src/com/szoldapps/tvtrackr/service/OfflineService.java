package com.szoldapps.tvtrackr.service;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;

import com.szoldapps.tvtrackr.db.model.Episode;
import com.szoldapps.tvtrackr.db.model.OfflineEpisodeToUnfollow;
import com.szoldapps.tvtrackr.db.model.OfflineShowToUnfollow;
import com.szoldapps.tvtrackr.db.model.Show;



public interface OfflineService {

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// OFFLINE SHOW TO UNFOLLOW
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	/**
	 * Creates {@link OfflineShowToUnfollow} in DB (if it's not already in there) 
	 * incl. all {@link OfflineEpisodeToUnfollow} Objects
	 * @param show
	 * @return
	 * @throws SQLException
	 */
	public OfflineShowToUnfollow createIfNotExistOfflineShowToUnfollow(Show show) throws SQLException;

	/**
	 * Returns all {@link OfflineShowToUnfollow} Objects that are stored in the DB
	 * @return	{@link List}<{@link OfflineShowToUnfollow}>
	 * @throws SQLException
	 */
	public List<OfflineShowToUnfollow> getAllOfflineShowsToUnfollow() throws SQLException;

	/**
	 * Deletes the {@link OfflineShowToUnfollow} with <code>tvdbId</code>
	 * incl. all related {@link OfflineEpisodeToUnfollow}
	 * @param tvdbId
	 * @throws SQLException
	 */
	public void deleteOfflineShowToUnfollowById(int tvdbId) throws SQLException;

	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// OFFLINE MARKED EPISODES
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	/**
	 * Returns all {@link Episode}s marked offline for the current User
	 * @return
	 * @throws SQLException
	 */
	public List<Episode> getAllOfflineMarkedEpisodes() throws SQLException;

	public HashMap<Show, List<Episode>> getAllOfflineMarkedEpisodesShowMap() throws SQLException;
}
