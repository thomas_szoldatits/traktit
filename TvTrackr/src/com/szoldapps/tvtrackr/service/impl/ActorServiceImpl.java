package com.szoldapps.tvtrackr.service.impl;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.Header;
import org.json.JSONObject;

import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.szoldapps.tvtrackr.db.dao.ActorDAO;
import com.szoldapps.tvtrackr.db.manager.DBHelper;
import com.szoldapps.tvtrackr.db.manager.DBManager;
import com.szoldapps.tvtrackr.db.model.Actor;
import com.szoldapps.tvtrackr.dto.show.ActorDTO;
import com.szoldapps.tvtrackr.helper.LogCatHelper;
import com.szoldapps.tvtrackr.service.ActorService;
import com.szoldapps.tvtrackr.service.listener.ServiceResponseListener;
import com.szoldapps.tvtrackr.traktAccess.DefaultRequestsHandler;
import com.szoldapps.tvtrackr.traktAccess.RestClient;
import com.szoldapps.tvtrackr.traktAccess.RestResponseListener;
import com.szoldapps.tvtrackr.traktAccess.RestClient.RestURLs;

public class ActorServiceImpl implements ActorService {

	public static final String LOGTAG = LogCatHelper.getAppTag()+ActorServiceImpl.class.getSimpleName().toString();
	private static ActorService sActorService;
	
	DBHelper mDbHelper;
	private ActorDAO mActorDAO;

	public static ActorService getService(Context context) throws SQLException {
		if(sActorService == null) {
			sActorService = new ActorServiceImpl(context);
		}
		return sActorService;
	}
	
	private ActorServiceImpl(Context context) throws SQLException {
		mDbHelper = DBManager.getHelper(context);
		mActorDAO = mDbHelper.getActorDAO();
	}

	@Override
	public Actor getActorBySlug(String slug) throws SQLException {
		return mActorDAO.queryForId(slug);
	}

	@Override
	public void downloadActorDetails(final Actor actor, final ServiceResponseListener listener) {
		String query = actor.getName().replace(" ", "+");
		RestClient.postSync(RestURLs.SEARCH_PEOPLE + query + "/1", null, new DefaultRequestsHandler(new RestResponseListener() {

			@Override
			public void onProgress(int bytesWritten, int totalSize) {
				//Log.d(LOGTAG, String.format("Progress %d from %d (%d%%)", bytesWritten, totalSize, (totalSize > 0) ? (int) (((float) bytesWritten / (float) totalSize) * 100f) : -1));
			}

			@Override
			public void onDownloadSuccess(int statusCode, Header[] headers, String jsonStr) {
				ArrayList<ActorDTO> actors = new Gson().fromJson(jsonStr, new TypeToken<List<ActorDTO>>(){}.getType());
				if(actors.size()>0) {
					try {
						ActorDTO actorDTO = actors.get(0);
						actor.setBiography(actorDTO.getBiography());
						actor.setBirthday(actorDTO.getBirthday());
						actor.setBirthplace(actorDTO.getBirthplace());
						actor.setHeadshot(actorDTO.getImages().getHeadshot());
						actor.setName(actorDTO.getName());
						actor.setTmd_id(actorDTO.getTmdb_id());
						actor.setFullyLoaded(true);
						actor.setUrl(actorDTO.getUrl());

						mActorDAO.update(actor);
						listener.onSuccess(actor);
					} catch (SQLException e) {
						onDownloadFailure(e, null);
					}
					return;
				}
				listener.onSuccess(null);
			}

			@Override
			public void onDownloadFailure(Throwable throwable, JSONObject jsonObject) {
				if(jsonObject != null) {
					Log.e(LOGTAG, jsonObject.toString());
				}
				listener.onFailure(throwable);
			}
		}));
	}

}
