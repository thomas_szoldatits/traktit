package com.szoldapps.tvtrackr.service.impl;

import java.io.UnsupportedEncodingException;
import java.sql.SQLException;
import java.util.List;

import org.apache.http.Header;
import org.apache.http.entity.StringEntity;
import org.json.JSONObject;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.szoldapps.tvtrackr.db.manager.DBHelper;
import com.szoldapps.tvtrackr.db.manager.DBManager;
import com.szoldapps.tvtrackr.db.model.Episode;
import com.szoldapps.tvtrackr.db.model.Season;
import com.szoldapps.tvtrackr.db.model.Show;
import com.szoldapps.tvtrackr.db.model.User;
import com.szoldapps.tvtrackr.dto.internal.InternalCommentsDTO;
import com.szoldapps.tvtrackr.dto.request.RequestCommentDTO;
import com.szoldapps.tvtrackr.dto.response.ResponseCommentsDTO;
import com.szoldapps.tvtrackr.helper.LogCatHelper;
import com.szoldapps.tvtrackr.service.CommentsService;
import com.szoldapps.tvtrackr.service.UserService;
import com.szoldapps.tvtrackr.service.listener.ServiceResponseListener;
import com.szoldapps.tvtrackr.traktAccess.DefaultRequestsHandler;
import com.szoldapps.tvtrackr.traktAccess.RestClient;
import com.szoldapps.tvtrackr.traktAccess.RestResponseListener;
import com.szoldapps.tvtrackr.traktAccess.RestClient.RestURLs;

public class CommentsServiceImpl implements CommentsService {

	public static final String LOGTAG = LogCatHelper.getAppTag()+CommentsServiceImpl.class.getSimpleName().toString();

	private Context mContext;
	DBHelper mDbHelper;
	private UserService mUserService;
	private User mUser;
	private static CommentsService sCommentsService;

	public static CommentsService getService(Context context) throws SQLException {
		if(sCommentsService == null) {
			sCommentsService = new CommentsServiceImpl(context);
		}
		return sCommentsService;
	}
	
	private CommentsServiceImpl(Context context) throws SQLException {
		mContext = context;
		mDbHelper = DBManager.getHelper(context);
		mUserService = UserServiceImpl.getService(context);

		mUser = mUserService.getCurrentUser();
	}

	/* --------------------- SHOW RELATED --------------------- */

	@Override
	public void getShowComments(Show show, final ServiceResponseListener listener) {
		getCommentsFromTrakt(RestURLs.SHOW_COMMENTS + show.getTvdbID(), listener);
	}


	@Override
	public void sendComment(Show show, String msg, boolean spoiler, final ServiceResponseListener listener) {
		try {
			StringEntity entity = new StringEntity(new Gson().toJson(new RequestCommentDTO(mUser, show, msg, spoiler)));
			sendCommentToTrakt(entity, RestURLs.COMMENT_SHOW, msg, spoiler, listener);
		} catch (UnsupportedEncodingException e) {
			listener.onFailure(e);
		}
	}

	/* --------------------- EPISODE RELATED --------------------- */


	@Override
	public void getEpisodeComments(Episode episode, final ServiceResponseListener listener) {
		Season season = episode.getSeason();
		Show show = season.getShow();

		// get Episode Stats from Trakt
		String url = RestURLs.SHOW_EPISODE_COMMENTS + show.getTvdbID() + "/" + season.getSeasonNr() + "/" + episode.getNumber();
		getCommentsFromTrakt(url, listener);
	}


	@Override
	public void sendComment(Episode episode, String msg, boolean spoiler, final ServiceResponseListener listener) {
		try {
			StringEntity entity = new StringEntity(new Gson().toJson(new RequestCommentDTO(mUser, episode, msg, spoiler)));
			sendCommentToTrakt(entity, RestURLs.COMMENT_EPISODE, msg, spoiler, listener);
		} catch (UnsupportedEncodingException e) {
			listener.onFailure(e);
		}
	}


	/* --------------------- FOR BOTH --------------------- */
	private void getCommentsFromTrakt(String url, final ServiceResponseListener listener) {
		RestClient.postSync(url, null, new DefaultRequestsHandler(new RestResponseListener() {
			@Override public void onProgress(int bytesWritten, int totalSize) { }

			@Override
			public void onDownloadSuccess(int statusCode, Header[] headers, String jsonStr) {
				List<ResponseCommentsDTO> comments = new Gson().fromJson(jsonStr, new TypeToken<List<ResponseCommentsDTO>>(){}.getType());
				listener.onSuccess(new InternalCommentsDTO(mUser.getUsername(), comments));
			}

			@Override
			public void onDownloadFailure(Throwable throwable, JSONObject jsonObject) {
				listener.onFailure(throwable);
			}
		}));
	}

	private void sendCommentToTrakt(StringEntity entity, String url, String msg, boolean spoiler, final ServiceResponseListener listener) {
		RestClient.postSyncWithJSON(mContext, url, entity,  new DefaultRequestsHandler(new RestResponseListener() {
			@Override public void onProgress(int bytesWritten, int totalSize) { }

			@Override
			public void onDownloadSuccess(int statusCode, Header[] headers, String jsonStr) {
				listener.onSuccess(true);
			}

			@Override
			public void onDownloadFailure(Throwable throwable, JSONObject jsonObject) {
				listener.onFailure(throwable);
			}
		}));
	}

}

