package com.szoldapps.tvtrackr.service.impl;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.Header;
import org.apache.http.entity.StringEntity;
import org.json.JSONObject;

import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.szoldapps.tvtrackr.db.dao.EpisodeDAO;
import com.szoldapps.tvtrackr.db.dao.EpisodeUserDAO;
import com.szoldapps.tvtrackr.db.dao.RatingDAO;
import com.szoldapps.tvtrackr.db.dao.SeasonDAO;
import com.szoldapps.tvtrackr.db.dao.ShowDAO;
import com.szoldapps.tvtrackr.db.manager.DBHelper;
import com.szoldapps.tvtrackr.db.manager.DBManager;
import com.szoldapps.tvtrackr.db.model.Episode;
import com.szoldapps.tvtrackr.db.model.EpisodeUser;
import com.szoldapps.tvtrackr.db.model.Rating;
import com.szoldapps.tvtrackr.db.model.Season;
import com.szoldapps.tvtrackr.db.model.Show;
import com.szoldapps.tvtrackr.dto.request.RequestRateShowEpisodeDTO;
import com.szoldapps.tvtrackr.dto.response.ResponseRateShowEpisodeDTO;
import com.szoldapps.tvtrackr.dto.response.ResponseStatsDTO;
import com.szoldapps.tvtrackr.exception.EpisodeUserNotFoundException;
import com.szoldapps.tvtrackr.helper.LogCatHelper;
import com.szoldapps.tvtrackr.service.EpisodeService;
import com.szoldapps.tvtrackr.service.UserService;
import com.szoldapps.tvtrackr.service.listener.ServiceResponseListener;
import com.szoldapps.tvtrackr.traktAccess.DefaultRequestsHandler;
import com.szoldapps.tvtrackr.traktAccess.RestClient;
import com.szoldapps.tvtrackr.traktAccess.RestResponseListener;
import com.szoldapps.tvtrackr.traktAccess.RestClient.RestURLs;

public class EpisodeServiceImpl implements EpisodeService {

	public static final String LOGTAG = LogCatHelper.getAppTag()+EpisodeServiceImpl.class.getSimpleName().toString();

	private static EpisodeService sEpisodeService;

	private Context mContext;
	DBHelper mDbHelper;
	private UserService mUserService;

	private ShowDAO mShowDAO;
	private SeasonDAO mSeasonDAO;
	private EpisodeDAO mEpisodeDAO;
	private EpisodeUserDAO mEpisodeUserDAO;
	private RatingDAO mRatingDAO;
	private Gson mGson;

	public static EpisodeService getService(Context context) throws SQLException {
		if(sEpisodeService == null) {
			sEpisodeService = new EpisodeServiceImpl(context);
		}
		return sEpisodeService;
	}

	private EpisodeServiceImpl(Context context) throws SQLException {
		mContext = context;
		mDbHelper = DBManager.getHelper(context);

		mUserService = UserServiceImpl.getService(context);

		mShowDAO = mDbHelper.getShowDAO();
		mSeasonDAO = mDbHelper.getSeasonDAO();
		mEpisodeDAO = mDbHelper.getEpisodeDAO();
		mEpisodeUserDAO = mDbHelper.getEpisodeUserDAO();
		mRatingDAO = mDbHelper.getRatingDAO();

		mGson = new Gson();
	}

//	public void downloadEpisode(final int showTvdbId, int seasonNr, int episodeNr, 
//			final ServiceResponseListener listener) {
//		try {
//			String url = RestURLs.SHOW_EPISODE_SUMMARY + showTvdbId + "/" + seasonNr + "/" + episodeNr;
//			RestClient.postSync(url, mUserService.getCurUserRequestParams(), new DefaultRequestsHandler(new RestResponseListener() {
//				@Override public void onProgress(int bytesWritten, int totalSize) { }
//				@Override
//				public void onDownloadSuccess(int statusCode, Header[] headers, String jsonStr) {
//					EpisodeSummaryDTO esDto = mGson.fromJson(jsonStr, new TypeToken<EpisodeSummaryDTO>() {}.getType());
//					EpisodeDTO eDto = esDto.getEpisode();
//					try {
//						boolean followedByUser = false; // Only Updates Episode data, NOT EpisodeUser data
//						Season season = mSeasonDAO.getSeason(eDto.getSeason(), showTvdbId);
//						mShowDAO.createOrUpdateEpisode(eDto, season, followedByUser, 
//								mUserService.getCurrentUser(), mEpisodeDAO, mEpisodeUserDAO, mRatingDAO);
//					} catch (SQLException e) {
//						onDownloadFailure(e, null);
//						return;
//					}
//				}
//
//				@Override
//				public void onDownloadFailure(Throwable throwable, JSONObject jsonObject) {
//					mUserService.defaultOnDownloadFailure(throwable, jsonObject, listener);
//				}
//			}));
//		} catch (SQLException e) {
//			listener.onDownloadFailure(e);
//		}
//	}
	
	@Override
	public boolean isEpisodeSeen(Episode episode) throws SQLException {
		EpisodeUser eu = mEpisodeUserDAO.getEpisodeUser(episode, mUserService.getCurrentUser());
		if(eu != null) {
			return eu.isWatched();
		}
		return false;
	}



	@Override
	public EpisodeUser getEpisodeUser(Episode episode) throws SQLException, EpisodeUserNotFoundException {
		EpisodeUser eu = mEpisodeUserDAO.getEpisodeUser(episode, mUserService.getCurrentUser());
		if(eu != null) {
			return eu;
		}
		throw new EpisodeUserNotFoundException(episode, mUserService.getCurrentUser());
	}

	@Override
	public void setEpisodeWatchedAndOffline(Episode episode, boolean watched, boolean offlineMarked) throws SQLException, EpisodeUserNotFoundException {
		List<Episode> episodes = new ArrayList<Episode>();
		episodes.add(episode);
		setEpisodesWatchedAndOffline(episodes, watched, offlineMarked);
	}

	@Override
	public void setEpisodesWatchedAndOffline(List<Episode> episodes, boolean watched, boolean offlineMarked) throws SQLException, EpisodeUserNotFoundException {
		mEpisodeUserDAO.markEpisodesAsSeenOrUnseen(episodes, watched, offlineMarked, mUserService.getCurrentUser());
	}


	/** ----------- CALENDAR RELATED ----------- **/

	@Override
	public List<Episode> getEpisodesOfDay(int offset) throws SQLException {
		return mEpisodeDAO.getEpisodesOfDay(offset, mUserService.getCurrentUser(), mEpisodeUserDAO);
	}

	@Override
	public Long getUnwatchedEpisodeCountOfDay(int offset) throws SQLException {
		return mEpisodeDAO.getUnwatchedEpisodeCountOfDay(offset, mUserService.getCurrentUser(), mEpisodeUserDAO);
	}

	@Override
	public List<Show> getToWatchShows() throws SQLException {
		return mShowDAO.getToWatchShows(mUserService.getCurrentUser(), mEpisodeUserDAO, mSeasonDAO, mEpisodeDAO);
	}

	/** TOWATCH RELATED **/

	@Override
	public Episode getNextEpisodeToWatch(int tvdbId) throws SQLException {
		return mEpisodeDAO.getNextEpisodeToWatch(tvdbId, mUserService.getCurrentUser(), mEpisodeUserDAO, mSeasonDAO);
	}

	@Override
	public Integer getEpisodesToWatchCountOfShow(int tvdbId) throws SQLException {
		return mEpisodeDAO.getEpisodesToWatchCountOfShow(tvdbId, mUserService.getCurrentUser(), mEpisodeUserDAO, mSeasonDAO);
	}

	@Override
	public Long getAllEpisodesToWatchCount() throws SQLException {
		return mEpisodeDAO.getAllEpisodesToWatchCount(mUserService.getCurrentUser(), mEpisodeUserDAO, mSeasonDAO);
	}

	@Override
	public void downloadStats(final Episode episode, final ServiceResponseListener listener) throws SQLException {
		Season season = episode.getSeason();
		// get Episode Stats from Trakt
		String url = RestURLs.SHOW_EPISODE_STATS + episode.getShowTvdbId() + "/" + season.getSeasonNr() + "/" + episode.getNumber();
		RestClient.postSync(url, mUserService.getCurUserRequestParams(), new DefaultRequestsHandler(new RestResponseListener() {
			@Override public void onProgress(int bytesWritten, int totalSize) { }
			@Override
			public void onDownloadSuccess(int statusCode, Header[] headers, String jsonStr) {
				ResponseStatsDTO statsDTO = mGson.fromJson(jsonStr, new TypeToken<ResponseStatsDTO>() {}.getType());

				// update Episode in DB
				episode.setStatsPlays(statsDTO.getPlays());
				episode.setStatsWatchers(statsDTO.getWatchers());
				episode.setStatsCollected(statsDTO.getCollection().getAll());
				episode.setStatsLists(statsDTO.getLists().getAll());
				try {
					mEpisodeDAO.update(episode);
				} catch (SQLException e) {
					onDownloadFailure(e, null);
				}
				listener.onSuccess(statsDTO);
			}

			@Override
			public void onDownloadFailure(Throwable throwable, JSONObject jsonObject) {
				if(jsonObject != null) {
					Log.e(LOGTAG, jsonObject.toString());
				}
				listener.onFailure(throwable);
			}
		}));
	}

	/** RATING **/

	@Override
	public Rating getRating(Episode episode) throws SQLException {
		return mRatingDAO.getEpisodeRating(episode, mUserService.getCurrentUser());
	}

	@Override
	public void saveRating(final Episode episode, final int rating, final ServiceResponseListener listener) throws UnsupportedEncodingException {
		try {
			StringEntity entity = new StringEntity(new Gson().toJson(new RequestRateShowEpisodeDTO(mUserService.getCurrentUser(), episode, rating)));

			RestClient.postSyncWithJSON(mContext, RestURLs.RATE_EPISODE, entity, new DefaultRequestsHandler(new RestResponseListener() {
				@Override public void onProgress(int bytesWritten, int totalSize) { }
				@Override
				public void onDownloadSuccess(int statusCode, Header[] headers, String jsonStr) {
					try {
						Type listType = new TypeToken<ResponseRateShowEpisodeDTO>() {}.getType();
						ResponseRateShowEpisodeDTO redto = mGson.fromJson(jsonStr, listType);

						// save user's rating in DB
						try {
							redto.ratingAdvanced = Integer.parseInt(redto.rating);
						} catch(NumberFormatException e) {
							redto.ratingAdvanced = 0;
						}
						saveEpisodeRatingInDB(episode, redto.ratingAdvanced);

						// save general rating in DB
						episode.setRatingsPercentage(redto.ratings.getPercentage());
						episode.setRatingsVotes(redto.ratings.getVotes());
						episode.setRatingsLoved(redto.ratings.getLoved());
						episode.setRatingsHated(redto.ratings.getHated());
						mEpisodeDAO.update(episode);

						listener.onSuccess(episode);
					} catch (SQLException e) {
						onDownloadFailure(e, null);
					}
				}

				@Override
				public void onDownloadFailure(Throwable throwable, JSONObject jsonObject) {
					if(jsonObject != null) {
						Log.e(LOGTAG, jsonObject.toString());
					}
					listener.onFailure(throwable);
				}
			}));
		} catch (SQLException e) {
			listener.onFailure(e);
		}
	}

	private Rating saveEpisodeRatingInDB(Episode episode, Integer ratingAdvanced) throws SQLException {
		Rating rating = mRatingDAO.getEpisodeRating(episode, mUserService.getCurrentUser());
		if(rating == null) {
			rating = new Rating(mUserService.getCurrentUser(), episode.getSeason().getShow(), episode, null, ratingAdvanced);
			mRatingDAO.create(rating);
		} else {
			rating.setRatingAdvanced(ratingAdvanced);
			mRatingDAO.update(rating);
		}
		mRatingDAO.refresh(rating);
		return rating;
	}

	@Override
	public Episode getEpisodeByPosition(int position, int showTvdbId) throws SQLException {
		return mEpisodeDAO.getEpisodeByPosition(position, showTvdbId, mSeasonDAO);
	}

	@Override
	public long getEpisodeSeenCountOfShow(Show show) throws SQLException {
		return mEpisodeDAO.getEpisodeSeenCountOfShow(show, mUserService.getCurrentUser(), mEpisodeUserDAO);
	}

}

