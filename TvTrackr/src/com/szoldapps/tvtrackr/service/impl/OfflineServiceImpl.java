package com.szoldapps.tvtrackr.service.impl;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;

import android.content.Context;

import com.szoldapps.tvtrackr.db.dao.EpisodeDAO;
import com.szoldapps.tvtrackr.db.dao.EpisodeUserDAO;
import com.szoldapps.tvtrackr.db.dao.OfflineEpisodeToUnfollowDAO;
import com.szoldapps.tvtrackr.db.dao.OfflineShowToUnfollowDAO;
import com.szoldapps.tvtrackr.db.dao.ShowDAO;
import com.szoldapps.tvtrackr.db.manager.DBHelper;
import com.szoldapps.tvtrackr.db.manager.DBManager;
import com.szoldapps.tvtrackr.db.model.Episode;
import com.szoldapps.tvtrackr.db.model.OfflineEpisodeToUnfollow;
import com.szoldapps.tvtrackr.db.model.OfflineShowToUnfollow;
import com.szoldapps.tvtrackr.db.model.Season;
import com.szoldapps.tvtrackr.db.model.Show;
import com.szoldapps.tvtrackr.helper.LogCatHelper;
import com.szoldapps.tvtrackr.service.OfflineService;
import com.szoldapps.tvtrackr.service.UserService;

public class OfflineServiceImpl implements OfflineService {

	public static final String LOGTAG = LogCatHelper.getAppTag()+OfflineServiceImpl.class.getSimpleName().toString();
	private static OfflineService sOfflineService;
	private UserService mUserService;

	DBHelper mDbHelper;
	// DAOs
	private OfflineShowToUnfollowDAO mOfflineShowToUnfollowDAO;
	private OfflineEpisodeToUnfollowDAO mOfflineEpisodeToUnfollowDAO;
	private ShowDAO mShowDAO;
	private EpisodeDAO mEpisodeDAO;
	private EpisodeUserDAO mEpisodeUserDAO;

	public static OfflineService getService(Context context) throws SQLException {
		if(sOfflineService == null) {
			sOfflineService = new OfflineServiceImpl(context);
		}
		return sOfflineService;
	}

	private OfflineServiceImpl(Context context) throws SQLException {
		mDbHelper = DBManager.getHelper(context);
		mOfflineShowToUnfollowDAO = mDbHelper.getOfflineShowToUnfollowDAO();
		mOfflineEpisodeToUnfollowDAO = mDbHelper.getOfflineEpisodeToUnfollowDAO();
		mShowDAO = mDbHelper.getShowDAO();
		mEpisodeDAO = mDbHelper.getEpisodeDAO();
		mEpisodeUserDAO = mDbHelper.getEpisodeUserDAO();
		mUserService = UserServiceImpl.getService(context);
	}


	////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// OFFLINE SHOW TO UNFOLLOW
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	@Override
	public OfflineShowToUnfollow createIfNotExistOfflineShowToUnfollow(Show show) throws SQLException {
		OfflineShowToUnfollow oShow = new OfflineShowToUnfollow(show.getTvdbID());
		oShow = mOfflineShowToUnfollowDAO.createIfNotExists(oShow);

		mShowDAO.refresh(show);
		for (Season season : show.getSeasons()) {
			for (Episode episode : season.getEpisodes()) {
				createOfflineEpisodeToUnfollow(season.getSeasonNr(), episode, oShow);
			}
		}
		mOfflineShowToUnfollowDAO.refresh(oShow);
		return oShow;
	}

	private OfflineEpisodeToUnfollow createOfflineEpisodeToUnfollow(int season, Episode episode, OfflineShowToUnfollow oShow) throws SQLException {
		OfflineEpisodeToUnfollow oEpisode = new OfflineEpisodeToUnfollow();
		oEpisode.setSeason(season);
		oEpisode.setEpisode(episode.getNumber());
		oEpisode.setOfflineShowToUnfollow(oShow);
		mOfflineEpisodeToUnfollowDAO.create(oEpisode);
		return oEpisode;
	}

	@Override
	public List<OfflineShowToUnfollow> getAllOfflineShowsToUnfollow() throws SQLException {
		return mOfflineShowToUnfollowDAO.queryForAll();

	}

	@Override
	public void deleteOfflineShowToUnfollowById(int tvdbId) throws SQLException {
		OfflineShowToUnfollow oShow = mOfflineShowToUnfollowDAO.queryForId(tvdbId);
		if(oShow != null) {
			for (OfflineEpisodeToUnfollow oEpisode : oShow.getEpisodes()) {
				mOfflineEpisodeToUnfollowDAO.deleteById(oEpisode.getId());
			}
			mOfflineShowToUnfollowDAO.deleteById(tvdbId);
		}
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// OFFLINE MARKED EPISODES
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	@Override
	public List<Episode> getAllOfflineMarkedEpisodes() throws SQLException {
		return mEpisodeUserDAO.getAllOfflineMarkedEpisodes(mUserService.getCurrentUser(), mEpisodeDAO);
	}
	
	@Override
	public HashMap<Show, List<Episode>> getAllOfflineMarkedEpisodesShowMap() throws SQLException {
		return mEpisodeUserDAO.getAllOfflineMarkedEpisodesShowMap(mUserService.getCurrentUser(), mEpisodeDAO, mShowDAO);
	}

}
