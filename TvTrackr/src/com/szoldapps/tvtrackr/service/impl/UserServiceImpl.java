package com.szoldapps.tvtrackr.service.impl;

import java.io.UnsupportedEncodingException;
import java.sql.SQLException;

import org.apache.http.Header;
import org.apache.http.client.HttpResponseException;
import org.apache.http.entity.StringEntity;
import org.json.JSONObject;

import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.RequestParams;
import com.szoldapps.tvtrackr.R;
import com.szoldapps.tvtrackr.db.dao.UserDAO;
import com.szoldapps.tvtrackr.db.manager.DBHelper;
import com.szoldapps.tvtrackr.db.manager.DBManager;
import com.szoldapps.tvtrackr.db.model.User;
import com.szoldapps.tvtrackr.dto.request.RequestAccountCreateDTO;
import com.szoldapps.tvtrackr.dto.response.ResponseAccountSettingsDTO;
import com.szoldapps.tvtrackr.dto.response.ResponseAccountSettingsDTO.ResponseUserProfileDTO;
import com.szoldapps.tvtrackr.dto.response.ResponseFailureDTO;
import com.szoldapps.tvtrackr.dto.response.ResponseSuccessDTO;
import com.szoldapps.tvtrackr.exception.EmailAlreadyExistsException;
import com.szoldapps.tvtrackr.exception.UserNotFoundException;
import com.szoldapps.tvtrackr.exception.UsernameAlreadyExistsException;
import com.szoldapps.tvtrackr.helper.DateHelper;
import com.szoldapps.tvtrackr.helper.LogCatHelper;
import com.szoldapps.tvtrackr.service.UserService;
import com.szoldapps.tvtrackr.service.listener.ServiceResponseListener;
import com.szoldapps.tvtrackr.traktAccess.DefaultRequestsHandler;
import com.szoldapps.tvtrackr.traktAccess.RestClient;
import com.szoldapps.tvtrackr.traktAccess.RestResponseListener;
import com.szoldapps.tvtrackr.traktAccess.RestClient.RestURLs;

public class UserServiceImpl implements UserService {

	public static final String LOGTAG = LogCatHelper.getAppTag()+UserServiceImpl.class.getSimpleName().toString();
	// Helper
	private Context mContext;
	DBHelper mDbHelper;
	private User mUser;
	// Services
	private static UserService sUserService;
	// DAOs
	private UserDAO mUserDAO;

	public static enum UserServiceStatus {
		IDLE, SIGNING_IN, OPTION3;
	}

	public static UserService getService(Context context) throws SQLException {
		if(sUserService == null) {
			sUserService = new UserServiceImpl(context);
		}
		return sUserService;
	}

	private UserServiceImpl(Context context) throws SQLException {
		mContext = context;
		mDbHelper = DBManager.getHelper(mContext);
		mUserDAO = mDbHelper.getUserDAO();
	}


	@Override
	public void login(final User user, final ServiceResponseListener listener) {
		try {
			Log.d(LOGTAG, "Attemting login of user "+user.getUsername()+ " " + user.getPassword());
			StringEntity entity = new StringEntity(new Gson().toJson(new RequestAccountCreateDTO(user)));
			RestClient.postSyncWithJSON(mContext, RestURLs.ACCOUNT_TEST, entity, new DefaultRequestsHandler(new RestResponseListener() {
				@Override public void onProgress(int bytesWritten, int totalSize) { /* not necessary */ }

				@Override
				public void onDownloadSuccess(int statusCode, Header[] headers, String jsonStr) {
					ResponseSuccessDTO loginDTO = new Gson().fromJson(jsonStr, new TypeToken<ResponseSuccessDTO>() {}.getType());
					if(loginDTO.getStatus().equals("success")) {
						// setCurrentUser will return a valid User object or a failure
						try {
							mUser = mUserDAO.setCurrentUser(user);
							mUserDAO.refresh(mUser);
							listener.onSuccess(mUser);
							return;
						} catch (SQLException e) {
							onDownloadFailure(e, null);
						}
					} else {
						onDownloadFailure(new HttpResponseException(0, "Wrong Credentials"), null);
					}
				}
				@Override
				public void onDownloadFailure(Throwable throwable, JSONObject jsonObject) {
					if(jsonObject!=null) {
						Log.e(LOGTAG, jsonObject.toString());
					}
					listener.onFailure(throwable);
				}
			}));
		} catch (UnsupportedEncodingException e) {
			listener.onFailure(e);
		}
	}

	@Override
	public void checkCredentialsOfCurrentUser(ServiceResponseListener listener) {
		try {
			User curUser = getCurrentUser();
			if(curUser != null) {
				login(curUser, listener);
			}
		} catch (SQLException e) {
			listener.onFailure(e);
			return;
		}

	}

	@Override
	public User logout() throws SQLException {
		return mUserDAO.resetCurrentUserFlag();
	}

	@Override
	public User getCurrentUser() throws SQLException {
		if(mUser == null) {
			mUser = mUserDAO.getCurrentUser();
		}
		return mUser;
	}

	@Override
	public void downloadCurrentUserDetails(final ServiceResponseListener listener) throws SQLException, UnsupportedEncodingException {
		final User user = getCurrentUser();
		if(user == null) {
			listener.onFailure(new UserNotFoundException("No currentUser set!"));
			return;
		}
		StringEntity entity = new StringEntity(new Gson().toJson(new RequestAccountCreateDTO(user)));

		RestClient.postSyncWithJSON(mContext, RestURLs.ACCOUNT_SETTINGS, entity, new DefaultRequestsHandler(new RestResponseListener() {
			@Override 
			public void onProgress(int bytesWritten, int totalSize) { /* not necessary */ }

			@Override
			public void onDownloadSuccess(int statusCode, Header[] headers, String jsonStr) {
				ResponseAccountSettingsDTO accountSettings = new Gson().fromJson(jsonStr, new TypeToken<ResponseAccountSettingsDTO>() {}.getType());
				ResponseUserProfileDTO profile = accountSettings.getProfile();
				user.setFullName(profile.getFull_name());
				user.setGender(profile.getGender());
				user.setAge(profile.getAge());
				user.setLocation(profile.getLocation());
				user.setAbout(profile.getAbout());
				user.setJoined(profile.getJoined());
				// last Trakt login MISSING (but probably not needed)
				user.setAvatar(profile.getAvatar());
				user.setUrl(profile.getUrl());
				// isVip MISSING (but probably not needed)
				user.setFullyLoaded(true);
				try {
					mUserDAO.update(user);
					mUser = user;
				} catch (SQLException e) {
					onDownloadFailure(e, null);
					return;
				}
				listener.onSuccess(user);
			}
			@Override
			public void onDownloadFailure(Throwable throwable, JSONObject jsonObject) {
				if(jsonObject!=null) {
					Log.e(LOGTAG, jsonObject.toString());
				}
				listener.onFailure(throwable);
			}
		}));
	}

	@Override
	public String getLastCompleteUpdateString() throws SQLException {
		long lastUpdate = mUserDAO.getCurrentUser().getLastCompleteUpdate();
		if(lastUpdate <= 0) {
			return mContext.getString(R.string.navigation_click_to_update);
		}
		return String.format(mContext.getString(R.string.navigation_updated), DateHelper.getTimeAgo(lastUpdate, mContext));
	}


	@Override
	public void updateLastCompleteUpdate() throws SQLException {
		User currentUser = mUserDAO.getCurrentUser();
		currentUser.setLastCompleteUpdate(DateHelper.getCurrentTime());
		mUserDAO.update(currentUser);
		mUser = currentUser;
	}

	@Override
	public void setFirstTimeUserOnDevice(boolean b) throws SQLException, UserNotFoundException {
		mUser = getCurrentUser();
		if(mUser != null) {
			mUser.setFirstTimeUserOnDevice(b);
			mUserDAO.update(mUser);
		} else {
			throw new UserNotFoundException("Current User not set");
		}
	}

	@Override
	public void register(final User user, final ServiceResponseListener listener) {
		try {
			Log.d(LOGTAG, "Attemting registration of user "+user.getUsername()+ " " + user.getPassword());
			StringEntity entity = new StringEntity(new Gson().toJson(new RequestAccountCreateDTO(user)));
			RestClient.postSyncWithJSON(mContext, RestURLs.ACCOUNT_CREATE, entity, new DefaultRequestsHandler(new RestResponseListener() {
				@Override public void onProgress(int bytesWritten, int totalSize) { /* not necessary */ }

				@Override
				public void onDownloadSuccess(int statusCode, Header[] headers, String jsonStr) {
					ResponseSuccessDTO sucessDTO = new Gson().fromJson(jsonStr, new TypeToken<ResponseSuccessDTO>() {}.getType());
					if(sucessDTO.getStatus().equals("success")) {
						try {
							mUser = mUserDAO.setCurrentUser(user);
							listener.onSuccess(mUser);
						} catch (SQLException e) {
							onDownloadFailure(e, null);
						}
					}
				}
				@Override
				public void onDownloadFailure(Throwable throwable, JSONObject jsonObject) {
					// MOCK END
					if(jsonObject!=null) {
						Log.e(LOGTAG, jsonObject.toString());
						ResponseFailureDTO failureDTO = new Gson().fromJson(jsonObject.toString(), new TypeToken<ResponseFailureDTO>() {}.getType());
						// MOCK BEGIN
						boolean isMockCase = false;
						if(user.getUsername().equals("tvtrackr") && !user.getEmail().equals("wrong@b.com")) {
//							try {
//								try {
//									user.setPassword("pepi");
//								} catch (UnsupportedEncodingException e) { }
//								mUser = mUserDAO.setCurrentUser(user);
								listener.onSuccess(user);
//							} catch (SQLException e) {
//								listener.onFailure(e);
//							}
							return;
						} else if(user.getUsername().equals("wrong")) {
							failureDTO.setError("wrong is already a registered username");
							isMockCase = true;
						} else if(user.getEmail().equals("wrong@b.com")) {
							failureDTO.setError("wrong@b.com is already a registered e-mail");
							isMockCase = true;
						} 
						if(!isMockCase){
							listener.onFailure(new UsernameAlreadyExistsException("MOCK"));
							return;
						}
						// MOCK END

						if(failureDTO.getError().endsWith("username")) {
							listener.onFailure(new UsernameAlreadyExistsException(failureDTO.getError()));
						} else {
							listener.onFailure(new EmailAlreadyExistsException(failureDTO.getError()));
						}
						return;
					}
					listener.onFailure(throwable);
				}
			}));
		} catch (UnsupportedEncodingException e) {
			listener.onFailure(e);
		}

	}


	@Override
	public RequestParams getCurUserRequestParams() throws SQLException {
		return this.getUserRequestParams(getCurrentUser());
	}

	private RequestParams getUserRequestParams(User u) {
		RequestParams requestParams = new RequestParams();
		requestParams.put("username", u.getUsername());
		requestParams.put("password", u.getPassword());
		return requestParams;
	}
	
	@Override
	public void defaultOnDownloadFailure(Throwable throwable, JSONObject jsonObject, ServiceResponseListener listener) {
		if(jsonObject != null) {
			Log.e(LOGTAG, jsonObject.toString());
		}
		listener.onFailure(throwable);
	}

}
