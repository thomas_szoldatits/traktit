package com.szoldapps.tvtrackr.service.impl;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.http.Header;
import org.apache.http.client.HttpResponseException;
import org.apache.http.entity.StringEntity;
import org.json.JSONObject;

import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.szoldapps.tvtrackr.db.dao.ActorDAO;
import com.szoldapps.tvtrackr.db.dao.ActorShowDAO;
import com.szoldapps.tvtrackr.db.dao.EpisodeDAO;
import com.szoldapps.tvtrackr.db.dao.EpisodeUserDAO;
import com.szoldapps.tvtrackr.db.dao.GenreDAO;
import com.szoldapps.tvtrackr.db.dao.GenreShowDAO;
import com.szoldapps.tvtrackr.db.dao.RatingDAO;
import com.szoldapps.tvtrackr.db.dao.SeasonDAO;
import com.szoldapps.tvtrackr.db.dao.ShowDAO;
import com.szoldapps.tvtrackr.db.dao.ShowUserDAO;
import com.szoldapps.tvtrackr.db.manager.DBHelper;
import com.szoldapps.tvtrackr.db.manager.DBManager;
import com.szoldapps.tvtrackr.db.model.Episode;
import com.szoldapps.tvtrackr.db.model.OfflineShowToUnfollow;
import com.szoldapps.tvtrackr.db.model.Rating;
import com.szoldapps.tvtrackr.db.model.Season;
import com.szoldapps.tvtrackr.db.model.Show;
import com.szoldapps.tvtrackr.db.model.Show.ShowDownloadStatus;
import com.szoldapps.tvtrackr.db.model.ShowUser;
import com.szoldapps.tvtrackr.dto.internal.InternalProgressDTO;
import com.szoldapps.tvtrackr.dto.internal.InternalShowDownloadResponseDTO;
import com.szoldapps.tvtrackr.dto.request.RequestMarkAsSeenDTO;
import com.szoldapps.tvtrackr.dto.request.RequestOfflineShowToUnfollowDTO;
import com.szoldapps.tvtrackr.dto.request.RequestRateShowEpisodeDTO;
import com.szoldapps.tvtrackr.dto.request.RequestRecommendationsDismissDTO;
import com.szoldapps.tvtrackr.dto.request.RequestRecommendationsShowsDTO;
import com.szoldapps.tvtrackr.dto.request.RequestShowDTO;
import com.szoldapps.tvtrackr.dto.request.RequestShowWatchlistDTO;
import com.szoldapps.tvtrackr.dto.response.InWatchlistShowDTO;
import com.szoldapps.tvtrackr.dto.response.ResponseGenreDTO;
import com.szoldapps.tvtrackr.dto.response.ResponseRateShowEpisodeDTO;
import com.szoldapps.tvtrackr.dto.response.ResponseStatsDTO;
import com.szoldapps.tvtrackr.dto.show.CompleteShowDTO;
import com.szoldapps.tvtrackr.dto.show.LibraryShowDTO;
import com.szoldapps.tvtrackr.dto.show.MediumShowDTO;
import com.szoldapps.tvtrackr.dto.show.MinShowDTO;
import com.szoldapps.tvtrackr.dto.show.RecommendationShowDTO;
import com.szoldapps.tvtrackr.dto.show.SearchShowDTO;
import com.szoldapps.tvtrackr.dto.show.ShowSummariesDTO;
import com.szoldapps.tvtrackr.dto.show.TrendingShowDTO;
import com.szoldapps.tvtrackr.dto.show.WatchlistShowDTO;
import com.szoldapps.tvtrackr.exception.WrongObjectReturnedException;
import com.szoldapps.tvtrackr.helper.DateHelper;
import com.szoldapps.tvtrackr.helper.DialogHelper;
import com.szoldapps.tvtrackr.helper.LogCatHelper;
import com.szoldapps.tvtrackr.helper.DateHelper.DateHelperFormat;
import com.szoldapps.tvtrackr.service.ShowService;
import com.szoldapps.tvtrackr.service.UserService;
import com.szoldapps.tvtrackr.service.listener.ServiceResponseListener;
import com.szoldapps.tvtrackr.traktAccess.DefaultRequestsHandler;
import com.szoldapps.tvtrackr.traktAccess.RestClient;
import com.szoldapps.tvtrackr.traktAccess.RestResponseListener;
import com.szoldapps.tvtrackr.traktAccess.RestClient.RestURLs;

public final class ShowServiceImpl implements ShowService {

	public static final String LOGTAG = LogCatHelper.getAppTag()+ShowServiceImpl.class.getSimpleName().toString();

	private static ShowService sShowService;

	private Context mContext;
	private UserService mUserService;

	private DBHelper mDbHelper;
	private ShowDAO mShowDAO;
	private GenreDAO mGenreDAO;
	private GenreShowDAO mGenreShowDAO;
	private ActorDAO mActorDAO;
	private ActorShowDAO mActorShowDAO;
	private SeasonDAO mSeasonDAO;
	private RatingDAO mRatingDAO;
	private EpisodeDAO mEpisodeDAO;
	private EpisodeUserDAO mEpisodeUserDAO;
	private ShowUserDAO mShowUserDAO;

	private Gson mGson;

	protected Boolean mSuccess;


	public static ShowService getService(final Context context) throws SQLException {
		if(sShowService == null) {
			sShowService = new ShowServiceImpl(context);
		}
		return sShowService;
	}

	private ShowServiceImpl(final Context context) throws SQLException {
		mContext = context;
		mUserService = UserServiceImpl.getService(context);

		mDbHelper = DBManager.getHelper(context);
		mShowDAO = mDbHelper.getShowDAO();
		mGenreDAO = mDbHelper.getGenreDAO();
		mGenreShowDAO = mDbHelper.getGenreShowDAO();
		mActorDAO = mDbHelper.getActorDAO();
		mActorShowDAO = mDbHelper.getActorShowDAO();
		mSeasonDAO = mDbHelper.getSeasonDAO();
		mRatingDAO = mDbHelper.getRatingDAO();
		mEpisodeDAO = mDbHelper.getEpisodeDAO();
		mEpisodeUserDAO = mDbHelper.getEpisodeUserDAO();
		mShowUserDAO = mDbHelper.getShowUserDAO();

		mGson = new Gson();
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// ----- Follow_Unfollow ---------------------------------------------------------------------------------------- //

	@Override
	public void uploadWatchlist(final int tvdbId, final ServiceResponseListener listener) {
		try {
			List<RequestShowDTO> shows = new ArrayList<RequestShowDTO>();
			shows.add(new RequestShowDTO(tvdbId));
			String json = mGson.toJson(new RequestShowWatchlistDTO(mUserService.getCurrentUser(), shows));
			StringEntity entity = new StringEntity(json);

			RestClient.postSyncWithJSON(mContext, RestURLs.SHOW_WATCHLIST, entity, new DefaultRequestsHandler(
					new RestResponseListener() {
						@Override 
						public void onProgress(final int bytesWritten, final int totalSize) { }

						@Override
						public void onDownloadSuccess(int statusCode, Header[] headers, final String jsonStr) {
							listener.onSuccess(true);
						}
						@Override
						public void onDownloadFailure(final Throwable throwable, final JSONObject jsonObject) {
							mUserService.defaultOnDownloadFailure(throwable, jsonObject, listener);
						}
					}));
		} catch (SQLException e) {
			listener.onFailure(e);
		} catch (UnsupportedEncodingException e) {
			listener.onFailure(e);
		}
	}

	@Override
	public void uploadUnfollow(final Show show, final ServiceResponseListener listener) {
		mThrowable = null;
		mSuccess = false;
		uploadUnwatch(show, new ServiceResponseListener() {
			@Override public void onProgress(final int bytesWritten, final int totalSize) { }

			@Override
			public void onSuccess(final Object o) {
				if(o instanceof Boolean) {
					mSuccess = (Boolean) o;
				}
			}

			@Override
			public void onFailure(final Throwable throwable) {
				mThrowable = throwable;
			}
		});
		if(mThrowable != null) {
			listener.onFailure(mThrowable);
			return;
		}
		uploadShowUnwatchlist(show.getTvdbID(), new ServiceResponseListener() {
			@Override public void onProgress(final int bytesWritten, final int totalSize) { }
			@Override
			public void onSuccess(final Object o) {
				if(o instanceof Boolean) {
					mSuccess = (Boolean) o;
				}
			}

			@Override
			public void onFailure(final Throwable throwable) {
				mThrowable = throwable;
			}
		});

		// Unfollow Show locally (no matter what happened before)
		try {
			unfollowShowInDB(show.getTvdbID());
		} catch (SQLException e) {
			listener.onFailure(e);
		}

		if(mThrowable != null) {
			listener.onFailure(mThrowable);
		} else {
			listener.onSuccess(mSuccess);
		}
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// ----- Seen_Unseen -------------------------------------------------------------------------------------------- //

	@Override
	public void uploadShowSeenOrUnseen(final boolean markAsSeen, final Show show, 
			final ServiceResponseListener listener) {
		try {
			mThrowable = null;
			mSuccess = null;
			if(show == null) { return; }
			StringEntity entity = null;
			String url = "";
			RequestMarkAsSeenDTO dto = new RequestMarkAsSeenDTO(mUserService.getCurrentUser(), show);
			if(markAsSeen) {	// if Show should be marked as seen, show/seen can be used
				entity = new StringEntity(mGson.toJson(dto));
				url = RestURLs.SHOW_SEEN;
			} else {	// if Show should be marked as UNseen, show/episode/unseen has to be used
				dto.addEpisodesOfShow(show, DateHelper.getDateStr(new Date(), DateHelperFormat.DATE_FOR_TRAKT, mContext));
				entity = new StringEntity(mGson.toJson(dto));
				url = RestURLs.SHOW_EPISODE_UNSEEN;
			}

			RestClient.postSyncWithJSON(mContext, url, entity, new DefaultRequestsHandler(new RestResponseListener() {
				@Override 
				public void onProgress(final int bytesWritten, final int totalSize) { /* not needed */}

				@Override
				public void onDownloadSuccess(final int statusCode, final Header[] headers, final String jsonStr) {
					try {
						// if Episodes were marked as seen, ShowUser and isn't already followed by currentUser
						// relation is created/updated
						if(markAsSeen && !isShowFollowedByCurrentUser(show.getTvdbID())) {	
							createShowUserAndEpisodeUserData(show, false);
						}
						mEpisodeUserDAO.markShowAsSeenIfAiredOrUnseen(markAsSeen, false, show, mUserService.getCurrentUser());
						mSuccess = true;
					} catch (SQLException e) {
						onDownloadFailure(e, null);
						return;
					}
				}

				@Override
				public void onDownloadFailure(final Throwable throwable, final JSONObject jsonObject) {
					// if no Internet -> save for later
					if(DialogHelper.isInternetError(throwable)) {
						try {
							mEpisodeUserDAO.markShowAsSeenIfAiredOrUnseen(markAsSeen, true, show, mUserService.getCurrentUser());
							mSuccess = false;
						} catch (SQLException e) {
							mThrowable = e;
							return;
						}
					} else {
						mUserService.defaultOnDownloadFailure(throwable, jsonObject, listener);
					}
				}
			}));

			if(mThrowable != null) { 
				listener.onFailure(mThrowable);
				return;
			}

			// Check if Show has to be watchlisted
			// (only if online, this is otherwise done in DonwloadService.offlineUploads())
			if(mSuccess) {
				mThrowable = watchlistShowIfNoMoreSeenEpisodesLeft(show, markAsSeen);
			}
			
			if(mThrowable != null) { 
				listener.onFailure(mThrowable);
			} else {
				listener.onSuccess(mSuccess);
			}

		} catch (SQLException e) {
			listener.onFailure(e);
		} catch (UnsupportedEncodingException e) {
			listener.onFailure(e);
		}
	}


	@Override
	public void uploadSeasonSeenOrUnseen(final boolean markAsSeen, final Season season, 
			final ServiceResponseListener listener) {
		try {
			mThrowable = null;
			mSuccess = null;
			final Show show = season.getShow();
			if(show == null) { return; }
			StringEntity entity = null;
			String url = "";
			RequestMarkAsSeenDTO dto = 
					new RequestMarkAsSeenDTO(mUserService.getCurrentUser(), show, season.getSeasonNr());
			if(markAsSeen) {	// if Season should be marked as seen, show/season/seen can be used
				entity = new StringEntity(mGson.toJson(dto));
				url = RestURLs.SHOW_SEASON_SEEN;
			} else {	// if Show should be marked as UNseen, show/episode/unseen has to be used
				dto.addEpisodesOfSeason(season, 
						DateHelper.getDateStr(new Date(), DateHelperFormat.DATE_FOR_TRAKT, mContext));
				entity = new StringEntity(mGson.toJson(dto));
				url = RestURLs.SHOW_EPISODE_UNSEEN;
			}

			RestClient.postSyncWithJSON(mContext, url, entity, new DefaultRequestsHandler(new RestResponseListener() {
				@Override 
				public void onProgress(final int bytesWritten, final int totalSize) { /* not needed */}

				@Override
				public void onDownloadSuccess(final int statusCode, final Header[] headers, final String jsonStr) {
					try {
						// if Episodes were marked as seen, ShowUser and isn't already followed by currentUser
						// relation is created/updated
						if(markAsSeen && !isShowFollowedByCurrentUser(show.getTvdbID())) {	
							createShowUserAndEpisodeUserData(show, false);
						}
						mEpisodeUserDAO.markSeasonAsSeenIfAiredOrUnseen(markAsSeen, false, season, 
								mUserService.getCurrentUser());
						mSuccess = true;
					} catch (SQLException e) {
						onDownloadFailure(e, null);
						return;
					}
				}

				@Override
				public void onDownloadFailure(final Throwable throwable, final JSONObject jsonObject) {
					// if no Internet -> save for later
					if(DialogHelper.isInternetError(throwable)) {
						try {
							mEpisodeUserDAO.markSeasonAsSeenIfAiredOrUnseen(markAsSeen, true, season, 
									mUserService.getCurrentUser());
							mSuccess = false;
						} catch (SQLException e) {
							mThrowable = e;
							return;
						}
					} else {
						if(jsonObject != null) {
							Log.e(LOGTAG, jsonObject.toString());
						}
						mThrowable = throwable;
					}
				}
			}));

			if(mThrowable != null) { 
				listener.onFailure(mThrowable);
				return;
			}

			// Check if Show has to be watchlisted
			// (only if online, this is otherwise done in DonwloadService.offlineUploads())
			if(mSuccess) {
				mThrowable = watchlistShowIfNoMoreSeenEpisodesLeft(show, markAsSeen);
			}
			
			if(mThrowable != null) { 
				listener.onFailure(mThrowable);
			} else {
				listener.onSuccess(mSuccess);
			}
		} catch (SQLException e) {
			listener.onFailure(e);
		} catch (UnsupportedEncodingException e) {
			listener.onFailure(e);
		}
	}

	@Override
	public void uploadEpisodeSeenOrUnseen(final boolean markAsSeen, final Episode episode, final ServiceResponseListener listener) {
		List<Episode> episodes = new ArrayList<Episode>();
		episodes.add(episode);
		uploadEpisodesSeenOrUnseen(markAsSeen, episode.getSeason().getShow(), episodes, listener);
	}

	@Override
	public void uploadEpisodesSeenOrUnseen(final boolean markAsSeen, Show show, final List<Episode> episodes, final ServiceResponseListener listener) {
		try {
			mThrowable = null;
			mSuccess = null;
			String url = markAsSeen ? RestURLs.SHOW_EPISODE_SEEN : RestURLs.SHOW_EPISODE_UNSEEN;

			// JSON
			RequestMarkAsSeenDTO dto = new RequestMarkAsSeenDTO(mUserService.getCurrentUser(), show);
			dto.addEpisodes(episodes, DateHelper.getDateStr(new Date(), DateHelperFormat.DATE_FOR_TRAKT, mContext));
			StringEntity entity = new StringEntity(mGson.toJson(dto));

			// REST call
			RestClient.postSyncWithJSON(mContext, url, entity, new DefaultRequestsHandler(new RestResponseListener() {
				@Override public void onProgress(int bytesWritten, int totalSize) { /* not needed */}
				@Override
				public void onDownloadSuccess(int statusCode, Header[] headers, String jsonStr) {
					try {
						// persist in local DB
						mEpisodeUserDAO.markEpisodesAsSeenOrUnseen(episodes, markAsSeen, false, mUserService.getCurrentUser());
						mSuccess = true;
					} catch (SQLException e) {
						onDownloadFailure(e, null);
						return;
					}
				}

				@Override
				public void onDownloadFailure(Throwable throwable, JSONObject jsonObject) {
					// if no Internet -> save for later
					if(DialogHelper.isInternetError(throwable)) {
						try {
							mEpisodeUserDAO.markEpisodesAsSeenOrUnseen(episodes, markAsSeen, true, mUserService.getCurrentUser());
							mSuccess = false;
						} catch (SQLException e) {
							mThrowable = e;
							return;
						}
					} else {
						if(jsonObject != null) {
							Log.e(LOGTAG, jsonObject.toString());
						}
						mThrowable = throwable;
					}
				}
			}));

			if(mThrowable != null) { 
				listener.onFailure(mThrowable);
				return;
			}

			// Check if Show has to be watchlisted 
			// (only if online, this is otherwise done in DonwloadService.offlineUploads())
			if(mSuccess) {
				mThrowable = watchlistShowIfNoMoreSeenEpisodesLeft(show, markAsSeen);
			}
			
			if(mThrowable != null) { 
				listener.onFailure(mThrowable);
			} else {
				listener.onSuccess(mSuccess);
			}
		} catch (SQLException e) {
			listener.onFailure(e);
		} catch (UnsupportedEncodingException e) {
			listener.onFailure(e);
		}
	}

	/**
	 * Checks whether Show was automatically unfollowed by trakt (as there are no more seen Episodes left)
	 * If TRUE -> Show is watchlisted. <br>
	 * Sets mThrowable = null, ONLY works if markAsSeen = TRUE, otherwise it wouldn't make sense to call it anyway.
	 * Used by SEEN_UNSEEN Methods
	 * 
	 * @param show
	 * @param markAsSeen 
	 * @return Throwable	if null -> everything OK, else -> the throwable causing the problem
	 * @throws SQLException 
	 */
	private Throwable watchlistShowIfNoMoreSeenEpisodesLeft(Show show, boolean markAsSeen) throws SQLException {
		if(!markAsSeen) {
			mThrowable = null;
			long seenEpisodesCount = mEpisodeDAO.getEpisodeSeenCountOfShow(show, mUserService.getCurrentUser(), mEpisodeUserDAO);
			if(seenEpisodesCount == 0) {
				sShowService.uploadWatchlist(show.getTvdbID(), new ServiceResponseListener() {
					@Override public void onProgress(int bytesWritten, int totalSize) { }

					@Override
					public void onSuccess(Object o) {
						if(!(o instanceof Boolean)) {
							mThrowable = new WrongObjectReturnedException(Boolean.class);
						}
					}

					@Override
					public void onFailure(Throwable throwable) {
						mThrowable = throwable;
					}
				});
			}
			if(mThrowable != null) {
				return mThrowable;
			}
		}
		return null;
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// ----- PROGRESS ----------------------------------------------------------------------------------------------- //

	@Override
	public InternalProgressDTO getProgressOfAiredEpisodes(final Show show) throws SQLException {
		long watchedEpisodeCount = mEpisodeDAO.getSeenAiredEpisodeCountWithoutSpecialSeason(show, mUserService.getCurrentUser(), mSeasonDAO, mEpisodeUserDAO);
		long airedEpisodeCount = mEpisodeDAO.getAiredEpisodeCountWithoutSpecialSeason(show, mSeasonDAO);
		long episodeCount = mEpisodeDAO.getEpisodeCountWithoutSpecialSeason(show, mSeasonDAO);
		return new InternalProgressDTO(watchedEpisodeCount, airedEpisodeCount, episodeCount, show.getRuntime());
	}

	@Override
	public InternalProgressDTO getProgressOfAiredEpisodes(final Season season) throws SQLException {
		long watchedEpisodeCount = mEpisodeDAO.getSeenAiredEpisodeCount(season, mUserService.getCurrentUser(), mSeasonDAO, mEpisodeUserDAO);
		long airedEpisodeCount = mEpisodeDAO.getAiredEpisodeCount(season, mSeasonDAO);
		long episodeCount = season.getEpisodeCount();
		return new InternalProgressDTO(
				watchedEpisodeCount, airedEpisodeCount, episodeCount, season.getShow().getRuntime());
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


	@Override
	public List<Show> getAllShowsOfUser() throws SQLException {
		return mShowDAO.getAllShowsOfUser(mUserService.getCurrentUser(), mShowUserDAO);
	}


	private void unfollowShowInDB(final int tvdbId) throws SQLException {
		mShowUserDAO.unfollowShowInDB(mUserService.getCurrentUser(), tvdbId, mEpisodeUserDAO, mEpisodeDAO);
	}



	@Override
	public void uploadUnfollow(final OfflineShowToUnfollow oShow, final ServiceResponseListener listener) {
		mThrowable = null;
		mSuccess = false;
		uploadUnseen(oShow, new ServiceResponseListener() {
			@Override public void onProgress(final int bytesWritten, final int totalSize) { }

			@Override
			public void onSuccess(final Object o) {
				if(o instanceof Boolean) {
					mSuccess = (Boolean) o;
				}
			}

			@Override
			public void onFailure(final Throwable throwable) {
				mThrowable = throwable;
			}
		});
		if(mThrowable != null) {
			listener.onFailure(mThrowable);
			return;
		}
		uploadShowUnwatchlist(oShow.getTvdbID(), new ServiceResponseListener() {
			@Override public void onProgress(final int bytesWritten, final int totalSize) { }
			@Override
			public void onSuccess(final Object o) {
				if(o instanceof Boolean) {
					mSuccess = (Boolean) o;
				}
			}

			@Override
			public void onFailure(final Throwable throwable) {
				mThrowable = throwable;
			}
		});
		if(mThrowable != null) {
			listener.onFailure(mThrowable);
		} else {
			listener.onSuccess(mSuccess);
		}
	}

	/**
	 * <p>Marks every Episode of {@link Show} with tvdbId <code>oShow.getTvdbId()</code> as unseen on Trakt<br><br>
	 * Returns (via <code>listener</code>) either</p>
	 * {@link ServiceResponseListener#onSuccess(Object)}
	 * <ul>
	 * 		<li>{@link Boolean#TRUE}</li>
	 * </ul>
	 * or {@link ServiceResponseListener#onFailure(Throwable)}
	 * <ul>
	 * 		<li>{@link SQLException} ... on any SQL Problem (e.g. Access-Problem)</li>
	 * 		<li>{@link HttpResponseException} ... Wrong Login Credentials</li>
	 * 		<li>{@link IOException} ... No Internet Access (or URL wrong)</li>
	 * 		<li>{@link UnsupportedEncodingException} ... Problem during {@link StringEntity} creation</li>
	 * </ul>
	 * 
	 * @param oShow
	 * @param listener
	 */
	private void uploadUnseen(final OfflineShowToUnfollow oShow, final ServiceResponseListener listener) {
		try {
			RequestOfflineShowToUnfollowDTO dto = new RequestOfflineShowToUnfollowDTO(mUserService.getCurrentUser(), oShow);
			StringEntity entity = new StringEntity(mGson.toJson(dto));
			uploadUnseen(entity, listener);
		} catch (UnsupportedEncodingException e) {
			listener.onFailure(e);
		} catch (SQLException e) {
			listener.onFailure(e);
		}
	}

	/**
	 * <p>Marks every Episode of <code>show</code> as unseen on Trakt<br><br>
	 * Returns (via <code>listener</code>) either</p>
	 * {@link ServiceResponseListener#onSuccess(Object)}
	 * <ul>
	 * 		<li>{@link Boolean#TRUE}</li>
	 * </ul>
	 * or {@link ServiceResponseListener#onFailure(Throwable)}
	 * <ul>
	 * 		<li>{@link SQLException} ... on any SQL Problem (e.g. Access-Problem)</li>
	 * 		<li>{@link HttpResponseException} ... Wrong Login Credentials</li>
	 * 		<li>{@link IOException} ... No Internet Access (or URL wrong)</li>
	 * 		<li>{@link UnsupportedEncodingException} ... Problem during {@link StringEntity} creation</li>
	 * </ul>
	 * 
	 * @param show
	 * @param listener
	 */
	private void uploadUnwatch(final Show show, final ServiceResponseListener listener) {
		try {
			mShowDAO.refresh(show);
		} catch (SQLException e) {
			listener.onFailure(e);
			return;
		}
		final List<Episode> episodes = new ArrayList<Episode>();
		for (Season season : show.getSeasons()) {
			episodes.addAll(season.getEpisodes());
		}
		if(episodes.isEmpty()) { return; }
		try {
			final Show mShow = show;
			RequestMarkAsSeenDTO dto = new RequestMarkAsSeenDTO(mUserService.getCurrentUser(), mShow);
			dto.addEpisodes(episodes, DateHelper.getDateStr(new Date(), DateHelperFormat.DATE_FOR_TRAKT, mContext));
			StringEntity entity = new StringEntity(mGson.toJson(dto));
			uploadUnseen(entity, listener);

		} catch (UnsupportedEncodingException e) {
			listener.onFailure(e);
		} catch (SQLException e) {
			listener.onFailure(e);
		}
	}

	private void uploadUnseen(final StringEntity entity, final ServiceResponseListener listener) {
		RestClient.postSyncWithJSON(mContext, RestURLs.SHOW_EPISODE_UNSEEN, entity, new DefaultRequestsHandler(new RestResponseListener() {
			@Override public void onProgress(final int bytesWritten, final int totalSize) { /* not needed */}
			@Override
			public void onDownloadSuccess(final int statusCode, final Header[] headers, final String jsonStr) {
				listener.onSuccess(true);
			}

			@Override
			public void onDownloadFailure(final Throwable throwable, final JSONObject jsonObject) {
				if(jsonObject != null) {
					Log.e(LOGTAG, jsonObject.toString());
				}
				listener.onFailure(throwable);
			}
		}));
	}

	/**
	 * <p>Removes <code>show</code> from Watchlist on Trakt<br><br>
	 * Returns (via <code>listener</code>) either</p>
	 * {@link ServiceResponseListener#onSuccess(Object)}
	 * <ul>
	 * 		<li>{@link Boolean#TRUE}</li>
	 * </ul>
	 * or {@link ServiceResponseListener#onFailure(Throwable)}
	 * <ul>
	 * 		<li>{@link SQLException} ... on any SQL Problem (e.g. Access-Problem)</li>
	 * 		<li>{@link HttpResponseException} ... Wrong Login Credentials</li>
	 * 		<li>{@link IOException} ... No Internet Access (or URL wrong)</li>
	 * 		<li>{@link UnsupportedEncodingException} ... Problem during {@link StringEntity} creation</li>
	 * </ul>
	 * 
	 * @param show
	 * @param listener
	 */
	private void uploadShowUnwatchlist(final int tvdbId, final ServiceResponseListener listener) {
		try {
			List<RequestShowDTO> shows = new ArrayList<RequestShowDTO>();
			shows.add(new RequestShowDTO(tvdbId));
			RequestShowWatchlistDTO watchlistDTO = new RequestShowWatchlistDTO(mUserService.getCurrentUser(), shows);
			StringEntity entity = new StringEntity(mGson.toJson(watchlistDTO));

			RestClient.postSyncWithJSON(mContext, RestURLs.SHOW_UNWATCHLIST, entity, new DefaultRequestsHandler(new RestResponseListener() {
				@Override public void onProgress(final int bytesWritten, final int totalSize) { /* nothing to do */ }
				@Override
				public void onDownloadSuccess(final int statusCode, final Header[] headers, final String jsonStr) {
					listener.onSuccess(true);
				}

				@Override
				public void onDownloadFailure(final Throwable throwable, final JSONObject jsonObject) {
					if(jsonObject != null) {
						Log.e(LOGTAG, jsonObject.toString());
					}
					listener.onFailure(throwable);
				}
			}));
		} catch (UnsupportedEncodingException e) {
			listener.onFailure(e);
		} catch (SQLException e) {
			listener.onFailure(e);
		}
	}

	@Override
	public void deleteShowForUser(final int tvdbId, final boolean preventCompleteRemoval) throws SQLException {
		Show show = mShowDAO.queryForId(tvdbId);
		// delete ShowUser (if it exists) & delete All EpisodeUsers (if they exist)
		mShowUserDAO.unfollowShowInDB(mUserService.getCurrentUser(), tvdbId, mEpisodeUserDAO, mEpisodeDAO);

		// check if Show is followed by other users
		if(mShowUserDAO.hasOtherUsers(tvdbId)) {
			Log.i(LOGTAG, "Show ("+tvdbId+") cannnot be removed completely, other users still follow it");
			return;
		} else if(preventCompleteRemoval) {
			Log.i(LOGTAG, "Show ("+tvdbId+") cannnot be removed completely, preventCompleteRemoval was set TRUE");
			return;
		} else {
			// delete All User Ratings (if they exist, this is done here, because ratings can exist without a ShowUser relation)
			mRatingDAO.deleteForUser(mUserService.getCurrentUser(), tvdbId);

			// delete All GenreShows for Show
			mGenreShowDAO.deleteForShow(tvdbId);

			// delete unused Genres
			mGenreDAO.deleteUnused(mGenreShowDAO);

			mActorShowDAO.deleteForShow(tvdbId);

			// delete unused Actors
			mActorDAO.deleteUnused(mActorShowDAO);

			// delete all Episodes of Show
			mEpisodeDAO.deleteAllEpisodesOfShow(tvdbId);

			// delete all Seasons of Show
			mSeasonDAO.deleteForShow(tvdbId);

			// delete Show
			mShowDAO.delete(show);
			Log.i(LOGTAG, "Show ("+tvdbId+") DELETED");
		}
	}

	// ---------- SEARCH -----------------------------------------------------------------------------------------------------------------------------------------

	@Override
	public void search(final String query, final int limit, final ServiceResponseListener listener) {
		RestClient.postSync(RestURLs.SEARCH_SHOW + query.replace(" ", "+") + "/"+limit, null, new DefaultRequestsHandler(new RestResponseListener() {
			@Override public void onProgress(final int bytesWritten, final int totalSize) { /* not needed */ }

			@Override
			public void onDownloadSuccess(final int statusCode, final Header[] headers, final String jsonStr) {
				List<SearchShowDTO> searchResults = mGson.fromJson(jsonStr, new TypeToken<List<SearchShowDTO>>(){}.getType());
				try {
					for (SearchShowDTO sDTO : searchResults) {
						ShowUser su = mShowUserDAO.getShowUser(mUserService.getCurrentUser(), sDTO.getTvdb_id());
						if(su != null) {
							sDTO.setFollowedByUser(true);
						}
					}
					listener.onSuccess(searchResults);
				} catch (SQLException e) {
					onDownloadFailure(e, null);
				}
			}

			@Override
			public void onDownloadFailure(final Throwable throwable, final JSONObject jsonObject) {
				if(jsonObject != null) {
					Log.e(LOGTAG, jsonObject.toString());
				}
				listener.onFailure(throwable);
			}
		}));
	}

	@Override
	public int getPositionByEpisodeId(final int episodeId, final int tvdbId) throws SQLException {
		return mEpisodeDAO.getPositionByEpisodeId(episodeId, tvdbId, mSeasonDAO);
	}

	@Override
	public void getGenreStringList(final ServiceResponseListener listener) {
		RestClient.postSync(RestURLs.GENRES_SHOWS, null, new DefaultRequestsHandler(new RestResponseListener() {
			@Override public void onProgress(final int bytesWritten, final int totalSize) { /* nothing to do */ }
			@Override
			public void onDownloadSuccess(final int statusCode, final Header[] headers, final String jsonStr) {
				List<ResponseGenreDTO> genreDTOs = mGson.fromJson(jsonStr, new TypeToken<List<ResponseGenreDTO>>(){}.getType());
				List<String> genres = new ArrayList<String>();
				for (ResponseGenreDTO genreDTO : genreDTOs) {
					genres.add(genreDTO.getName());
				}
				listener.onSuccess(genres);
			}

			@Override
			public void onDownloadFailure(final Throwable throwable, final JSONObject jsonObject) {
				if(jsonObject != null) {
					Log.e(LOGTAG, jsonObject.toString());
				}
				listener.onFailure(throwable);
			}
		}));
	}

	@Override
	public void dismissRecommendation(final int tvdbId, final ServiceResponseListener listener) throws UnsupportedEncodingException {
		try {
			RequestRecommendationsDismissDTO reqDTO = new RequestRecommendationsDismissDTO(mUserService.getCurrentUser(), tvdbId);
			StringEntity entity = new StringEntity(mGson.toJson(reqDTO));
			RestClient.postSyncWithJSON(mContext, RestURLs.RECOMMENDATIONS_SHOW_DISMISS, entity, new DefaultRequestsHandler(new RestResponseListener() {
				@Override public void onProgress(final int bytesWritten, final int totalSize) { /* nothing to do */ }
				@Override
				public void onDownloadSuccess(final int statusCode, final Header[] headers, final String jsonStr) {
					listener.onSuccess(jsonStr);
				}

				@Override
				public void onDownloadFailure(final Throwable throwable, final JSONObject jsonObject) {
					if(jsonObject != null) {
						Log.e(LOGTAG, jsonObject.toString());
					}
					listener.onFailure(throwable);
				}
			}));
		} catch (SQLException e) {
			listener.onFailure(e);
		}
	}


	@Override
	public void getRecommendations(final String genre, final Integer startYear, final Integer endYear, final ServiceResponseListener listener) throws UnsupportedEncodingException {
		try {
			RequestRecommendationsShowsDTO recDTO = new RequestRecommendationsShowsDTO(mUserService.getCurrentUser(), genre, startYear, endYear);
			StringEntity entity = new StringEntity(mGson.toJson(recDTO));
			RestClient.postSyncWithJSON(mContext, RestURLs.RECOMMENDATIONS_SHOW, entity, new DefaultRequestsHandler(new RestResponseListener() {
				@Override public void onProgress(final int bytesWritten, final int totalSize) { /* nothing to do */ }
				@Override
				public void onDownloadSuccess(final int statusCode, final Header[] headers, final String jsonStr) {
					List<RecommendationShowDTO> recShowDTOs = mGson.fromJson(jsonStr, new TypeToken<List<RecommendationShowDTO>>(){}.getType());
					listener.onSuccess(recShowDTOs);
					//							for (RecommendationShowDTO show : recShowDTOs) {
					//								Log.i(LOGTAG, show.getTitle() + " " + show.getYear());
					//							}
				}

				@Override
				public void onDownloadFailure(final Throwable throwable, final JSONObject jsonObject) {
					if(jsonObject != null) {
						Log.e(LOGTAG, jsonObject.toString());
					}
					listener.onFailure(throwable);
				}
			}));
		} catch (SQLException e) {
			listener.onFailure(e);
		}
	}

	@Override
	public void getTrendingShows(final ServiceResponseListener listener) throws SQLException {
		RestClient.postSync(RestURLs.SHOWS_TRENDING, mUserService.getCurUserRequestParams(), new DefaultRequestsHandler(new RestResponseListener() {
			@Override public void onProgress(final int bytesWritten, final int totalSize) { /* nothing to do */ }
			@Override
			public void onDownloadSuccess(final int statusCode, final Header[] headers, final String jsonStr) {
				List<TrendingShowDTO> trendDTOs = mGson.fromJson(jsonStr, new TypeToken<List<TrendingShowDTO>>(){}.getType());
				listener.onSuccess(trendDTOs.subList(0, 25));
			}

			@Override
			public void onDownloadFailure(final Throwable throwable, final JSONObject jsonObject) {
				mUserService.defaultOnDownloadFailure(throwable, jsonObject, listener);
			}
		}));
	}

	@Override
	public Show getShowById(final Integer tvdbId) throws SQLException {
		return mShowDAO.queryForId(tvdbId);
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// ----- STATS -------------------------------------------------------------------------------------------------- //
	/**
	 * @category STATS 
	 */
	@Override
	public void downloadStats(final Show show, final ServiceResponseListener listener) throws SQLException {
		// get Show Stats from Trakt
		String url = RestURLs.SHOW_STATS + show.getTvdbID();
		RestClient.postSync(url, mUserService.getCurUserRequestParams(), new DefaultRequestsHandler(new RestResponseListener() {
			@Override public void onProgress(final int bytesWritten, final int totalSize) { }
			@Override
			public void onDownloadSuccess(final int statusCode, final Header[] headers, final String jsonStr) {
				ResponseStatsDTO statsDTO = mGson.fromJson(jsonStr, new TypeToken<ResponseStatsDTO>() {}.getType());
				// update Show in DB
				show.setStatsPlays(statsDTO.getPlays());
				show.setStatsWatchers(statsDTO.getWatchers());
				show.setStatsCollected(statsDTO.getCollection().getAll());
				show.setStatsLists(statsDTO.getLists().getAll());
				try {
					mShowDAO.update(show);
				} catch (SQLException e) {
					onDownloadFailure(e, null);
				}
				listener.onSuccess(statsDTO);
			}

			@Override
			public void onDownloadFailure(final Throwable throwable, final JSONObject jsonObject) {
				mUserService.defaultOnDownloadFailure(throwable, jsonObject, listener);
			}
		}));
	}

	/** RATING **/

	@Override
	public Rating getRating(final Show show) throws SQLException {
		return mRatingDAO.getShowRating(show, mUserService.getCurrentUser());
	}

	@Override
	public void uploadRating(final Show show, final int rating, final ServiceResponseListener listener) throws UnsupportedEncodingException {
		try {
			StringEntity entity = new StringEntity(mGson.toJson(new RequestRateShowEpisodeDTO(mUserService.getCurrentUser(), show, rating)));

			RestClient.postSyncWithJSON(mContext, RestURLs.RATE_SHOW, entity, new DefaultRequestsHandler(new RestResponseListener() {
				@Override public void onProgress(final int bytesWritten, final int totalSize) { }
				@Override
				public void onDownloadSuccess(final int statusCode, final Header[] headers, final String jsonStr) {
					try {
						Type listType = new TypeToken<ResponseRateShowEpisodeDTO>() {}.getType();
						ResponseRateShowEpisodeDTO redto = mGson.fromJson(jsonStr, listType);

						// save user's rating in DB
						try {
							redto.ratingAdvanced = Integer.parseInt(redto.rating);
						} catch(NumberFormatException e) {
							redto.ratingAdvanced = 0;
						}
						saveShowRatingInDB(show, redto.ratingAdvanced);

						// save general rating in DB
						show.setRatingsPercentage(redto.ratings.getPercentage());
						show.setRatingsVotes(redto.ratings.getVotes());
						show.setRatingsLoved(redto.ratings.getLoved());
						show.setRatingsHated(redto.ratings.getHated());
						mShowDAO.update(show);

						listener.onSuccess(show);
					} catch (SQLException e) {
						listener.onFailure(e);
						return;
					}
				}

				@Override
				public void onDownloadFailure(final Throwable throwable, final JSONObject jsonObject) {
					mUserService.defaultOnDownloadFailure(throwable, jsonObject, listener);
				}
			}));
		} catch (SQLException e) {
			listener.onFailure(e);
		}
	}

	private Rating saveShowRatingInDB(final Show show, final Integer ratingAdvanced) throws SQLException {
		Rating rating = mRatingDAO.getShowRating(show, mUserService.getCurrentUser());
		if(rating == null) {
			rating = new Rating(mUserService.getCurrentUser(), show, null, null, ratingAdvanced);
			mRatingDAO.create(rating);
		} else {
			rating.setRatingAdvanced(ratingAdvanced);
			mRatingDAO.update(rating);
		}
		mRatingDAO.refresh(rating);
		return rating;
	}

	private Throwable mThrowable;

	@Override
	public void downloadShowlistFromTraktAndSaveToDB(final ServiceResponseListener listener) {
		try {
			mThrowable = null;
			final List<MinShowDTO> minShowDTOs = new ArrayList<MinShowDTO>();
			// User Library Shows
			String relativeUrl = RestURLs.USER_LIBRARY_SHOWS_WATCHED + mUserService.getCurrentUser().getUsername();
			RestClient.postSync(relativeUrl , mUserService.getCurUserRequestParams(), new DefaultRequestsHandler(new RestResponseListener() {
				@Override public void onProgress(final int bytesWritten, final int totalSize) { }
				@Override
				public void onDownloadSuccess(final int statusCode, final Header[] headers, final String jsonStr) {
					List<LibraryShowDTO> libMinDTOs = mGson.fromJson(jsonStr, new TypeToken<List<LibraryShowDTO>>(){}.getType());
					minShowDTOs.addAll(libMinDTOs);
				}
				@Override
				public void onDownloadFailure(final Throwable throwable, final JSONObject jsonObject) {
					if(jsonObject != null) {
						Log.e(LOGTAG, jsonObject.toString());
					}
					listener.onFailure(throwable);
					mThrowable = throwable;
				}
			}));

			if(mThrowable != null) { return; }

			// User ShowUser Shows
			relativeUrl = RestURLs.USER_WATCHLIST_SHOWS + mUserService.getCurrentUser().getUsername();
			RestClient.postSync(relativeUrl , mUserService.getCurUserRequestParams(), new DefaultRequestsHandler(new RestResponseListener() {
				@Override public void onProgress(final int bytesWritten, final int totalSize) { }
				@Override
				public void onDownloadSuccess(final int statusCode, final Header[] headers, final String jsonStr) {
					List<WatchlistShowDTO> watchlistDTOs = mGson.fromJson(jsonStr, new TypeToken<List<WatchlistShowDTO>>(){}.getType());
					// As trakt allows a Show to be in the Library and in the Watchlist at the same time
					// Watchlist Shows are only added to minShowDTOs if they are NOT already in.
					for (MinShowDTO watchlistDTO : watchlistDTOs) {
						if(!minShowDTOs.contains(watchlistDTO)) {
							minShowDTOs.add(watchlistDTO);
						}
					}
				}
				@Override
				public void onDownloadFailure(final Throwable throwable, final JSONObject jsonObject) {
					if(jsonObject != null) {
						Log.e(LOGTAG, jsonObject.toString());
					}
					listener.onFailure(throwable);
					mThrowable = throwable;
				}
			}));

			if(mThrowable != null) { return; }

			List<Show> shouldBeRemoveds = new ArrayList<Show>();
			List<Show> showsInDb = mShowDAO.getAllShowsOfUser(mUserService.getCurrentUser(), mShowUserDAO);
			for (Show show : showsInDb) {
				boolean isUserShow = false;
				for (MinShowDTO minShowDTO : minShowDTOs) {
					if(show.getTvdbID().equals(minShowDTO.getTvdb_id())) {
						isUserShow = true;
						break;
					}
				}
				if(!isUserShow) {
					shouldBeRemoveds.add(show);
				}
			}

			// remove Shows
			Log.i(LOGTAG, "--- shouldBeRemoveds (size="+shouldBeRemoveds.size()+") ---");
			if(shouldBeRemoveds.size() > 0) {
				for (Show show : shouldBeRemoveds) {
					Log.i(LOGTAG, show.getTitle());
					showsInDb.remove(show);
					deleteShowForUser(show.getTvdbID(), false);
				}
				Log.i(LOGTAG, "----------------");
			}
			//			Log.i(LOGTAG, "--- showsInDb (size="+showsInDb.size()+") ---");
			//			for (Show show : showsInDb) {
			//				Log.i(LOGTAG, show.getTitle());
			//			}
			//			Log.i(LOGTAG, "----------------");
			//			Log.i(LOGTAG, "--- traktShowDTOs (size="+minShowDTOs.size()+") ---");
			//			for (MinShowDTO showDTO : minShowDTOs) {
			//				Log.i(LOGTAG, showDTO.getTitle());
			//			}
			//			Log.i(LOGTAG, "----------------");

			try {
				mShowDAO.createOrUpdate(minShowDTOs, mUserService.getCurrentUser(), mShowUserDAO, mEpisodeUserDAO);
			} catch (SQLException e) {
				listener.onFailure(e);
			}
			listener.onSuccess(mShowDAO.getAllShowsOfUser(mUserService.getCurrentUser(), mShowUserDAO));
		} catch (SQLException e) {
			listener.onFailure(e);
		}
	}

	@Override
	public Show saveShow(final MediumShowDTO medDTO) throws SQLException {
		return mShowDAO.createOrUpdate(medDTO, mUserService.getCurrentUser(), mShowUserDAO, mEpisodeUserDAO);
	}

	@Override
	public void createShowUserAndEpisodeUserData(Show show, boolean watchlist) throws SQLException {
		mShowUserDAO.createOrUpdateShowUser(mUserService.getCurrentUser(), show, watchlist);
		mEpisodeUserDAO.createOrUpdateEpisodeUsers(mUserService.getCurrentUser(), show);
	}

	private String getShowSummariesURL(final List<Show> shows) {
		String url = RestURLs.SHOW_SUMMARIES;
		boolean first = true;
		for (Show show : shows) {
			if(first) {
				first = false;
			} else {
				url += ",";
			}
			url += show.getTvdbID();
		}
		return url;
	}

	@Override
	public void updateShowData(final List<Show> shows, final ServiceResponseListener listener) {
		if(shows == null || shows.isEmpty()) {
			return;
		}

		try {
			RestClient.postSync(getShowSummariesURL(shows), mUserService.getCurUserRequestParams(), new DefaultRequestsHandler(new RestResponseListener() {

				@Override public void onProgress(final int bytesWritten, final int totalSize) { }

				@Override
				public void onDownloadSuccess(final int statusCode, final Header[] headers, final String jsonStr) {
					List<ShowSummariesDTO> minDTOs = mGson.fromJson(jsonStr, new TypeToken<List<ShowSummariesDTO>>(){}.getType());
					List<Show> retShows = new ArrayList<Show>();
					for (Show show : shows) {
						long newLastUpdated = getLastUpdated(show.getTvdbID(), minDTOs);
						//						if(show.getTitle().equals("Breaking Bad") || show.getTitle().equals("Game of Thrones"))
						//							newLastUpdated++;
						Log.e(LOGTAG, show.getLastUpdated() + " <= " + newLastUpdated + ", " + (show.getLastUpdated() >= newLastUpdated));
						if(newLastUpdated > -1) {
							if(show.getLastUpdated() < newLastUpdated) { 	// SHOULD BE UPDATED
								Log.e(LOGTAG, show.getTitle() + " SHOULD BE UPDATED");
								show.setFullyDownloaded(false);
								retShows.add(show);
							} else {	// UP2Date no Update necessary
								Log.e(LOGTAG, show.getTitle() + " is UP2Date no Update necessary");
							}
						}
					}
					listener.onSuccess(retShows);
				}

				@Override
				public void onDownloadFailure(final Throwable throwable, final JSONObject jsonObject) {
					if(jsonObject != null) {
						Log.e(LOGTAG, jsonObject.toString());
					}
					listener.onFailure(throwable);
				}

				private long getLastUpdated(final int tvdbId, final List<ShowSummariesDTO> minDTOs) {
					for (ShowSummariesDTO minDTO : minDTOs) {
						if(minDTO.getTvdb_id() == tvdbId) {
							return minDTO.getLast_updated();
						}
					}
					return -1;
				}
			}));
		} catch (SQLException e) {
			listener.onFailure(e);
		}
	}

	@Override
	public void updateAllShowUserData(final ServiceResponseListener listener) {
		// User Library Shows
		try {
			String relativeUrl = RestURLs.USER_LIBRARY_SHOWS_WATCHED + mUserService.getCurrentUser().getUsername();
			RestClient.postSync(relativeUrl , mUserService.getCurUserRequestParams(), new DefaultRequestsHandler(new RestResponseListener() {
				@Override public void onProgress(final int bytesWritten, final int totalSize) { }
				@Override
				public void onDownloadSuccess(final int statusCode, final Header[] headers, final String jsonStr) {
					List<LibraryShowDTO> libShowDTOs = mGson.fromJson(jsonStr, new TypeToken<List<LibraryShowDTO>>(){}.getType());
					try {
						mShowDAO.updateAllShowUserData(libShowDTOs, mUserService.getCurrentUser(), mShowUserDAO, mEpisodeUserDAO);
						listener.onSuccess(mShowDAO.getAllShowsOfUser(mUserService.getCurrentUser(), mShowUserDAO));
					} catch (SQLException e) {
						listener.onFailure(e);
					}
				}
				@Override
				public void onDownloadFailure(final Throwable throwable, final JSONObject jsonObject) {
					if(jsonObject != null) {
						Log.e(LOGTAG, jsonObject.toString());
					}
					listener.onFailure(throwable);
				}
			}));
		} catch (SQLException e) {
			listener.onFailure(e);
		}
	}

	@Override
	public void downloadShowFromTraktAndSaveToDB(final int tvdbId, final ServiceResponseListener listener) {
		// Load Show from Trakt
		try {
			RestClient.postSync(RestURLs.SHOW_SUMMARY+tvdbId+"/extended", mUserService.getCurUserRequestParams(), new DefaultRequestsHandler(new RestResponseListener() {

				@Override public void onProgress(final int bytesWritten, final int totalSize) { }

				@Override
				public void onDownloadSuccess(final int statusCode, final Header[] headers, final String jsonStr) {
					// Publish that Show is currently saving
					listener.onProgress(-1, -1);

					boolean followedByUser = isFollowedByUser(jsonStr);

					try {
						CompleteShowDTO showDTO = mGson.fromJson(jsonStr, new TypeToken<CompleteShowDTO>() {}.getType());
						Log.d(LOGTAG, "Saving Show ("+showDTO.getTitle()+")");
						Show show = mShowDAO.createOrUpdateCompleteShow(showDTO, followedByUser, mUserService.getCurrentUser(), mActorDAO, mActorShowDAO, mGenreDAO, mGenreShowDAO, mRatingDAO, mSeasonDAO, mEpisodeDAO, mEpisodeUserDAO, mShowUserDAO);
						listener.onSuccess(new InternalShowDownloadResponseDTO(show, !followedByUser));
					} catch (SQLException e) {
						onDownloadFailure(e, null);
					}
				}

				@Override
				public void onDownloadFailure(final Throwable throwable, final JSONObject jsonObject) {
					listener.onFailure(throwable);
				}
			}));
		} catch (SQLException e) {
			listener.onFailure(e);
		}
	}

	/**
	 * Checks jsonStr.
	 * @param jsonStr
	 * @return	TRUE if 'In_watchlist'=true or jsonStr contains "watched":true
	 */
	private boolean isFollowedByUser(final String jsonStr) {
		InWatchlistShowDTO watchlistDTO = mGson.fromJson(jsonStr, new TypeToken<InWatchlistShowDTO>() {}.getType());
		if(watchlistDTO.getIn_watchlist()) {
			return true;
		} else {
			if(jsonStr.contains("\"watched\":true")) {
				return true;
			}
		}
		return false;
	}

	//	@Override
	//	public boolean isShowCache(int tvdbId) throws SQLException {
	//		return mShowUserDAO.isShowCache(mUserService.getCurrentUser(), tvdbId);
	//	}

	@Override
	public void setDownloadStatus(final int tvdbId, final ShowDownloadStatus showDownloadStatus) throws SQLException {
		Show show = getShowById(tvdbId);
		if(show != null) {
			show.setDownloadStatus(showDownloadStatus);
			mShowDAO.update(show);
		}
	}

	@Override
	public List<String> getEpisodesTabNameList(final int tvdbId) throws SQLException {
		return mShowDAO.getEpisodesTabNameList(tvdbId, mSeasonDAO, mEpisodeDAO);
	}

	@Override
	public boolean isShowFollowedByCurrentUser(final int tvdbId) throws SQLException {
		return mShowUserDAO.isShowFollowedByUser(mUserService.getCurrentUser(), tvdbId);
	}

	@Override
	public boolean hasFollowedStatusChanged(final boolean oldFollowedStatus, final int tvdbId) throws SQLException {
		if(isShowFollowedByCurrentUser(tvdbId) != oldFollowedStatus) {
			return true;
		}
		return false;
	}

}
