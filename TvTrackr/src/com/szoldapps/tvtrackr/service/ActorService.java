package com.szoldapps.tvtrackr.service;

import java.io.IOException;
import java.sql.SQLException;

import com.szoldapps.tvtrackr.db.model.Actor;
import com.szoldapps.tvtrackr.service.listener.ServiceResponseListener;


public interface ActorService {

	/**
	 * Gets Actor from local DB by <code>slug</code>
	 * @param slug
	 * @return
	 * @throws SQLException
	 */
	public Actor getActorBySlug(String slug) throws SQLException;

	/**
	 * <p>Downloads {@link Actor} details from trakt and saves him/her locally in the DB (in the {@link Actor} object)
	 * <br>Returns (via <code>listener</code>) either</p>
	 * {@link ServiceResponseListener#onSuccess(Object)}
	 * <ul>
	 * <li>{@link Actor}</li>
	 * <li><code>null</code> ... if not found</li>
	 * </ul>
	 * or {@link ServiceResponseListener#onFailure(Throwable)}
	 * <ul>
	 * <li>{@link SQLException} ... on any SQL Problem (e.g. Access-Problem)</li>
	 * <li>{@link IOException} ... No Internet Access (or URL wrong)</li>
	 * </ul>
	 * 
	 * @param actor
	 * @param listener
	 */
	public void downloadActorDetails(Actor actor, ServiceResponseListener listener);



}
