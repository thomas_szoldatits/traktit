package com.szoldapps.tvtrackr.service;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.sql.SQLException;

import org.json.JSONObject;

import android.util.Log;

import com.loopj.android.http.RequestParams;
import com.szoldapps.tvtrackr.db.model.User;
import com.szoldapps.tvtrackr.exception.EmailAlreadyExistsException;
import com.szoldapps.tvtrackr.exception.FailedAuthentificationException;
import com.szoldapps.tvtrackr.exception.UserNotFoundException;
import com.szoldapps.tvtrackr.exception.UsernameAlreadyExistsException;
import com.szoldapps.tvtrackr.service.listener.ServiceResponseListener;


public interface UserService {
	
	/**
	 * <p>Tries to log in {@link User} <code>user</code>. </br>Returns (via <code>listener</code>) either </p>
	 * {@link ServiceResponseListener#onSuccess(Object)}
	 * <ul>
	 * <li>{@link User} ... which is also set as the current User</li>
	 * </ul>
	 * or {@link ServiceResponseListener#onFailure(Throwable)}
	 * <ul>
	 * <li>{@link FailedAuthentificationException} ... invalid user credentials</li>
	 * <li>{@link SQLException} (onDownloadFailure(Throwable throwable) ... on any SQL Problem (e.g. Access-Problem)</li>
	 * <li>{@link IOException} ... No Internet Access (or URL wrong)</li>
	 * </ul>
	 * @param user
	 * @param listener
	 */
	public void login(User user, ServiceResponseListener listener);

	/**
	 * <p>Tries to log in current {@link User}. </br>Returns (via <code>listener</code>) either </p>
	 * {@link ServiceResponseListener#onSuccess(Object)}
	 * <ul>
	 * <li>{@link User}</li>
	 * </ul>
	 * or {@link ServiceResponseListener#onFailure(Throwable)}
	 * <ul>
	 * <li>{@link FailedAuthentificationException} ... invalid user credentials</li>
	 * <li>{@link SQLException} (onDownloadFailure(Throwable throwable) ... on any SQL Problem (e.g. Access-Problem)</li>
	 * <li>{@link IOException} ... No Internet Access (or URL wrong)</li>
	 * </ul>
	 * @param user
	 * @param listener
	 */
	public void checkCredentialsOfCurrentUser(ServiceResponseListener listener);
	
	/**
	 * Logs out current User -> sets isCurrentUser to false
	 * @return Altered User or null (if no current User was set)
	 * @throws SQLException
	 */
	public User logout() throws SQLException;

	/**
	 * Gets current {@link User} from DB
	 * @return {@link User} or null if current User not set
	 * @throws SQLException
	 */
	public User getCurrentUser() throws SQLException;

	/**
	 * <p>Downloads current {@link User} from Trakt. </br>Returns (via <code>listener</code>) either </p>
	 * {@link ServiceResponseListener#onSuccess(Object)}
	 * <ul>
	 * <li>{@link User}</li>
	 * </ul>
	 * or {@link ServiceResponseListener#onFailure(Throwable)}
	 * <ul>
	 * <li>{@link UserNotFoundException} ... no currentUser set in DB</li>
	 * <li>{@link FailedAuthentificationException} ... invalid user credentials</li>
	 * <li>{@link SQLException} (onDownloadFailure(Throwable throwable) ... on any SQL Problem (e.g. Access-Problem)</li>
	 * <li>{@link IOException} ... No Internet Access (or URL wrong)</li>
	 * </ul>
	 * @param listener
	 * @throws SQLException
	 * @throws UnsupportedEncodingException
	 */
	public void downloadCurrentUserDetails(ServiceResponseListener listener) throws SQLException, UnsupportedEncodingException;

	/**
	 * Returns LastCompleteUpdateString.<br>
	 * 'Click to update' if lastCompleteUpdate = 0
	 * 'Updated x time ago' if lastCompleteUpdate > 0
	 * @return
	 * @throws SQLException
	 */
	public String getLastCompleteUpdateString() throws SQLException;

	/**
	 * Sets {@link User#setFirstTimeUserOnDevice(boolean)} with the provided value <code>b</code>
	 * 
	 * @param b
	 * @throws SQLException
	 * @throws UserNotFoundException  if not current User is set
	 */
	public void setFirstTimeUserOnDevice(boolean b) throws SQLException, UserNotFoundException;

	/**
	 * Updates {@link User#setLastCompleteUpdate(long)} to current Time
	 * @throws SQLException
	 */
	public void updateLastCompleteUpdate() throws SQLException;

	/**
	 * <p>Tries to register new {@link User} <code>user</code> with trakt.tv. </br>Returns (via <code>listener</code>) either </p>
	 * {@link ServiceResponseListener#onSuccess(Object)}
	 * <ul>
	 * <li>{@link User} object ... which is also set as the current User</li>
	 * </ul>
	 * or {@link ServiceResponseListener#onFailure(Throwable)}
	 * <ul>
	 * <li>{@link UsernameAlreadyExistsException} ... username already exists</li>
	 * <li>{@link EmailAlreadyExistsException} ... email already exists</li>
	 * <li>{@link SQLException} (onDownloadFailure(Throwable throwable) ... on any SQL Problem (e.g. Access-Problem)</li>
	 * <li>{@link IOException} ... No Internet Access (or URL wrong)</li>
	 * </ul>
	 * @param user
	 * @param listener
	 */
	public void register(User user, ServiceResponseListener listener);

	public RequestParams getCurUserRequestParams() throws SQLException;

	/**
	 * If <code>jsonObject</code> is not null -> prints toString() to {@link Log} <br/>
	 * calls {@link ServiceResponseListener#onFailure(Throwable)} with <code>throwable</code> as parameter
	 * @param throwable
	 * @param jsonObject
	 * @param listener
	 */
	public void defaultOnDownloadFailure(Throwable throwable, JSONObject jsonObject, ServiceResponseListener listener);

}
