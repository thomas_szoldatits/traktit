package com.szoldapps.tvtrackr.service;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.sql.SQLException;
import java.util.List;

import org.apache.http.client.HttpResponseException;
import org.apache.http.entity.StringEntity;

import com.szoldapps.tvtrackr.db.model.Episode;
import com.szoldapps.tvtrackr.db.model.EpisodeUser;
import com.szoldapps.tvtrackr.db.model.OfflineShowToUnfollow;
import com.szoldapps.tvtrackr.db.model.Rating;
import com.szoldapps.tvtrackr.db.model.Season;
import com.szoldapps.tvtrackr.db.model.Show;
import com.szoldapps.tvtrackr.db.model.Show.ShowDownloadStatus;
import com.szoldapps.tvtrackr.db.model.ShowUser;
import com.szoldapps.tvtrackr.db.model.User;
import com.szoldapps.tvtrackr.dto.internal.InternalProgressDTO;
import com.szoldapps.tvtrackr.dto.response.ResponseStatsDTO;
import com.szoldapps.tvtrackr.dto.show.MediumShowDTO;
import com.szoldapps.tvtrackr.dto.show.SearchShowDTO;
import com.szoldapps.tvtrackr.exception.NotFoundInDbException;
import com.szoldapps.tvtrackr.service.listener.ServiceResponseListener;



public interface ShowService {

	//////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// ----- Follow_Unfollow ---------------------------------------------------------------------------------- //

	/**
	 * <p>Watchlists Show (with <code>tvdbId</code>) on trakt.tv.</p>
	 * 
	 * <b>Returns (via <code>listener</code>) either</b><br>
	 * {@link ServiceResponseListener#onSuccess(Object)}
	 * <ul>
	 * 		<li>{@link Boolean#TRUE}</li>
	 * </ul>
	 * or {@link ServiceResponseListener#onFailure(Throwable)}
	 * <ul>
	 * 		<li>{@link SQLException} ... on any SQL Problem (e.g. Access-Problem)</li>
	 * 		<li>{@link UnsupportedEncodingException} ... Problem during {@link StringEntity} creation</li>
	 * 		<li>{@link HttpResponseException} ... Wrong Login Credentials</li>
	 * 		<li>{@link IOException} ... No Internet Access (or URL wrong)</li>
	 * </ul>
	 * 
	 * @param tvdbId
	 * @param listener
	 * @category follow_unfollow
	 */
	public void uploadWatchlist(int tvdbId, ServiceResponseListener listener);


	/**
	 * <p>Unfollows Show on Trakt => Unsees every Episode of Show and unwatchlists the Show.</p>
	 * <b>Returns (via <code>listener</code>) either</b><br>
	 * {@link ServiceResponseListener#onSuccess(Object)}
	 * <ul>
	 * 		<li>{@link Boolean#TRUE}</li>
	 * </ul>
	 * or {@link ServiceResponseListener#onFailure(Throwable)}
	 * <ul>
	 * 		<li>{@link SQLException} ... on any SQL Problem (e.g. Access-Problem)</li>
	 * 		<li>{@link HttpResponseException} ... Wrong Login Credentials</li>
	 * 		<li>{@link IOException} ... No Internet Access (or URL wrong)</li>
	 * 		<li>{@link UnsupportedEncodingException} ... Problem during {@link StringEntity} creation</li>
	 * </ul>
	 * 
	 * @param show
	 * @param listener
	 * @category follow_unfollow
	 */
	public void uploadUnfollow(Show show, ServiceResponseListener listener);

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// ----- SEEN_UNSEEN -------------------------------------------------------------------------------------------- //

	/**
	 * <p>If <code>markAsSeen</code>=TRUE -> Marks all <b>aired</b> Episodes of <code>show</code> as seen for 
	 * currentUser.<br>
	 * If <code>markAsSeen</code>=FALSE -> Marks all Episodes of <code>show</code> as unseen for currentUser and checks
	 * whether all Episodes are already marked unseen. If that is TRUE, show is watchlisted.<br>
	 * If there is no Internet connection, Episodes are markedOffline.</p>
	 * Returns (via <code>listener</code>) either:<br> 
	 * {@link ServiceResponseListener#onSuccess(Object)}
	 * <ul>
	 * 		<li>{@link Boolean} ... TRUE if everything worked, FALSE if there was no Internet Connection 
	 * 			-> Episodes marked Offline</li>
	 * </ul>
	 * or {@link ServiceResponseListener#onFailure(Throwable)}
	 * <ul>
	 * 		<li>{@link HttpResponseException} ... invalid user credentials</li>
	 * 		<li>{@link UnsupportedEncodingException} ... Problem during {@link StringEntity} creation</li>
	 * 		<li>{@link SQLException} ... on any SQL Problem (e.g. Access-Problem)</li>
	 * </ul>
	 * 
	 * @param markAsSeen
	 * @param show
	 * @param listener
	 * @throws UnsupportedEncodingException
	 * @category SEEN_UNSEEN
	 */
	public void uploadShowSeenOrUnseen(boolean markAsSeen, Show show, ServiceResponseListener listener);

	/**
	 * <p>If <code>markAsSeen</code>=TRUE -> Marks all <b>aired</b> Episodes of <code>season</code> as seen for 
	 * currentUser.<br>
	 * If <code>markAsSeen</code>=FALSE -> Marks all Episodes of <code>season</code> as unseen for currentUser and checks
	 * whether all Episodes are already marked unseen. If that is TRUE, show is watchlisted.<br>
	 * If there is no Internet connection, Episodes are markedOffline.</p>
	 * Returns (via <code>listener</code>) either:<br> 
	 * {@link ServiceResponseListener#onSuccess(Object)}
	 * <ul>
	 * 		<li>{@link Boolean} ... TRUE if everything worked, FALSE if there was no Internet Connection 
	 * 			-> Episodes marked Offline</li>
	 * </ul>
	 * or {@link ServiceResponseListener#onFailure(Throwable)}
	 * <ul>
	 * 		<li>{@link HttpResponseException} ... invalid user credentials</li>
	 * 		<li>{@link UnsupportedEncodingException} ... Problem during {@link StringEntity} creation</li>
	 * 		<li>{@link SQLException} ... on any SQL Problem (e.g. Access-Problem)</li>
	 * </ul>
	 * 
	 * @param markAsSeen
	 * @param episodes
	 * @param listener
	 * @throws UnsupportedEncodingException
	 * @category SEEN_UNSEEN
	 */
	public void uploadSeasonSeenOrUnseen(boolean markAsSeen, Season season, ServiceResponseListener listener);

	/**
	 * <p>Marks <code>episode</code> as seen/unseen for currentUser (depending on <code>markAsSeen</code>).<br>
	 * If <code>markAsSeen</code>=FALSE -> Checks whether all Episodes are already marked unseen. If that is TRUE, 
	 * show is watchlisted.<br>
	 * If there is no Internet connection, Episode is markedOffline.</p>
	 * Returns (via <code>listener</code>) either<br>
	 * {@link ServiceResponseListener#onSuccess(Object)}
	 * <ul>
	 * 		<li>{@link Boolean} ... TRUE if everything worked, FALSE if there was no Internet Connection 
	 * 			-> Episodes marked Offline</li>
	 * </ul>
	 * or {@link ServiceResponseListener#onFailure(Throwable)}
	 * <ul>
	 * 		<li>{@link SQLException} ... on any SQL Problem (e.g. Access-Problem)</li>
	 * 		<li>{@link UnsupportedEncodingException} ... Problem during {@link StringEntity} creation</li>
	 * 		<li>{@link HttpResponseException} ... Wrong Login Credentials</li>
	 * </ul>
	 * 
	 * @param markAsSeen
	 * @param episode
	 * @param listener
	 */
	public void uploadEpisodeSeenOrUnseen(boolean markAsSeen, Episode episode, ServiceResponseListener listener);

	/**
	 * <p>Marks <code>episodes</code> as seen/unseen for currentUser (depending on <code>markAsSeen</code>).<br>
	 * If <code>markAsSeen</code>=FALSE -> Checks whether all Episodes are already marked unseen. If that is TRUE, 
	 * show is watchlisted.<br>
	 * If there is no Internet connection, Episodes are markedOffline.</p>
	 * Returns (via <code>listener</code>) either<br>
	 * {@link ServiceResponseListener#onSuccess(Object)}
	 * <ul>
	 * 		<li>{@link Boolean} ... TRUE if everything worked, FALSE if there was no Internet Connection 
	 * 			-> Episodes marked Offline</li>
	 * </ul>
	 * or {@link ServiceResponseListener#onFailure(Throwable)}
	 * <ul>
	 * 		<li>{@link SQLException} ... on any SQL Problem (e.g. Access-Problem)</li>
	 * 		<li>{@link UnsupportedEncodingException} ... Problem during {@link StringEntity} creation</li>
	 * 		<li>{@link HttpResponseException} ... Wrong Login Credentials</li>
	 * </ul>
	 * 
	 * @param markAsSeen
	 * @param show
	 * @param episodes
	 * @param listener
	 */
	public void uploadEpisodesSeenOrUnseen(boolean markAsSeen, Show show, List<Episode> episodes, 
			final ServiceResponseListener listener);

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// ----- PROGRESS ----------------------------------------------------------------------------------------------- //

	/**
	 * Calculates watchedEpisodeCount and episodeCount of Show and current User and returns it via 
	 * {@link InternalProgressDTO} 
	 * Only already aired Episodes are taken into consideration. <br>
	 * <i>If a Show has 13 Episodes and only 10 have aired so far and the User has watched all 10 these aired 
	 * Episodes -> Progress = 100%</i>
	 * 
	 * @param show
	 * @return {@link InternalProgressDTO}
	 * @throws SQLException
	 * @category progress
	 */
	public InternalProgressDTO getProgressOfAiredEpisodes(final Show show) throws SQLException;

	/**
	 * Calculates watchedEpisodeCount and episodeCount of Show and current User and returns it via 
	 * {@link InternalProgressDTO} 
	 * Only already aired Episodes are taken into consideration. <br>
	 * <i>If a Season has 13 Episodes and only 10 have aired so far and the User has watched all 10 these aired 
	 * Episodes -> Progress = 100%</i>
	 * 
	 * @param season
	 * @return
	 * @throws SQLException
	 * @category progress
	 */
	public InternalProgressDTO getProgressOfAiredEpisodes(final Season season) throws SQLException;


	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



	public List<Show> getAllShowsOfUser() throws SQLException;





	/**
	 * <p>Unfollows Show on Trakt => Unsees every Episode of Show and unwatchlists the Show.</p>
	 * <b>Returns (via <code>listener</code>) either</b><br>
	 * {@link ServiceResponseListener#onSuccess(Object)}
	 * <ul>
	 * 		<li>{@link Boolean#TRUE}</li>
	 * </ul>
	 * or {@link ServiceResponseListener#onFailure(Throwable)}
	 * <ul>
	 * 		<li>{@link SQLException} ... on any SQL Problem (e.g. Access-Problem)</li>
	 * 		<li>{@link HttpResponseException} ... Wrong Login Credentials</li>
	 * 		<li>{@link IOException} ... No Internet Access (or URL wrong)</li>
	 * 		<li>{@link UnsupportedEncodingException} ... Problem during {@link StringEntity} creation</li>
	 * </ul>
	 * 
	 * @param oShow
	 * @param listener
	 */
	public void uploadUnfollow(OfflineShowToUnfollow oShow, ServiceResponseListener listener);

	/**
	 * deletes Show for a User and all relations. If no other User follows this Show, Show is completely deleted
	 * @param tvdbId
	 * @throws SQLException
	 */
	public void deleteShowForUser(int tvdbId, boolean preventCompleteRemoval) throws SQLException;

	/**
	 * <p>Searches for Shows. Returns (via <code>listener</code>) either</p>
	 * {@link ServiceResponseListener#onSuccess(Object)}
	 * <ul>
	 * <li>List<{@link SearchShowDTO}></li>
	 * </ul>
	 * or {@link ServiceResponseListener#onFailure(Throwable)}
	 * <ul>
	 * <li>{@link SQLException} ... on any SQL Problem (e.g. Access-Problem)</li>
	 * <li>{@link IOException} ... No Internet Access (or URL wrong)</li>
	 * </ul>
	 * 
	 * @param query
	 * @param limit
	 * @param listener
	 */
	public void search(String query, int limit, ServiceResponseListener listener);

	public int getPositionByEpisodeId(int episodeId, int tvdbId) throws SQLException;

	public void getGenreStringList(ServiceResponseListener listener);

	public void dismissRecommendation(int tvdbId, ServiceResponseListener listener) throws UnsupportedEncodingException;

	public void getRecommendations(String genre, Integer startYear, Integer endYear, ServiceResponseListener listener) throws UnsupportedEncodingException;


	/**
	 * TODO: JAVADOC
	 * returns list of TrendingShowDTO
	 * @param listener
	 * @throws SQLException
	 */
	public void getTrendingShows(ServiceResponseListener listener) throws SQLException;

	/**
	 * Returns Show from DB or null if not found
	 * @param tvdbId
	 * @return {@link Show} or null
	 * @throws SQLException
	 */
	public Show getShowById(Integer tvdbId) throws SQLException;

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// ----- STATS -------------------------------------------------------------------------------------------------- //

	/**
	 * <p>Downloads Stats of <code>show</code> from trakt and saves them locally in the DB (in the {@link Show} object)
	 * <br>Returns (via <code>listener</code>) either</p>
	 * {@link ServiceResponseListener#onSuccess(Object)}
	 * <ul>
	 * <li><{@link ResponseStatsDTO}></li>
	 * </ul>
	 * or {@link ServiceResponseListener#onFailure(Throwable)}
	 * <ul>
	 * <li>{@link SQLException} ... on any SQL Problem (e.g. Access-Problem)</li>
	 * <li>{@link HttpResponseException} ... invalid user credentials</li>
	 * <li>{@link IOException} ... No Internet Access (or URL wrong)</li>
	 * </ul>
	 * @param show
	 * @param listener
	 * @throws SQLException
	 * @category STATS
	 */
	public void downloadStats(Show show, ServiceResponseListener serviceResponseListener) throws SQLException;

	/**
	 * Gets <code>show</code> Rating from DB
	 * @param show
	 * @return {@link Rating}
	 * @throws SQLException
	 */
	public Rating getRating(Show show) throws SQLException;

	/**
	 * First, uploads the current User's <code>show</code> <code>rating</code> to trakt. After trakt-response -> updates {@link Show} ratings in DB
	 * <br>Returns (via <code>listener</code>) either</p>
	 * {@link ServiceResponseListener#onSuccess(Object)}
	 * <ul>
	 * <li><{@link Show}></li>
	 * </ul>
	 * or {@link ServiceResponseListener#onFailure(Throwable)}
	 * <ul>
	 * <li>{@link SQLException} ... on any SQL Problem (e.g. Access-Problem)</li>
	 * <li>{@link HttpResponseException} ... invalid user credentials</li>
	 * <li>{@link IOException} ... No Internet Access (or URL wrong)</li>
	 * </ul>
	 * 
	 * @param show
	 * @param rating
	 * @param listener
	 * @throws UnsupportedEncodingException
	 */
	public void uploadRating(Show show, int rating, ServiceResponseListener listener) throws UnsupportedEncodingException;

	public void downloadShowlistFromTraktAndSaveToDB(ServiceResponseListener listener);

	/**
	 * TODO: JAVADOC
	 * @param medDTO
	 * @return
	 * @throws SQLException
	 */
	public Show saveShow(MediumShowDTO medDTO) throws SQLException;

	public void updateShowData(List<Show> shows, ServiceResponseListener listener);

	public void updateAllShowUserData(ServiceResponseListener listener);

	public void downloadShowFromTraktAndSaveToDB(int tvdbId, ServiceResponseListener listener);

	//	public boolean isShowCache(int tvdbId) throws SQLException;

	/**
	 * Sets {@link ShowDownloadStatus} of the {@link Show} with the corresponding <code>tvdbId</code> in DB
	 * If {@link Show} not found, nothing is done.
	 * @param tvdbId
	 * @param showDownloadStatus
	 * @throws SQLException
	 */
	public void setDownloadStatus(int tvdbId, ShowDownloadStatus showDownloadStatus) throws SQLException;

	/**
	 * Creates a List that contain all {@link Episode#getSeasonEpisodeText()} of the {@link Show} with <code>tvdbId</code>
	 * ordered by {@link Episode#getFirstAiredUTC()}
	 * 
	 * @param tvdbId
	 * @return
	 * @throws SQLException
	 */
	public List<String> getEpisodesTabNameList(int tvdbId) throws SQLException;

	/**
	 * Checks whether {@link Show} (with <code>tvdbId</code>) is followed by current {@link User}
	 * 
	 * @param show
	 * @return
	 * @throws SQLException
	 * @throws NotFoundInDbException ... if show wasn't found in DB
	 */
	public boolean isShowFollowedByCurrentUser(int tvdbId) throws SQLException;

	/**
	 * Calls {@link ShowService#isShowFollowedByCurrentUser(int)} and checks if this new status equals <code>oldFollowedStatus</code>.
	 * @param oldFollowedStatus
	 * @param tvdbId
	 * @return TRUE if status changed; FALSE if statuses are the same
	 */
	public boolean hasFollowedStatusChanged(boolean oldFollowedStatus, int tvdbId) throws SQLException;

	/**
	 * Creates {@link ShowUser} & {@link EpisodeUser} data for an existing <code>show</code>.
	 * 
	 * @param show
	 */
	public void createShowUserAndEpisodeUserData(Show show, boolean watchlist) throws SQLException;

}
