package com.szoldapps.tvtrackr.service;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.sql.SQLException;
import java.util.List;

import org.apache.http.client.HttpResponseException;

import com.szoldapps.tvtrackr.db.model.Episode;
import com.szoldapps.tvtrackr.db.model.EpisodeUser;
import com.szoldapps.tvtrackr.db.model.Rating;
import com.szoldapps.tvtrackr.db.model.Show;
import com.szoldapps.tvtrackr.dto.response.ResponseStatsDTO;
import com.szoldapps.tvtrackr.exception.EpisodeUserNotFoundException;
import com.szoldapps.tvtrackr.service.listener.ServiceResponseListener;


public interface EpisodeService {

	/**
	 * Checks whether the episode is seen or not seen by the current User
	 * @param episode
	 * @return <code>true</code> if seen, <code>false</code> if unseen
	 * @throws SQLException
	 */
	public boolean isEpisodeSeen(Episode episode) throws SQLException;

	/**
	 * TODO: JAVADOC
	 * @param episode
	 * @param watched
	 * @param offlineMarked
	 * @throws SQLException
	 * @throws EpisodeUserNotFoundException
	 */
	public void setEpisodeWatchedAndOffline(Episode episode, boolean watched, boolean offlineMarked) throws SQLException, EpisodeUserNotFoundException;
	
	/**
	 * TODO: JAVADOC CHECK IF EVEN NEEDED
	 * @param episodes
	 * @param watched
	 * @param offlineMarked
	 * @throws SQLException
	 * @throws EpisodeUserNotFoundException
	 */
	public void setEpisodesWatchedAndOffline(List<Episode> episodes, boolean watched, boolean offlineMarked) throws SQLException, EpisodeUserNotFoundException;
	

	/**
	 * Returns {@link List} of {@link Episode}s of the current Day plus minus the offset (e.g. yesterday has an offset of -1, tomorrow +1)
	 * @param offset
	 * @return 
	 * @throws SQLException
	 */
	public List<Episode> getEpisodesOfDay(int offset) throws SQLException;

	/**
	 * Returns a count of all unwatched Episodes of the day (offset=0 equals today, offset=1 equals tomorrow, etc.)
	 * @param offset
	 * @return
	 * @throws SQLException
	 */
	public Long getUnwatchedEpisodeCountOfDay(int offset) throws SQLException;
	
	/**
	 * Returns {@link List} of {@link Show}s that has unseen Episodes
	 * @return
	 * @throws SQLException
	 */
	public List<Show> getToWatchShows() throws SQLException;

	public Episode getNextEpisodeToWatch(int tvdbId) throws SQLException;

	/**
	 * Returns {@link Integer} count of all Episodes left to watch for currentUser of a specific {@link Show} with <code>tvdbId</code>
	 * @param tvdbId
	 * @return
	 * @throws SQLException
	 */
	public Integer getEpisodesToWatchCountOfShow(int tvdbId) throws SQLException;

	/**
	 * Returns {@link Long} count of all Episodes left to watch for currentUser
	 * @return
	 * @throws SQLException
	 */
	public Long getAllEpisodesToWatchCount() throws SQLException;
	
	/**
	 * TODO: JAVADOC
	 * @param show
	 * @return
	 * @throws SQLException
	 */
	public long getEpisodeSeenCountOfShow(Show show) throws SQLException;

	/**
	 * <p>Downloads Stats of <code>episode</code> from trakt and saves them locally in the DB (in the {@link Episode} object)
	 * <br>Returns (via <code>listener</code>) either</p>
	 * {@link ServiceResponseListener#onSuccess(Object)}
	 * <ul>
	 * <li><{@link ResponseStatsDTO}></li>
	 * </ul>
	 * or {@link ServiceResponseListener#onFailure(Throwable)}
	 * <ul>
	 * <li>{@link HttpResponseException} ... invalid user credentials</li>
	 * <li>{@link IOException} ... No Internet Access (or URL wrong)</li>
	 * </ul>
	 * @param episode
	 * @param listener
	 * @throws SQLException
	 */
	public void downloadStats(Episode episode, ServiceResponseListener listener) throws SQLException;

	/**
	 * Gets <code>episode</code> Rating from DB
	 * @param episode
	 * @return {@link Rating}
	 * @throws SQLException
	 */
	public Rating getRating(Episode episode) throws SQLException;

	/**
	 * First, uploads the current User's <code>episode</code> <code>rating</code> to trakt. After trakt-response -> updates {@link Episode} ratings in DB
	 * <br>Returns (via <code>listener</code>) either</p>
	 * {@link ServiceResponseListener#onSuccess(Object)}
	 * <ul>
	 * 		<li><{@link Episode}></li>
	 * </ul>
	 * or {@link ServiceResponseListener#onFailure(Throwable)}
	 * <ul>
	 * 		<li>{@link SQLException} ... on any SQL Problem (e.g. Access-Problem)</li>
	 * 		<li>{@link HttpResponseException} ... invalid user credentials</li>
	 * 		<li>{@link IOException} ... No Internet Access (or URL wrong)</li>
	 * </ul>
	 * 
	 * @param episode
	 * @param rating
	 * @param listener
	 */
	public void saveRating(Episode episode, int rating, ServiceResponseListener listener) throws UnsupportedEncodingException;

	/**
	 * Gets {@link Episode} of {@link Show} with <code>showTvdbId</code> by <code>position</code>
	 * @param position
	 * @param showTvdbId
	 * @return
	 * @throws SQLException
	 */
	public Episode getEpisodeByPosition(int position, int showTvdbId) throws SQLException;

	
	/**
	 * Returns corresponding {@link EpisodeUser} or throws {@link EpisodeUserNotFoundException}
	 * @param episode
	 * @return
	 * @throws SQLException ... on any DB related Error
	 * @throws EpisodeUserNotFoundException
	 */
	public EpisodeUser getEpisodeUser(Episode episode) throws SQLException, EpisodeUserNotFoundException;

	
//	/**	TODO: v1.1: for later use
//	 * <p>Downloads requested Episode.</p>
//	 * <b>Returns (via <code>listener</code>) either:</b><br>
//	 * {@link ServiceResponseListener#onDownloadSuccess(Object)}
//	 * <ul>
//	 * 		<li>{@link Episode}</li>
//	 * </ul>
//	 * or {@link ServiceResponseListener#onDownloadFailure(Throwable)}
//	 * <ul>
//	 * 		<li>{@link SQLException} ... on any SQL Problem (e.g. Access-Problem)</li>
//	 * 		<li>{@link HttpResponseException} ... invalid user credentials</li>
//	 * 		<li>{@link IOException} ... No Internet Access (or URL wrong)</li>
//	 * </ul>
//	 * 
//	 * @param showTvdbId
//	 * @param seasonNr
//	 * @param episodeNr
//	 * @param listener
//	 */
//	public void downloadEpisode(int showTvdbId, int seasonNr, int episodeNr, ServiceResponseListener listener);

}
