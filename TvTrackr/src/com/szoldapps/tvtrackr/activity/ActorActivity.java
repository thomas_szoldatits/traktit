package com.szoldapps.tvtrackr.activity;

import java.io.IOException;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.style.StyleSpan;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.szoldapps.tvtrackr.R;
import com.szoldapps.tvtrackr.activity.show.card.BiographyCard;
import com.szoldapps.tvtrackr.activity.show.card.LinksCard;
import com.szoldapps.tvtrackr.db.model.Actor;
import com.szoldapps.tvtrackr.exception.IntentExtraException;
import com.szoldapps.tvtrackr.exception.WrongObjectReturnedException;
import com.szoldapps.tvtrackr.helper.Constants;
import com.szoldapps.tvtrackr.helper.DateHelper;
import com.szoldapps.tvtrackr.helper.DialogHelper;
import com.szoldapps.tvtrackr.helper.ImageLoaderHelper;
import com.szoldapps.tvtrackr.helper.ImageURLHelper;
import com.szoldapps.tvtrackr.helper.LinkingHelper;
import com.szoldapps.tvtrackr.helper.LogCatHelper;
import com.szoldapps.tvtrackr.helper.MenuHelper;
import com.szoldapps.tvtrackr.service.async.AsyncAccessService;
import com.szoldapps.tvtrackr.service.async.AsyncAccessServiceImpl;
import com.szoldapps.tvtrackr.service.listener.ServiceResponseListener;

/**
 * <b>Required Intent Extras:</b>
 * <ul>
 * 		<li>{@link Constants#EXTRA_ACTOR_SLUG}</li>
 * 		<li>{@link Constants#EXTRA_SHOW_TITLE}</li>
 * </ul>
 * 
 * @author thomasszoldatits
 *
 */
public class ActorActivity extends Activity {
	// Logger
	public static final String LOGTAG = LogCatHelper.getAppTag()+ActorActivity.class.getSimpleName().toString();

	// Intent Extras
	private String mActorSlug;
	private String mShowTitle;

	// Service
	private AsyncAccessService mAsyncAccessService;

	// UI Elements
	private RelativeLayout mRelLoading;
	private RelativeLayout mRelInfoBox;
	private ImageView mImgHeadshot;
	private TextView mTxtName;
	private TextView mTxtBirthday;
	private TextView mTxtBirthplace;
	private BiographyCard mBiographyCard;
	private LinearLayout mLinCards;
	private LinksCard mLinksCard;
	private MenuItem mRefreshMenuItem;

	// Helper
	private Actor mActor;
	private boolean mUpdateRunning;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_actor);

		// Check for intent extras
		mActorSlug = getIntent().getStringExtra(Constants.EXTRA_ACTOR_SLUG);
		if(mActorSlug == null || mActorSlug.isEmpty()) {
			mActorSlug = "";
			showError(new IntentExtraException("EXTRA_ACTOR_SLUG is required to display ActorActivity."));
			return;
		}
		mShowTitle = getIntent().getStringExtra(Constants.EXTRA_SHOW_TITLE);
		if(mShowTitle == null || mShowTitle.isEmpty()) {
			showError(new IntentExtraException("EXTRA_SHOW_TITLE is required to display ActorActivity."));
			return;
		}

		Log.d(LOGTAG, "Loading Actor ("+mActorSlug+")");
		// Async Access
		mAsyncAccessService = AsyncAccessServiceImpl.getInstance(getApplicationContext());
		// Build UI
		buildUI();
		// Load Actor from DB
		loadActorFromDB();
	}

	private void buildUI() {
		// Activate Home Button in Actionbar
		getActionBar().setDisplayHomeAsUpEnabled(true);
		getActionBar().setHomeButtonEnabled(true);
		getActionBar().setTitle(mShowTitle);

		// Loading
		mRelLoading = (RelativeLayout) findViewById(R.id.loading_rel_container);

		mRelInfoBox = (RelativeLayout) findViewById(R.id.actor_rel_info_box);
		mImgHeadshot = (ImageView) findViewById(R.id.actor_img_headshot);
		mTxtName = (TextView) findViewById(R.id.actor_txt_name);
		mTxtBirthday = (TextView) findViewById(R.id.actor_txt_birthday);
		mTxtBirthplace = (TextView) findViewById(R.id.actor_txt_birthplace);

		mLinCards = (LinearLayout) findViewById(R.id.actor_lin_cards);

		// BiographyCard
		mBiographyCard = new BiographyCard(true, getApplicationContext(), getLayoutInflater(), mLinCards);
		mBiographyCard.preventCardFromOpeningOrClosing(true);
		mLinCards.addView(mBiographyCard.getCard());
		// LinksCard
		mLinksCard = new LinksCard(getLayoutInflater(), mLinCards, this);
		mLinCards.addView(mLinksCard.getCard());
	}

	private void loadActorFromDB() {
		mAsyncAccessService.getActorBySlug(mActorSlug, new ServiceResponseListener() {
			@Override
			public void onProgress(int bytesWritten, int totalSize) { }

			@Override
			public void onSuccess(Object o) {
				if(o instanceof Actor) {
					mActor = (Actor) o;
					fillUIWithData(true);
				} else {
					onFailure(new WrongObjectReturnedException(Actor.class));
				}
			}

			@Override
			public void onFailure(Throwable throwable) {
				showError(throwable);
			}
		});
	}

	private void showLoading(boolean b) {
		if(b) {
			mRelLoading.setVisibility(View.VISIBLE);
			mRelInfoBox.setVisibility(View.INVISIBLE);
			mLinCards.setVisibility(View.INVISIBLE);
		} else {
			mRelLoading.setVisibility(View.GONE);
			mRelInfoBox.setVisibility(View.VISIBLE);
			mLinCards.setVisibility(View.VISIBLE);
		}
	}

	private void fillUIWithData(boolean initialLoad) {
		// Headshot	(only load Headshot, if it's NOT the default image url)
		if(!ImageURLHelper.isTraktDefaultImgURL(mActor.getHeadshot())) {
			ImageLoader.getInstance().displayImage(
					mActor.getHeadshot(), mImgHeadshot, ImageLoaderHelper.getDIOPosterOrHeadShot(getApplicationContext()));
		}
		
		// Name
		SpannableString spannable = new SpannableString(mActor.getName());
		int lastNamePosStart = mActor.getName().lastIndexOf(" ")+1;
		lastNamePosStart = (lastNamePosStart==-1) ? 0 : lastNamePosStart;
		spannable.setSpan(new StyleSpan(android.graphics.Typeface.BOLD), lastNamePosStart, spannable.length(), 0);
		mTxtName.setText(spannable);

		if(mActor.isFullyLoaded()) {
			String birthday = DateHelper.getDateStrFromStr(mActor.getBirthday(), "yyyy-MM-dd", getApplicationContext());
			if(birthday != "") {
				mTxtBirthday.setText(birthday);
			}
			String birthplace = mActor.getBirthplace();
			if(birthplace != null) {
				mTxtBirthplace.setText(birthplace);
			}
			String biography = mActor.getBiography();
			if(biography == null || biography.isEmpty()) {
				biography = getString(R.string.actor_no_bio_available);
			}
			mBiographyCard.loadDynamicContent(biography);
			
			mLinksCard.loadDynamicContent(mActor);
		} 
		showLoading(false);

		if(initialLoad){
			initialLoad = false;
			loadActorFromTrakt();
		}
	}


	private void loadActorFromTrakt() {
		mUpdateRunning = true;
		MenuHelper.updateStarted(mRefreshMenuItem);
		mAsyncAccessService.downloadActorDetails(mActor, new ServiceResponseListener() {
			@Override
			public void onProgress(int bytesWritten, int totalSize) { }

			@Override
			public void onSuccess(Object o) {
				if(o instanceof Actor) {
					mActor = (Actor) o;
					fillUIWithData(false);
				} else {
					onFailure(new WrongObjectReturnedException(Actor.class));
				}
				mRefreshMenuItem.setIcon(getResources().getDrawable(R.drawable.ic_refresh_white));
				mUpdateRunning = false;
				MenuHelper.updateEnded(mRefreshMenuItem);
			}

			@Override
			public void onFailure(Throwable throwable) {
				showError(throwable);
			}
		});
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// ----- MENU --------------------------------------------------------------------------------------------------- //
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.actor, menu);
		// Refresh icon
		mRefreshMenuItem = menu.findItem(R.id.menu_refresh);
		if(mUpdateRunning) {
			MenuHelper.updateStarted(mRefreshMenuItem);
		}
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.menu_refresh:
			loadActorFromTrakt();
			return true;
		case android.R.id.home:
			LinkingHelper.goBack(ActorActivity.this);
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// ----- ERROR -------------------------------------------------------------------------------------------------- //
	
	/**
	 * Handles {@link WrongObjectReturnedException} and {@link IntentExtraException}.<br />
	 * All other {@link Throwable}s are redirected to {@link DialogHelper#handleDialogErrors(Throwable, Activity)}
	 * @param throwable
	 * @category error
	 */
	private void showError(Throwable throwable) {
		// Show Error Dialog if Actor wasn't found in the DB
		if(throwable instanceof WrongObjectReturnedException || throwable instanceof IntentExtraException) {
			String actor = mActorSlug.replace("-", " ");
			String msg = "";
			if(actor.isEmpty()) {
				msg = getString(R.string.actor_dialog_requested_not_found_msg);
			} else {
				msg = String.format(getString(R.string.actor_dialog_not_found_msg), actor);
			}
			new AlertDialog.Builder(this)
			.setTitle(getString(R.string.actor_dialog_not_found_title))	// Title
			.setMessage(msg)	// Message
			.setPositiveButton(getString(R.string.back), new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog,int id) {
					LinkingHelper.goBack(ActorActivity.this);
				}
			}).create().show();
			return;
		} else if(throwable instanceof IOException) {
			mRefreshMenuItem.setIcon(getResources().getDrawable(R.drawable.ic_refresh_red));
			MenuHelper.updateEnded(mRefreshMenuItem);
			DialogHelper.showInfoShort(getString(R.string.actor_toast_no_internet), this);
			return;
		}
		DialogHelper.handleDialogErrors(throwable, this);
	}

}
