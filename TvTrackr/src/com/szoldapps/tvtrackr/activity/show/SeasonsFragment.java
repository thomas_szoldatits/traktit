package com.szoldapps.tvtrackr.activity.show;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnChildClickListener;
import android.widget.RelativeLayout;

import com.szoldapps.tvtrackr.R;
import com.szoldapps.tvtrackr.db.model.Episode;
import com.szoldapps.tvtrackr.db.model.Season;
import com.szoldapps.tvtrackr.dto.internal.InternalSeasonsFragmentDTO;
import com.szoldapps.tvtrackr.exception.IntentExtraException;
import com.szoldapps.tvtrackr.exception.WrongObjectReturnedException;
import com.szoldapps.tvtrackr.helper.Constants;
import com.szoldapps.tvtrackr.helper.DialogHelper;
import com.szoldapps.tvtrackr.helper.LinkingHelper;
import com.szoldapps.tvtrackr.helper.LogCatHelper;
import com.szoldapps.tvtrackr.service.async.AsyncAccessService;
import com.szoldapps.tvtrackr.service.async.AsyncAccessServiceImpl;
import com.szoldapps.tvtrackr.service.listener.ServiceResponseListener;

/**
 * <b>Required Intent Extras:</b>
 * <ul>
 * 		<li>{@link Constants#EXTRA_SHOW_TVDB_ID}</li>
 * 		<li>{@link Constants#EXTRA_SHOW_TITLE}</li>
 * </ul>
 * 
 * <b>Optional Intent Extras:</b>
 * <ul>
 * 		<li>{@link Constants#EXTRA_SHOWACTIVITY_BOOL}</li>
 * </ul>
 * @author thomasszoldatits
 *
 */
public class SeasonsFragment extends Fragment {

	public static final String LOGTAG = LogCatHelper.getAppTag()+SeasonsFragment.class.getSimpleName().toString();

	private AsyncAccessService mAsyncAccessService;

	private Activity mActivity;
	private View mRootView;

	// LOADING
	private RelativeLayout mRelLoading;

	// ExpandableList
	SeasonsExpListAdapter mListAdapter;
	ExpandableListView mExpListView;
	List<Season> mListDataHeader;
	HashMap<Season, List<Episode>> mListDataChild;

	// Intent Extras
	private int mTvdbId;
	private String mTitle;

	private boolean mIsShowActivity;


	public SeasonsFragment() { /* Empty constructor required for fragment subclasses */ }

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		mActivity = getActivity();
		mRootView = inflater.inflate(R.layout.fragment_seasons, container, false);
		setHasOptionsMenu(true);

		if(getArguments() == null) {
			showError(new IntentExtraException("No IntentExtras at all")); 
			return mRootView;
		}

		mTvdbId = getArguments().getInt(Constants.EXTRA_SHOW_TVDB_ID, -1);
		mTitle = getArguments().getString(Constants.EXTRA_SHOW_TITLE);
		mIsShowActivity = getArguments().getBoolean(Constants.EXTRA_SHOWACTIVITY_BOOL, false);
		if(mTvdbId == -1) {
			showError(new IntentExtraException("mTvdbId == -1"));
			return mRootView;
		}

		mAsyncAccessService = AsyncAccessServiceImpl.getInstance(mActivity.getApplicationContext());

		buildUI();
		showLoading(true);

		return mRootView;
	}

	@Override
	public void onResume() {
		displayShowFromDB();
		super.onResume();
	}

	private void buildUI() {
		mRelLoading = (RelativeLayout) mRootView.findViewById(R.id.loading_rel_container);
		mExpListView = (ExpandableListView) mRootView.findViewById(R.id.seasons_exp_list);
		mExpListView.setDividerHeight(0);
		mExpListView.setGroupIndicator(null);
		mListAdapter = new SeasonsExpListAdapter(mActivity, new ArrayList<Season>(), new HashMap<Season, List<Episode>>(), SeasonsFragment.this);
		mExpListView.setAdapter(mListAdapter);
		// Add Link to Episode
		mExpListView.setOnChildClickListener(new OnChildClickListener() {
			@Override
			public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
				if(mIsShowActivity) {
					Episode episode = mListAdapter.getChild(groupPosition, childPosition);
					LinkingHelper.goToEpisode(episode.getId(), mTvdbId, mTitle, SeasonsFragment.this.getActivity());
				}
				return true;
			}
		});
	}

	private void showLoading(boolean b) {
		if(b) {
			mRelLoading.setVisibility(View.VISIBLE);
			mExpListView.setVisibility(View.INVISIBLE);
		} else {
			mRelLoading.setVisibility(View.GONE);
			mExpListView.setVisibility(View.VISIBLE);
		}
	}


	public void displayShowFromDB() {
		mAsyncAccessService.getSeasonsFragmentDTO(mTvdbId, new ServiceResponseListener() {
			@Override public void onProgress(int bytesWritten, int totalSize) { }
			@Override
			public void onSuccess(Object o) {
				if(o instanceof InternalSeasonsFragmentDTO) {
					InternalSeasonsFragmentDTO dto = (InternalSeasonsFragmentDTO) o;
					if(dto.isFullyDownloaded()) {
						mListAdapter.setFragmentData(dto.getHeaderList(), dto.getChildMap());
						showLoading(false);
					} else {
						Log.w(LOGTAG, "SHOW ("+mTvdbId+") not ready yet. Waiting for DownloadService");
					}
				} else {
					onFailure(new WrongObjectReturnedException(InternalSeasonsFragmentDTO.class));
				}
			}

			@Override
			public void onFailure(Throwable throwable) {
				showError(throwable);
			}
		});
	}

	public void notifyParentActivity() {
		if(mIsShowActivity) {
			((ShowActivity)mActivity).notifyShowFragment();
		}
	}


	/**
	 * Reloads SeasonsFragment Show data
	 * @param tvdbId
	 */
	public void loadFragmentData(int tvdbId) {
		mTvdbId = tvdbId;
		displayShowFromDB();
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// ----- ERROR -------------------------------------------------------------------------------------------------- //

	private void showError(Throwable throwable) {
		DialogHelper.handleDialogErrors(throwable, mActivity);
	}

}
