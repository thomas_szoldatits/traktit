package com.szoldapps.tvtrackr.activity.show.card;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.szoldapps.tvtrackr.R;
import com.szoldapps.tvtrackr.helper.LogCatHelper;

public class BiographyCard extends DefaultCard {

    public static final String LOGTAG = LogCatHelper.getAppTag()+BiographyCard.class.getSimpleName().toString();
    private TextView mTxtBiographyContent;

    public BiographyCard(boolean isOpen, Context context, LayoutInflater inflater, LinearLayout linCards) {
        super(isOpen, context, inflater, linCards, linCards.getResources().getString(R.string.hl_biography));

        mTxtBiographyContent = (TextView) mRelCard.findViewById(R.id.card_txt_overview);
        mTxtBiographyContent.setVisibility(View.VISIBLE);
    }
    
    public void preventCardFromOpeningOrClosing(boolean b) {
    	if(b) {
    		mRelCard.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) { /* Do nothing */ }
			});
    	} else {
    		mRelCard.setOnClickListener(getCardOnClickListener());
    	}
    }
    

    @Override
	public OnClickListener getCardOnClickListener() {
		return super.getCardOnClickListener();
	}

	public void loadDynamicContent(String content) {
        mTxtBiographyContent.setText(content);
    }
}
