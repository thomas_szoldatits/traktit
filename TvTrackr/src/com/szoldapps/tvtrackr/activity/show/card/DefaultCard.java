package com.szoldapps.tvtrackr.activity.show.card;

import com.szoldapps.tvtrackr.R;
import com.szoldapps.tvtrackr.service.async.AsyncAccessService;
import com.szoldapps.tvtrackr.service.async.AsyncAccessServiceImpl;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

public abstract class DefaultCard {

    protected RelativeLayout mRelCard;
    protected TextView mTxtHl;
    protected RelativeLayout mRelContent;
    protected ImageView mImgExpColl;
    protected LayoutInflater mInflater;
    protected Context mContext;
	protected AsyncAccessService mAsyncAccessService;
    
    /**
     * Instantiates mRelCard & mTxtHl; sets the onClickListener and the mTxtHl
     * @param isOpen 
     * @param inflater
     * @param linCards ... rootView
     * @param headline
     * @param context 
     */
    protected DefaultCard(boolean isOpen, Context context, LayoutInflater inflater, LinearLayout linCards, String headline) {
        mInflater = inflater;
        mContext = context;
		mAsyncAccessService = AsyncAccessServiceImpl.getInstance(mContext);
        
        mRelCard = (RelativeLayout) inflater.inflate(R.layout.card_default, linCards, false);
        mRelCard.setOnClickListener(getCardOnClickListener());

        mImgExpColl = (ImageView) mRelCard.findViewById(R.id.card_img_expand_collapse);

        mTxtHl = (TextView) mRelCard.findViewById(R.id.card_txt_hl);
        mTxtHl.setText(headline);
        
        mRelContent = (RelativeLayout) mRelCard.findViewById(R.id.card_rel_content);
        
        if(isOpen) {
            openCard();
        } else {
            closeCard();
        }
    }
    
    public OnClickListener getCardOnClickListener() {
        return new OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mRelContent.getVisibility() == View.VISIBLE) {
                    mRelContent.setVisibility(View.GONE);
                    mImgExpColl.setImageDrawable(mImgExpColl.getResources().getDrawable(R.drawable.ic_expand));
                } else {
                    mRelContent.setVisibility(View.VISIBLE);
                    mImgExpColl.setImageDrawable(mImgExpColl.getResources().getDrawable(R.drawable.ic_collapse));
                }
            }
        };
    }
    
    public void openCard() {
        setOpenClose(View.VISIBLE, R.drawable.ic_collapse);
    }
    
    public void closeCard() {
        setOpenClose(View.GONE, R.drawable.ic_expand);
    }
    
    private void setOpenClose(int visibility, int drawable) {
        mRelContent.setVisibility(visibility);
        mImgExpColl.setImageDrawable(mImgExpColl.getResources().getDrawable(drawable));
    }
    
    public RelativeLayout getCard() {
        return mRelCard;
    }
    
}
