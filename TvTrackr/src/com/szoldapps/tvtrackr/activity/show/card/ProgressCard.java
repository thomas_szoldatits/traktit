package com.szoldapps.tvtrackr.activity.show.card;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.szoldapps.tvtrackr.R;
import com.szoldapps.tvtrackr.dto.internal.InternalProgressDTO;
import com.szoldapps.tvtrackr.helper.LogCatHelper;

public class ProgressCard extends DefaultCard {

	public static final String LOGTAG = LogCatHelper.getAppTag()+ProgressCard.class.getSimpleName().toString();
	private RelativeLayout mRelProgress;
	private ProgressBar mPbarProgress;
	private TextView mTxtValue;
	private TextView mTxtDescription;

	public ProgressCard(boolean isOpen, Context context, LayoutInflater inflater, LinearLayout linCards) {
		super(isOpen, context, inflater, linCards, linCards.getResources().getString(R.string.hl_progress));

		mRelProgress = (RelativeLayout) mRelCard.findViewById(R.id.card_rel_progress);
		mRelProgress.setVisibility(View.VISIBLE);

		mPbarProgress = (ProgressBar) mRelCard.findViewById(R.id.card_pbar_progress);
		mTxtValue = (TextView) mRelCard.findViewById(R.id.card_txt_progress_value);
		mTxtDescription = (TextView) mRelCard.findViewById(R.id.card_txt_progress_description);
	}

	public void loadDynamicContent(InternalProgressDTO progressDTO) {
		mPbarProgress.setProgress(progressDTO.getPercent());
		mTxtValue.setText(progressDTO.getPercent()+mTxtValue.getResources().getString(R.string.symbol_percent));
		String description = String.format(mTxtDescription.getResources().getString(R.string.progress_description), progressDTO.getWatchedEpisodeCount(), progressDTO.getEpisodeCount());
		mTxtDescription.setText(Html.fromHtml(description), TextView.BufferType.SPANNABLE);
	}
	
	public void setVisibility(int visibility) {
		mRelCard.setVisibility(visibility);
	}

}
