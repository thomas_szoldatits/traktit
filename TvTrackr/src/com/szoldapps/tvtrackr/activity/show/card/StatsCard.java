package com.szoldapps.tvtrackr.activity.show.card;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.szoldapps.tvtrackr.R;
import com.szoldapps.tvtrackr.db.model.Episode;
import com.szoldapps.tvtrackr.db.model.Show;
import com.szoldapps.tvtrackr.dto.response.ResponseStatsDTO;
import com.szoldapps.tvtrackr.exception.WrongObjectReturnedException;
import com.szoldapps.tvtrackr.helper.DialogHelper;
import com.szoldapps.tvtrackr.helper.LogCatHelper;
import com.szoldapps.tvtrackr.helper.NumbersHelper;
import com.szoldapps.tvtrackr.service.listener.ServiceResponseListener;

public class StatsCard extends DefaultCard {

	public static final String LOGTAG = LogCatHelper.getAppTag()+StatsCard.class.getSimpleName().toString();

	private LinearLayout mLinStats;
	private TextView mTxtPlays;
	private TextView mTxtWatchers;
	private TextView mTxtCollected;
	private TextView mTxtLists;

	private Activity mActivity;

	public StatsCard(boolean isOpen, Context context, LayoutInflater inflater, LinearLayout linCards, Activity activity) {
		super(isOpen, context, inflater, linCards, linCards.getResources().getString(R.string.hl_stats));

		mActivity = activity;

		mLinStats = (LinearLayout) mRelCard.findViewById(R.id.card_lin_stats);
		mLinStats.setVisibility(View.VISIBLE);

		mTxtPlays = (TextView) mRelCard.findViewById(R.id.card_txt_stats_plays_value);
		mTxtWatchers = (TextView) mRelCard.findViewById(R.id.card_txt_stats_watchers_value);
		mTxtCollected = (TextView) mRelCard.findViewById(R.id.card_txt_stats_collected_value);
		mTxtLists = (TextView) mRelCard.findViewById(R.id.card_txt_stats_lists_value);
	}

	private ServiceResponseListener mListener = new ServiceResponseListener() {
		@Override public void onProgress(int bytesWritten, int totalSize) { }
		@Override
		public void onSuccess(Object o) {
			if(o instanceof ResponseStatsDTO) {
				fillStats((ResponseStatsDTO) o);
			} else {
				onFailure(new WrongObjectReturnedException(ResponseStatsDTO.class));
			}
		}
		@Override
		public void onFailure(Throwable throwable) {
			showError(throwable);
		}
	};

	/**
	 * Loads Show Stats. At first from local DB (actually from show object), then from trakt (if possible).
	 * 
	 * @param show
	 * @param showService
	 */
	public void loadDynamicContent(Show show) {
		fillStats(NumbersHelper.format(show.getStatsPlays()), 
				NumbersHelper.format(show.getStatsWatchers()), 
				NumbersHelper.format(show.getStatsCollected()), 
				NumbersHelper.format(show.getStatsLists()));
		mAsyncAccessService.downloadStats(show, mListener);
	}

	/**
	 * Loads Episode Stats. At first from local DB (actually from episode object), then from trakt (if possible).
	 * 
	 * @param episode
	 * @param episodeService
	 */
	public void loadDynamicContent(Episode episode) {
		fillStats(NumbersHelper.format(episode.getStatsPlays()), 
				NumbersHelper.format(episode.getStatsWatchers()), 
				NumbersHelper.format(episode.getStatsCollected()), 
				NumbersHelper.format(episode.getStatsLists()));
		mAsyncAccessService.downloadStats(episode, mListener);
	}

	private void fillStats(ResponseStatsDTO dto) {
		fillStats(NumbersHelper.format(dto.getPlays()), 
				NumbersHelper.format(dto.getWatchers()), 
				NumbersHelper.format(dto.getCollection().getAll()), 
				NumbersHelper.format(dto.getLists().getAll()));
	}

	private void fillStats(String plays, String watchers, String collected, String lists) {
		mTxtPlays.setText(plays);
		mTxtWatchers.setText(watchers);
		mTxtCollected.setText(collected);
		mTxtLists.setText(lists);
	}

	private void showError(Throwable throwable) {
		if(DialogHelper.isInternetError(throwable)) {
			// Not needed, - is shown instead of actual value
		} else {
			DialogHelper.handleDialogErrors(throwable, mActivity);
		}
	}
}
