package com.szoldapps.tvtrackr.activity.show.card;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.j256.ormlite.dao.ForeignCollection;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.szoldapps.tvtrackr.R;
import com.szoldapps.tvtrackr.db.model.Actor;
import com.szoldapps.tvtrackr.db.model.ActorShow;
import com.szoldapps.tvtrackr.db.model.Show;
import com.szoldapps.tvtrackr.helper.DialogHelper;
import com.szoldapps.tvtrackr.helper.ImageLoaderHelper;
import com.szoldapps.tvtrackr.helper.ImageURLHelper;
import com.szoldapps.tvtrackr.helper.LinkingHelper;
import com.szoldapps.tvtrackr.helper.LogCatHelper;

public class CastCard extends DefaultCard {

	public static final String LOGTAG = LogCatHelper.getAppTag()+CastCard.class.getSimpleName().toString();

	private LinearLayout mLinCast;
	private Show mShow;
	private Activity mActivity;

	public CastCard(boolean isOpen, Context context, LayoutInflater inflater, LinearLayout linCards, 
			Activity activity) {
		super(isOpen, context, inflater, linCards, linCards.getResources().getString(R.string.hl_cast));
		mLinCast = (LinearLayout) mRelCard.findViewById(R.id.card_lin_cast);
		mLinCast.setVisibility(View.VISIBLE);
		mActivity = activity;
	}

	public void loadDynamicContent(final Show show) {
		mShow = show;
		mLinCast.removeAllViews();
		ForeignCollection<ActorShow> actorShows = mShow.getActorShows();
		TextView txtOverview = (TextView) mRelCard.findViewById(R.id.card_txt_overview);
		if(actorShows.isEmpty()) {
			txtOverview.setText(mContext.getString(R.string.cast_not_found));
			mLinCast.setVisibility(View.GONE);
			txtOverview.setVisibility(View.VISIBLE);
			return;
		} else {
			txtOverview.setVisibility(View.GONE);
			mLinCast.setVisibility(View.VISIBLE);
		}
		
		for (ActorShow actorShow : actorShows) {
			final Actor actor = actorShow.getActor();
			// List Item UI Elements
			RelativeLayout relListItemActor = 
					(RelativeLayout) mInflater.inflate(R.layout.list_item_actor, mLinCast, false);

			// Link to ActorActivity (only if Actor Name is NOT unknown)
			if(actor.getName().equals("Unknown Actor")) {
				relListItemActor.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						DialogHelper.showInfoShort(mContext.getString(R.string.cast_toast_unknown_actor), mContext);
					}
				});
			} else {
				relListItemActor.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						LinkingHelper.goToActor(actor, show, mActivity);
					}
				});
			}

			// Headshot	(only load Headshot, if it's NOT the default image url)
			ImageView imgHeadshot = (ImageView) relListItemActor.findViewById(R.id.li_actors_img_headshot);
			if(!ImageURLHelper.isTraktDefaultImgURL(actor.getHeadshot())) {
				ImageLoader.getInstance().displayImage(actor.getHeadshot(), imgHeadshot, 
						ImageLoaderHelper.getDIOPosterOrHeadShot(mContext));
			}


			// Actor Name
			TextView txtName = (TextView) relListItemActor.findViewById(R.id.li_actor_txt_name);
			String name = actor.getName();
			if(name==null || name.isEmpty()) {
				name = mContext.getResources().getString(R.string.cast_n_a);
			}
			txtName.setText(name);

			// Character
			String character = actorShow.getCharacter();
			if(character==null || character.isEmpty()) {
				character = mContext.getResources().getString(R.string.cast_n_a);
			}
			TextView txtCharacter = (TextView) relListItemActor.findViewById(R.id.li_actor_txt_character);
			txtCharacter.setText(String.format(mContext.getString(R.string.cast_as), character));

			mLinCast.addView(relListItemActor);
		}

	}

}
