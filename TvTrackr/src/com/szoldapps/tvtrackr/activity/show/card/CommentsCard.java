package com.szoldapps.tvtrackr.activity.show.card;

import java.util.List;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.szoldapps.tvtrackr.R;
import com.szoldapps.tvtrackr.activity.show.ShowFragment;
import com.szoldapps.tvtrackr.activity.show.episodes.EpisodeFragment;
import com.szoldapps.tvtrackr.db.model.Episode;
import com.szoldapps.tvtrackr.db.model.Show;
import com.szoldapps.tvtrackr.dto.internal.InternalCommentsDTO;
import com.szoldapps.tvtrackr.dto.response.ResponseCommentsDTO;
import com.szoldapps.tvtrackr.exception.WrongObjectReturnedException;
import com.szoldapps.tvtrackr.helper.DateHelper;
import com.szoldapps.tvtrackr.helper.DialogHelper;
import com.szoldapps.tvtrackr.helper.ImageLoaderHelper;
import com.szoldapps.tvtrackr.helper.ImageURLHelper;
import com.szoldapps.tvtrackr.helper.KeyBoardHelper;
import com.szoldapps.tvtrackr.helper.LogCatHelper;
import com.szoldapps.tvtrackr.service.listener.ServiceResponseListener;

public class CommentsCard extends DefaultCard {

	public static final String LOGTAG = LogCatHelper.getAppTag()+CommentsCard.class.getSimpleName().toString();

	private ShowFragment mShowFragment;
	private Show mShow;

	private EpisodeFragment mEpisodeFragment;
	private Episode mEpisode;

	// Cache Vars
	private List<ResponseCommentsDTO> mComments;
	private Integer mCommentsShownCount = 0;

	// UI Elements
	private ProgressBar mPbarLoading;
	private RelativeLayout mRelComments;
	private LinearLayout mLinCommentsList;
	private EditText mEtxtComments;
	private RelativeLayout mRelCommentsLoadMore;
	private RelativeLayout mRelLoadFiveMore;
	private RelativeLayout mRelLoadAll;
	private ProgressBar mPbarLoadFiveMore;
	private ImageView mImgLoadFiveMore;
	private ImageView mImgLoadAll;
	private ProgressBar mPbarLoadAll;
	private CheckBox mChkSpoilerAlert;
	private ImageView mImgCommentSend;
	private ProgressBar mPbarCommentSendLoading;

	// optional HL, used for no internet
	private RelativeLayout mRelHlOptional;
	private TextView mTxtHlOptional;

	private TextView mTxtNoComments;



	private CommentsCard(boolean isOpen, Context context, LayoutInflater inflater, LinearLayout linCards) {
		super(isOpen, context, inflater, linCards, linCards.getResources().getString(R.string.hl_comments));

		// Init State UI Element
		mPbarLoading = (ProgressBar) mRelCard.findViewById(R.id.card_pbar_loading);
		mPbarLoading.setVisibility(View.VISIBLE);

		mRelHlOptional = (RelativeLayout) mRelCard.findViewById(R.id.card_rel_hl_optional);
		mTxtHlOptional = (TextView) mRelCard.findViewById(R.id.card_txt_hl_optional);
		mTxtHlOptional.setText(mContext.getString(R.string.comments_n_a_offline));
		
		// Other UI Elements
		mRelComments = (RelativeLayout) mRelCard.findViewById(R.id.card_rel_comments);
		mLinCommentsList = (LinearLayout) mRelCard.findViewById(R.id.card_lin_comments_list);
		mTxtNoComments = (TextView) mRelCard.findViewById(R.id.card_txt_comments_no);

		mRelCommentsLoadMore = (RelativeLayout) mRelCard.findViewById(R.id.card_rel_comments_load_more);

		mRelLoadFiveMore = (RelativeLayout) mRelCard.findViewById(R.id.card_rel_comments_load_more_loading);
		mRelLoadAll = (RelativeLayout) mRelCard.findViewById(R.id.card_rel_comments_load_more_loading_all);

		// Comments Loading Animations
		mPbarLoadFiveMore = (ProgressBar) mRelCard.findViewById(R.id.card_pbar_comments_load_more_loading);
		mImgLoadFiveMore = (ImageView) mRelCard.findViewById(R.id.card_img_comments_load_more_loading);
		mPbarLoadAll = (ProgressBar) mRelCard.findViewById(R.id.card_pbar_comments_load_more_loading_all);
		mImgLoadAll = (ImageView) mRelCard.findViewById(R.id.card_img_comments_load_more_loading_all);

		// Comment writing
		mEtxtComments = (EditText) mRelCard.findViewById(R.id.card_etxt_comments);
		mChkSpoilerAlert = (CheckBox) mRelCard.findViewById(R.id.card_chk_comments_spoiler_alert);
		mImgCommentSend = (ImageView) mRelCard.findViewById(R.id.card_img_comment_send);
		mPbarCommentSendLoading = (ProgressBar) mRelCard.findViewById(R.id.card_pbar_comment_send);
	}

	public CommentsCard(boolean isOpen, Context context, LayoutInflater inflater, LinearLayout linCards, 
			ShowFragment fragment) {
		this(isOpen, context, inflater, linCards);
		mShowFragment = fragment;
	}

	public CommentsCard(boolean isOpen, Context context, LayoutInflater inflater, LinearLayout linCards, 
			EpisodeFragment fragment) {
		this(isOpen, context, inflater, linCards);
		mEpisodeFragment = fragment;
	}

	@Override
	public OnClickListener getCardOnClickListener() {
		return new OnClickListener() {
			@Override
			public void onClick(View v) {
				if(mRelContent.getVisibility() == View.VISIBLE) {
					mRelContent.setVisibility(View.GONE);
					mImgExpColl.setImageDrawable(v.getResources().getDrawable(R.drawable.ic_expand));
				} else {
					mRelContent.setVisibility(View.VISIBLE);
					mImgExpColl.setImageDrawable(v.getResources().getDrawable(R.drawable.ic_collapse));
					loadDynamicContent();
					openCard();
				}
			}
		};
	}

	public void showAndServiceAreReadyNow(boolean open, Show show) {
		mShow = show;
		if(open) {
			loadDynamicContent();
			this.openCard();
		}
	}

	public void showAndServiceAreReadyNow(boolean open, Episode episode) {
		mEpisode = episode;
		if(open) {
			loadDynamicContent();
			this.openCard();
		}
	}

	/**
	 * Loads Show Stats
	 * @param show
	 * @param showService
	 */
	public void loadDynamicContent(){
		if(mShow != null || mEpisode != null) {
			mRelHlOptional.setVisibility(View.INVISIBLE);
			mRelLoadFiveMore.setTag(5);
			mRelLoadAll.setTag(-1);
			OnClickListener loadCommentsListener = new OnClickListener() {
				@Override
				public void onClick(View v) {
					int x = (Integer) v.getTag();
					if(x==-1) {
						mCommentsShownCount = -1;
						mPbarLoadAll.setVisibility(View.VISIBLE);
						mImgLoadAll.setVisibility(View.INVISIBLE);
					} else {
						mCommentsShownCount += 5;
						mPbarLoadFiveMore.setVisibility(View.VISIBLE);
						mImgLoadFiveMore.setVisibility(View.INVISIBLE);
					}
					loadComments();
				}
			};
			mRelLoadFiveMore.setOnClickListener(loadCommentsListener);
			mRelLoadAll.setOnClickListener(loadCommentsListener);

			mImgCommentSend.setEnabled(false);

			mEtxtComments.setOnFocusChangeListener(new OnFocusChangeListener() {
				@Override
				public void onFocusChange(View v, boolean hasFocus) {
					if(hasFocus) {
						mChkSpoilerAlert.setVisibility(View.VISIBLE);
					} else {
						mChkSpoilerAlert.setVisibility(View.GONE);  
					}
				}
			});
			mEtxtComments.addTextChangedListener(new TextWatcher () {
				@Override public void beforeTextChanged(CharSequence s, int start, int count, int after) { }
				@Override public void onTextChanged(CharSequence s, int start, int before, int count) { }
				@Override
				public void afterTextChanged(Editable s) {
					if(s.toString().trim().length() > 0) {
						mImgCommentSend.setEnabled(true);
					}
				}
			});
			mImgCommentSend.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					mImgCommentSend.setVisibility(View.INVISIBLE);
					mPbarCommentSendLoading.setVisibility(View.VISIBLE);
					mEtxtComments.setEnabled(false);
					KeyBoardHelper.hideKeyboard(v, mContext);
					sendComment();
				}
			});
			loadComments();
		}
	}

	private void sendComment() {
		String msg = mEtxtComments.getText().toString();
		boolean spoiler = mChkSpoilerAlert.isChecked();
		ServiceResponseListener listener = new ServiceResponseListener() {
			@Override public void onProgress(int bytesWritten, int totalSize) { /* not needed */}
			@Override
			public void onSuccess(Object o) {
				if(o instanceof Boolean) {
					mEtxtComments.setText("");
					mImgCommentSend.setVisibility(View.VISIBLE);
					mPbarCommentSendLoading.setVisibility(View.GONE);
					mEtxtComments.setEnabled(true);
					DialogHelper.showInfoShort(mContext.getString(R.string.comments_successful_upload), mContext);
					loadComments();
				} else {
					onFailure(new WrongObjectReturnedException(Boolean.class));
				}
			}

			@Override
			public void onFailure(Throwable throwable) {
				showError(throwable);
			}
		};
		if(mEpisode == null) {
			mAsyncAccessService.sendComment(mShow, msg, spoiler, listener);
		} else {
			mAsyncAccessService.sendComment(mEpisode, msg, spoiler, listener);
		}
	}

	private void loadComments() {
		ServiceResponseListener listener = new ServiceResponseListener() {

			@Override public void onProgress(int bytesWritten, int totalSize) { /* not needed */}

			@Override
			public void onSuccess(Object o) {
				if(o instanceof InternalCommentsDTO) {
					InternalCommentsDTO dto = (InternalCommentsDTO) o;
					String username = dto.getUsername();
					mComments = dto.getComments();
					if(mCommentsShownCount == 0) {
						mCommentsShownCount = 5;
					}
					if(mComments.size() > 0) {
						mTxtNoComments.setVisibility(View.GONE);
						mLinCommentsList.setVisibility(View.VISIBLE);
						mRelCommentsLoadMore.setVisibility(View.VISIBLE);
						showComments(mCommentsShownCount, username);
					} else {
						showNoComments();
					}
				} else {
					onFailure(new WrongObjectReturnedException(InternalCommentsDTO.class));
				}
			}

			@Override
			public void onFailure(Throwable throwable) {
				showError(throwable);
			}
		};
		if(mEpisode == null) {
			mAsyncAccessService.getShowComments(mShow, listener);
		} else {
			mAsyncAccessService.getEpisodeComments(mEpisode, listener);
		}

	}
	
	private void showNoComments() {
		mTxtNoComments.setVisibility(View.VISIBLE);
		mLinCommentsList.setVisibility(View.GONE);
		mPbarLoading.setVisibility(View.GONE);
		mRelComments.setVisibility(View.VISIBLE);
		mRelCommentsLoadMore.setVisibility(View.GONE);
	}

	private void showComments(int count, String username) {
		//        Log.i(LOGTAG, "count = "+ count);
		//        Log.i(LOGTAG, "mcomments size = "+ mComments.size());
		if(count == -1) {
			mRelCommentsLoadMore.setVisibility(View.GONE);
			count = mComments.size();
		}

		if((mComments.size()-mCommentsShownCount) < 5) {
			mRelLoadFiveMore.setVisibility(View.GONE);
		}

		int linCommentsListCount = mLinCommentsList.getChildCount()-1;
		for (ResponseCommentsDTO comment : mComments) {
			if(count-- > 0) {
				RelativeLayout relListItemComments = null;
				if(linCommentsListCount >= 0) { //overwrite the list elements, that are already there
					relListItemComments = (RelativeLayout) mLinCommentsList.getChildAt(linCommentsListCount);
					linCommentsListCount--;
				} else {
					relListItemComments = 
							(RelativeLayout) mInflater.inflate(R.layout.list_item_comments, mLinCommentsList, false);
					mLinCommentsList.addView(relListItemComments, 0);
				}
				ImageView imgAvatar = (ImageView) relListItemComments.findViewById(R.id.li_comments_img_avatar);
				TextView txtUsername = (TextView) relListItemComments.findViewById(R.id.li_comments_txt_username);
				TextView txtMsg = (TextView) relListItemComments.findViewById(R.id.li_comments_txt_msg);
				TextView txtDate = (TextView) relListItemComments.findViewById(R.id.li_comments_txt_date);

				boolean currentUserComment = comment.user.username.equals(username);
				txtUsername.setText(comment.user.username);
				if(comment.spoiler && !currentUserComment) {
					txtMsg.setText(mContext.getResources().getString(R.string.comments_spoiler_alert));
					txtMsg.setTag(comment.text);
					txtMsg.setTextColor(mContext.getResources().getColor(R.color.red));
					txtMsg.setOnClickListener(new OnClickListener() {
						@Override
						public void onClick(View v) {
							TextView txt = (TextView) v;
							txt.setText((String) v.getTag());
							txt.setTextColor(mContext.getResources().getColor(R.color.black));
						}
					});
				} else {
					txtMsg.setText(comment.text);
				}
				txtDate.setText(DateHelper.getTimeAgoInS(comment.inserted, mContext));

				if(currentUserComment) {
					txtUsername.setTextColor(mContext.getResources().getColor(R.color.blue));
				} else {
					txtUsername.setTextColor(mContext.getResources().getColor(R.color.black));
				}

				String imgURL = comment.user.avatar;
				if(!ImageURLHelper.isTraktDefaultImgURL(imgURL)) {
					ImageLoader.getInstance().displayImage(imgURL, imgAvatar, ImageLoaderHelper.getDIOAvatar(mContext));
				} else {
					imgAvatar.setImageDrawable(mContext.getResources().getDrawable(R.drawable.default_avatar));
				}

			} else { break; }

		}

		mPbarLoading.setVisibility(View.GONE);
		mRelComments.setVisibility(View.VISIBLE);

		mPbarLoadFiveMore.setVisibility(View.GONE);
		mPbarLoadAll.setVisibility(View.GONE);
		mImgLoadFiveMore.setVisibility(View.VISIBLE);
		mImgLoadAll.setVisibility(View.VISIBLE);

		if(mCommentsShownCount == 5) {
			if(mEpisode == null) {
				mShowFragment.scrollToView(this.getCard());
			} else {
				mEpisodeFragment.focusOnView(this.getCard());
			}
		}
	}

	private void showError(Throwable throwable) {
		if(DialogHelper.isInternetError(throwable)) {
			mRelHlOptional.setVisibility(View.VISIBLE);
			closeCard();
			DialogHelper.showInfoShort(mContext.getString(R.string.comments_n_a_offline_toast), mContext);
		} else {
			if(mEpisode == null) {
				DialogHelper.handleDialogErrors(throwable, mShowFragment.getActivity());
			} else {
				DialogHelper.handleDialogErrors(throwable, mEpisodeFragment.getActivity());
			}
		}
	}

}
