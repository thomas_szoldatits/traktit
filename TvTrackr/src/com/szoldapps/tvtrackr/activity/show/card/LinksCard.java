package com.szoldapps.tvtrackr.activity.show.card;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;

import com.szoldapps.tvtrackr.R;
import com.szoldapps.tvtrackr.db.model.Actor;
import com.szoldapps.tvtrackr.db.model.Episode;
import com.szoldapps.tvtrackr.db.model.Season;
import com.szoldapps.tvtrackr.db.model.Show;
import com.szoldapps.tvtrackr.helper.LogCatHelper;

public class LinksCard {

    public static final String LOGTAG = LogCatHelper.getAppTag()+LinksCard.class.getSimpleName().toString();

    private Activity mActivity;

    private LinearLayout mRelCard;

    private Button mBtnTrakt;
    private Button mBtnImdb;
    private Button mBtnTvdb;
    @SuppressWarnings("unused")
	private Button mBtnWikipedia;
    private Button mBtnTmdb;

    public LinksCard(LayoutInflater inflater, LinearLayout linCards, Activity activity) {
        mActivity = activity;

        mRelCard = (LinearLayout) inflater.inflate(R.layout.card_links, linCards, false);

        mBtnTrakt = (Button) mRelCard.findViewById(R.id.card_links_btn_trakt);
        mBtnImdb = (Button) mRelCard.findViewById(R.id.card_links_btn_imdb);
        mBtnTvdb = (Button) mRelCard.findViewById(R.id.card_links_btn_tvdb);
        mBtnWikipedia = (Button) mRelCard.findViewById(R.id.card_links_btn_wikipedia);
        mBtnTmdb = (Button) mRelCard.findViewById(R.id.card_links_btn_tmdb);
    }

    public void loadDynamicContent(final Show show) {
        
        String traktLink = null;
        if(show.getSlug() != null) {
            traktLink = getLink(LinkType.TRAKT, show.getSlug());
        }
        
        String imdbLink = null;
        if(show.getImdbID() != null) {
            imdbLink = getLink(LinkType.IMDB, show.getImdbID());
        }
        
        String tvdbLink = null;
        if(show.getTvdbID() != null) {
            tvdbLink = getLink(LinkType.TVDB_SHOW, show.getTvdbID().toString());
        }
        
        setLinksAndShowButtons(traktLink, imdbLink, tvdbLink, null);

    }

    public void loadDynamicContent(Episode episode) {

        String traktLink = null;
        if(episode.getSeason().getShow().getSlug() != null) {
            Season season = episode.getSeason();
            Show show = season.getShow();
            traktLink = getLink(LinkType.TRAKT, show.getSlug() + "/season/"+season.getSeasonNr()+"/episode/"+episode.getNumber());
        }
        
        String imdbLink = null;
        if(episode.getImdbID() != null) {
            imdbLink = getLink(LinkType.IMDB, episode.getImdbID());
        }
        
        String tvdbLink = null;
        if(episode.getTvdbID() != null) {
            tvdbLink = getLink(LinkType.TVDB_EPISODE, episode.getTvdbID().toString());
        }
        
        setLinksAndShowButtons(traktLink, imdbLink, tvdbLink, null);
    }
    
    public void loadDynamicContent(Actor actor) {

        String traktLink = null;
        if(actor.getUrl() != null) {
            traktLink = actor.getUrl();
        }
        
        String tmdbLink = actor.getTmd_id().toString();
        if(tmdbLink != null) {
            tmdbLink = getLink(LinkType.ACTOR_TMDB, tmdbLink);
        }
        
        setLinksAndShowButtons(traktLink, null, null, tmdbLink);
    }
    
    private void setLinksAndShowButtons(final String traktLink, final String imdbLink, final String tvdbLink, final String tmdbLink) {
        if(traktLink != null) {
            mBtnTrakt.setVisibility(View.VISIBLE);
            mBtnTrakt.setOnClickListener(new OnClickListener() {
                @Override public void onClick(View v) { openLinkInBrowser(traktLink); } });
        }
        if(imdbLink != null) {
            mBtnImdb.setVisibility(View.VISIBLE);
            mBtnImdb.setOnClickListener(new OnClickListener() {
                @Override public void onClick(View v) { openLinkInBrowser(imdbLink); } });
        }
        if(tvdbLink != null) {
            mBtnTvdb.setVisibility(View.VISIBLE);
            mBtnTvdb.setOnClickListener(new OnClickListener() {
                @Override public void onClick(View v) { openLinkInBrowser(tvdbLink); } });
        }
        if(tmdbLink != null) {
            mBtnTmdb.setVisibility(View.VISIBLE);
            mBtnTmdb.setOnClickListener(new OnClickListener() {
                @Override public void onClick(View v) { openLinkInBrowser(tmdbLink); } });
        }
    }

    private static enum LinkType {
        TRAKT, IMDB, TVDB_SHOW, WIKIPEDIA, TVDB_EPISODE, ACTOR_TMDB;
    }

    private String getLink(LinkType type, String param) {
        switch (type) {
        case ACTOR_TMDB:
            return "http://www.themoviedb.org/person/"+param;
        case TRAKT:
            return "http://trakt.tv/show/" + param;
        case IMDB:
            return "http://www.imdb.com/title/" + param + "/";
        case TVDB_SHOW:
            return "http://thetvdb.com/?tab=series&id=" + param;
        case TVDB_EPISODE:
            return "http://thetvdb.com/?tab=episode&id=" + param;
        default:
            return null;
        }
    }


    private void openLinkInBrowser(String url) {
        Log.i(LOGTAG, "Opening url:'" + url+"' in Browser");
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        mActivity.startActivity(browserIntent);
    }

    public LinearLayout getCard() {
        return mRelCard;
    }
}
