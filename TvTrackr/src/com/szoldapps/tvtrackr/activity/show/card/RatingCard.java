package com.szoldapps.tvtrackr.activity.show.card;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.RatingBar.OnRatingBarChangeListener;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.szoldapps.tvtrackr.R;
import com.szoldapps.tvtrackr.activity.show.ShowFragment;
import com.szoldapps.tvtrackr.activity.show.episodes.EpisodeFragment;
import com.szoldapps.tvtrackr.db.model.Episode;
import com.szoldapps.tvtrackr.db.model.Rating;
import com.szoldapps.tvtrackr.db.model.Show;
import com.szoldapps.tvtrackr.helper.DialogHelper;
import com.szoldapps.tvtrackr.helper.LogCatHelper;
import com.szoldapps.tvtrackr.service.listener.ServiceResponseListener;

public class RatingCard extends DefaultCard {

	public static final String LOGTAG = LogCatHelper.getAppTag()+RatingCard.class.getSimpleName().toString();
	private RatingBar mRbarRatingBar;
	private RelativeLayout mRelRatingHlOptional;
	private TextView mTxtRatingValue;
	private ImageView mImgRatingExpColl;
	private RelativeLayout mRelRating;
	private ImageView mImgRatingCancel;
	private ImageView mImgRatingSave;
	private View mVdivRating1;
	private View mVdivRating2;
	private ProgressBar mPbarRatingProgress;
	private Show mShow;
	private ShowFragment mShowFragment;
	private Episode mEpisode;
	private EpisodeFragment mEpisodeFragment;
	private boolean mShowMode;

	public RatingCard(boolean isOpen, Context context, LayoutInflater inflater, LinearLayout linCards) {
		super(isOpen, context, inflater, linCards, linCards.getResources().getString(R.string.hl_rating));

		mRelRating = (RelativeLayout) mRelCard.findViewById(R.id.card_rel_rating);
		mRelRatingHlOptional = (RelativeLayout) mRelCard.findViewById(R.id.card_rel_hl_optional);
		mRbarRatingBar = (RatingBar) mRelCard.findViewById(R.id.card_rbar_rating);
		mTxtRatingValue = (TextView) mRelCard.findViewById(R.id.card_txt_hl_optional);

		mImgRatingExpColl = (ImageView) mRelCard.findViewById(R.id.card_img_expand_collapse);

		mVdivRating1 = (View) mRelCard.findViewById(R.id.card_vdiv_rating_1);
		mVdivRating2 = (View) mRelCard.findViewById(R.id.card_vdiv_rating_2);
		mImgRatingSave = (ImageView) mRelCard.findViewById(R.id.card_img_rating_save);
		mImgRatingCancel = (ImageView) mRelCard.findViewById(R.id.card_img_rating_cancel);
		mPbarRatingProgress = (ProgressBar) mRelCard.findViewById(R.id.card_pbar_rating_progress);

		mRelRating.setVisibility(View.VISIBLE);
		mRelRatingHlOptional.setVisibility(View.VISIBLE);

	}

	public void loadDynamicContent(Episode episode, EpisodeFragment episodeFragment) {
		mEpisode = episode;
		mEpisodeFragment = episodeFragment;
		mShowMode = false;
		loadDynamicContent();
	}

	public void loadDynamicContent(Show show, ShowFragment showFragment) {
		mShow = show;
		mShowFragment = showFragment;
		mShowMode = true;
		loadDynamicContent();
	}

	/**
	 * Gets Rating from local DB and calls {@link #fillRating(Object)}.
	 */
	private void loadDynamicContent() {
		mRbarRatingBar.setOnRatingBarChangeListener(mRatingBarChangeListener);
		mImgRatingSave.setOnClickListener(mRatingBarSaveOnClickListener);
		mImgRatingCancel.setOnClickListener(mRatingBarCancelOnClickListener);

		if(mShowMode) {
			mAsyncAccessService.getRating(mShow, new ServiceResponseListener() {
				@Override public void onProgress(int bytesWritten, int totalSize) { }

				@Override
				public void onSuccess(Object o) {
					fillRating(o);
				}

				@Override
				public void onFailure(Throwable throwable) { 
					showError(throwable); 
				}
			});
		} else {
			mAsyncAccessService.getRating(mEpisode, new ServiceResponseListener() {
				@Override public void onProgress(int bytesWritten, int totalSize) { }

				@Override
				public void onSuccess(Object o) {
					fillRating(o);
				}

				@Override
				public void onFailure(Throwable throwable) { 
					showError(throwable); 
				}
			});
		}
	}


	private void fillRating(Object o) {
		Integer advRating = 0;
		if(o instanceof Rating) {
			Rating rating = (Rating) o;
			if(rating != null) {
				if(rating.getRatingAdvanced() != null) {
					advRating = rating.getRatingAdvanced();
					hideCardContent(true, mImgRatingExpColl, mRelContent);
				} else {
					hideCardContent(false, mImgRatingExpColl, mRelContent);
				}
			}
		} else {
			hideCardContent(false, mImgRatingExpColl, mRelContent);
		}
		setRatingBarValue(advRating);
		mRbarRatingBar.setRating(advRating);
		hideSaveButton(true);
		mPbarRatingProgress.setVisibility(View.INVISIBLE);
	}

	public void hideCardContent(boolean hide, ImageView imgExpColl, RelativeLayout relContent) {
		if(hide) {
			relContent.setVisibility(View.GONE);
			imgExpColl.setImageDrawable(imgExpColl.getResources().getDrawable(R.drawable.ic_expand));
		} else {
			relContent.setVisibility(View.VISIBLE);
			imgExpColl.setImageDrawable(imgExpColl.getResources().getDrawable(R.drawable.ic_collapse));
		}
	}

	private void setRatingBarValue(float rating) {
		if(rating == 0) {
			mTxtRatingValue.setText(mTxtRatingValue.getResources().getString(R.string.rating_value_na));
			hideCancelButton(true);
		} else {
			mTxtRatingValue.setText(String.format(mTxtRatingValue.getResources().getString(R.string.rating_value), (int)rating));
			hideCancelButton(false);
		}
	}

	/**
	 * Sets the Episode rating-percentage & number of votes in the upper info box.
	 */
	private void setRatingsFromTrakt() {
		if(mShowMode) {
			mShowFragment.setShowRatingsFromTrakt(mShow);
		} else {
			mEpisodeFragment.setEpisodeRatingsFromTrakt(mEpisode);
		}
	}

	OnRatingBarChangeListener mRatingBarChangeListener = new OnRatingBarChangeListener() {

		@Override
		public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
			setRatingBarValue(rating);
			hideSaveButton(false);
			if(rating != 0) {
			}
		}
	};

	OnClickListener mRatingBarSaveOnClickListener = new OnClickListener() {
		@Override
		public void onClick(View v) {
			saveRating(mRbarRatingBar.getRating());
		}
	};

	OnClickListener mRatingBarCancelOnClickListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			mRbarRatingBar.setRating(0);
			saveRating(0);
		}
	};

	private void saveRating(float rating) {
		mPbarRatingProgress.setVisibility(View.VISIBLE);
		hideSaveButton(true);
		hideCancelButton(true);
		ServiceResponseListener listener = new ServiceResponseListener() {
			@Override public void onProgress(int bytesWritten, int totalSize) { /* not needed */}
			@Override
			public void onSuccess(Object o) {
				if(mShowMode) {
					if(o instanceof Show) {
						mShow = (Show) o;
					}
				} else {
					if(o instanceof Episode) {
						mEpisode = (Episode) o;
					}
				}

				setRatingsFromTrakt();
				// INFO TOAST
				String msg = "";
				if(mRbarRatingBar.getRating() > 0) {
					hideCancelButton(false);
					msg = String.format(mContext.getString(R.string.rating_toast_saved), (int) mRbarRatingBar.getRating());
				} else {
					msg = mContext.getString(R.string.rating_toast_removed);
				}
				DialogHelper.showInfoShort(msg, mContext);

				mPbarRatingProgress.setVisibility(View.INVISIBLE);
				hideSaveButton(true);
			}

			@Override
			public void onFailure(Throwable throwable) {
				showError(throwable);
			}
		};
		if(mShowMode) {
			mAsyncAccessService.uploadRating(mShow, (int) rating, listener);
		} else {
			mAsyncAccessService.uploadRating(mEpisode, (int) rating, listener);
		}
	}

	private void hideSaveButton(boolean hide) {
		int visibility = View.VISIBLE;
		if(hide) {
			visibility = View.INVISIBLE;
		}
		mImgRatingSave.setVisibility(visibility);
		mVdivRating1.setVisibility(visibility);
	}

	private void hideCancelButton(boolean hide) {
		int visibility = View.VISIBLE;
		if(hide) {
			visibility = View.INVISIBLE;
		}
		mImgRatingCancel.setVisibility(visibility);
		mVdivRating2.setVisibility(visibility);
	}

	private void showError(Throwable throwable) {
		if(DialogHelper.isInternetError(throwable)) {
			DialogHelper.showInfoShort(mContext.getString(R.string.rating_no_internet_error_toast), mContext);
			loadDynamicContent();
		} else {
			if(mShowMode) {
				DialogHelper.handleDialogErrors(throwable, mShowFragment.getActivity());
			} else {
				DialogHelper.handleDialogErrors(throwable, mEpisodeFragment.getActivity());
			}
		}
	}


}
