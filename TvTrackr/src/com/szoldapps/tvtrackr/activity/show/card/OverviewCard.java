package com.szoldapps.tvtrackr.activity.show.card;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.szoldapps.tvtrackr.R;
import com.szoldapps.tvtrackr.helper.LogCatHelper;

public class OverviewCard extends DefaultCard {

    public static final String LOGTAG = LogCatHelper.getAppTag()+OverviewCard.class.getSimpleName().toString();
    private TextView mTxtOverviewContent;
    //    private WebView mWebOverviewContent;

    public OverviewCard(boolean isOpen, Context context, LayoutInflater inflater, LinearLayout linCards) {
        super(isOpen, context, inflater, linCards, linCards.getResources().getString(R.string.hl_overview));

        //        mWebOverviewContent = (WebView) mRelCard.findViewById(R.id.card_web_overview);
        //        mWebOverviewContent.setVisibility(View.VISIBLE);
        mTxtOverviewContent = (TextView) mRelCard.findViewById(R.id.card_txt_overview);
        mTxtOverviewContent.setVisibility(View.VISIBLE);
    }

    public void loadDynamicContent(String content) {
        mTxtOverviewContent.setText(content);
        //        String text = "<html><body><span align='justify'>" + content + "</p></body></html>";
        //        mWebOverviewContent.loadData(text, "text/html", "utf-8");
    }
}
