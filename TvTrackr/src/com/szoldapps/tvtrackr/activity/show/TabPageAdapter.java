package com.szoldapps.tvtrackr.activity.show;

import java.util.List;
import java.util.Locale;

import android.app.Activity;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.szoldapps.tvtrackr.R;

class TabPageAdapter extends FragmentPagerAdapter {

    private List<Fragment> mFragments;
    private Activity mActivity;

    public TabPageAdapter(FragmentManager fm, List<Fragment> fragments, Activity activity) {
        super(fm);
        this.mFragments = fragments;
        this.mActivity = activity;
    }

    @Override 
    public Fragment getItem(int position) {
        return this.mFragments.get(position);
    }

    @Override
    public int getCount() {
        return this.mFragments.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        Locale l = Locale.getDefault();
        switch (position) {
        case 0:
            return mActivity.getString(R.string.show_tab_title_overview).toUpperCase(l);
        case 1:
            return mActivity.getString(R.string.show_tab_title_seasons).toUpperCase(l);
        case 2:
            return mActivity.getString(R.string.show_tab_title_episode).toUpperCase(l);
        }
        return null;
    }
}

