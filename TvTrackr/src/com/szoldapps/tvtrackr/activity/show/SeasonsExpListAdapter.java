package com.szoldapps.tvtrackr.activity.show;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.szoldapps.tvtrackr.R;
import com.szoldapps.tvtrackr.db.model.Episode;
import com.szoldapps.tvtrackr.db.model.Season;
import com.szoldapps.tvtrackr.dto.internal.InternalEpisodeWatchedDTO;
import com.szoldapps.tvtrackr.dto.internal.InternalProgressDTO;
import com.szoldapps.tvtrackr.exception.WrongObjectReturnedException;
import com.szoldapps.tvtrackr.helper.DateHelper;
import com.szoldapps.tvtrackr.helper.DialogHelper;
import com.szoldapps.tvtrackr.helper.ImageLoaderHelper;
import com.szoldapps.tvtrackr.helper.ImageURLHelper;
import com.szoldapps.tvtrackr.helper.LogCatHelper;
import com.szoldapps.tvtrackr.helper.ScaleImageView;
import com.szoldapps.tvtrackr.helper.DateHelper.DateHelperFormat;
import com.szoldapps.tvtrackr.service.async.AsyncAccessService;
import com.szoldapps.tvtrackr.service.async.AsyncAccessServiceImpl;
import com.szoldapps.tvtrackr.service.listener.ServiceResponseListener;

public class SeasonsExpListAdapter extends BaseExpandableListAdapter {

	public static final String LOGTAG = LogCatHelper.getAppTag()+SeasonsExpListAdapter.class.getSimpleName().toString();

	private AsyncAccessService mAsyncAccessService;

	private Context mContext;
	private List<Season> mHeaderList; // header titles
	// child data in format of header title, child title
	private HashMap<Season, List<Episode>> mChildMap;

	private SeasonsFragment mSeasonsFragment;


	public SeasonsExpListAdapter(Context context, List<Season> headerList, HashMap<Season, List<Episode>> childMap, SeasonsFragment seasonsFragment) {
		this.mContext = context;
		this.mHeaderList = headerList;
		this.mChildMap = childMap;
		mAsyncAccessService = AsyncAccessServiceImpl.getInstance(mContext);
		mSeasonsFragment = seasonsFragment;
	}

	/** ---------- GROUP Methods ---------- **/

	@Override
	public Object getGroup(int groupPosition) {
		return mHeaderList.get(groupPosition);
	}

	@Override
	public long getGroupId(int groupPosition) {
		return groupPosition;
	}

	@Override
	public int getGroupCount() {
		return mHeaderList.size();
	}

	@SuppressWarnings("deprecation")
	@SuppressLint("NewApi")
	private void setHeaderBackGround(RelativeLayout relCard, boolean isExpanded) {
		int bottom;
		Drawable drawable;
		if(isExpanded) {
			drawable = mContext.getResources().getDrawable(R.drawable.bg_header_rounded_corners_no_shadow);
			bottom = mContext.getResources().getDimensionPixelSize(R.dimen.margin_horizontal);
		} else {
			drawable = mContext.getResources().getDrawable(R.drawable.bg_card);
			bottom = mContext.getResources().getDimensionPixelSize(R.dimen.margin_vertical_for_footer_incl_shadow_compensation);
		}

		if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN){
			relCard.setBackground(drawable);
		} else {
			relCard.setBackgroundDrawable(drawable);
		}
		int left = mContext.getResources().getDimensionPixelSize(R.dimen.margin_horizontal);
		int top = mContext.getResources().getDimensionPixelSize(R.dimen.margin_horizontal);
		int right = mContext.getResources().getDimensionPixelSize(R.dimen.margin_horizontal);
		relCard.setPadding(left, top, right, bottom);
	}


	@Override
	public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
		//Log.d(LOGTAG, "Group just got refreshed groupPos = "+groupPosition);
		final Season season = (Season) getGroup(groupPosition);
		if (convertView == null) {
			LayoutInflater infalInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = infalInflater.inflate(R.layout.li_seasons_group, null);
		}
		// imgPoster
		ScaleImageView imgPoster = (ScaleImageView) convertView.findViewById(R.id.li_seasons_img_poster);
		String imgURL = ImageURLHelper.getImageURLByImageType(season.getPoster(), ImageURLHelper.POSTER_SMALL);
		ImageLoader.getInstance().displayImage(imgURL, imgPoster, ImageLoaderHelper.getDIOPosterOrHeadShot(mContext));

		// imgExpColl
		ImageView imgExpColl = (ImageView) convertView.findViewById(R.id.li_seasons_img_expand_collapse);
		if(isExpanded) {
			imgExpColl.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_collapse));
		} else {
			imgExpColl.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_expand));
		}

		// Card
		RelativeLayout relCard = (RelativeLayout) convertView.findViewById(R.id.li_seasons_rel_card);
		this.setHeaderBackGround(relCard, isExpanded);

		final TextView txtHeadline = (TextView) convertView.findViewById(R.id.li_seasons_txt_headline);
		final ImageView imgSeenSeason = (ImageView) convertView.findViewById(R.id.li_seasons_img_seen_season);
		final ProgressBar pbarSeenSeason = (ProgressBar) convertView.findViewById(R.id.li_seasons_img_seen_season_loading);
		final TextView txtPercentage = (TextView) convertView.findViewById(R.id.li_seasons_txt_percentage);
		final TextView txtEpsSeen = (TextView) convertView.findViewById(R.id.li_seasons_txt_episodes_seen);
		final ProgressBar pbarProgress = (ProgressBar) convertView.findViewById(R.id.li_seasons_pbar_season_progress);

		showIcSeenLoading(true, pbarSeenSeason, imgSeenSeason);
		mAsyncAccessService.getProgressOfAiredEpisodes(season, new ServiceResponseListener() {
			@Override public void onProgress(int bytesWritten, int totalSize) { }
			@Override
			public void onSuccess(Object o) {
				InternalProgressDTO pDto = (InternalProgressDTO) o;
				// txtEpsSeen
				String episodesSeen = "";
				if(pDto.getAiredEpisodeCount() > 0) {
					episodesSeen = String.format(mContext.getString(R.string.progress_seen_episodes), 
							pDto.getWatchedEpisodeCount(), pDto.getAiredEpisodeCount());
				}
				long unairedCount = pDto.getEpisodeCount() - pDto.getAiredEpisodeCount();
				if(unairedCount > 0) {
					boolean withBrackets = (episodesSeen.isEmpty()) ? false : true;
					episodesSeen += withBrackets ? mContext.getString(R.string.progress_space_bracket_open) : "";
					episodesSeen += String.format(mContext.getString(R.string.progress_unaired_episodes), unairedCount);
					episodesSeen += withBrackets ? mContext.getString(R.string.progress_bracket_close) : "";
				}
				txtEpsSeen.setText(episodesSeen);

				// txtHeadline & imgSeenSeason
				boolean seasonIsSeen = false;
				if(pDto.getPercent() == 100) {
					txtHeadline.setTextColor(mContext.getResources().getColor(R.color.blue));
					imgSeenSeason.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_seen_blue));
					seasonIsSeen = true;
				} else {
					txtHeadline.setTextColor(mContext.getResources().getColor(R.color.black));
					imgSeenSeason.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_seen_gray));
				}

				// txtPercentage
				txtPercentage.setText(pDto.getPercent() + mContext.getString(R.string.symbol_percent));

				// pbarProgress
				pbarProgress.setProgress(pDto.getPercent());

				// setTag for onClickListener
				imgSeenSeason.setTag(seasonIsSeen);

				// hide Loading
				showIcSeenLoading(false, pbarSeenSeason, imgSeenSeason);
			}

			@Override
			public void onFailure(Throwable throwable) {
				showError(throwable);
			}
		});

		imgSeenSeason.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				final boolean seasonIsSeen = (Boolean) imgSeenSeason.getTag();
				showIcSeenLoading(true, pbarSeenSeason, imgSeenSeason);
				mAsyncAccessService.uploadSeasonSeenOrUnseen(!seasonIsSeen, season, new ServiceResponseListener() {
					@Override public void onProgress(int bytesWritten, int totalSize) { /* not needed */}
					@Override
					public void onSuccess(Object o) {
						if(o instanceof Boolean) {
							if(!(Boolean) o) {
								String seen = seasonIsSeen ? mContext.getString(R.string.show_seen_lc)
										: mContext.getString(R.string.show_unseen_lc);
								String msg = String.format(mContext.getString(R.string.season_marked_offline), 
										season.getSeasonNr(), seen);
								DialogHelper.showInfoLong(msg, mSeasonsFragment.getActivity());
							}
							showIcSeenLoading(false, pbarSeenSeason, imgSeenSeason);
							SeasonsExpListAdapter.this.notifyDataSetChanged();
							mSeasonsFragment.notifyParentActivity();
						} else {
							onFailure(new WrongObjectReturnedException(Boolean.class));
						}
					}

					@Override
					public void onFailure(Throwable throwable) {
						showError(throwable);
					}
				});
			}
		});


		// txtHeadline
		if(season.getSeasonNr().equals(0)) {
			txtHeadline.setText(String.format(mContext.getString(R.string.season_specials), season.getSeasonNr()));
		} else {
			txtHeadline.setText(String.format(mContext.getString(R.string.season), season.getSeasonNr()));
		}
		return convertView;
	}

	/** ---------- CHILD Methods ---------- **/

	@Override
	public Episode getChild(int groupPosition, int childPosition) {
		return this.mChildMap.get(this.mHeaderList.get(groupPosition)).get(childPosition);
	}

	@Override
	public long getChildId(int groupPosition, int childPosition) {
		return childPosition;
	}

	@Override
	public int getChildrenCount(int groupPosition) {
		return mChildMap.get(mHeaderList.get(groupPosition)).size();
	}

	@Override
	public View getChildView(int groupPosition, final int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {

		final Episode episode = (Episode) getChild(groupPosition, childPosition);
		if (convertView == null) {
			LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater.inflate(R.layout.li_seasons_item, null);
		}

		// imgEpisodeScreen
		ImageView imgEpisodeScreen = (ImageView) convertView.findViewById(R.id.li_seasons_item_img_screen);
		imgEpisodeScreen.setTag(episode);
		String imgURL = ImageURLHelper.getImageURLByImageType(episode.getScreen(), ImageURLHelper.EPISODES_SMALL);
		ImageLoader.getInstance().displayImage(imgURL, imgEpisodeScreen, ImageLoaderHelper.getDIOScreen(mContext));

		// txtAirDate
		TextView txtAirDate = (TextView) convertView.findViewById(R.id.li_seasons_item_txt_date);
		String dateTimeString = DateHelper.getDateStrWithTimeInS(episode.getFirstAiredUTC(), DateHelperFormat.MEDIUM_DATE_AND_TIME_WITH_WEEKDAY, mContext);
		txtAirDate.setText(dateTimeString);

		// txtEpisodeHeadline
		TextView txtEpisodeHeadline = (TextView) convertView.findViewById(R.id.li_seasons_item_txt_headline);
		txtEpisodeHeadline.setText(episode.getSeasonEpisodeText());

		// txtEpisodeTitle
		TextView txtEpisodeTitle = (TextView) convertView.findViewById(R.id.li_seasons_item_txt_title);
		txtEpisodeTitle.setText(episode.getTitle());

		hasEpisodeBeenAired(episode, txtAirDate, txtEpisodeHeadline, txtEpisodeTitle);

		// imgIcSeen
		final ImageView imgIcSeen = (ImageView) convertView.findViewById(R.id.li_seasons_item_img_ic_seen);
		final ProgressBar pbarIcSeen = (ProgressBar) convertView.findViewById(R.id.li_seasons_item_pbar_ic_seen_loading);


		mAsyncAccessService.isEpisodeSeen(episode, new ServiceResponseListener() {
			@Override public void onProgress(int bytesWritten, int totalSize) { /* not needed */}

			@Override
			public void onSuccess(Object o) {
				if(o instanceof Boolean) {
					boolean isSeen = (Boolean) o;
					//Log.d(LOGTAG, "Episode "+episode.getSeasonEpisodeText()+" isSeen="+isSeen);
					if(isSeen) {
						imgIcSeen.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_seen_blue));
					} else {
						imgIcSeen.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_seen_gray));
					}
				} else {
					onFailure(new WrongObjectReturnedException(Boolean.class));
				}
			}

			@Override
			public void onFailure(Throwable throwable) {
				showError(throwable);
			}
		});


		imgIcSeen.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				showIcSeenLoading(true, pbarIcSeen, imgIcSeen);
				mAsyncAccessService.uploadEpisodeSeenToggle(episode, new ServiceResponseListener() {
					@Override public void onProgress(int bytesWritten, int totalSize) { /* not needed */}
					@Override
					public void onSuccess(Object o) {
						if(o instanceof InternalEpisodeWatchedDTO) {
							InternalEpisodeWatchedDTO dto = (InternalEpisodeWatchedDTO) o;
							if(dto.isOfflineMarked()) {
								Log.v(LOGTAG, "Episode marked offline");
								String seen = dto.isWatched() ? mContext.getString(R.string.show_seen_lc)
										: mContext.getString(R.string.show_unseen_lc);
								String msg = String.format(mContext.getString(R.string.episode_marked_offline), episode.getSeasonEpisodeText(), seen);
								DialogHelper.showInfoShort(msg, mSeasonsFragment.getActivity());
							}
							showIcSeenLoading(false, pbarIcSeen, imgIcSeen);
							SeasonsExpListAdapter.this.notifyDataSetChanged();
							mSeasonsFragment.notifyParentActivity();
						}
					}

					@Override
					public void onFailure(Throwable throwable) {
						showError(throwable);
					}
				});
			}
		});

		// Footer
		RelativeLayout relBottomDivider = (RelativeLayout) convertView.findViewById(R.id.li_seasons_item_rel_bottom_divider);
		View vieFooter = (View) convertView.findViewById(R.id.li_seasons_item_vie_footer);
		if(isLastChild) {
			relBottomDivider.setVisibility(View.VISIBLE);
			vieFooter.setVisibility(View.VISIBLE);
		} else {
			relBottomDivider.setVisibility(View.GONE);
			vieFooter.setVisibility(View.GONE);
		}

		return convertView;
	}

	/**
	 * Checks whether Episode has already aired. If true -> textViews (black). If false -> textViews (gray).
	 * 
	 * @param episode
	 * @param txtAirDate
	 * @param txtEpisodeHeadline
	 * @param txtEpisodeTitle
	 */
	private void hasEpisodeBeenAired(final Episode episode, final TextView txtAirDate,
			final TextView txtEpisodeHeadline, final TextView txtEpisodeTitle) {
		mAsyncAccessService.hasEpisodeBeenAired(episode, new ServiceResponseListener() {
			@Override
			public void onProgress(int bytesWritten, int totalSize) { }

			@Override
			public void onSuccess(Object o) {
				if(o instanceof Boolean) {
					boolean hasAired = (Boolean) o;
					int color = -1;
					if(hasAired) {
						color = mContext.getResources().getColor(R.color.black);
					} else {
						color = mContext.getResources().getColor(R.color.gray);
					}
					txtAirDate.setTextColor(color);
					txtEpisodeHeadline.setTextColor(color);
					txtEpisodeTitle.setTextColor(color);
				} else {
					onFailure(new WrongObjectReturnedException(Boolean.class));
				}

			}

			@Override
			public void onFailure(Throwable throwable) {
				showError(throwable);
			}
		});

	}

	private void showIcSeenLoading(boolean showLoading, ProgressBar pbarIcSeen, ImageView imgSeen) {
		if(showLoading) {
			pbarIcSeen.setVisibility(View.VISIBLE);
			imgSeen.setVisibility(View.INVISIBLE);
		} else {
			pbarIcSeen.setVisibility(View.INVISIBLE);
			imgSeen.setVisibility(View.VISIBLE);
		}
	}

	//    public class ExpListCache {
	//    	ProgressBar pbarSeen;
	//    	ExpandableListAdapter expListAdapter;
	//		private boolean seasonIsSeen;
	//    	public ExpListCache(ProgressBar pbarIcSeen, ExpandableListAdapter expListAdapter) {
	//    		this.pbarSeen = pbarIcSeen;
	//    		this.expListAdapter = expListAdapter;
	//    	}
	//		public ExpListCache(boolean seasonIsSeen, ProgressBar pbarIcSeen, ExpandableListAdapter expListAdapter) {
	//			this.seasonIsSeen = seasonIsSeen;
	//    		this.pbarSeen = pbarIcSeen;
	//    		this.expListAdapter = expListAdapter;
	//		}
	//    }


	/** ---------- Other Methods ---------- **/


	@Override
	public boolean hasStableIds() {
		return false;
	}

	@Override
	public boolean isChildSelectable(int groupPosition, int childPosition) {
		return true;
	}

	public void setFragmentData(ArrayList<Season> headerList, HashMap<Season, List<Episode>> childMap) {
		mHeaderList = headerList;
		mChildMap = childMap;
		notifyDataSetChanged();
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// ----- ERROR -------------------------------------------------------------------------------------------------- //

	private void showError(Throwable throwable) {
		DialogHelper.handleDialogErrors(throwable, mSeasonsFragment.getActivity());
	}
}
