package com.szoldapps.tvtrackr.activity.show.episodes;

import java.util.List;

import android.app.ActionBar;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;

import com.astuetz.PagerSlidingTabStrip;
import com.szoldapps.tvtrackr.R;
import com.szoldapps.tvtrackr.dto.internal.InternalEpisodeInfosDTO;
import com.szoldapps.tvtrackr.exception.IntentExtraException;
import com.szoldapps.tvtrackr.exception.WrongObjectReturnedException;
import com.szoldapps.tvtrackr.helper.Constants;
import com.szoldapps.tvtrackr.helper.DialogHelper;
import com.szoldapps.tvtrackr.helper.LinkingHelper;
import com.szoldapps.tvtrackr.helper.LogCatHelper;
import com.szoldapps.tvtrackr.service.async.AsyncAccessService;
import com.szoldapps.tvtrackr.service.async.AsyncAccessServiceImpl;
import com.szoldapps.tvtrackr.service.listener.ServiceResponseListener;

/**
 * <b>Required Intent Extras:</b>
 * <ul>
 * 		<li>{@link Constants#EXTRA_SHOW_TITLE}</li>
 * 		<li>{@link Constants#EXTRA_SHOW_TVDB_ID}</li>
 * </ul>
 * <b>Optional Intent Extras:</b>
 * <ul>
 * 		<li>{@link Constants#EXTRA_EPISODE_ID} ... sets ViewPager to this EpisodeId, 
 * 		otherwise last Episode is displayed</li>
 * </ul>
 * 
 * @author thomasszoldatits
 *
 */
public class EpisodesActivity extends FragmentActivity {

	public static final String LOGTAG = LogCatHelper.getAppTag()+EpisodesActivity.class.getSimpleName().toString();

	// Activity variables
	private ActionBar mActionBar;

	// Intent Extras
	private int mTvdbId;
	private String mShowTitle;
	private int mEpisodeId;

	// Helper
	private boolean mFirstRun = true;

	// Async Access
	private AsyncAccessService mAsyncAccessService;

	// UI-Elements
	private RelativeLayout mRelLoading;
	private PagerSlidingTabStrip mTabs;
	public ViewPager mPager;
	private EpisodesPageAdapter mPagerAdapter;

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// ----- ON_CREATE ---------------------------------------------------------------------------------------------- //

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_episodes);

		// Load Intent Extras
		mTvdbId = getIntent().getIntExtra(Constants.EXTRA_SHOW_TVDB_ID, -1);
		mShowTitle = getIntent().getStringExtra(Constants.EXTRA_SHOW_TITLE);
		mEpisodeId = getIntent().getIntExtra(Constants.EXTRA_EPISODE_ID, -1);

		Log.d(LOGTAG, "title="+mShowTitle+" eid="+mEpisodeId);

		// Check Intent Extras
		if(mTvdbId == -1 || mShowTitle == null) {
			String msg = "EXTRA_SHOW_TVDB_ID and EXTRA_SHOW_TITLE are required to display EpisodeActivity";
			showError(new IntentExtraException(msg));
			return;
		}

		// Load AsyncAccess
		mAsyncAccessService = AsyncAccessServiceImpl.getInstance(getApplicationContext());

		// Setup ActionBar
		mActionBar = getActionBar();
		mActionBar.setDisplayHomeAsUpEnabled(true);
		mActionBar.setTitle(mShowTitle);

		buildUI();
		loadEpisodeInfosOfShow();

	}

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// ----- BUILD_UI ----------------------------------------------------------------------------------------------- //

	private void buildUI() {
		mRelLoading = (RelativeLayout) findViewById(R.id.loading_rel_container);
		mPager = (ViewPager) findViewById(R.id.episodes_pager);
		mTabs = (PagerSlidingTabStrip) findViewById(R.id.episodes_tabs);
	}


	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// ----- DYNAMIC_CONTENT ---------------------------------------------------------------------------------------- //

	private void fillUIWithData(int position, List<String> tabNameList) {
		if(tabNameList.size() > 0) {
			if(mFirstRun) {
				mPagerAdapter = new EpisodesPageAdapter(getSupportFragmentManager(), tabNameList, mTvdbId);
				mPager.setOffscreenPageLimit(2);
				mPager.setAdapter(mPagerAdapter);
				if(position != -1) {
					mPager.setCurrentItem(position);
				}
				mFirstRun = false;

			}

			// Bind the tabs to the ViewPager
			mTabs.setViewPager(mPager);

			mRelLoading.setVisibility(View.GONE);
			mTabs.setVisibility(View.VISIBLE);
			mPager.setVisibility(View.VISIBLE);
		} else {
			LinkingHelper.goToShowFromEpisodes(mTvdbId, mShowTitle, this);
		}
	}


	private void loadEpisodeInfosOfShow() {
		mAsyncAccessService.getEpisodeInfosOfShow(mTvdbId, mEpisodeId, new ServiceResponseListener() {
			@Override public void onProgress(int bytesWritten, int totalSize) { }
			@Override
			public void onSuccess(Object o) {
				if(o instanceof InternalEpisodeInfosDTO) {
					InternalEpisodeInfosDTO dto = (InternalEpisodeInfosDTO) o;
					fillUIWithData(dto.getPosition(), dto.getTabNameList());
				} else {
					showError(new WrongObjectReturnedException(InternalEpisodeInfosDTO.class));
				}
			}
			@Override
			public void onFailure(Throwable throwable) {
				showError(throwable);
			}
		});
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// ----- MENU --------------------------------------------------------------------------------------------------- //

	@Override
	public boolean onMenuItemSelected(int featureId, MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			LinkingHelper.goToShowFromEpisodes(mTvdbId, mShowTitle, this);
			return true;
		}
		return super.onMenuItemSelected(featureId, item);
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// ----- ERROR -------------------------------------------------------------------------------------------------- //

	/**
	 * Simply calls {@link DialogHelper#handleDialogErrors(Throwable, android.app.Activity)}.
	 * 
	 * @param throwable
	 */
	private void showError(Throwable throwable) {
		DialogHelper.handleDialogErrors(throwable, this);
	}

}
