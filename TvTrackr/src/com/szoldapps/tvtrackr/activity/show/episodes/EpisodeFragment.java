package com.szoldapps.tvtrackr.activity.show.episodes;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.szoldapps.tvtrackr.R;
import com.szoldapps.tvtrackr.activity.show.ShowActivity;
import com.szoldapps.tvtrackr.activity.show.card.CommentsCard;
import com.szoldapps.tvtrackr.activity.show.card.LinksCard;
import com.szoldapps.tvtrackr.activity.show.card.OverviewCard;
import com.szoldapps.tvtrackr.activity.show.card.RatingCard;
import com.szoldapps.tvtrackr.db.model.Episode;
import com.szoldapps.tvtrackr.db.model.Show;
import com.szoldapps.tvtrackr.dto.internal.InternalEpisodeWatchedDTO;
import com.szoldapps.tvtrackr.exception.IntentExtraException;
import com.szoldapps.tvtrackr.exception.WrongObjectReturnedException;
import com.szoldapps.tvtrackr.helper.Constants;
import com.szoldapps.tvtrackr.helper.DateHelper;
import com.szoldapps.tvtrackr.helper.DialogHelper;
import com.szoldapps.tvtrackr.helper.ImageLoaderHelper;
import com.szoldapps.tvtrackr.helper.ImageURLHelper;
import com.szoldapps.tvtrackr.helper.LogCatHelper;
import com.szoldapps.tvtrackr.helper.MenuHelper;
import com.szoldapps.tvtrackr.helper.ScaleImageView;
import com.szoldapps.tvtrackr.helper.Constants.DownloadErrorType;
import com.szoldapps.tvtrackr.helper.Constants.DownloadResponseType;
import com.szoldapps.tvtrackr.helper.DateHelper.DateHelperFormat;
import com.szoldapps.tvtrackr.service.async.AsyncAccessService;
import com.szoldapps.tvtrackr.service.async.AsyncAccessServiceImpl;
import com.szoldapps.tvtrackr.service.intent.DownloadService;
import com.szoldapps.tvtrackr.service.listener.ServiceResponseListener;

public class EpisodeFragment extends Fragment {

	public static final String LOGTAG = LogCatHelper.getAppTag()+EpisodeFragment.class.getSimpleName().toString();

	private LayoutInflater mInflater;
	private Activity mActivity;
	private Context mContext;

	// Extras
	private int mPosition;

	// Services
	private AsyncAccessService mAsyncAccessService;

	// Entities
	private Episode mEpisode;

	// UI-Elements
	private View mRootView;
	private ScrollView mScrEpisode;
	private LinearLayout mLinCards;
	// INFO BOX
	private ScaleImageView mImgScreen;
	private TextView mTxtSeasonEpisode;
	private TextView mTxtTitle;
	// INFO BOX RATING
	private TextView mTxtRatingPercent;
	private TextView mTxtRatingVotes;
	// INFO BOX 3 Params
	private TextView mTxtAired;
	private TextView mTxtCountry;
	private TextView mTxtRuntime;
	// SEEN BUTTON
	private ImageView mImgSeen;
	private ProgressBar mPbarSeen;

	// Cards
	private OverviewCard mOverviewCard;
	//	private StatsCard mStatsCard;
	private CommentsCard mCommentsCard;
	private RatingCard mRatingCard;
	private LinksCard mLinksCard;

	// Menu
	private MenuItem mRefreshMenuItem;
	private boolean mUpdateRunning;
	private int mTvdbId;

	// Error (IMPORTANT: has to be static, otherwise all running EpisodeFragments report the same Error)
	private static AlertDialog sNoInternetDialog;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		mInflater = inflater;
		mActivity = getActivity();
		mContext = mActivity.getApplicationContext();
		mAsyncAccessService = AsyncAccessServiceImpl.getInstance(mContext);

		mRootView = mInflater.inflate(R.layout.fragment_episode_detail, container, false);


		/*
		 *  it's ok to call this, because EpisodeFragment.mPager isn't instatiated until everything is preloaded
		 *  and EpisodeFragment.mPager then calls this
		 */
		if(getArguments() != null) {
			mPosition = getArguments().getInt(Constants.EXTRA_EPISODE_MAP_POSITION, -1);
			mTvdbId = getArguments().getInt(Constants.EXTRA_SHOW_TVDB_ID, -1);
			if(mPosition < 0 || mTvdbId == -1) { 
				showError(new IntentExtraException("mPosition < 0 || tvdbId == -1"));
				return mRootView;
			}

			// Build up static UI
			buildUI();
		} else { 
			showError(new IntentExtraException("No IntentExtras at all")); 
		}

		return mRootView;
	}



	@Override
	public void onResume() {
		// Load Episode
		loadEpisodeByPosition();
		super.onResume();
	}



	private void loadEpisodeByPosition() {
		mAsyncAccessService.getEpisodeByPosition(mPosition, mTvdbId, new ServiceResponseListener() {

			@Override
			public void onProgress(int bytesWritten, int totalSize) { }

			@Override
			public void onSuccess(Object o) {
				if(o instanceof Episode) {
					mEpisode = (Episode) o;
					fillUIWithData();
				} else {
					onFailure(new WrongObjectReturnedException(Episode.class));
				}
			}

			@Override
			public void onFailure(Throwable throwable) {
				showError(throwable);
			}
		});
	}

	private void buildUI() {
		setHasOptionsMenu(true);
		// UI-Elements
		mScrEpisode = (ScrollView) mRootView.findViewById(R.id.episode_detail_scrollView);
		mLinCards = (LinearLayout) mRootView.findViewById(R.id.episode_detail_lin_cards);
		// INFO BOX
		mImgScreen = (ScaleImageView) mRootView.findViewById(R.id.episode_detail_img_screen);
		mTxtSeasonEpisode = (TextView) mRootView.findViewById(R.id.episode_detail_txt_season_episode);
		mTxtTitle = (TextView) mRootView.findViewById(R.id.episode_detail_txt_title);
		// INFO BOX RATING
		mTxtRatingPercent = (TextView) mRootView.findViewById(R.id.episode_detail_txt_rating_percent);
		mTxtRatingVotes = (TextView) mRootView.findViewById(R.id.episode_detail_txt_rating_votes);
		// INFO BOX 3 Params
		mTxtAired = (TextView) mRootView.findViewById(R.id.episode_detail_txt_aired_value);
		mTxtCountry = (TextView) mRootView.findViewById(R.id.episode_detail_txt_country_value);
		mTxtRuntime = (TextView) mRootView.findViewById(R.id.episode_detail_txt_runtime_value);

		// SEEN BUTTON
		mImgSeen = (ImageView) mRootView.findViewById(R.id.episode_detail_img_btn_seen);
		mPbarSeen = (ProgressBar) mRootView.findViewById(R.id.episode_detail_pbar_seen_loading);

		// CARDS
		/** --------- Your Rating Card --------- **/
		mRatingCard = new RatingCard(true, mContext, mInflater, mLinCards);
		mLinCards.addView(mRatingCard.getCard());
		/** --------- Overview Card --------- **/
		mOverviewCard = new OverviewCard(true, mContext, mInflater, mLinCards);
		mLinCards.addView(mOverviewCard.getCard());
		//		/** --------- Stats Card --------- **/
		//		mStatsCard = new StatsCard(true, mContext, mInflater, mLinCards);
		//		mLinCards.addView(mStatsCard.getCard());
		/** --------- Comments Card --------- **/
		mCommentsCard = new CommentsCard(false, mContext, mInflater, mLinCards, this);
		mLinCards.addView(mCommentsCard.getCard());
		/** --------- Links Card --------- **/
		mLinksCard = new LinksCard(mInflater, mLinCards, mActivity);
		mLinCards.addView(mLinksCard.getCard());
	}

	private void fillUIWithData() {
		// INFO BOX
		String imgURL = mEpisode.getScreen();
		if(!ImageURLHelper.isTraktDefaultImgURL(imgURL)) {
			ImageLoader.getInstance().displayImage(imgURL, mImgScreen, ImageLoaderHelper.getDIOScreen(mContext));
		} else {
			mImgScreen.setImageDrawable(mContext.getResources().getDrawable(R.drawable.default_screen));
		}
		mTxtSeasonEpisode.setText(mEpisode.getSeasonEpisodeText());
		mTxtTitle.setText(mEpisode.getTitle());

		// INFO BOX RATING
		setEpisodeRatingsFromTrakt(mEpisode);

		// INFO BOX 3 Params
		Show show = mEpisode.getSeason().getShow();
		mTxtAired.setText(DateHelper.getDateStrWithTimeInS(mEpisode.getFirstAiredUTC(), DateHelperFormat.MEDIUM_DATE_AND_TIME, mActivity));
		mTxtCountry.setText(show.getCountry());
		mTxtRuntime.setText(show.getRuntime() + getResources().getString(R.string.runtime_minutes_abbr));

		// SEEN BUTTON
		loadEpisodeSeen();
		setBtnSeenOnClick();

		// CARDS
		mRatingCard.loadDynamicContent(mEpisode, this);
		mOverviewCard.loadDynamicContent(mEpisode.getOverview());
		//		mStatsCard.loadDynamicContent(mEpisode);	TODO: v1.1: Implement a delay, or something like that
		mCommentsCard.showAndServiceAreReadyNow(false, mEpisode);
		mLinksCard.loadDynamicContent(mEpisode);
	}

	private void loadEpisodeSeen() {
		mAsyncAccessService.isEpisodeSeen(mEpisode, new ServiceResponseListener() {
			@Override
			public void onProgress(int bytesWritten, int totalSize) { }

			@Override
			public void onSuccess(Object o) {
				if(o instanceof Boolean) {
					boolean isSeen = (Boolean) o;
					if(isSeen) {
						mImgSeen.setImageDrawable(getResources().getDrawable(R.drawable.ic_seen_blue));
						mTxtSeasonEpisode.setTextColor(getResources().getColor(R.color.blue));
					} else {
						mImgSeen.setImageDrawable(getResources().getDrawable(R.drawable.ic_seen_gray));
						mTxtSeasonEpisode.setTextColor(getResources().getColor(R.color.black));
					}
					showBtnSeenLoading(false);
				} else {
					onFailure(new WrongObjectReturnedException(Boolean.class));
				}
			}

			@Override
			public void onFailure(Throwable throwable) {
				showError(throwable);
			}
		});
	}

	private void showBtnSeenLoading(boolean b) {
		if(b) {
			mPbarSeen.setVisibility(View.VISIBLE);
			mImgSeen.setVisibility(View.INVISIBLE);
		} else {
			mImgSeen.setVisibility(View.VISIBLE);
			mPbarSeen.setVisibility(View.INVISIBLE);
		}
	}

	private void setBtnSeenOnClick() {
		mImgSeen.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				showBtnSeenLoading(true);
				mAsyncAccessService.uploadEpisodeSeenToggle(mEpisode, new ServiceResponseListener() {
					@Override
					public void onProgress(int bytesWritten, int totalSize) { }

					@Override
					public void onSuccess(Object o) {
						if(o instanceof InternalEpisodeWatchedDTO) {
							InternalEpisodeWatchedDTO dto = (InternalEpisodeWatchedDTO) o;
							if(dto.isOfflineMarked()) {
								Log.v(LOGTAG, "Episode marked offline");
								String seen = dto.isWatched() ? mContext.getString(R.string.show_seen_lc)
										: mContext.getString(R.string.show_unseen_lc);
								String msg = String.format(mContext.getString(R.string.episode_marked_offline), mEpisode.getSeasonEpisodeText(), seen);
								DialogHelper.showInfoShort(msg, EpisodeFragment.this.getActivity());
							}
							loadEpisodeSeen();
						} else {
							onFailure(new WrongObjectReturnedException(InternalEpisodeWatchedDTO.class));
						}

					}

					@Override
					public void onFailure(Throwable throwable) {
						showError(throwable);
					}
				});

			}
		});
	}

	public void setEpisodeRatingsFromTrakt(Episode episode) {
		if(mTxtRatingPercent != null && mTxtRatingVotes != null && episode != null) {
			mTxtRatingPercent.setText(episode.getRatingsPercentage() + getResources().getString(R.string.symbol_percent));
			mTxtRatingVotes.setText(episode.getRatingsVotes() + getResources().getString(R.string.rating_votes));
		}
	}

	public final void focusOnView(final View x){
		new Handler().post(new Runnable() {

			@Override
			public void run() {
				mScrEpisode.scrollTo(0, x.getTop() + getActivity().getActionBar().getHeight()); // + mActivity.mPager.getHeight());
				//                mScrollView.scrollTo(0, mScrollView.getBottom() + getActivity().getActionBar().getHeight() + mActivity.mViewPager.getHeight());
			}
		});
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// ----- DOWNLOAD SERVICE --------------------------------------------------------------------------------------- //

	@Override
	public void onStart() {
		super.onStart();
		mActivity.registerReceiver(receiver, new IntentFilter(Constants.EXTRA_DOWNLOAD_SERVICE_NOTIFICATION));
	}

	@Override
	public void onStop() {
		super.onStop();
		mActivity.unregisterReceiver(receiver);
	}

	/**
	 * 1) Sets {@link ShowActivity#mUpdateRunning}=TRUE
	 * 2) Changes RefreshMenuItem to Loading
	 * 3) Requests Show from {@link DownloadService#requestLoadShow(int, Context)} and 
	 * 
	 * @category download_service
	 */
	private void requestShowFromDownloadService(boolean forceDownload) {
		mUpdateRunning = true;
		MenuHelper.updateStarted(mRefreshMenuItem);
		DownloadService.requestLoadShow(mTvdbId, forceDownload, mActivity.getApplicationContext());
	}

	private BroadcastReceiver receiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			Bundle bundle = intent.getExtras();
			if (bundle != null) {
				if(bundle.getInt(Constants.EXTRA_RESULT) == Activity.RESULT_OK) {
					switch ((DownloadResponseType) intent.getSerializableExtra(Constants.EXTRA_RESPONSE_TYPE)) {
					case ONE_SHOW_UPDATE:
						int tvdbId = bundle.getInt(Constants.EXTRA_SHOW_TVDB_ID);
						if(mTvdbId == tvdbId) {
							boolean completelyDone = intent.getBooleanExtra(Constants.EXTRA_UP2DATE_STATUS, false);
							if(completelyDone) {
								// only end Menu Loading, if complete process is done.
								mUpdateRunning = false;
								MenuHelper.updateEnded(mRefreshMenuItem);
								loadEpisodeByPosition();
							}
						}
						break;
					default: /* DO NOTHING */ break;
					}
				} else {	// AN ERROR HAPPENED
					MenuHelper.updateEnded(mRefreshMenuItem);
					showErrorFromIntentService(
							(DownloadErrorType) intent.getSerializableExtra(Constants.EXTRA_ERROR_TYPE));
				}
			}
		}
	};

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// ----- MENU --------------------------------------------------------------------------------------------------- //

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		inflater.inflate(R.menu.episodes, menu);
		mRefreshMenuItem = menu.findItem(R.id.menu_refresh);
		if(mUpdateRunning) {
			MenuHelper.updateStarted(mRefreshMenuItem);
		}
		super.onCreateOptionsMenu(menu, inflater);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.menu_refresh:
			requestShowFromDownloadService(true);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// ----- ERROR -------------------------------------------------------------------------------------------------- //
	/**
	 * Handles {@link Throwable} Error.
	 * 
	 * @param throwable
	 * @category ERROR
	 */
	public void showError(Throwable throwable) {
		DialogHelper.handleDialogErrors(throwable, mActivity);
	}

	/**
	 * Handles {@link DownloadErrorType} Error.
	 * 
	 * @param errorType
	 * @category ERROR
	 */
	protected void showErrorFromIntentService(DownloadErrorType errorType) {
		switch (errorType) {
		case NO_VALID_REQUEST_TYPE_ERROR:	// should not happen
			Log.e(LOGTAG, "INVALID DownloadRequestType was send to DownloadService!");
			break;
		case NO_INTERNET_ERROR:
			if(sNoInternetDialog == null) {
				sNoInternetDialog = DialogHelper.getNoInternetErrorDialogBuilder(getActivity())
						.setPositiveButton(R.string.error_btn_retry, new AlertDialog.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog, int id) {
								requestShowFromDownloadService(true);
							}
						}).create();
			}
			if(!sNoInternetDialog.isShowing()) {
				sNoInternetDialog.show();
			}
			break;
		default: // handles SQL_ERROR, WRONG_CREDENTIALS_ERROR & UNKNOWN_ERROR
			DialogHelper.handleDialogErrors(errorType, getActivity());
			break;
		}
	}

}
