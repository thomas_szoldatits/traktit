package com.szoldapps.tvtrackr.activity.show.episodes;

import java.util.List;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.szoldapps.tvtrackr.helper.Constants;

class EpisodesPageAdapter extends FragmentPagerAdapter {

    private List<String> mTabNameList;
	private int mTvdbId;

    public EpisodesPageAdapter(FragmentManager fm, List<String> tabNameList, int tvdbId) {
        super(fm);
        mTabNameList = tabNameList;
        mTvdbId = tvdbId;
    }

    @Override 
    public Fragment getItem(int position) {
//      Log.v(LOGTAG, "pos = "+position);
      Bundle args = new Bundle();
      args.putInt(Constants.EXTRA_EPISODE_MAP_POSITION, position);
      args.putInt(Constants.EXTRA_SHOW_TVDB_ID, mTvdbId);
      EpisodeFragment fragment = new EpisodeFragment();
      fragment.setArguments(args);
      return fragment;
    }

    @Override
    public int getCount() {
        return mTabNameList.size();
    }
    
    @Override
    public CharSequence getPageTitle(int position) {
    	if(mTabNameList.get(position) != null) {
    		return mTabNameList.get(position);
    	}
        return "";
    }

}

