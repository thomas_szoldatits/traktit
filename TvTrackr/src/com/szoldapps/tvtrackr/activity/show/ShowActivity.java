package com.szoldapps.tvtrackr.activity.show;

import java.util.ArrayList;
import java.util.List;

import android.app.ActionBar;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.astuetz.PagerSlidingTabStrip;
import com.szoldapps.tvtrackr.R;
import com.szoldapps.tvtrackr.db.model.Show.ShowDownloadStatus;
import com.szoldapps.tvtrackr.helper.Constants;
import com.szoldapps.tvtrackr.helper.Constants.DownloadErrorType;
import com.szoldapps.tvtrackr.helper.Constants.DownloadResponseType;
import com.szoldapps.tvtrackr.helper.DialogHelper;
import com.szoldapps.tvtrackr.helper.LinkingHelper;
import com.szoldapps.tvtrackr.helper.LogCatHelper;
import com.szoldapps.tvtrackr.helper.MenuHelper;
import com.szoldapps.tvtrackr.service.intent.DownloadService;

/**
 * <b>Required Intent Extras:</b>
 * <ul>
 * 		<li>{@link Constants#EXTRA_SHOW_TVDB_ID}</li>
 * 		<li>{@link Constants#EXTRA_SHOW_TITLE}</li>
 * </ul>
 * 
 * @author thomasszoldatits
 *
 */
public class ShowActivity extends FragmentActivity {

	public static final String LOGTAG = LogCatHelper.getAppTag()+ShowActivity.class.getSimpleName().toString();

	// Activity variables
	private ActionBar mActionBar;

	// ViewPager and Adapter
	private TabPageAdapter mPageAdapter;
	public ViewPager mPager;
	private List<Fragment> mFragList;

	// Intent Extras
	protected int mTvdbId;
	private String mTitle;
	//	private int mEpisodeId;

	// Fragments
	private ShowFragment mShowFrag;
	private SeasonsFragment mSeasonFrag;

	// Menu
	private MenuItem mRefreshMenuItem;
	private boolean mUpdateRunning = false;


	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// ----- ON_CREATE ---------------------------------------------------------------------------------------------- //

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_show);

		// Load Intent Extras
		mTvdbId = getIntent().getIntExtra(Constants.EXTRA_SHOW_TVDB_ID, -1);
		mTitle = getIntent().getStringExtra(Constants.EXTRA_SHOW_TITLE);

		if(mTvdbId == -1) {
			Log.e(LOGTAG, "No TVDBID Extra!");
			return;
		}

		// Setup ActionBar
		mActionBar = getActionBar();
		mActionBar.setDisplayHomeAsUpEnabled(true);
		mActionBar.setTitle(mTitle);

		buildUI(mTvdbId);

	}

	private void buildUI(final int tvdbId) {
		mFragList = new ArrayList<Fragment>();
		Bundle args = new Bundle();
		args.putInt(Constants.EXTRA_SHOW_TVDB_ID, tvdbId);
		args.putString(Constants.EXTRA_SHOW_TITLE, mTitle);
		args.putSerializable(Constants.EXTRA_SHOW_DOWNLOAD_STATUS, ShowDownloadStatus.DONE);
		args.putBoolean(Constants.EXTRA_SHOWACTIVITY_BOOL, true);

		mShowFrag = new ShowFragment();
		mShowFrag.setArguments(args);
		mFragList.add(mShowFrag);

		mSeasonFrag = new SeasonsFragment();
		mSeasonFrag.setArguments(args);
		mFragList.add(mSeasonFrag);

		mPageAdapter = new TabPageAdapter(getSupportFragmentManager(), mFragList, this);

		mPager = (ViewPager) findViewById(R.id.viewpager);
		mPager.setAdapter(mPageAdapter);
		mPager.setOffscreenPageLimit(2);

		//		setUpTabs();
		// Bind the tabs to the ViewPager
		PagerSlidingTabStrip tabs = (PagerSlidingTabStrip) findViewById(R.id.show_tabs);
		tabs.setViewPager(mPager);

	}

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// ----- MENU --------------------------------------------------------------------------------------------------- //

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.show, menu);
		mRefreshMenuItem = menu.findItem(R.id.menu_refresh);
		if(mUpdateRunning) {
			MenuHelper.updateStarted(mRefreshMenuItem);
		}
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			LinkingHelper.goToMain(this);
			return true;
		case R.id.menu_refresh:
			requestShowFromDownloadService(true);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}


	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// ----- DOWNLOAD SERVICE --------------------------------------------------------------------------------------- //

	@Override
	protected void onResume() {
		// Request the Show from DownloadService
		requestShowFromDownloadService(false);
		super.onResume();
	}

	@Override
	public void onStart() {
		super.onStart();
		registerReceiver(receiver, new IntentFilter(Constants.EXTRA_DOWNLOAD_SERVICE_NOTIFICATION));
	}

	@Override
	public void onStop() {
		super.onStop();
		unregisterReceiver(receiver);
	}

	/**
	 * 1) Sets {@link ShowActivity#mUpdateRunning}=TRUE
	 * 2) Changes RefreshMenuItem to Loading
	 * 3) Requests Show from {@link DownloadService#requestLoadShow(int, Context)} and 
	 * 
	 * @category download_service
	 */
	private void requestShowFromDownloadService(boolean forceDownload) {
		mUpdateRunning = true;
		MenuHelper.updateStarted(mRefreshMenuItem);
		if(mRefreshMenuItem != null) {
			mRefreshMenuItem.setIcon(getResources().getDrawable(R.drawable.ic_refresh_white));
		}
		DownloadService.requestLoadShow(mTvdbId, forceDownload, getApplicationContext());
	}

	private BroadcastReceiver receiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			Bundle bundle = intent.getExtras();
			if (bundle != null) {
				if(bundle.getInt(Constants.EXTRA_RESULT) == Activity.RESULT_OK) {
					switch ((DownloadResponseType) intent.getSerializableExtra(Constants.EXTRA_RESPONSE_TYPE)) {
					case ONE_SHOW_UPDATE:
						int tvdbId = bundle.getInt(Constants.EXTRA_SHOW_TVDB_ID);
						if(mTvdbId == tvdbId) {
							boolean completelyDone = intent.getBooleanExtra(Constants.EXTRA_UP2DATE_STATUS, false);
							if(!completelyDone) {
								ShowDownloadStatus status = 
										(ShowDownloadStatus) 
										intent.getSerializableExtra(Constants.EXTRA_SHOW_DOWNLOAD_STATUS);
								if(status.equals(ShowDownloadStatus.DONE)) {
									Log.w(LOGTAG, "Show " + tvdbId + 
											" is ready to be grabbed from DB. ShowDownloadStatus="+status);
									// Tell Fragments that Show is ready now
									notifyShowFragment();
									notifySeasonsFragment();
								}
							} else {	// only end Menu Loading, if complete process is done.
								mUpdateRunning = false;
								MenuHelper.updateEnded(mRefreshMenuItem);
							}
						}
						break;
					default: /* DO NOTHING */ break;
					}
				} else {	// AN ERROR HAPPENED
					MenuHelper.updateEnded(mRefreshMenuItem);
					showErrorFromIntentService(
							(DownloadErrorType) intent.getSerializableExtra(Constants.EXTRA_ERROR_TYPE));
				}
			}
		}
	};



	public void notifyShowFragment() {
		mShowFrag.displayShowFromDB();
	}

	public void notifySeasonsFragment() {
		mSeasonFrag.displayShowFromDB();
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// ----- ERROR -------------------------------------------------------------------------------------------------- //
	/**
	 * Handles {@link Throwable} Error.
	 * 
	 * @param throwable
	 * @category ERROR
	 */
	public void showError(Throwable throwable) {
		DialogHelper.handleDialogErrors(throwable, this);
	}

	/**
	 * Handles {@link DownloadErrorType} Error.
	 * 
	 * @param errorType
	 * @category ERROR
	 */
	protected void showErrorFromIntentService(DownloadErrorType errorType) {
		switch (errorType) {
		case NO_VALID_REQUEST_TYPE_ERROR:	// should not happen
			Log.e(LOGTAG, "INVALID DownloadRequestType was send to DownloadService!");
			break;
		case NO_INTERNET_ERROR:
			DialogHelper.showInfoLong(getString(R.string.error_no_internet_show), getApplicationContext());
			mRefreshMenuItem.setIcon(getResources().getDrawable(R.drawable.ic_refresh_red));
			break;
		default: // handles SQL_ERROR, WRONG_CREDENTIALS_ERROR & UNKNOWN_ERROR
			DialogHelper.handleDialogErrors(errorType, this);
			break;
		}
	}

}

