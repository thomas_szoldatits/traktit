package com.szoldapps.tvtrackr.activity.show;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.szoldapps.tvtrackr.R;
import com.szoldapps.tvtrackr.activity.show.card.CastCard;
import com.szoldapps.tvtrackr.activity.show.card.CommentsCard;
import com.szoldapps.tvtrackr.activity.show.card.LinksCard;
import com.szoldapps.tvtrackr.activity.show.card.OverviewCard;
import com.szoldapps.tvtrackr.activity.show.card.ProgressCard;
import com.szoldapps.tvtrackr.activity.show.card.RatingCard;
import com.szoldapps.tvtrackr.activity.show.card.StatsCard;
import com.szoldapps.tvtrackr.db.model.Show;
import com.szoldapps.tvtrackr.dto.internal.InternalProgressDTO;
import com.szoldapps.tvtrackr.exception.IntentExtraException;
import com.szoldapps.tvtrackr.exception.WrongObjectReturnedException;
import com.szoldapps.tvtrackr.helper.Constants;
import com.szoldapps.tvtrackr.helper.DateHelper;
import com.szoldapps.tvtrackr.helper.DateHelper.DateHelperFormat;
import com.szoldapps.tvtrackr.helper.DialogHelper;
import com.szoldapps.tvtrackr.helper.ImageLoaderHelper;
import com.szoldapps.tvtrackr.helper.ImageURLHelper;
import com.szoldapps.tvtrackr.helper.LogCatHelper;
import com.szoldapps.tvtrackr.helper.NumbersHelper;
import com.szoldapps.tvtrackr.helper.ScaleImageView;
import com.szoldapps.tvtrackr.service.async.AsyncAccessService;
import com.szoldapps.tvtrackr.service.async.AsyncAccessServiceImpl;
import com.szoldapps.tvtrackr.service.listener.ServiceResponseListener;

/**
 * Fragment that appears in <code>NewShowActivity</code><br/><br/>
 * 
 * <b>Precondition:</b> <code>Show</code> has to be fully loaded in the DB.
 * 
 */
public class ShowFragment extends Fragment {

	public static final String LOGTAG = LogCatHelper.getAppTag()+ShowFragment.class.getSimpleName().toString();

	private LayoutInflater mInflater;
	private ShowActivity mShowActivity;
	private Context mContext;

	// Service
	private AsyncAccessService mAsyncAccessService;

	// UI-Elements
	private View mRootView;
	private RelativeLayout mRelLoading;
	private ScrollView mScrShow;

	private ImageView mImgFanart;
	private ScaleImageView mImgPoster;
	private TextView mTxtInfoRating;
	private TextView mTxtInfoRatingVoteCount;
	private TextView mPremiered;
	private TextView mCountry;
	private TextView mRuntime;
	private TextView mGenre;
	private TextView mTxtTitle;
	private TextView mTxtAirsOn;
	private LinearLayout mLinCards;

	// Follow Button
	private RelativeLayout mRelBtnFollow;
	private ImageView mImgBtnFollow;
	private ProgressBar mPbarBtnFollow;
	private TextView mTxtBtnFollow;
	protected Boolean mFollowingShow;
	// Seen Button
	private RelativeLayout mRelBtnSeen;
	private ImageView mImgBtnSeen;
	private ProgressBar mPbarBtnSeen;
	private TextView mTxtBtnSeen;

	// Cards
	private CastCard mCastCard;
	private OverviewCard mOverviewCard;
	private ProgressCard mProgressCard;
	private RatingCard mRatingCard;
	private StatsCard mStatsCard;
	private CommentsCard mCommentsCard;
	private LinksCard mLinksCard;

	// Intent Extras
	private int mTvdbId;

	public ShowFragment() { /* Empty constructor required for fragment subclasses */ }
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		mInflater = inflater;
		mShowActivity = (ShowActivity) getActivity();
		mContext = mShowActivity.getApplicationContext();
		mAsyncAccessService = AsyncAccessServiceImpl.getInstance(mContext);

		mRootView = mInflater.inflate(R.layout.fragment_show, container, false);

		if(getArguments() != null) {
			mTvdbId = getArguments().getInt(Constants.EXTRA_SHOW_TVDB_ID, -1);
			if(mTvdbId == -1) {
				showError(new IntentExtraException("mTvdbId == -1")); 
				return mRootView;
			}
			buildUI();
			showLoading(true);
		} else { 
			showError(new IntentExtraException("No IntentExtras at all")); 
		}
		return mRootView;
	}

	public void displayShowFromDB() {
		mAsyncAccessService.getShowById(mTvdbId, new ServiceResponseListener() {
			@Override public void onProgress(int bytesWritten, int totalSize) { }
			@Override
			public void onSuccess(Object o) {
				if(o instanceof Show) {
					fillUIWithData((Show) o);
				} else {
					onFailure(new WrongObjectReturnedException(Show.class));
				}
			}

			@Override
			public void onFailure(Throwable throwable) {
				showError(throwable);
			}
		});
	}


	private void buildUI() {
		//		setHasOptionsMenu(true);
		// UI-Elements
		mRelLoading = (RelativeLayout) mRootView.findViewById(R.id.loading_rel_container);
		mScrShow = (ScrollView) mRootView.findViewById(R.id.show_scroll_view);
		mLinCards = (LinearLayout) mRootView.findViewById(R.id.show_lin_cards);

		mTxtTitle = (TextView) mRootView.findViewById(R.id.show_txt_info_title);

		mImgFanart = (ImageView) mRootView.findViewById(R.id.show_img_fanart);
		mImgPoster = (ScaleImageView) mRootView.findViewById(R.id.show_img_poster);

		mTxtInfoRating = (TextView) mRootView.findViewById(R.id.show_txt_info_rating_value);
		mTxtInfoRatingVoteCount = (TextView) mRootView.findViewById(R.id.show_txt_info_rating_vote_count);

		mPremiered = (TextView) mRootView.findViewById(R.id.show_txt_info_premiered_value);
		mCountry = (TextView) mRootView.findViewById(R.id.show_txt_info_country_value);
		mRuntime = (TextView) mRootView.findViewById(R.id.show_txt_info_runtime_value);
		mGenre = (TextView) mRootView.findViewById(R.id.show_txt_info_genre_value);

		mTxtAirsOn = (TextView) mRootView.findViewById(R.id.show_txt_info_status);

		// Follow Button
		mRelBtnFollow = (RelativeLayout) mRootView.findViewById(R.id.show_rel_btn_follow);
		mImgBtnFollow = (ImageView) mRootView.findViewById(R.id.show_img_btn_follow);
		mPbarBtnFollow = (ProgressBar) mRootView.findViewById(R.id.show_pbar_btn_follow);
		mTxtBtnFollow = (TextView) mRootView.findViewById(R.id.show_txt_btn_follow);

		// Seen Button
		mRelBtnSeen = (RelativeLayout) mRootView.findViewById(R.id.show_rel_btn_seen);
		mImgBtnSeen = (ImageView) mRootView.findViewById(R.id.show_img_btn_seen);
		mPbarBtnSeen = (ProgressBar) mRootView.findViewById(R.id.show_pbar_btn_seen);
		mTxtBtnSeen = (TextView) mRootView.findViewById(R.id.show_txt_btn_seen);

		/** --------- Rating Card --------- **/
		mRatingCard = new RatingCard(true, mContext, mInflater, mLinCards);
		mLinCards.addView(mRatingCard.getCard());

		/** --------- Overview Card --------- **/
		mOverviewCard = new OverviewCard(true, mContext, mInflater, mLinCards);
		mLinCards.addView(mOverviewCard.getCard());

		/** --------- Cast Card --------- **/
		mCastCard = new CastCard(false, mContext, mInflater, mLinCards, getActivity());
		mLinCards.addView(mCastCard.getCard());

		/** --------- Progress Card --------- **/
		mProgressCard = new ProgressCard(true, mContext, mInflater, mLinCards);
		mLinCards.addView(mProgressCard.getCard());

		/** --------- Stats Card --------- **/
		mStatsCard = new StatsCard(true, mContext, mInflater, mLinCards, getActivity());
		mLinCards.addView(mStatsCard.getCard());

		/** --------- Comments Card --------- **/
		mCommentsCard = new CommentsCard(false, mContext, mInflater, mLinCards, this);
		mLinCards.addView(mCommentsCard.getCard());

		/** --------- Links Card --------- **/
		mLinksCard = new LinksCard(mInflater, mLinCards, mShowActivity);
		mLinCards.addView(mLinksCard.getCard());

	}

	@SuppressWarnings("deprecation")
	private void fillUIWithData(Show show) {
		//		Log.i(LOGTAG, "ShowID "+show.getTvdbID());
		// Fanart
		String url = ImageURLHelper.getImageURLByImageType(show.getFanartURL(), ImageURLHelper.FANART_LARGE);
		ImageLoader.getInstance().displayImage(url, mImgFanart, ImageLoaderHelper.getDIOFanart(mContext));

		// Poster
		url = ImageURLHelper.getImageURLByImageType(show.getPosterURL(), ImageURLHelper.POSTER_LARGE);
		ImageLoader.getInstance().displayImage(url, mImgPoster, ImageLoaderHelper.getDIOPosterOrHeadShot(mShowActivity.getApplicationContext()));

		// Info Box Title
		mTxtTitle.setText(show.getTitle());

		// Info Box Rating
		mTxtInfoRating.setText(show.getRatingsPercentage()+getResources().getString(R.string.symbol_percent));
		mTxtInfoRatingVoteCount.setText(NumbersHelper.format(show.getRatingsVotes())+getResources().getString(R.string.rating_votes));

		// Info Box Premiered, Country, Runtime, Genre
		mPremiered.setText(DateHelper.getDateStrWithTimeInS(show.getFirstAiredUTC(), DateHelperFormat.MEDIUM_DATE_FORMAT, mShowActivity));
		mCountry.setText(show.getCountry());
		mRuntime.setText(show.getRuntime()+getResources().getString(R.string.runtime_minutes_abbr));
		mGenre.setText(show.getGenresStr());

		if(show.getStatus().equals("Continuing")) {
			String day = DateHelper.getDateStrWithTimeInS(show.getFirstAiredUTC(), DateHelperFormat.WEEKDAY, mShowActivity);
			String time = DateHelper.getDateStrWithTimeInS(show.getFirstAiredUTC(), DateHelperFormat.TIME, mShowActivity);
			String spanStr = String.format(getResources().getString(R.string.show_info_airs_on), day, time, show.getNetwork());
			mTxtAirsOn.setText(Html.fromHtml(spanStr), TextView.BufferType.SPANNABLE);
		} else {
			mTxtAirsOn.setText(getString(R.string.show_info_airs_on_ended));
			int sdk = android.os.Build.VERSION.SDK_INT;
			if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
				mTxtAirsOn.setBackgroundDrawable(getResources().getDrawable(R.drawable.bg_show_airs_on_ended));
			} else {
				setTxtAirsOnBackground();
			}
			mTxtAirsOn.setTypeface(null, Typeface.BOLD);
		}

		// Follow Button
		checkIfFollowedByCurrentUser(show);
		setBtnFollowOnClickListener(show);
		// Seen Button (Seen or Unseen is set via loadProgress();

		// Cards
		mRatingCard.loadDynamicContent(show, this);
		mOverviewCard.loadDynamicContent(show.getOverview());
		mCastCard.loadDynamicContent(show);
		loadProgress(show);
		mStatsCard.loadDynamicContent(show);
		mLinksCard.loadDynamicContent(show);
		mCommentsCard.showAndServiceAreReadyNow(false, show);

		showLoading(false);
	}

	private void loadProgress(final Show show) {
		setBtnSeenLoading(true);
		mAsyncAccessService.getProgressOfAiredEpisodes(show, new ServiceResponseListener() {
			@Override 
			public void onProgress(int bytesWritten, int totalSize) { }

			@Override
			public void onSuccess(Object o) {
				if(o instanceof InternalProgressDTO) {
					InternalProgressDTO progressDTO = (InternalProgressDTO) o;
					if(mFollowingShow) {
						mProgressCard.loadDynamicContent(progressDTO);
						mProgressCard.setVisibility(View.VISIBLE);
					} else {
						mProgressCard.setVisibility(View.GONE);
					}
					setBtnSeen(progressDTO.getPercent());
					setBtnSeenOnClickListener(show, progressDTO.getPercent());
				} else {
					onFailure(new WrongObjectReturnedException(InternalProgressDTO.class));
				}
			}

			@Override
			public void onFailure(Throwable throwable) {
				showError(throwable);
			}
		});
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// ----- SEEN Button -------------------------------------------------------------------------------------------- //

	private void setBtnSeen(int percent) {
		if(percent == 100) {
			mImgBtnSeen.setImageDrawable(getResources().getDrawable(R.drawable.ic_seen_blue));
			mTxtBtnSeen.setTextColor(getResources().getColor(R.color.blue));
		} else {
			mImgBtnSeen.setImageDrawable(getResources().getDrawable(R.drawable.ic_seen_gray));
			mTxtBtnSeen.setTextColor(getResources().getColor(R.color.darker_gray));
		}
		setBtnSeenLoading(false);
	}

	private void setBtnSeenLoading(boolean b) {
		if(b) {
			mPbarBtnSeen.setVisibility(View.VISIBLE);
			mImgBtnSeen.setVisibility(View.INVISIBLE);
		} else {
			mImgBtnSeen.setVisibility(View.VISIBLE);
			mPbarBtnSeen.setVisibility(View.GONE);
		}
	}

	private void setBtnSeenOnClickListener(final Show show, final int percent) {
		mRelBtnSeen.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if(percent == 100) {
					new AlertDialog.Builder(ShowFragment.this.getActivity())
					.setTitle(getString(R.string.show_unseen_dialog_title))
					.setMessage(String.format(mContext.getString(R.string.show_unseen_dialog_msg), show.getTitle()))
					.setNegativeButton(getString(R.string.cancel), DialogHelper.getDefaultCancelDialogListener())
					.setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog,int id) {
							markShowAsSeenOrUnseen(false, show);
						}
					}).create().show();
				} else {
					markShowAsSeenOrUnseen(true, show);
				}
			}
		});
	}

	private void markShowAsSeenOrUnseen(final boolean markAsSeen, final Show show) {
		setBtnSeenLoading(true);
		Log.e(LOGTAG, "CALLING uploadShowSeenOrUnseen(markAsSeen="+markAsSeen+")");
		mAsyncAccessService.uploadShowSeenOrUnseen(markAsSeen, show, new ServiceResponseListener() {
			@Override
			public void onProgress(int bytesWritten, int totalSize) { }

			@Override
			public void onSuccess(Object o) {
				if(o instanceof Boolean) {
					if(!(Boolean) o) {
						String seen = markAsSeen ? mContext.getString(R.string.show_seen_lc)
								: mContext.getString(R.string.show_unseen_lc);
						String msg = String.format(mContext.getString(R.string.show_marked_offline), 
								show.getTitle(), seen);
						DialogHelper.showInfoLong(msg, getActivity());
					}
					checkIfFollowedByCurrentUser(show);
					loadProgress(show);
					mShowActivity.notifySeasonsFragment();
				} else {
					onFailure(new WrongObjectReturnedException(Boolean.class));
				}
			}

			@Override
			public void onFailure(Throwable throwable) {
				showError(throwable);
			}
		});
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// ----- FOLLOW Button ------------------------------------------------------------------------------------------ //

	private void checkIfFollowedByCurrentUser(Show show) {
		setBtnFollowLoading(true);
		mAsyncAccessService.isShowFollowedByCurrentUser(show, new ServiceResponseListener() {
			@Override
			public void onProgress(int bytesWritten, int totalSize) { }
			@Override
			public void onSuccess(Object o) {
				if(o instanceof Boolean) {
					mFollowingShow = (Boolean) o;
					setBtnFollow();
				} else {
					onFailure(new WrongObjectReturnedException(Boolean.class));
				}
			}

			@Override
			public void onFailure(Throwable throwable) {
				showError(throwable);
			}
		});
	}

	private void setBtnFollow() {
		if(mFollowingShow != null) {
			if(mFollowingShow) {
				mImgBtnFollow.setImageDrawable(getResources().getDrawable(R.drawable.ic_follow_blue));
				mTxtBtnFollow.setText(getString(R.string.episode_btn_following));
				mTxtBtnFollow.setTextColor(getResources().getColor(R.color.blue));
			} else {
				mImgBtnFollow.setImageDrawable(getResources().getDrawable(R.drawable.ic_follow_gray));
				mTxtBtnFollow.setText(getString(R.string.episode_btn_follow));
				mTxtBtnFollow.setTextColor(getResources().getColor(R.color.darker_gray));
			}
			setBtnFollowLoading(false);
		}
	}

	private void setBtnFollowLoading(boolean b) {
		if(b) {
			mPbarBtnFollow.setVisibility(View.VISIBLE);
			mImgBtnFollow.setVisibility(View.INVISIBLE);
		} else {
			mImgBtnFollow.setVisibility(View.VISIBLE);
			mPbarBtnFollow.setVisibility(View.GONE);
		}
	}

	private void setBtnFollowOnClickListener(final Show show) {
		mRelBtnFollow.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if(mFollowingShow != null) {
					if(!mFollowingShow) {
						followShow(show);
					} else {
						DialogHelper.getUnfollowErrorDialogBuilder(show.getTitle(), mShowActivity)
						.setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,int id) {
								unfollowShow(show);
							}
						}).create().show();
					}
				}
			}
		});
	}

	private void followShow(final Show show) {
		setBtnFollowLoading(true);
		mAsyncAccessService.uploadFollowAndSaveShow(show, new ServiceResponseListener() {
			@Override
			public void onProgress(int bytesWritten, int totalSize) { }

			@Override
			public void onSuccess(Object o) {
				if(o instanceof Boolean) {
					if((Boolean) o) {
						String msg = String.format(mContext.getString(R.string.followed_show), show.getTitle());
						DialogHelper.showInfoShort(msg, getActivity());
						mFollowingShow = true;
					} else {
						DialogHelper.getFollowErrorDialogBuilder(mShowActivity)
						.setPositiveButton(getString(R.string.error_btn_retry), new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,int id) {
								followShow(show);
							}
						}).create().show();
						mFollowingShow = false;
					}
					setBtnFollow();
					loadProgress(show);
				} else {
					onFailure(new WrongObjectReturnedException(Boolean.class));
				}
			}

			@Override
			public void onFailure(Throwable throwable) {
				showError(throwable);
			}
		});

	}

	private void unfollowShow(final Show show) {
		setBtnFollowLoading(true);
		mAsyncAccessService.uploadUnfollowShow(show, true, new ServiceResponseListener() {
			@Override
			public void onProgress(int bytesWritten, int totalSize) { }

			@Override
			public void onSuccess(Object o) {
				if(o instanceof Boolean) {
					String msg;
					if((Boolean) o) {
						msg = String.format(mContext.getString(R.string.unfollowed_show), show.getTitle());
					} else {
						msg = String.format(mContext.getString(R.string.unfollowed_show_offline), show.getTitle());
					}
					DialogHelper.showInfoShort(msg, getActivity());
					mFollowingShow = false;
					setBtnFollow();
					loadProgress(show);
					mShowActivity.notifySeasonsFragment();
				} else {
					onFailure(new WrongObjectReturnedException(Boolean.class));
				}
			}

			@Override
			public void onFailure(Throwable throwable) {
				showError(throwable);
			}
		});
	}


	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// ----- OTHER -------------------------------------------------------------------------------------------------- //

	private void showLoading(boolean b) {
		if(b) {
			mRelLoading.setVisibility(View.VISIBLE);
			mScrShow.setVisibility(View.INVISIBLE);
		} else {
			mRelLoading.setVisibility(View.GONE);
			mScrShow.setVisibility(View.VISIBLE);
		}
	}

	@TargetApi(Build.VERSION_CODES.JELLY_BEAN)
	private void setTxtAirsOnBackground() {
		mTxtAirsOn.setBackground(getResources().getDrawable(R.drawable.bg_show_airs_on_ended));
	}

	public void setShowRatingsFromTrakt(Show show) {
		mTxtInfoRating.setText(show.getRatingsPercentage() + getResources().getString(R.string.symbol_percent));
		mTxtInfoRatingVoteCount.setText(show.getRatingsVotes() + getResources().getString(R.string.rating_votes));
	}


	public final void scrollToView(final View x){
		new Handler().post(new Runnable() {
			@Override
			public void run() {
				mScrShow.scrollTo(0, x.getTop() + getActivity().getActionBar().getHeight()
						+ mShowActivity.mPager.getHeight());
				// mScrollView.scrollTo(0, mScrollView.getBottom() + 
				// getActivity().getActionBar().getHeight() + mActivity.mViewPager.getHeight());
			}
		});
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// ----- ERROR -------------------------------------------------------------------------------------------------- //

	private void showError(Throwable throwable) {
		DialogHelper.handleDialogErrors(throwable, mShowActivity);
	}

}
