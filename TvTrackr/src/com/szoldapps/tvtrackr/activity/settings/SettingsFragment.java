package com.szoldapps.tvtrackr.activity.settings;

import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.os.Bundle;
import android.preference.EditTextPreference;
import android.preference.Preference;
import android.preference.PreferenceCategory;
import android.preference.PreferenceFragment;
import android.preference.PreferenceScreen;

import com.szoldapps.tvtrackr.R;
import com.szoldapps.tvtrackr.service.async.AsyncAccessService;
import com.szoldapps.tvtrackr.service.async.AsyncAccessServiceImpl;
import com.szoldapps.tvtrackr.service.listener.ServiceResponseListener;

public class SettingsFragment extends PreferenceFragment implements OnSharedPreferenceChangeListener {

	private AsyncAccessService mAsyncAccess;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		mAsyncAccess = AsyncAccessServiceImpl.getInstance(getActivity());
		
		// Load the preferences from an XML resource
		addPreferencesFromResource(R.xml.preferences);
		// show the current value in the settings screen
		for (int i = 0; i < getPreferenceScreen().getPreferenceCount(); i++) {
			initSummary(getPreferenceScreen().getPreference(i));
		}
		
	}

	@Override
	public void onResume() {
		super.onResume();
		getPreferenceScreen().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);
	}

	@Override
	public void onPause() {
		super.onPause();
		getPreferenceScreen().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(this);
	}

	@Override
	public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
		updatePreferences(findPreference(key));
	}


	@Override
	public boolean onPreferenceTreeClick(PreferenceScreen preferenceScreen, final Preference preference) {
		if(preference.getKey().equals(getResources().getString(R.string.pref_key_clear_photo_cache))) {
			preference.setIcon(getResources().getDrawable(R.drawable.ic_refresh_gray));
			preference.setEnabled(false);
			preference.setSummary("Loading ...");
			
			mAsyncAccess.clearPhotoDiscCache(new ServiceResponseListener() {
				@Override
				public void onProgress(int bytesWritten, int totalSize) {
					if(bytesWritten == -1 & totalSize == -1) {
						preference.setIcon(getResources().getDrawable(R.drawable.ic_storage_gray));
						preference.setSummary("DONE");
					}
				}
				
				@Override
				public void onSuccess(Object o) {
					preference.setSummary(null);
					preference.setEnabled(true);
				}
				
				@Override
				public void onFailure(Throwable throwable) { }
			});
			
		}
		return super.onPreferenceTreeClick(preferenceScreen, preference);
	}

	private void initSummary(Preference p) {
		if (p instanceof PreferenceCategory) {
			PreferenceCategory cat = (PreferenceCategory) p;
			for (int i = 0; i < cat.getPreferenceCount(); i++) {
				initSummary(cat.getPreference(i));
			}
		} else {
			updatePreferences(p);
		}
	}

	private void updatePreferences(Preference p) {
		if (p instanceof EditTextPreference) {
			EditTextPreference editTextPref = (EditTextPreference) p;
			p.setSummary(editTextPref.getText());
		}
	}
}
