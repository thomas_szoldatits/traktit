package com.szoldapps.tvtrackr.activity.settings;

import android.app.Activity;
import android.os.Bundle;
import android.view.MenuItem;

import com.szoldapps.tvtrackr.helper.LinkingHelper;

/**
 * Activity which loads {@link SettingsFragment} and defines Home-Button call.
 * 
 * @author thomasszoldatits
 *
 */
public class SettingsActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// Enable Home Button
		getActionBar().setDisplayHomeAsUpEnabled(true);
		
		// Display the fragment as the main content.
		getFragmentManager().beginTransaction()
		.replace(android.R.id.content, new SettingsFragment())
		.commit();
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			LinkingHelper.goToMain(this);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
}
