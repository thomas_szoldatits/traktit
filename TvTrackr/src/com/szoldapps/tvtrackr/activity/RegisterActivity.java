package com.szoldapps.tvtrackr.activity;

import java.io.UnsupportedEncodingException;
import java.util.regex.Pattern;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.szoldapps.tvtrackr.R;
import com.szoldapps.tvtrackr.db.model.User;
import com.szoldapps.tvtrackr.exception.EmailAlreadyExistsException;
import com.szoldapps.tvtrackr.exception.UsernameAlreadyExistsException;
import com.szoldapps.tvtrackr.helper.DialogHelper;
import com.szoldapps.tvtrackr.helper.FormValidationHelper;
import com.szoldapps.tvtrackr.helper.KeyBoardHelper;
import com.szoldapps.tvtrackr.helper.LogCatHelper;
import com.szoldapps.tvtrackr.service.async.AsyncAccessService;
import com.szoldapps.tvtrackr.service.async.AsyncAccessServiceImpl;
import com.szoldapps.tvtrackr.service.listener.ServiceResponseListener;

public class RegisterActivity extends Activity {

	public static final String LOGTAG = LogCatHelper.getAppTag()+RegisterActivity.class.getSimpleName().toString();

	private AsyncAccessService mAsyncAccessService;

	// Loading
	private RelativeLayout mRelLoading;
	private TextView mTxtLoadingMsg;
	// Form
	private LinearLayout mLinRegister;
	private EditText mEtxtUsername;
	private EditText mEtxtEmail;
	private EditText mEtxtPassword;
	private EditText mEtxtPasswordRetype;
	private Button mBtnRegister;
	// Back
	private TextView mTxtRegisterBack;

	private User mUser;

	private boolean mIsRegisterBusy = false;



	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_register);
		// Show the Up button in the action bar.

		// Load Services
		mAsyncAccessService = AsyncAccessServiceImpl.getInstance(getApplicationContext());
		// Define UI Elements
		buildUI();
		showRegisterForm();
	}

	private void buildUI() {

		// Loading
		mRelLoading = (RelativeLayout) findViewById(R.id.register_rel_loading);
		mTxtLoadingMsg = (TextView) findViewById(R.id.register_txt_loading);
		// Register
		mLinRegister = (LinearLayout) findViewById(R.id.register_lin_register_form);
		mEtxtUsername = (EditText) findViewById(R.id.register_etxt_username);
		mEtxtEmail = (EditText) findViewById(R.id.register_etxt_email);
		mEtxtPassword = (EditText)findViewById(R.id.register_etxt_password);
		mEtxtPasswordRetype = (EditText)findViewById(R.id.register_etxt_password_retype);
		mBtnRegister = (Button) findViewById(R.id.register_btn_register);
		// Back
		mTxtRegisterBack = (TextView) findViewById(R.id.register_txt_back);
		SpannableString spanString = new SpannableString(mTxtRegisterBack.getText());
		spanString.setSpan(new UnderlineSpan(), 0, spanString.length(), 0);
		mTxtRegisterBack.setText(spanString);

		setListeners();

	}

	private void setListeners() {

		// BUTTON LISTENERS
		mEtxtPasswordRetype.setOnEditorActionListener(new TextView.OnEditorActionListener() {
			@Override
			public boolean onEditorAction(TextView view, int actionId, KeyEvent event) {
				if (actionId == EditorInfo.IME_ACTION_GO) {                    
					register();
					return true;
				}
				return false;
			}
		});
		mBtnRegister.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) { register(); }
		});
		mTxtRegisterBack.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) { goBackToLogin(); }
		});

		// VALIDATION LISTENERS

		// auto validation on username
		mEtxtUsername.addTextChangedListener(new TextWatcher() {
			@Override
			public void afterTextChanged(Editable s) {
				String text = s.toString();
				int length = text.length();
				if(!Pattern.matches("^[a-zA-Z0-9-_.]*$", text)) {
					if(length > 0) {     
						s.delete(length - 1, length);
					}
				}
			}
			@Override public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) { }
			@Override public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) { }
		});

		// USERNAME
		mEtxtUsername.setOnFocusChangeListener(FormValidationHelper.getUsernameFocusChangeListener(mEtxtUsername, this));
		// EMAIL
		mEtxtEmail.setOnFocusChangeListener(FormValidationHelper.getEmailFocusChangeListener(mEtxtEmail, this));
		// PASSWORD
		mEtxtPassword.setOnFocusChangeListener(FormValidationHelper.getPasswordFocusChangeListener(mEtxtPassword, this));
		// PASSWORD RETYPE
		mEtxtPasswordRetype.setOnFocusChangeListener(FormValidationHelper.getPasswordRetypeFocusChangeListener(mEtxtPasswordRetype, mEtxtPassword, this));

	}

	private void showRegisterForm() {
		mRelLoading.setVisibility(View.INVISIBLE);
		mLinRegister.setVisibility(View.VISIBLE);
		mTxtRegisterBack.setVisibility(View.VISIBLE);
		mEtxtUsername.clearFocus();
		mLinRegister.requestFocus();
	}

	private void showLoading() {
		KeyBoardHelper.hideKeyboard(mRelLoading, this);
		mRelLoading.setVisibility(View.VISIBLE);
		mLinRegister.setVisibility(View.INVISIBLE);
		mTxtRegisterBack.setVisibility(View.INVISIBLE);
	}

	private void showError(Throwable throwable) {
		DialogHelper.getGeneralErrorDialog(throwable, this).show();
	}

	private void showSuccessDialog() {
		AlertDialog alertDialog = new AlertDialog.Builder(this)
		.setTitle(getString(R.string.register_success_title))
		.setMessage(String.format(getString(R.string.register_success), mUser.getUsername()))
		.setNeutralButton(getString(R.string.continue_str), new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog,int id) {
				dialog.cancel();
				goBackToLogin();
			}
		}).create();
		alertDialog.show();
	}


	private void register() {
		if(mIsRegisterBusy) {
			Log.w(LOGTAG, "BLOCKED second RegisterRequest");
			return;
		}

		mIsRegisterBusy = true;
		if(isFormValid()) {
			mUser = new User();
			try {
				mUser.setUsername(mEtxtUsername.getText().toString());
				mUser.setPassword(mEtxtPassword.getText().toString());
				mUser.setEmail(mEtxtEmail.getText().toString());
			} catch (UnsupportedEncodingException e) {
				showError(e);
			}
			mTxtLoadingMsg.setText(String.format(getString(R.string.register_status_trying_to_register_user), mUser.getUsername()));
			showLoading();
			mAsyncAccessService.register(mUser, new ServiceResponseListener() {
				@Override public void onProgress(int bytesWritten, int totalSize) { }
				@Override
				public void onSuccess(Object o) {
					mIsRegisterBusy = false;
					mUser = (User) o;
					mTxtLoadingMsg.setText(String.format(getString(R.string.register_status_success), mUser.getUsername()));
					showSuccessDialog();
				}
				@Override
				public void onFailure(Throwable throwable) {
					mIsRegisterBusy = false;
					showRegisterForm();
					if(throwable instanceof UsernameAlreadyExistsException) {
						// DEBUG MOCK BEGIN
						if(throwable.getMessage().equals("MOCK")) {
							KeyBoardHelper.hideKeyboard(mBtnRegister, getApplicationContext());
							new AlertDialog.Builder(RegisterActivity.this)
							.setTitle("REGISTER DOES NOT ACTUALLY WORK!")
							.setMessage("No Developer access for now!\nMock Option:\n"+
									"- Success -> un: tvtrackr\n" +
									"- Username-Failure -> un: wrong\n"+
									"- E-mail-Failure: email=wrong@b.com")
									.setPositiveButton("Continue", new Dialog.OnClickListener() {
										@Override
										public void onClick(DialogInterface dialog, int id) {
											dialog.dismiss();
										}
									}).create().show();
						} else {
							// DEBUG MOCK END
							String errorStr = String.format(getString(R.string.error_username_already_exists), mUser.getUsername());
							setValidationErrorAndFocus(mEtxtUsername, errorStr);
						}
					} else if(throwable instanceof EmailAlreadyExistsException) {
						String errorStr = String.format(getString(R.string.error_email_already_exists), mUser.getEmail());
						setValidationErrorAndFocus(mEtxtEmail, errorStr);
					} else {
						showError(throwable);
					}
				}
			});
		} else {
			mIsRegisterBusy = false;
		}
	}

	private void setValidationErrorAndFocus(EditText etxt, String errorMsg) {
		etxt.setError(errorMsg);
		etxt.requestFocus();
		KeyBoardHelper.showKeyboard(etxt, getApplicationContext());
	}

	protected void goBackToLogin() {
		Class<?> cls = LoginActivity.class;
		Intent intent = new Intent(getApplicationContext(), cls);
		startActivity(intent);
		overridePendingTransition(R.anim.slide_down_in, R.anim.slide_down_out);
	}


	/** --------- FORM VALIDATION --------- **/

	private boolean isFormValid() {
		EditText firstErrorView = null;

		FormValidationHelper.validateUsername(mEtxtUsername, this);
		FormValidationHelper.validateEmail(mEtxtEmail, this);
		FormValidationHelper.validatePassword(mEtxtPassword, this);
		FormValidationHelper.validatePasswordRetype(mEtxtPasswordRetype, mEtxtPassword, this);

		if(!TextUtils.isEmpty(mEtxtUsername.getError())) {
			firstErrorView = mEtxtUsername;
		} else if(!TextUtils.isEmpty(mEtxtEmail.getError())) {
			firstErrorView = mEtxtEmail;
		} else if(!TextUtils.isEmpty(mEtxtPassword.getError())) {
			firstErrorView = mEtxtPassword;
		} else if(!TextUtils.isEmpty(mEtxtPasswordRetype.getError())) {
			firstErrorView = mEtxtPasswordRetype;
		}

		// SHOW ERROR if present
		if(firstErrorView != null) {
			firstErrorView.requestFocus();
			KeyBoardHelper.showKeyboard(firstErrorView, getApplicationContext());
			return false;
		}
		return true;
	}


}
