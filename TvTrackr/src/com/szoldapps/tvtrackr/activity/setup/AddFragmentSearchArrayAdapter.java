package com.szoldapps.tvtrackr.activity.setup;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.szoldapps.tvtrackr.R;
import com.szoldapps.tvtrackr.dto.show.SearchShowDTO;
import com.szoldapps.tvtrackr.helper.ImageLoaderHelper;
import com.szoldapps.tvtrackr.helper.ImageURLHelper;
import com.szoldapps.tvtrackr.helper.ScaleImageView;

public class AddFragmentSearchArrayAdapter extends ArrayAdapter<SearchShowDTO> {

	List<SearchShowDTO> mSearchShowDTOs = new ArrayList<SearchShowDTO>();
	private AddFragment mAddFragment;
	private Context mContext;

	public AddFragmentSearchArrayAdapter(AddFragment addFragment, Context context, int resource, List<SearchShowDTO> objects) {
		super(context, resource, objects);
		mContext = context;
		this.mAddFragment = addFragment;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater) mAddFragment.getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		RelativeLayout relListItem = null;

		if(mSearchShowDTOs == null) {	// Too few characters typed
			relListItem = (RelativeLayout) inflater.inflate(R.layout.li_setup_add_search_info, parent, false);
		} else {
			if(mSearchShowDTOs.isEmpty()) {
				relListItem = (RelativeLayout) inflater.inflate(R.layout.li_setup_add_search_info, parent, false);
				ProgressBar pbarLoading = (ProgressBar) relListItem.findViewById(R.id.li_setup_add_search_info_pbar_download_loading);
				TextView txtHl = (TextView) relListItem.findViewById(R.id.li_setup_add_search_info_txt_hl);
				txtHl.setText(mAddFragment.getString(R.string.search_no_shows_found));
				pbarLoading.setVisibility(View.GONE);
			} else {
				relListItem = (RelativeLayout) inflater.inflate(R.layout.li_setup_add_search, parent, false);
				// UI-Elements
				ScaleImageView imgPoster = (ScaleImageView) relListItem.findViewById(R.id.li_setup_add_search_img_poster);
				TextView txtHl = (TextView) relListItem.findViewById(R.id.li_setup_add_search_txt_hl);
				TextView txtSubHl = (TextView) relListItem.findViewById(R.id.li_setup_add_search_txt_subhl);
				ImageView imgFollow = (ImageView) relListItem.findViewById(R.id.li_setup_add_search_img_follow);
				
				SearchShowDTO ssDTO = mSearchShowDTOs.get(position);
				txtHl.setText(ssDTO.getTitle());
				txtSubHl.setText(Integer.valueOf(ssDTO.getYear()).toString());
				
				String imgUrl = ImageURLHelper.getImageURLByImageType(ssDTO.getImages().getPoster(), ImageURLHelper.POSTER_SMALL);
				ImageLoader.getInstance().displayImage(imgUrl, imgPoster, ImageLoaderHelper.getDIOPosterOrHeadShot(mContext));
				
				if(ssDTO.isFollowedByUser()) {
					imgFollow.setImageDrawable(mAddFragment.getResources().getDrawable(R.drawable.ic_follow_blue));
				} else {
					imgFollow.setTag(ssDTO);
					imgFollow.setOnClickListener(new OnClickListener() {

						@Override
						public void onClick(View v) {
							SearchShowDTO ssDTO = (SearchShowDTO) v.getTag();
							mAddFragment.addShowToList(ssDTO);
						}
					});
				}
			}
		}
		return relListItem;
	}

	public void setShowSuggestions(List<SearchShowDTO> showSuggestions) {
		mSearchShowDTOs = showSuggestions;
		this.notifyDataSetChanged();
	}

	@Override
	public int getCount() {
		if(mSearchShowDTOs == null) {
			return 1;
		}
		if(mSearchShowDTOs.isEmpty()) {
			return 1;
		}
		return mSearchShowDTOs.size();
	}
}