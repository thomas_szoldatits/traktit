package com.szoldapps.tvtrackr.activity.setup;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListPopupWindow;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.SearchView;
import android.widget.SearchView.OnQueryTextListener;
import android.widget.TextView;

import com.szoldapps.tvtrackr.R;
import com.szoldapps.tvtrackr.db.model.Show;
import com.szoldapps.tvtrackr.db.model.Show.ShowDownloadStatus;
import com.szoldapps.tvtrackr.dto.show.SearchShowDTO;
import com.szoldapps.tvtrackr.exception.WrongObjectReturnedException;
import com.szoldapps.tvtrackr.helper.Constants;
import com.szoldapps.tvtrackr.helper.DialogHelper;
import com.szoldapps.tvtrackr.helper.LogCatHelper;
import com.szoldapps.tvtrackr.service.async.AsyncAccessService;
import com.szoldapps.tvtrackr.service.async.AsyncAccessServiceImpl;
import com.szoldapps.tvtrackr.service.listener.ServiceResponseListener;

public class AddFragment extends Fragment {

	public static final String LOGTAG = LogCatHelper.getAppTag()+AddFragment.class.getSimpleName().toString();

	// Service
	private AsyncAccessService mAsyncAccessService;

	private SetupActivity mSetupActivity;
	private View mRootView;

	private LayoutInflater mInflater;

	// ShowList
	private AddFragmentShowListArrayAdapter mListAdapter;
	private ListView mListShows;

	// Header
	private TextView mTxtHeaderCount;
	private ProgressBar mPbarHeaderLoading;

	// Search
	private ListPopupWindow mListPopupWindow;
	private SearchView mSearchView;
	private AddFragmentSearchArrayAdapter mSearchAdapter;
	protected String mCurrentQuery;

	// Loading
	private RelativeLayout mRelLoading;

	// Menu
	private MenuItem mRefreshMenuItem;


	public AddFragment() { /* Empty constructor required for fragment subclasses */ }

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		mInflater = inflater;
		mSetupActivity = (SetupActivity) getActivity();
		mRootView = inflater.inflate(R.layout.fragment_setup_add, container, false);
		mAsyncAccessService = AsyncAccessServiceImpl.getInstance(mSetupActivity.getApplicationContext());

		buildUI();

		return mRootView;
	}


	private void buildUI() {
		setHasOptionsMenu(true);
		// Loading
		mRelLoading = (RelativeLayout) mRootView.findViewById(R.id.loading_rel_container);
		// ListView
		mListShows = (ListView) mRootView.findViewById(R.id.setup_add_list);
		// ListView: Header and Footer
		RelativeLayout relHeader = (RelativeLayout) mInflater.inflate(R.layout.li_setup_add_header, mListShows, false);
		RelativeLayout relFooter = (RelativeLayout) mInflater.inflate(R.layout.li_footer_with_card_shadow, mListShows, false);
		mListShows.addHeaderView(relHeader);
		mListShows.addFooterView(relFooter);
		mTxtHeaderCount = (TextView) relHeader.findViewById(R.id.li_header_txt_count);
		mPbarHeaderLoading = (ProgressBar) relHeader.findViewById(R.id.li_header_pbar_loading);
		mListAdapter = new AddFragmentShowListArrayAdapter(AddFragment.this, mSetupActivity, R.layout.li_setup_add, new ArrayList<Show>());
		mListShows.setAdapter(mListAdapter);
	}


	public void setHeaderCount() {
		if(mListAdapter != null) {
			int showsSize = mListAdapter.getRealCount();
			int doneCount = 0;
			for (int i = 0; i < showsSize; i++) {
				Show show = mListAdapter.getItem(i);
				if(show.getDownloadStatus().equals(ShowDownloadStatus.DONE)) {
					doneCount++;
				}
			}
			if(doneCount < showsSize) {
				mPbarHeaderLoading.setVisibility(View.VISIBLE);
			} else {
				mPbarHeaderLoading.setVisibility(View.GONE);
			}
			String str = String.format(getString(R.string.setup_count), doneCount, showsSize);
			mTxtHeaderCount.setText(str);
		}
	}




	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		// Inflate the options menu from XML
		inflater.inflate(R.menu.setup_add, menu);

		mRefreshMenuItem = menu.getItem(1);
		if(mSetupActivity.isUpdateRunning()) {
			updateStarted();
		}

		// Get the SearchView and set the searchable configuration
		SearchManager searchManager = (SearchManager) getActivity().getSystemService(Context.SEARCH_SERVICE);
		mSearchView = (SearchView) menu.findItem(R.id.menu_setup_search).getActionView();
		// Assumes current activity is the searchable activity
		mSearchView.setSearchableInfo(searchManager.getSearchableInfo(getActivity().getComponentName()));
		mSearchView.setIconifiedByDefault(true); // Do not iconify the widget; expand it by default
		mSearchView.setOnQueryTextListener(new OnQueryTextListener() {

			private Timer timer=new Timer();
			@Override
			public boolean onQueryTextSubmit(String query) {
				onQueryTextChange(query);
				return false;
			}

			@Override
			public boolean onQueryTextChange(final String newText) {
				timer.cancel();
				timer = new Timer();
				timer.schedule(new TimerTask() {
					@Override
					public void run() {
						//						Log.i(LOGTAG, "query="+newText);
						mCurrentQuery = newText;
						mSearchHandler.sendEmptyMessage(Constants.SEARCH_DELAY);
					}
				}, Constants.SEARCH_DELAY);
				return false;
			}
		});

		mListPopupWindow = new ListPopupWindow(getActivity());
		mSearchAdapter = new AddFragmentSearchArrayAdapter(this, getActivity().getApplicationContext(), R.layout.li_setup_add_search, null);
		mListPopupWindow.setAdapter(mSearchAdapter);
		mListPopupWindow.setAnchorView(mSearchView);
		mListPopupWindow.setModal(false);
	}

	@SuppressLint("HandlerLeak")
	private Handler mSearchHandler = new Handler(){
		public void handleMessage(android.os.Message msg) {
			switch (msg.what) {
			case Constants.SEARCH_DELAY:
				search(mCurrentQuery);
			}

		};
	};

	public void updateEnded() {
		if(mRefreshMenuItem != null) {
			mRefreshMenuItem.setActionView(null);
		}
	}

	public void updateStarted() {
		if(mRefreshMenuItem != null) {
			mRefreshMenuItem.setActionView(R.layout.menu_progress_wheel);
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		return mSetupActivity.onOptionsItemSelected(item);
	}

	private AlertDialog mNoInternetDialog;

	public void search(String query) {
		query = query.trim();
		if(query.length()>=1) {
			mSearchAdapter.setShowSuggestions(null);
			showListPopupWindow();
			final String queryForRecall = query;
			mAsyncAccessService.search(query, 10, new ServiceResponseListener() {

				@Override public void onProgress(int bytesWritten, int totalSize) { /* not needed*/ }

				@Override @SuppressWarnings("unchecked")
				public void onSuccess(Object o) {
					List<SearchShowDTO> shows = (ArrayList<SearchShowDTO>)o;
					mSearchAdapter.setShowSuggestions(shows);
					showListPopupWindow();
				}

				@Override
				public void onFailure(Throwable throwable) {
					if(DialogHelper.isInternetError(throwable)) {
						// hide Search Loading
						mSearchAdapter.setShowSuggestions(new ArrayList<SearchShowDTO>());
						if(mNoInternetDialog == null) {
							mNoInternetDialog = DialogHelper.getNoInternetErrorDialogBuilder(getActivity())
									.setPositiveButton(getString(R.string.error_btn_retry), new AlertDialog.OnClickListener() {
										@Override
										public void onClick(DialogInterface dialog, int id) {
											search(queryForRecall);
										}
									}).create();
						}
						if(!mNoInternetDialog.isShowing()) {
							mNoInternetDialog.show();
						}
					} else {
						showError(throwable);
					}
				}
			});
		}
	}

	private void showListPopupWindow() {
		if(!mListPopupWindow.isShowing()) {
			mListPopupWindow.setWidth(mSearchView.getWidth());
			mListPopupWindow.setHeight(mRootView.getHeight());
			mListPopupWindow.show();
		}
	}

	public void addShowToList(final SearchShowDTO show) {
		mSearchView.setQuery("", false);
		mListPopupWindow.dismiss();

		mAsyncAccessService.uploadFollowAndSaveShow(show, new ServiceResponseListener() {
			@Override 
			public void onProgress(int bytesWritten, int totalSize) { }

			@Override
			public void onSuccess(Object o) {
				if(o instanceof Boolean) {
					if((Boolean) o) {
						String msg = String.format(getString(R.string.followed_show), show.getTitle());
						DialogHelper.showInfoShort(msg, getActivity());
						mSetupActivity.notifyFragmentsCompleteRefresh();
					} else {
						DialogHelper.getFollowErrorDialogBuilder(getActivity())
						.setPositiveButton(getString(R.string.error_btn_retry), 
								new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,int id) {
								addShowToList(show);
							}
						}).create().show();
					}
				} else {
					onFailure(new WrongObjectReturnedException(Boolean.class));
				}
			}

			@Override
			public void onFailure(Throwable throwable) {
				showError(throwable);
			}
		});
		
	}

	/** ****** NOTIFICATION from SetupActivity METHODS ******* **/

	public void refreshCompleteShowlist(List<Show> shows) {
		mRelLoading.setVisibility(View.GONE);
		mListShows.setVisibility(View.VISIBLE);
		Log.e(LOGTAG, "NOTIFICATION: Refresh ALL Shows");
		mListAdapter.clear();
		mListAdapter.addAll(shows);
		setHeaderCount();
	}



	public void refreshOneShowInShowlist(Show show) {
		int position = mListAdapter.getPosition(show);
		Log.e(LOGTAG, "NOTIFICATION: Refresh ONE Show (tvdbId:"+show.getTitle()+", status:"+show.getDownloadStatus()+")");
		RelativeLayout relListItem = null;

		int listPosition = position;
		if(mListShows.getFirstVisiblePosition() == 0) {		// if Header is also visible +1
			listPosition++;
		}
		if(listPosition == mListAdapter.getRealCount()+2) {		// = FOOTER
			listPosition = -1;
		}
		if(listPosition >= mListShows.getFirstVisiblePosition() && listPosition <= mListShows.getLastVisiblePosition()) {
			relListItem = (RelativeLayout) mListShows.getChildAt(listPosition);
		}


		mListAdapter.updateShow(position, show, relListItem);

		setHeaderCount();
	}

	public void removeShow(Show show) {
		mListAdapter.remove(show);
		mListAdapter.notifyDataSetChanged();
		mSetupActivity.removeShow(show);
		setHeaderCount();
	}

	/**
	 * Redirects to {@link SetupActivity#goToTrackFragAndDisplayShow(int)}.
	 * @param position
	 */
	public void goToTrackFragAndDisplayShow(int position) {
		mSetupActivity.goToTrackFragAndDisplayShow(position);
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// ----- ERROR -------------------------------------------------------------------------------------------------- //

	private void showError(Throwable throwable) {
		DialogHelper.handleDialogErrors(throwable, mSetupActivity);
	}

}

