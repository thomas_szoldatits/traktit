package com.szoldapps.tvtrackr.activity.setup;

import java.util.ArrayList;
import java.util.List;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.szoldapps.tvtrackr.R;
import com.szoldapps.tvtrackr.db.model.Show;
import com.szoldapps.tvtrackr.helper.LogCatHelper;
import com.szoldapps.tvtrackr.helper.NumbersHelper;

public class TrackFragment extends Fragment {

	public static final String LOGTAG = LogCatHelper.getAppTag()+TrackFragment.class.getSimpleName().toString();

	private SetupActivity mSetupActivity;
	private View mRootView;
	private Menu mMenu;

	// Service
	//	private AsyncAccessService mAsyncAccessService;

	// ViewPager and Adapter
	private TrackTabPageAdapter mPageAdapter;
	public ViewPager mPager;

	// Navigation
	protected int mPosition = 0;
	private ImageView mImgPrev;
	private ImageView mImgNext;

	private RelativeLayout mRelProgress;
	private TextView mTxtProgress;
	private ProgressBar mPbarProgress;

	private TextView mTxtShowTitle;

	// Loading
	private RelativeLayout mRelLoading;
	private ProgressBar mPbarLoading;
	private TextView mTxtLoading;

	private MenuItem mRefreshMenuItem;




	public TrackFragment() { /* Empty constructor required for fragment subclasses */ }

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		mSetupActivity = (SetupActivity) getActivity();
		mRootView = inflater.inflate(R.layout.fragment_setup_track, container, false);

		//		mAsyncAccessService = AsyncAccessServiceImpl.getInstance(mSetupActivity.getApplicationContext());

		buildUI();

		return mRootView;
	}


	private void buildUI() {
		setHasOptionsMenu(true);

		// Loading
		mRelLoading = (RelativeLayout) mRootView.findViewById(R.id.loading_rel_container);
		mPbarLoading = (ProgressBar) mRootView.findViewById(R.id.loading_pbar);
		mTxtLoading = (TextView) mRootView.findViewById(R.id.loading_txt);

		mPageAdapter = new TrackTabPageAdapter(getChildFragmentManager(), new ArrayList<Show>());

		mPager = (ViewPager) mRootView.findViewById(R.id.setup_track_pag_pager);
		mPager.setAdapter(mPageAdapter);
		mPager.setOffscreenPageLimit(2);
		mPager.setOnPageChangeListener(
				new ViewPager.SimpleOnPageChangeListener() {
					@Override
					public void onPageSelected(int position) {
						mPosition = position;
						Log.d(LOGTAG, "SHOWING PAGE " + mPosition);
						refreshFragmentData(position);
						loadProgress();
					}
				});
		mRelProgress = (RelativeLayout) mRootView.findViewById(R.id.setup_track_rel_container);
		mTxtProgress = (TextView) mRootView.findViewById(R.id.setup_track_txt_progress);
		mPbarProgress = (ProgressBar) mRootView.findViewById(R.id.setup_track_pbar_progress);

		OnClickListener onClickListener = new OnClickListener() {
			@Override
			public void onClick(View v) {
				int pos = mPosition+(Integer)v.getTag();
				if(pos >= 0 && pos < mPageAdapter.getCount()) {
					mPager.setCurrentItem(pos, true);
				}
			}
		};
		mImgPrev = (ImageView) mRootView.findViewById(R.id.setup_track_img_previous);
		mImgPrev.setTag(-1);
		mImgPrev.setOnClickListener(onClickListener);

		mTxtShowTitle = (TextView) mRootView.findViewById(R.id.setup_track_txt_show_title);

		mImgNext = (ImageView) mRootView.findViewById(R.id.setup_track_img_next);
		mImgNext.setTag(1);
		mImgNext.setOnClickListener(onClickListener);

	}


	public void refreshFragmentData(int position) {
		mPageAdapter.refreshFragmentData(position);
	}

	private void loadProgress() {
		if(mPageAdapter != null) {
			List<Show> shows = mPageAdapter.getShows();
			if(!shows.isEmpty()) {
				Show show = shows.get(mPosition);
				if(show != null) {
					mTxtShowTitle.setText(show.getTitle());
				}
				mTxtProgress.setText(String.format(getString(R.string.setup_track_progress), mPosition+1, mPageAdapter.getCount()));
				mPbarProgress.setProgress(NumbersHelper.getPercentage(mPosition+1, mPageAdapter.getCount()));

				if(mPosition == mPageAdapter.getCount()-1) {
					mImgNext.setVisibility(View.INVISIBLE);
				} else {
					mImgNext.setVisibility(View.VISIBLE);
				}

				if(mPosition == 0) {
					mImgPrev.setVisibility(View.INVISIBLE);
				} else {
					mImgPrev.setVisibility(View.VISIBLE);
				}
				mRelLoading.setVisibility(View.GONE);
				mRelProgress.setVisibility(View.VISIBLE);
				mPager.setVisibility(View.VISIBLE);
			} else {
				mTxtLoading.setText(getString(R.string.setup_empty_show_list));
				mPbarLoading.setVisibility(View.INVISIBLE);
				mRelProgress.setVisibility(View.INVISIBLE);
				mPager.setVisibility(View.INVISIBLE);
				mRelLoading.setVisibility(View.VISIBLE);
			}

		}
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		inflater.inflate(R.menu.setup_track, menu);
		mRefreshMenuItem = menu.getItem(0);
		if(mSetupActivity.isUpdateRunning()) {
			updateStarted();
		}
		mMenu = menu;
		if(mPosition == mPageAdapter.getCount()-1) {
			mMenu.getItem(1).setTitle(getString(R.string.ic_action_done));
		} else {
			mMenu.getItem(1).setTitle(getString(R.string.ic_action_skip));
		}
	}

	public void updateEnded() {
		if(mRefreshMenuItem != null) {
			mRefreshMenuItem.setActionView(null);
		}
	}

	public void updateStarted() {
		if(mRefreshMenuItem != null) {
			mRefreshMenuItem.setActionView(R.layout.menu_progress_wheel);
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		return mSetupActivity.onOptionsItemSelected(item);
	}

	public void refreshCompleteShowlist(List<Show> shows) {
		mPageAdapter.setShows(shows);
		loadProgress();
	}

	public void refreshOneShowInShowlist(Show show) {
		mPageAdapter.refreshFragmentData(show);
		loadProgress();
	}

	public void addShow(Show show) {
		mPageAdapter.addShow(show);
		loadProgress();
	}

	public void removeShow(Show show) {
		mPageAdapter.removeShow(show);
		loadProgress();
	}

	/**
	 * Moves Pager to requested Show.
	 * 
	 * @param position
	 */
	public void goToShow(int position) {
		if(position >= 0 && position < mPageAdapter.getCount()) {
			mPager.setCurrentItem(position);
		}
	}

	//	private void showError(Throwable throwable) {
	//		throwable.printStackTrace();
	//	}

}
