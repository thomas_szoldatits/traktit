package com.szoldapps.tvtrackr.activity.setup;

import java.util.List;

import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.szoldapps.tvtrackr.R;
import com.szoldapps.tvtrackr.db.model.Show;
import com.szoldapps.tvtrackr.helper.DialogHelper;
import com.szoldapps.tvtrackr.helper.ImageLoaderHelper;
import com.szoldapps.tvtrackr.helper.ImageURLHelper;
import com.szoldapps.tvtrackr.helper.LogCatHelper;
import com.szoldapps.tvtrackr.helper.ScaleImageView;

public class AddFragmentShowListArrayAdapter extends ArrayAdapter<Show> {
	
	public static final String LOGTAG = LogCatHelper.getAppTag()+AddFragmentShowListArrayAdapter.class.getSimpleName().toString();
	private final Context mContext;
	//		private List<ListItemCache> mListItemsCache = new ArrayList<ListItemCache>();
	private AddFragment mAddFragment;

	public AddFragmentShowListArrayAdapter(AddFragment addFragment, Context context, int resource, List<Show> shows) {
		super(context, resource, shows);
		this.mAddFragment = addFragment;
		this.mContext = context;
		this.mAddFragment.setHeaderCount();
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater) mAddFragment.getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		RelativeLayout relListItem;
		//		if(mShows == null) {
		//			relListItem = (RelativeLayout) inflater.inflate(R.layout.li_setup_add_info, parent, false);
		//			ProgressBar pbarLoading = (ProgressBar) relListItem.findViewById(R.id.li_setup_add_info_pbar_loading);
		//			TextView txtInfo = (TextView) relListItem.findViewById(R.id.li_setup_add_info_txt_hl);
		//			pbarLoading.setVisibility(View.VISIBLE);
		//			txtInfo.setText(mContext.getString(R.string.setup_loading_show_list));
		//		} else {
		if(getRealCount() == 0) {
			relListItem = (RelativeLayout) inflater.inflate(R.layout.li_setup_add_info, parent, false);
			ProgressBar pbarLoading = (ProgressBar) relListItem.findViewById(R.id.li_setup_add_info_pbar_loading);
			TextView txtInfo = (TextView) relListItem.findViewById(R.id.li_setup_add_info_txt_hl);
			pbarLoading.setVisibility(View.GONE);
			txtInfo.setText(mContext.getString(R.string.setup_empty_show_list));
		} else {
			relListItem = (RelativeLayout) inflater.inflate(R.layout.li_setup_add, parent, false);
			final Show show = this.getItem(position);

			TextView txtHl = (TextView) relListItem.findViewById(R.id.li_setup_add_txt_hl);
			TextView txtSubHl = (TextView) relListItem.findViewById(R.id.li_setup_add_txt_subhl);
			ScaleImageView imgPoster = (ScaleImageView) relListItem.findViewById(R.id.li_setup_add_img_poster);

			String imgUrl = ImageURLHelper.getImageURLByImageType(show.getPosterURL(), ImageURLHelper.POSTER_SMALL);
			ImageLoader.getInstance().displayImage(imgUrl, imgPoster, ImageLoaderHelper.getDIOPosterOrHeadShot(mContext));

			// Hl
			txtHl.setText(show.getTitle());

			// SubHl
			txtSubHl.setText(String.format(mAddFragment.getString(R.string.show_year_squared_brackets), show.getYear()));

			// Loading
			relListItem = setTxtDownloadText(show, relListItem);
			
			// Link to TrackFragment
			relListItem.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					mAddFragment.goToTrackFragAndDisplayShow(position);
				}
			});
		}
		//		}
		return relListItem;
	}

	private RelativeLayout setTxtDownloadText(final Show show, RelativeLayout relListItem) {

		// Loading
		ProgressBar pbarDownload = (ProgressBar) relListItem.findViewById(R.id.li_setup_add_pbar_download_loading);
		TextView txtDownload = (TextView) relListItem.findViewById(R.id.li_setup_add_txt_download);
		ImageView imgFollow = (ImageView) relListItem.findViewById(R.id.li_setup_add_img_follow);
		imgFollow.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				DialogHelper.getUnfollowErrorDialogBuilder(show.getTitle(), mAddFragment.getActivity())
				.setPositiveButton(mContext.getString(R.string.yes), new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog,int id) {
						mAddFragment.removeShow(show);
					}
				}).create().show();
			}
		});

		if(pbarDownload != null && txtDownload != null && imgFollow != null) {
			switch (show.getDownloadStatus()) {
			case DOWNLOADING:
				pbarDownload.setVisibility(View.VISIBLE);
				txtDownload.setVisibility(View.VISIBLE);
				imgFollow.setVisibility(View.GONE);
				txtDownload.setText(mContext.getString(R.string.setup_download_downloading));
				txtDownload.setTextColor(mContext.getResources().getColor(R.color.black));
				break;
			case SAVING:
				txtDownload.setText(mContext.getString(R.string.setup_download_saving));
				txtDownload.setTextColor(mContext.getResources().getColor(R.color.black));
				break;
			case UPDATING:
				txtDownload.setText(mContext.getString(R.string.setup_download_updating));
				txtDownload.setTextColor(mContext.getResources().getColor(R.color.black));
				break;
			case DONE:
				pbarDownload.setVisibility(View.GONE);
				txtDownload.setVisibility(View.GONE);
				imgFollow.setVisibility(View.VISIBLE);
				break;
			default:
				txtDownload.setText(mContext.getString(R.string.setup_download_waiting));
				break;
			}

			relListItem.setTag(new ShowListItemCache(pbarDownload, txtDownload, imgFollow, show));
		}
		return relListItem;
	}



	public void updateShow(int position, Show show, RelativeLayout relListItem) {
		Show adapterShow = this.getItem(position);
		adapterShow.setDownloadStatus(show.getDownloadStatus());
		if(relListItem != null) {
			setTxtDownloadText(adapterShow, relListItem);
		}
	}


	@Override
	public int getCount() {
		if(super.getCount() == 0) {
			return 1;
		}
		return super.getCount();
	}
	
	public int getRealCount() {
		return super.getCount();
	}


	public class ShowListItemCache {
		ProgressBar pbarDownload;
		TextView txtDownload;
		ImageView imgFollow;
		Show show;
		public ShowListItemCache(ProgressBar pbarDownload, TextView txtDownload, ImageView imgFollow, Show show) {
			super();
			this.txtDownload = txtDownload;
			this.imgFollow = imgFollow;
			this.show = show;
			this.pbarDownload = pbarDownload;
		}
	}

}