package com.szoldapps.tvtrackr.activity.setup;

import java.util.List;
import java.util.Locale;

import android.app.Activity;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.szoldapps.tvtrackr.R;

class SetupTabPageAdapter extends FragmentPagerAdapter {

    private List<Fragment> mFragments;
    private Activity mActivity;

    public SetupTabPageAdapter(FragmentManager fm, List<Fragment> fragments, Activity activity) {
        super(fm);
        this.mFragments = fragments;
        this.mActivity = activity;
    }

    @Override 
    public Fragment getItem(int position) {
        return this.mFragments.get(position);
    }

    @Override
    public int getCount() {
        return this.mFragments.size();
    }
    
    @Override
    public CharSequence getPageTitle(int position) {
        Locale l = Locale.getDefault();
        switch (position) {
        case 0:
            return mActivity.getString(R.string.setup_tab_title_add).toUpperCase(l);
        case 1:
            return mActivity.getString(R.string.setup_tab_title_track).toUpperCase(l);
        }
        return null;
    }
}

