package com.szoldapps.tvtrackr.activity.setup;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.client.HttpResponseException;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.MenuItem;

import com.astuetz.PagerSlidingTabStrip;
import com.szoldapps.tvtrackr.R;
import com.szoldapps.tvtrackr.db.model.Show;
import com.szoldapps.tvtrackr.exception.WrongObjectReturnedException;
import com.szoldapps.tvtrackr.helper.Constants;
import com.szoldapps.tvtrackr.helper.Constants.DownloadErrorType;
import com.szoldapps.tvtrackr.helper.Constants.DownloadRequestType;
import com.szoldapps.tvtrackr.helper.Constants.DownloadResponseType;
import com.szoldapps.tvtrackr.helper.DialogHelper;
import com.szoldapps.tvtrackr.helper.LinkingHelper;
import com.szoldapps.tvtrackr.helper.LogCatHelper;
import com.szoldapps.tvtrackr.service.async.AsyncAccessService;
import com.szoldapps.tvtrackr.service.async.AsyncAccessServiceImpl;
import com.szoldapps.tvtrackr.service.intent.DownloadService;
import com.szoldapps.tvtrackr.service.listener.ServiceResponseListener;

public class SetupActivity extends FragmentActivity {

	public static final String LOGTAG = LogCatHelper.getAppTag()+SetupActivity.class.getSimpleName().toString();

	private ActionBar mActionBar;

	// ViewPager and Adapter
	private SetupTabPageAdapter mPageAdapter;
	public ViewPager mPager;
	private List<Fragment> mFragList;

	// Service
	private AsyncAccessService mAsyncAccessService;

	// Fragments
	private AddFragment mAddFragment;
	private TrackFragment mTrackFragment;

	private boolean mUpdateIsRunning;

	//	// Crouton Error
	//	private View mRelCrError;
	//	private TextView mTxtCrError;
	//	private TextView mTxtCrErrorRetry;
	//	private ProgressBar mPbarCrErrorRetry;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_setup);

		// set setup title
		mActionBar = getActionBar();
		mActionBar.setTitle(getString(R.string.title_activity_setup));

		mAsyncAccessService = AsyncAccessServiceImpl.getInstance(getApplicationContext());

		buildUI();

		requestCompleteUpdate();
	}

	private void buildUI() {
		mFragList = new ArrayList<Fragment>();
		mAddFragment = new AddFragment();
		mFragList.add(mAddFragment);

		mTrackFragment = new TrackFragment();
		mFragList.add(mTrackFragment);
		mPageAdapter = new SetupTabPageAdapter(getSupportFragmentManager(), mFragList, this);

		mPager = (ViewPager) findViewById(R.id.setup_viewpager);
		mPager.setAdapter(mPageAdapter);

		// Bind the tabs to the ViewPager
		PagerSlidingTabStrip tabs = (PagerSlidingTabStrip) findViewById(R.id.setup_tabs);
		tabs.setViewPager(mPager);

		//		mRelCrError = (View) getLayoutInflater().inflate(R.layout.crouton_error, null);
		//		mTxtCrError = (TextView) mRelCrError.findViewById(R.id.cr_error_txt);
		//		mTxtCrErrorRetry = (TextView) mRelCrError.findViewById(R.id.cr_error_txt_retry);
		//		mPbarCrErrorRetry = (ProgressBar) mRelCrError.findViewById(R.id.cr_error_pbar_retry_loading);
	}


	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// ----- DOWNLOAD SERVICE --------------------------------------------------------------------------------------- //

	public void requestCompleteUpdate() {
		updateStarted();
		Intent intent = new Intent(this, DownloadService.class);
		intent.putExtra(Constants.EXTRA_REQUEST_TYPE, DownloadRequestType.COMPLETE_UPDATE_INCL_SHOWLIST_REFRESH);
		startService(intent);
	}

	public void requestOneShowUpdate(Show show) {
		// Start Download Service
		Intent intent = new Intent(this, DownloadService.class);
		intent.putExtra(Constants.EXTRA_REQUEST_TYPE, DownloadRequestType.SHOW_DOWNLOAD);
		intent.putExtra(Constants.EXTRA_SHOW_TVDB_ID, show.getTvdbID());
		startService(intent);
	}

	private BroadcastReceiver receiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			Bundle bundle = intent.getExtras();
			if (bundle != null) {
				if(bundle.getInt(Constants.EXTRA_RESULT) == Activity.RESULT_OK) {
					DownloadResponseType responseType = (DownloadResponseType) intent.getSerializableExtra(Constants.EXTRA_RESPONSE_TYPE);
					switch (responseType) {
					case COMPLETE_UPDATE_INTERIM_RESULT:
						Log.i(LOGTAG, "INTERIM_RESULT, Should be Updating!");
						notifyFragmentsCompleteRefresh();
						break;
					case COMPLETE_UPDATE_FINAL_RESULT:
						Log.i(LOGTAG, "FINAL_RESULT, Should be Updating!");
						updateEnded();
						break;
					case ONE_SHOW_UPDATE:
						int tvdbId = intent.getIntExtra(Constants.EXTRA_SHOW_TVDB_ID, -1);
						if(tvdbId != -1) {
							notifyFragmentsOneShowRefresh(tvdbId);
						}
						break;
					default: /* DO NOTHING */ break;
					}
				} else {	// AN ERROR HAPPENED
					Log.w(LOGTAG, "GOT SOMETHING");
					DownloadErrorType errorType = (DownloadErrorType) intent.getSerializableExtra(Constants.EXTRA_ERROR_TYPE);
					showErrorFromIntentService(errorType);
					updateEnded();
				}
			}
		}
	};

	@Override
	public void onResume() {
		super.onResume();
		registerReceiver(receiver, new IntentFilter(Constants.EXTRA_DOWNLOAD_SERVICE_NOTIFICATION));
	}

	@Override
	public void onStop() {
		super.onStop();
		unregisterReceiver(receiver);
	}


	public void addShow(Show show) {
		mTrackFragment.addShow(show);
		requestOneShowUpdate(show);
	}

	public void notifyFragmentsCompleteRefresh() {
		mAsyncAccessService.getAllShowsOfUser(new ServiceResponseListener() {
			@Override
			public void onProgress(int bytesWritten, int totalSize) { }

			@Override @SuppressWarnings("unchecked")
			public void onSuccess(Object o) {
				if(o != null) {
					List<Show> shows = (List<Show>) o;
					mAddFragment.refreshCompleteShowlist(shows);
					mTrackFragment.refreshCompleteShowlist(shows);
				} else {
					onFailure(new WrongObjectReturnedException("List<Show>"));
				}
			}

			@Override
			public void onFailure(Throwable throwable) {
				showError(throwable);
			}
		});
	}

	private void notifyFragmentsOneShowRefresh(int tvdbId) {
		mAsyncAccessService.getShowById(tvdbId, new ServiceResponseListener() {
			@Override
			public void onProgress(int bytesWritten, int totalSize) { }

			@Override
			public void onSuccess(Object o) {
				if(o instanceof Show) {
					Show show = (Show) o;
					mAddFragment.refreshOneShowInShowlist(show);
					mTrackFragment.refreshOneShowInShowlist(show);
				} else {
					onFailure(new WrongObjectReturnedException(Show.class));
				}
			}

			@Override
			public void onFailure(Throwable throwable) {
				showError(throwable);
			}
		});
	}



	public boolean isUpdateRunning() {
		return mUpdateIsRunning;
	}

	private void updateStarted() {
		mUpdateIsRunning = true;
		mAddFragment.updateStarted();
		mTrackFragment.updateStarted();
	}

	private void updateEnded() {
		mUpdateIsRunning = false;
		mAddFragment.updateEnded();
		mTrackFragment.updateEnded();
	}



	// MENU Methods
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.menu_setup_refresh:
			requestCompleteUpdate();
			return true;
		case R.id.menu_setup_skip:
			skip();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	public void skip() {
		// Fire the DB write request
		mAsyncAccessService.setFirstTimeUserOnDevice(false, new ServiceResponseListener() {
			@Override public void onProgress(int bytesWritten, int totalSize) { }
			@Override
			public void onSuccess(Object o) {
				if(o instanceof Boolean) {
					// ALL GOOD
				} else {
					onFailure(new WrongObjectReturnedException(Boolean.class));
				}
			}

			@Override
			public void onFailure(Throwable throwable) {
				showError(throwable);
			}
		});
		// Go to Main, no mather what, otherwise DB-Locks could block
		LinkingHelper.goToMainFromSetup(SetupActivity.this);
	}

	public void removeShow(final Show show) {
		mTrackFragment.removeShow(show);
		mAsyncAccessService.uploadUnfollowShow(show, false, new ServiceResponseListener() {

			@Override
			public void onProgress(int bytesWritten, int totalSize) { }

			@Override
			public void onSuccess(Object o) {
				if(o instanceof Boolean) {
					boolean success = (Boolean) o;
					if(success) {
						// DONE
						DialogHelper.showInfoShort(String.format(getString(R.string.unfollowed_show), show.getTitle()), 
								getApplicationContext());
					} else {
						String msg = String.format(getString(R.string.unfollowed_show_offline), show.getTitle());
						DialogHelper.showInfoShort(msg, SetupActivity.this);
					}
				} else {
					onFailure(new WrongObjectReturnedException(Boolean.class));
				}
			}

			@Override
			public void onFailure(Throwable throwable) {
				showError(throwable);
			}
		});
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// ----- ERROR -------------------------------------------------------------------------------------------------- //
	/**
	 * Handles {@link Throwable} Error.
	 * 
	 * @param throwable
	 * @category ERROR
	 */
	protected void showError(Throwable throwable) {
		if(throwable instanceof IOException) {
			if(!(throwable instanceof HttpResponseException)) {
				// Now it is definitely a NO_INTERNET_ERROR
				return;
			}
		}
		DialogHelper.handleDialogErrors(throwable, this);
	}

	/**
	 * Handles {@link DownloadErrorType} Error.
	 * 
	 * @param errorType
	 * @category ERROR
	 */
	protected void showErrorFromIntentService(DownloadErrorType errorType) {
		switch (errorType) {
		case NO_VALID_REQUEST_TYPE_ERROR:	// should not happen
			Log.e(LOGTAG, "INVALID DownloadRequestType was send to DownloadService!");
			break;
		case NO_INTERNET_ERROR:
			Log.e(LOGTAG, "NO_INTERNET_ERROR happened xyz");
			DialogHelper.getNoInternetErrorDialogBuilder(this)
			.setPositiveButton(getString(R.string.error_btn_retry), new AlertDialog.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int id) {
					requestCompleteUpdate();
				}
			}).create().show();
			break;
		default: // handles SQL_ERROR, WRONG_CREDENTIALS_ERROR & UNKNOWN_ERROR
			DialogHelper.handleDialogErrors(errorType, this);
			break;
		}
	}

	/**
	 * Changes to Pager to TrackFragment and calls {@link TrackFragment#goToShow(int)}.
	 * @param position
	 */
	public void goToTrackFragAndDisplayShow(int position) {
		mPager.setCurrentItem(1);
		mTrackFragment.goToShow(position);
	}
}
