package com.szoldapps.tvtrackr.activity.setup;

import java.util.Collections;
import java.util.List;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.util.Log;
import android.util.SparseArray;

import com.szoldapps.tvtrackr.activity.show.SeasonsFragment;
import com.szoldapps.tvtrackr.db.model.Show;
import com.szoldapps.tvtrackr.helper.Constants;
import com.szoldapps.tvtrackr.helper.LogCatHelper;

public class TrackTabPageAdapter extends FragmentPagerAdapter {

	public static final String LOGTAG = LogCatHelper.getAppTag()+TrackTabPageAdapter.class.getSimpleName().toString();
	SparseArray<SeasonsFragment> mFragments = new SparseArray<SeasonsFragment>();

	private List<Show> mShows;


	public TrackTabPageAdapter(FragmentManager fm, List<Show> shows) {
		super(fm);
		mShows = shows;
	}

	@Override 
	public Fragment getItem(int position) {
		Log.d(LOGTAG, "SeaonsTabPagerAdapter pos = "+ position);
		SeasonsFragment fragment = new SeasonsFragment();
		Bundle args = new Bundle();
		args.putInt(Constants.EXTRA_SHOW_TVDB_ID, mShows.get(position).getTvdbID());
		args.putSerializable(Constants.EXTRA_SHOW_DOWNLOAD_STATUS, mShows.get(position).getDownloadStatus());
		fragment.setArguments(args);
		mFragments.put(position, fragment);
		return fragment;
	}

	@Override
	public int getCount() {
		return mShows.size();
	}

	public void addShow(Show show) {
		mShows.add(show);
		Collections.sort(mShows, Show.getComparator());
		this.notifyDataSetChanged();
	}

	public List<Show> getShows() {
		return mShows;
	}

	public void setShows(List<Show> shows) {
		mShows = shows;
		notifyDataSetChanged();
	}

	public void refreshFragmentData(int position) {
		SeasonsFragment fragment = mFragments.get(position);
		if(fragment != null) {
			if(!mShows.isEmpty()) {
				Show show = mShows.get(position);
				fragment.loadFragmentData(show.getTvdbID());
			}
		}
	}

	public void refreshFragmentData(Show show) {
		refreshFragmentData(mShows.indexOf(show));
	}

	public void removeShow(Show show) {
		mShows.remove(mShows.indexOf(show));
		this.notifyDataSetChanged();
	}

}

