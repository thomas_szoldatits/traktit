package com.szoldapps.tvtrackr.activity.main.trending;

import java.util.ArrayList;
import java.util.List;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.szoldapps.tvtrackr.R;
import com.szoldapps.tvtrackr.activity.main.MainActivity;
import com.szoldapps.tvtrackr.activity.main.search.SearchFragment;
import com.szoldapps.tvtrackr.dto.show.TrendingShowDTO;
import com.szoldapps.tvtrackr.helper.DialogHelper;
import com.szoldapps.tvtrackr.helper.LogCatHelper;
import com.szoldapps.tvtrackr.helper.MenuHelper;
import com.szoldapps.tvtrackr.service.async.AsyncAccessService;
import com.szoldapps.tvtrackr.service.async.AsyncAccessServiceImpl;
import com.szoldapps.tvtrackr.service.listener.ServiceResponseListener;

public class TrendingShowsFragment extends Fragment {

	public static final String LOGTAG = LogCatHelper.getAppTag()+TrendingShowsFragment.class.getSimpleName().toString();

	private AsyncAccessService mAsyncAccessService;

	private MainActivity mMainActivity;
	private View mRootView;

	private RelativeLayout mRelLoading;
	private ListView mListView;
	private TrendingListArrayAdapter mListViewAdapter;
	// Menu
	private MenuItem mRefreshMenuItem;
	private boolean mUpdateRunning;
	// Error
	private Dialog mNoInternetDialog;

	public TrendingShowsFragment() { /* Empty constructor required for fragment subclasses */ }

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		mMainActivity = (MainActivity) getActivity();
		mRootView = inflater.inflate(R.layout.fragment_trending_shows, container, false);

		mAsyncAccessService = AsyncAccessServiceImpl.getInstance(mMainActivity.getApplicationContext());

		buildUI();

		return mRootView;
	}

	private void buildUI() {
		setHasOptionsMenu(true);
		// Loading
		mRelLoading = (RelativeLayout) mRootView.findViewById(R.id.loading_rel_container);
		// List
		mListView = (ListView) mRootView.findViewById(R.id.trending_shows_list);
		mListViewAdapter = new TrendingListArrayAdapter(this, mMainActivity.getApplicationContext(), 
				R.id.calendar_day_list, new ArrayList<TrendingShowDTO>());
		mListView.setAdapter(mListViewAdapter);

	}

	@Override
	public void onResume() {
		loadDynamicContent();
		super.onResume();
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// ----- LOAD_DYNAMIC_CONTENT ----------------------------------------------------------------------------------- //

	private void loadDynamicContent() {
		showLoading(true);
		mAsyncAccessService.getTrendingShows(new ServiceResponseListener() {
			@Override public void onProgress(int bytesWritten, int totalSize) { }

			@Override @SuppressWarnings("unchecked")
			public void onSuccess(Object o) {
				if(o != null) {
					List<TrendingShowDTO> shows = (List<TrendingShowDTO>) o;
					mListViewAdapter.clear();
					mListViewAdapter.addAll(shows);
					// Scrolls to the top of the ListView
					mListView.setSelectionAfterHeaderView();
					showLoading(false);
				}
			}

			@Override
			public void onFailure(Throwable throwable) {
				showError(throwable);
			}
		});
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// ----- LOADING ------------------------------------------------------------------------------------------------ //

	private void showLoading(boolean b) {
		if(b) {
			mUpdateRunning = true;
			MenuHelper.updateStarted(mRefreshMenuItem);
			mListView.setVisibility(View.INVISIBLE);
			mRelLoading.setVisibility(View.VISIBLE);
		} else {
			mUpdateRunning = false;
			MenuHelper.updateEnded(mRefreshMenuItem);
			mListView.setVisibility(View.VISIBLE);
			mRelLoading.setVisibility(View.GONE);
		}
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// ----- MENU --------------------------------------------------------------------------------------------------- //

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		inflater.inflate(R.menu.trending_shows, menu);
		mRefreshMenuItem = menu.findItem(R.id.menu_refresh);
		if(mUpdateRunning) {
			MenuHelper.updateStarted(mRefreshMenuItem);
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.menu_search:
			mMainActivity.changeFragment(
					new SearchFragment(), getString(R.string.navigation_item_search), MainActivity.NAVI_POS_SEARCH);
			break;
		case R.id.menu_refresh:
			loadDynamicContent();
			break;
		default:
			break;
		}
		return super.onOptionsItemSelected(item);
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// ----- ERROR -------------------------------------------------------------------------------------------------- //

	public void showError(Throwable throwable) {
		if(DialogHelper.isInternetError(throwable)) {
			showLoading(false);
			if(mNoInternetDialog == null) {
				mNoInternetDialog = DialogHelper.getNoInternetErrorDialogBuilder(getActivity())
						.setPositiveButton(getString(R.string.error_btn_retry), new AlertDialog.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog, int id) {
								loadDynamicContent();
							}
						}).create();
			}
			if(!mNoInternetDialog.isShowing()) {
				mNoInternetDialog.show();
			}
			return;
		}
		DialogHelper.handleDialogErrors(throwable, mMainActivity);
	}

}
