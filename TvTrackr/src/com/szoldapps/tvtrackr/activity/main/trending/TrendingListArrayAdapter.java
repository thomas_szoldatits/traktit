package com.szoldapps.tvtrackr.activity.main.trending;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.szoldapps.tvtrackr.R;
import com.szoldapps.tvtrackr.dto.show.MinShowDTO;
import com.szoldapps.tvtrackr.dto.show.TrendingShowDTO;
import com.szoldapps.tvtrackr.exception.WrongObjectReturnedException;
import com.szoldapps.tvtrackr.helper.DialogHelper;
import com.szoldapps.tvtrackr.helper.ImageLoaderHelper;
import com.szoldapps.tvtrackr.helper.ImageURLHelper;
import com.szoldapps.tvtrackr.helper.LinkingHelper;
import com.szoldapps.tvtrackr.helper.LogCatHelper;
import com.szoldapps.tvtrackr.helper.NumbersHelper;
import com.szoldapps.tvtrackr.service.async.AsyncAccessService;
import com.szoldapps.tvtrackr.service.async.AsyncAccessServiceImpl;
import com.szoldapps.tvtrackr.service.listener.ServiceResponseListener;

public class TrendingListArrayAdapter extends ArrayAdapter<TrendingShowDTO> {
	public static final String LOGTAG = LogCatHelper.getAppTag()+TrendingListArrayAdapter.class.getSimpleName().toString();

	private AsyncAccessService mAsyncAccessService;

	private final Context mContext;
	private TrendingShowsFragment mTrendingShowsFragment;

	public TrendingListArrayAdapter(TrendingShowsFragment recommendationsFrag, Context context, int resource, List<TrendingShowDTO> shows) {
		super(context, resource, shows);
		mTrendingShowsFragment = recommendationsFrag;
		mContext = context;
		mAsyncAccessService = AsyncAccessServiceImpl.getInstance(mContext);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater) mTrendingShowsFragment.getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		RelativeLayout relListItem = null;
		if(getRealCount() == 0) {
			relListItem = (RelativeLayout) inflater.inflate(R.layout.li_trending_shows_info, parent, false);
		} else {
			relListItem = (RelativeLayout) inflater.inflate(R.layout.li_trending_shows, parent, false);

			final TrendingShowDTO show = getItem(position);
			relListItem.setTag(show);
			relListItem.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					LinkingHelper.goToShow((MinShowDTO) v.getTag(), mTrendingShowsFragment.getActivity(), 
							false);
				}
			});
			// Poster
			ImageView imgPoster = (ImageView) relListItem.findViewById(R.id.li_trending_shows_img_poster);
			String imgUrl = ImageURLHelper.getImageURLByImageType(show.getImages().getPoster(), ImageURLHelper.POSTER_SMALL);
			ImageLoader.getInstance().displayImage(imgUrl, imgPoster, ImageLoaderHelper.getDIOPosterOrHeadShot(mContext));

			// Title
			TextView txtTitle = (TextView) relListItem.findViewById(R.id.li_trending_shows_txt_title);
			txtTitle.setText(show.getTitle());

			// Watching
			TextView txtWatching = (TextView) relListItem.findViewById(R.id.li_trending_shows_txt_watchers);
			txtWatching.setText(NumbersHelper.format(show.getWatchers()));

			// Rating
			TextView txtRating = (TextView) relListItem.findViewById(R.id.li_trending_shows_txt_rating);
			txtRating.setText(show.getRatings().getPercentage() + mContext.getString(R.string.symbol_percent));

			// Rating votes
			TextView txtRatingVotes = (TextView) relListItem.findViewById(R.id.li_trending_shows_txt_rating_votes);
			long votes = show.getRatings().getVotes();
			if(votes==1) {
				txtRatingVotes.setText(votes + mContext.getString(R.string.rating_vote));
			} else {
				txtRatingVotes.setText(NumbersHelper.format(votes) + mContext.getString(R.string.rating_votes));
			}

			// Follow Img
			final ImageView imgFollow = (ImageView) relListItem.findViewById(R.id.li_recomendations_img_follow);
			//			final ProgressBar pbarLoading = (ProgressBar) relListItem.findViewById(R.id.li_recomendations_pbar_download_loading);
			//			final TextView txtLoading = (TextView) relListItem.findViewById(R.id.li_recomendations_txt_download);
			imgFollow.setTag(show.getTvdb_id());
			imgFollow.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					followShow(imgFollow, show);
				}
			});
			// Check if Show followed (can only happen, if followed and then scrolled)
			mAsyncAccessService.isShowFollowedByCurrentUser(show.getTvdb_id(), new ServiceResponseListener() {
				@Override
				public void onProgress(int bytesWritten, int totalSize) { }

				@Override
				public void onSuccess(Object o) {
					if(o instanceof Boolean) {
						if((Boolean)o) {
							imgFollow.setOnClickListener(null);
						}
						int icon = ((Boolean)o) ? R.drawable.ic_follow_blue : R.drawable.ic_follow_gray;
						imgFollow.setImageDrawable(mContext.getResources().getDrawable(icon));
					} else {
						onFailure(new WrongObjectReturnedException(Boolean.class));
					}
				}

				@Override
				public void onFailure(Throwable throwable) {
					mTrendingShowsFragment.showError(throwable);
				}
			});

		}

		return relListItem;
	}

	private void followShow(final ImageView imgFollow, final TrendingShowDTO show) {
		//		imgFollow.setVisibility(View.INVISIBLE);
		//		pbarLoading.setVisibility(View.VISIBLE);
		//		txtLoading.setVisibility(View.VISIBLE);
		// Follow show
		mAsyncAccessService.uploadFollowAndSaveShow(show, new ServiceResponseListener() {
			@Override public void onProgress(int bytesWritten, int totalSize) { }

			@Override
			public void onSuccess(Object o) {
				if(o instanceof Boolean) {
					if((Boolean) o) {
						imgFollow.setImageDrawable(
								mContext.getResources().getDrawable(R.drawable.ic_follow_blue));
						String msg = String.format(mContext.getString(R.string.followed_show), show.getTitle());
						DialogHelper.showInfoShort(msg, mTrendingShowsFragment.getActivity());
						imgFollow.setOnClickListener(null);
					} else {
						DialogHelper.getFollowErrorDialogBuilder(mTrendingShowsFragment.getActivity())
						.setPositiveButton(mContext.getString(R.string.error_btn_retry),
								new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,int id) {
								followShow(imgFollow, show);
							}
						}).create().show();
					}
				} else {
					onFailure(new WrongObjectReturnedException(Boolean.class));
				}
			}
			@Override
			public void onFailure(Throwable throwable) {
				mTrendingShowsFragment.showError(throwable);
			}
		});
	}

	public CharSequence getGenresStr(ArrayList<String> genres) {
		String genreStr = "";
		boolean first = true;
		for (String gs : genres) {
			if(first) {
				genreStr += gs;
				first = false;
			} else {
				genreStr += ", " + gs;
			}
		}
		return genreStr;
	}


	@Override
	public int getCount() {
		if(super.getCount() == 0) {
			return 1;
		}
		return super.getCount();
	}

	public int getRealCount() {
		return super.getCount();
	}

	public int getTvdbIdOfShow(int position) {
		TrendingShowDTO show = getItem(position);
		if(show != null) {
			return show.getTvdb_id();
		}
		return -1;
	}

	//	public int getPositionOfShow(int tvdbId) {
	//		int position = 0;
	//		for (ResponseTrendingShowDTO show : mShows) {
	//			if(show.getTvdb_id() == tvdbId) {
	//				return position;
	//			}
	//			position++;
	//		}
	//		return -1;
	//	}

}