package com.szoldapps.tvtrackr.activity.main.towatch;

import java.util.List;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.szoldapps.tvtrackr.R;
import com.szoldapps.tvtrackr.activity.main.MainActivity;
import com.szoldapps.tvtrackr.activity.main.search.SearchFragment;
import com.szoldapps.tvtrackr.db.model.Show;
import com.szoldapps.tvtrackr.helper.DialogHelper;
import com.szoldapps.tvtrackr.helper.LogCatHelper;
import com.szoldapps.tvtrackr.service.async.AsyncAccessService;
import com.szoldapps.tvtrackr.service.async.AsyncAccessServiceImpl;
import com.szoldapps.tvtrackr.service.listener.ServiceResponseListener;

/**
 * Fragment that appears in the "content_frame", shows a planet
 */
public class ToWatchFragment extends Fragment {

	public static final String LOGTAG = LogCatHelper.getAppTag()+ToWatchFragment.class.getSimpleName().toString();

	private AsyncAccessService mAsyncAccessService;

	private MainActivity mMainActivity;
	private View mRootView;

	private ListView mListView;
	private ToWatchListArrayAdapter mListViewAdapter;

	public ToWatchFragment() { /* Empty constructor required for fragment subclasses */ }

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		mMainActivity = (MainActivity) getActivity();
		mRootView = inflater.inflate(R.layout.fragment_to_watch, container, false);

		mAsyncAccessService = AsyncAccessServiceImpl.getInstance(mMainActivity.getApplicationContext());

		buildUI();

		return mRootView;
	}

	@Override
	public void onResume() {
		loadDynamicContent();
		super.onResume();
	}

	private void buildUI() {
		setHasOptionsMenu(true);
		mListView = (ListView) mRootView.findViewById(R.id.to_watch_list);
		mListViewAdapter = new ToWatchListArrayAdapter(this, mMainActivity.getApplicationContext(), R.id.calendar_day_list, null);
		mListView.setAdapter(mListViewAdapter);
	}


	public void loadDynamicContent() {
		mAsyncAccessService.getToWatchShows(new ServiceResponseListener() {

			@Override public void onProgress(int bytesWritten, int totalSize) { }

			@Override @SuppressWarnings("unchecked")
			public void onSuccess(Object o) {
				if(o != null) {
					List<Show> shows = (List<Show>) o;
					for (Show show : shows) {
						Log.w(LOGTAG, show.getTitle());
					}
					mListViewAdapter.setShows(shows);
				}

			}

			@Override
			public void onFailure(Throwable throwable) {
				showError(throwable);
			}

		});
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		inflater.inflate(R.menu.to_watch, menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.menu_search:
			((MainActivity) getActivity())
			.changeFragment(new SearchFragment(), getString(R.string.navigation_item_search), 
					MainActivity.NAVI_POS_SEARCH);
			break;
		default:
			break;
		}
		return super.onOptionsItemSelected(item);
	}

	private void showError(Throwable throwable) {
		DialogHelper.handleDialogErrors(throwable, mMainActivity);
	}

}
