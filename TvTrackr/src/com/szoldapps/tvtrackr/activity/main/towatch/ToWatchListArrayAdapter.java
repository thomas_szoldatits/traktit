package com.szoldapps.tvtrackr.activity.main.towatch;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.szoldapps.tvtrackr.R;
import com.szoldapps.tvtrackr.db.model.Episode;
import com.szoldapps.tvtrackr.db.model.Show;
import com.szoldapps.tvtrackr.dto.internal.InternalToWatchDTO;
import com.szoldapps.tvtrackr.exception.WrongObjectReturnedException;
import com.szoldapps.tvtrackr.helper.DateHelper;
import com.szoldapps.tvtrackr.helper.DialogHelper;
import com.szoldapps.tvtrackr.helper.ImageLoaderHelper;
import com.szoldapps.tvtrackr.helper.LinkingHelper;
import com.szoldapps.tvtrackr.helper.LogCatHelper;
import com.szoldapps.tvtrackr.helper.DateHelper.DateHelperFormat;
import com.szoldapps.tvtrackr.service.async.AsyncAccessService;
import com.szoldapps.tvtrackr.service.async.AsyncAccessServiceImpl;
import com.szoldapps.tvtrackr.service.listener.ServiceResponseListener;

public class ToWatchListArrayAdapter extends ArrayAdapter<Show> {
	public static final String LOGTAG = 
			LogCatHelper.getAppTag()+ToWatchListArrayAdapter.class.getSimpleName().toString();

	private final Context mContext;
	private ToWatchFragment mToWatchFragment;
	private List<Show> mShows;

	private AsyncAccessService mAsyncAccessService;

	public ToWatchListArrayAdapter(ToWatchFragment toWatchFrag, Context context, int resource, List<Show> shows) {
		super(context, resource, shows);
		mToWatchFragment = toWatchFrag;
		mContext = context;
		mShows = shows;
		mAsyncAccessService = AsyncAccessServiceImpl.getInstance(mContext);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = 
				(LayoutInflater) mToWatchFragment.getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		RelativeLayout relListItem;
		if(mShows == null) {
			relListItem = (RelativeLayout) inflater.inflate(R.layout.li_calendar_day_info, parent, false);
			ProgressBar pbarLoading = (ProgressBar) relListItem.findViewById(R.id.li_calendar_day_info_pbar_loading);
			TextView txtInfo = (TextView) relListItem.findViewById(R.id.li_calendar_day_info_txt_hl);
			pbarLoading.setVisibility(View.VISIBLE);
			txtInfo.setText(mContext.getString(R.string.to_watch_loading_episodes));
		} else {
			if(mShows.isEmpty()) {
				relListItem = (RelativeLayout) inflater.inflate(R.layout.li_calendar_day_info, parent, false);
				ProgressBar pbarLoading = 
						(ProgressBar) relListItem.findViewById(R.id.li_calendar_day_info_pbar_loading);
				TextView txtInfo = (TextView) relListItem.findViewById(R.id.li_calendar_day_info_txt_hl);
				pbarLoading.setVisibility(View.INVISIBLE);
				txtInfo.setText(mContext.getString(R.string.to_watch_no_episodes));
			} else {
				final Show show = mShows.get(position);
				relListItem = (RelativeLayout) inflater.inflate(R.layout.li_to_watch, parent, false);
				ImageView imgBanner = (ImageView) relListItem.findViewById(R.id.li_to_watch_img_banner);
				ImageLoader.getInstance().displayImage(
						show.getBannerURL(), imgBanner, ImageLoaderHelper.getDIOBanner(mContext));

				TextView txtHl = (TextView) relListItem.findViewById(R.id.li_to_watch_txt_title);
				TextView txtTime = (TextView) relListItem.findViewById(R.id.li_to_watch_txt_time);
				TextView txtSeText = (TextView) relListItem.findViewById(R.id.li_to_watch_txt_se_text);
				ImageView imgSeen = (ImageView) relListItem.findViewById(R.id.li_to_watch_img_btn_seen);
				ProgressBar pbarSeenLoding = (ProgressBar) relListItem.findViewById(R.id.li_to_watch_pbar_seen_loading);
				TextView txtEpisodesLeft = (TextView) relListItem.findViewById(R.id.li_to_watch_txt_episodes_left);
				TextView txtEpisodesLeftSubLine = 
						(TextView) relListItem.findViewById(R.id.li_to_watch_txt_episodes_left_sub_text);

				imgSeen.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						v.setVisibility(View.INVISIBLE);
						final InternalToWatchDTO toWatchDTO = (InternalToWatchDTO) v.getTag();
						final Episode episode = toWatchDTO.getEpisode();
						toWatchDTO.getPbarSeenLoding().setVisibility(View.VISIBLE);
						mAsyncAccessService.uploadEpisodeSeenToggle(episode, new ServiceResponseListener() {

							@Override public void onProgress(int bytesWritten, int totalSize) { }

							@Override
							public void onSuccess(Object o) {
								String msg = String.format(mContext.getString(R.string.to_watch_marked_seen_success), 
										episode.getSeasonEpisodeText(), episode.getSeason().getShow().getTitle());
								DialogHelper.showInfoShort(msg, mContext);
								loadNextEpisodeDetails(toWatchDTO, episode.getShowTvdbId());
								toWatchDTO.getPbarSeenLoding().setVisibility(View.GONE);
								toWatchDTO.getImgSeen().setVisibility(View.VISIBLE);
							}

							@Override
							public void onFailure(Throwable throwable) {
								showError(throwable);
							}
						});
					}
				});
				relListItem.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						final Episode episode = ((InternalToWatchDTO) v.getTag()).getEpisode();
						LinkingHelper.goToEpisode(episode.getId(), episode.getShowTvdbId(), show.getTitle(), 
								mToWatchFragment.getActivity());
					}
				});

				InternalToWatchDTO toWatchDTO = new InternalToWatchDTO(relListItem, txtHl, txtTime, txtSeText, imgSeen, 
						pbarSeenLoding, txtEpisodesLeft, txtEpisodesLeftSubLine);
				loadNextEpisodeDetails(toWatchDTO, show.getTvdbID());
			}
		}
		return relListItem;
	}

	private void loadNextEpisodeDetails(InternalToWatchDTO toWatchDTO, int showTvdbId) {
		mAsyncAccessService.getNextEpisodeToWatchAndLeftEpisodeCount(toWatchDTO, showTvdbId, 
				new ServiceResponseListener() {
			@Override public void onProgress(int bytesWritten, int totalSize) { }

			@Override
			public void onSuccess(Object o) {
				if(o instanceof InternalToWatchDTO) {
					InternalToWatchDTO toWatchDTO = (InternalToWatchDTO) o;
					Episode episode = toWatchDTO.getEpisode();
					if(episode != null) {
						if(toWatchDTO.getTxtSeText() != null && toWatchDTO.getTxtHl() != null && 
								toWatchDTO.getTxtTime() != null && toWatchDTO.getImgSeen() != null && 
								toWatchDTO.getTxtEpisodesLeft() != null) {
							if(toWatchDTO.getEpisodesLeft().equals(1)) {
								toWatchDTO.getTxtEpisodesLeftSubLine()
								.setText(mContext.getString(R.string.to_watch_episodes_left_sub_line_single));
							}
							toWatchDTO.getTxtEpisodesLeft().setText(toWatchDTO.getEpisodesLeft().toString());
							toWatchDTO.getTxtSeText().setText(episode.getSeasonEpisodeText());
							toWatchDTO.getTxtHl().setText(episode.getTitle());
							String airedOn = DateHelper.getDateStrWithTimeInS(episode.getFirstAiredUTC(), 
									DateHelperFormat.MEDIUM_DATE_AND_TIME_WITH_WEEKDAY, mContext);
							airedOn = String.format(mContext.getString(R.string.to_watch_aired_on), airedOn, 
									episode.getSeason().getShow().getNetwork());
							toWatchDTO.getTxtTime().setText(airedOn);
							toWatchDTO.getImgSeen().setTag(toWatchDTO);
							toWatchDTO.getRelListItem().setTag(toWatchDTO);
						}
					} else {
						mToWatchFragment.loadDynamicContent();
					}
				} else {
					onFailure(new WrongObjectReturnedException(InternalToWatchDTO.class));
				}
			}

			@Override
			public void onFailure(Throwable throwable) {
				showError(throwable);
			}
		});
	}


	@Override
	public int getCount() {
		if(mShows == null) {
			return 1;
		}
		if(mShows.isEmpty()) {
			return 1;
		}
		return mShows.size();
	}

	public void setShows(List<Show> shows) {
		this.mShows = shows;
		this.notifyDataSetChanged();
	}
	
	private void showError(Throwable throwable) {
		DialogHelper.handleDialogErrors(throwable, mToWatchFragment.getActivity());
	}


}