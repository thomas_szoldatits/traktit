package com.szoldapps.tvtrackr.activity.main.yourshows;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.szoldapps.tvtrackr.R;
import com.szoldapps.tvtrackr.activity.main.MainActivity;
import com.szoldapps.tvtrackr.activity.main.search.SearchFragment;
import com.szoldapps.tvtrackr.db.model.Show;
import com.szoldapps.tvtrackr.helper.Constants;
import com.szoldapps.tvtrackr.helper.Constants.DownloadResponseType;
import com.szoldapps.tvtrackr.helper.DialogHelper;
import com.szoldapps.tvtrackr.helper.LinkingHelper;
import com.szoldapps.tvtrackr.helper.LogCatHelper;
import com.szoldapps.tvtrackr.service.async.AsyncAccessService;
import com.szoldapps.tvtrackr.service.async.AsyncAccessServiceImpl;
import com.szoldapps.tvtrackr.service.listener.ServiceResponseListener;

/**
 * Fragment that appears in the "content_frame", shows a planet
 */
public class YourShowsFragment extends Fragment {

	public static final String LOGTAG = LogCatHelper.getAppTag()+YourShowsFragment.class.getSimpleName().toString();

	private View mRootView;
	private MainActivity mMainActivity;
	private ListView mListView;
	private YourShowsListArrayAdapter mListViewAdapter;

	private AsyncAccessService mAsyncAccessService;
	private LinearLayout mLinLoading;


	public YourShowsFragment() { /* Empty constructor required for fragment subclasses */ }

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		mMainActivity = (MainActivity) getActivity();
		mRootView = inflater.inflate(R.layout.fragment_your_shows, container, false);
		mAsyncAccessService = AsyncAccessServiceImpl.getInstance(mMainActivity.getApplicationContext());

		buildUI();
		loadDynamicContent();

		return mRootView;
	}

	private void buildUI() {
		// Loading
		mLinLoading = (LinearLayout) mRootView.findViewById(R.id.your_shows_lin_loading);
		// List
		mListView = (ListView) mRootView.findViewById(R.id.your_shows_list);
		mListViewAdapter = new YourShowsListArrayAdapter(this, mMainActivity.getApplicationContext(), R.id.your_shows_list, new ArrayList<Show>());
		mListView.setAdapter(mListViewAdapter);
		mListView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View relListItem, int pos, long id) {
				Show show = mListViewAdapter.getItem(pos);
				if(show == null) {
					return;
				}
				LinkingHelper.goToShow(show, YourShowsFragment.this.getActivity(), false);
			}
		});
	}


	public void loadDynamicContent() {
		displayLoading(true);
		mAsyncAccessService.getAllShowsOfUser(new ServiceResponseListener() {
			@Override public void onProgress(int bytesWritten, int totalSize) { }

			@Override @SuppressWarnings("unchecked")
			public void onSuccess(Object o) {
				if(o != null) {
//					mListViewAdapter.setShows((List<Show>) o);
					mListViewAdapter.clear();
					mListViewAdapter.addAll((List<Show>) o);
					mListViewAdapter.notifyDataSetChanged();
					displayLoading(false);
				}
			}

			@Override
			public void onFailure(Throwable throwable) {
				showError(throwable);

			}
		});
	}

	private void displayLoading(boolean b) {
		if(b) {
			mListView.setVisibility(View.INVISIBLE);
			mLinLoading.setVisibility(View.VISIBLE);
		} else {
			mListView.setVisibility(View.VISIBLE);
			mLinLoading.setVisibility(View.GONE);
		}
	}


	private BroadcastReceiver receiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			Bundle bundle = intent.getExtras();
			if (bundle != null) {
				int resultCode = bundle.getInt(Constants.EXTRA_RESULT);
				if(resultCode == Activity.RESULT_OK) {
					DownloadResponseType responseType = (DownloadResponseType) intent.getSerializableExtra(Constants.EXTRA_RESPONSE_TYPE);
					switch (responseType) {
					case COMPLETE_UPDATE_FINAL_RESULT:
						loadDynamicContent();
						break;
					default: /* DO NOTHING */ break;
					}
				}
			}
		}
	};

	@Override
	public void onResume() {
		super.onResume();
		mMainActivity.registerReceiver(receiver, new IntentFilter(Constants.EXTRA_DOWNLOAD_SERVICE_NOTIFICATION));
	}


	@Override
	public void onStop() {
		super.onStop();
		mMainActivity.unregisterReceiver(receiver);
	}
	
	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		inflater.inflate(R.menu.your_shows, menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.menu_search:
			((MainActivity) getActivity()).changeFragment(new SearchFragment(), getString(R.string.navigation_item_search), MainActivity.NAVI_POS_SEARCH);
			break;
		default:
			break;
		}
		return super.onOptionsItemSelected(item);
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// ----- ERROR -------------------------------------------------------------------------------------------------- //

	private void showError(Throwable throwable) {
		DialogHelper.handleDialogErrors(throwable, mMainActivity);
	}
}
