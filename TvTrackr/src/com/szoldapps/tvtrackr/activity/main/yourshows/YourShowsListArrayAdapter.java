package com.szoldapps.tvtrackr.activity.main.yourshows;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.content.DialogInterface;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.szoldapps.tvtrackr.R;
import com.szoldapps.tvtrackr.db.model.Show;
import com.szoldapps.tvtrackr.dto.internal.InternalProgressDTO;
import com.szoldapps.tvtrackr.helper.DialogHelper;
import com.szoldapps.tvtrackr.helper.ImageLoaderHelper;
import com.szoldapps.tvtrackr.helper.ImageURLHelper;
import com.szoldapps.tvtrackr.helper.LogCatHelper;
import com.szoldapps.tvtrackr.service.async.AsyncAccessService;
import com.szoldapps.tvtrackr.service.async.AsyncAccessServiceImpl;
import com.szoldapps.tvtrackr.service.listener.ServiceResponseListener;

public class YourShowsListArrayAdapter extends ArrayAdapter<Show> {
	public static final String LOGTAG = LogCatHelper.getAppTag()+YourShowsListArrayAdapter.class.getSimpleName().toString();

	private final Context mContext;
	private YourShowsFragment mYourShowsFragment;
	private AsyncAccessService mAsyncAccessService;

	public YourShowsListArrayAdapter(YourShowsFragment yourShowsFragment, Context context, int resource, List<Show> shows) {
		super(context, resource, shows);
		mYourShowsFragment = yourShowsFragment;
		mContext = context;
		mAsyncAccessService = AsyncAccessServiceImpl.getInstance(mContext);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater) mYourShowsFragment.getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		if(super.getCount() == 0) {
			RelativeLayout relListItem = (RelativeLayout) inflater.inflate(R.layout.li_your_shows_info, parent, false);
			return relListItem;
		} else {
			RelativeLayout relListItem = (RelativeLayout) inflater.inflate(R.layout.li_your_shows, parent, false);
			relListItem = fillListItem(relListItem, position);
			return relListItem;
		}
	}
	
	private RelativeLayout fillListItem(final RelativeLayout relListItem, int position) {
		final Show show = getItem(position);	

		// Poster
		ImageView imgPoster = (ImageView) relListItem.findViewById(R.id.li_your_shows_img_poster);
		String imgUrl = ImageURLHelper.getImageURLByImageType(show.getPosterURL(), ImageURLHelper.POSTER_SMALL);
		ImageLoader.getInstance().displayImage(imgUrl, imgPoster, ImageLoaderHelper.getDIOPosterOrHeadShot(mContext));

		// Title
		TextView txtTitle = (TextView) relListItem.findViewById(R.id.li_your_shows_txt_title);
		txtTitle.setText(show.getTitle());

		mAsyncAccessService.getProgressOfAiredEpisodes(show, new ServiceResponseListener() {
			@Override public void onProgress(int bytesWritten, int totalSize) { }

			@Override
			public void onSuccess(Object o) {
				if(o instanceof InternalProgressDTO) {
					InternalProgressDTO pDto = (InternalProgressDTO) o;

					TextView txtPercentage = (TextView) relListItem.findViewById(R.id.li_your_shows_txt_percentage);
					TextView txtEpsSeen = (TextView) relListItem.findViewById(R.id.li_your_shows_txt_episodes_seen);
					ProgressBar pbarProgress = (ProgressBar) relListItem.findViewById(R.id.li_your_shows_pbar_season_progress);
					TextView txtTimeLeft = (TextView) relListItem.findViewById(R.id.li_your_shows_txt_time_left_to_watch);

					// txtEpsSeen
					String episodesSeen = "";
					if(pDto.getAiredEpisodeCount() > 0) {
						episodesSeen = String.format(mContext.getString(R.string.progress_seen_episodes), 
								pDto.getWatchedEpisodeCount(), pDto.getAiredEpisodeCount());
					}
					long unairedCount = pDto.getEpisodeCount() - pDto.getAiredEpisodeCount();
					if(unairedCount > 0) {
						boolean withBrackets = (episodesSeen.isEmpty()) ? false : true;
						episodesSeen += withBrackets ? mContext.getString(R.string.progress_space_bracket_open) : "";
						episodesSeen += String.format(mContext.getString(R.string.progress_unaired_episodes), unairedCount);
						episodesSeen += withBrackets ? mContext.getString(R.string.progress_bracket_close) : "";
					}
					txtEpsSeen.setText(episodesSeen);
					
					// Percentage
					txtPercentage.setText(pDto.getPercent() + mContext.getString(R.string.symbol_percent));

					// ProgressBar
					pbarProgress.setProgress(pDto.getPercent());

					// TimeLeft
					if(pDto.getEpisodesLeftToWatch() == 0) {
						txtTimeLeft.setText(mContext.getString(R.string.progress_watched_all));
					} else {
						String episodes = "";
						if(pDto.getEpisodesLeftToWatch() == 1) {
							episodes = pDto.getEpisodesLeftToWatch() + " " + mContext.getString(R.string.progress_episode);
						} else {
							episodes = pDto.getEpisodesLeftToWatch() + " " + mContext.getString(R.string.progress_episodes);
						}
						String time = "";
						if(pDto.getHoursLeftToWatch() != 0) {
							time = pDto.getHoursLeftToWatch() + mContext.getString(R.string.progress_hours);
						}
						if(pDto.getMinLeftToWatch() != 0) {
							if(!time.isEmpty()) {
								time += ", ";
							}
							time += pDto.getMinLeftToWatch() + mContext.getString(R.string.progress_minutes);
						}
						String spanStr = String.format(mContext.getString(R.string.progress_left_to_watch), episodes, time);
						txtTimeLeft.setText(Html.fromHtml(spanStr), TextView.BufferType.SPANNABLE);
					}
				}
			}

			@Override
			public void onFailure(Throwable throwable) {
				showError(throwable);
			}
		});

		// imgFollow
		final ImageView imgFollow = (ImageView) relListItem.findViewById(R.id.li_your_shows_img_follow);
		imgFollow.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				DialogHelper.getUnfollowErrorDialogBuilder(show.getTitle(), mYourShowsFragment.getActivity())
				.setPositiveButton(mContext.getString(R.string.yes), new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog,int id) {
						unfollowShow(show, relListItem, imgFollow);
					}
				}).create().show();
			}
		});
		return relListItem;
		
	}
	
	private void unfollowShow(final Show show, RelativeLayout relListItem, ImageView imgFollow) {
		ProgressBar pbarLoading = (ProgressBar) relListItem.findViewById(R.id.li_your_shows_pbar_download_loading);
		TextView txtDeleting = (TextView) relListItem.findViewById(R.id.li_your_shows_txt_deleting);
		showLoadingDeleting(true, imgFollow, pbarLoading, txtDeleting);
		mAsyncAccessService.uploadUnfollowShow(show, false, new ServiceResponseListener() {
			@Override public void onProgress(int bytesWritten, int totalSize) { }
			
			@Override
			public void onSuccess(Object o) {
				if(o instanceof Boolean) {
					String msg = String.format(mContext.getString(R.string.unfollowed_show), show.getTitle());
					DialogHelper.showInfoShort(msg, mContext);
					YourShowsListArrayAdapter.this.remove(show);
					YourShowsListArrayAdapter.this.notifyDataSetChanged();
				}
			}
			
			@Override
			public void onFailure(Throwable throwable) {
				showError(throwable);
			}
		});
		
	}

	private void showLoadingDeleting(boolean b, ImageView imgFollow, ProgressBar pbarLoading, TextView txtDeleting) {
		if(b) {
			pbarLoading.setVisibility(View.VISIBLE);
			txtDeleting.setVisibility(View.VISIBLE);
			imgFollow.setVisibility(View.INVISIBLE);
		} else {
			pbarLoading.setVisibility(View.GONE);
			txtDeleting.setVisibility(View.GONE);
			imgFollow.setVisibility(View.VISIBLE);
		}
	}

	public CharSequence getGenresStr(ArrayList<String> genres) {
		String genreStr = "";
		boolean first = true;
		for (String gs : genres) {
			if(first) {
				genreStr += gs;
				first = false;
			} else {
				genreStr += ", " + gs;
			}
		}
		return genreStr;
	}


	@Override
	public int getCount() {
		if(super.getCount() == 0) {
			return 1;
		}
		return super.getCount();
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// ----- ERROR -------------------------------------------------------------------------------------------------- //

	private void showError(Throwable throwable) {
		DialogHelper.handleDialogErrors(throwable, mYourShowsFragment.getActivity());
	}
	
}