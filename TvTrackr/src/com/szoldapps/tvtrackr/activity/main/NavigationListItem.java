package com.szoldapps.tvtrackr.activity.main;

public class NavigationListItem {

	private String name;
	private int drawable;
	
	public NavigationListItem(String name, int drawable) {
		super();
		this.name = name;
		this.drawable = drawable;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getDrawable() {
		return drawable;
	}

	public void setDrawable(int drawable) {
		this.drawable = drawable;
	}

}
