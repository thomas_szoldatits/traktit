package com.szoldapps.tvtrackr.activity.main.recommendations;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.szoldapps.tvtrackr.R;
import com.szoldapps.tvtrackr.activity.main.MainActivity;
import com.szoldapps.tvtrackr.activity.main.search.SearchFragment;
import com.szoldapps.tvtrackr.db.model.Show.ShowDownloadStatus;
import com.szoldapps.tvtrackr.dto.show.RecommendationShowDTO;
import com.szoldapps.tvtrackr.exception.WrongObjectReturnedException;
import com.szoldapps.tvtrackr.helper.Constants;
import com.szoldapps.tvtrackr.helper.DialogHelper;
import com.szoldapps.tvtrackr.helper.LogCatHelper;
import com.szoldapps.tvtrackr.helper.MenuHelper;
import com.szoldapps.tvtrackr.helper.Constants.DownloadResponseType;
import com.szoldapps.tvtrackr.service.async.AsyncAccessService;
import com.szoldapps.tvtrackr.service.async.AsyncAccessServiceImpl;
import com.szoldapps.tvtrackr.service.listener.ServiceResponseListener;

public class RecommendationsFragment extends Fragment {

	public static final String LOGTAG = LogCatHelper.getAppTag()+RecommendationsFragment.class.getSimpleName().toString();

	private AsyncAccessService mAsyncAccessService;

	private View mRootView;
	private MainActivity mMainActivity;
	private ListView mListView;
	private RecommendationsListArrayAdapter mListViewAdapter;
	// Filter
	private RelativeLayout mRelFilterContainer;
	private RelativeLayout mRelFilterHlContainer;
	private RelativeLayout mRelFilterContent;
	private TextView mTxtFilterSubHl;
	private LinearLayout mRelFilterButtons;
	private ImageView mImgExpCol;
	private boolean mNoFilterSet;
	// Filter: Spinner
	private Spinner mSpinGenres;
	private Spinner mSpinStartYear;
	private Spinner mSpinEndYear;
	private List<String> mGenreList = new ArrayList<String>();
	private List<String> mStartYearList = new ArrayList<String>();
	private List<String> mEndYearList = new ArrayList<String>();
	private ArrayAdapter<String> mGenreListAdapter;
	private ArrayAdapter<String> mStartYearListAdapter;
	private ArrayAdapter<String> mEndYearListAdapter;
	// Filter: Buttons
	private Button mBtnReset;
	private Button mBtnApply;
	// Loading
	private RelativeLayout mRelLoading;
	// Menu
	private MenuItem mRefreshMenuItem;
	private boolean mUpdateRunning;
	// Error
	private Dialog mNoInternetDialog;

	public RecommendationsFragment() { /* Empty constructor required for fragment subclasses */ }

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		mMainActivity = (MainActivity) getActivity();
		mRootView = inflater.inflate(R.layout.fragment_recommendations, container, false);
		mAsyncAccessService = AsyncAccessServiceImpl.getInstance(mMainActivity.getApplicationContext());
		buildUI();
		return mRootView;
	}

	private void buildUI() {
		setHasOptionsMenu(true);
		// Loading
		mRelLoading = (RelativeLayout) mRootView.findViewById(R.id.loading_rel_container);
		// List
		mListView = (ListView) mRootView.findViewById(R.id.recommendations_list);
		mListViewAdapter = new RecommendationsListArrayAdapter(this, mMainActivity.getApplicationContext(), 
				R.id.calendar_day_list, new ArrayList<RecommendationShowDTO>());
		mListView.setAdapter(mListViewAdapter);
		mListView.setOnItemLongClickListener(mLongClickListener);

		setupFilter();
	}


	OnItemLongClickListener mLongClickListener = new OnItemLongClickListener() {
		public boolean onItemLongClick(AdapterView<?> arg0, View relListItem, int pos, long id) {
			Log.i(LOGTAG, "LOONG"+pos);
			RelativeLayout relTextContainer = 
					(RelativeLayout) relListItem.findViewById(R.id.li_recommendations_rel_text_container);
			RelativeLayout relButtonContainer = 
					(RelativeLayout) relListItem.findViewById(R.id.li_recommendations_rel_button_container);
			relTextContainer.setVisibility(View.INVISIBLE);
			relButtonContainer.setVisibility(View.INVISIBLE);

			RelativeLayout relDismissContainer = 
					(RelativeLayout) relListItem.findViewById(R.id.li_recommendations_rel_dismiss_container);
			relDismissContainer.setVisibility(View.VISIBLE);
			relDismissContainer.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					// This is just here to prevent goToShow call
				}
			});
			Button btnCancel = (Button) relListItem.findViewById(R.id.li_recommendations_btn_dismiss_cancel);
			Button btnOk = (Button) relListItem.findViewById(R.id.li_recommendations_btn_dismiss_ok);

			btnCancel.setTag(relListItem);
			btnCancel.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					View relListItem = (View) v.getTag();
					RelativeLayout relTextContainer = 
							(RelativeLayout) relListItem.findViewById(R.id.li_recommendations_rel_text_container);
					RelativeLayout relButtonContainer = 
							(RelativeLayout) relListItem.findViewById(R.id.li_recommendations_rel_button_container);
					relTextContainer.setVisibility(View.VISIBLE);
					relButtonContainer.setVisibility(View.VISIBLE);

					RelativeLayout relDismissContainer = 
							(RelativeLayout) relListItem.findViewById(R.id.li_recommendations_rel_dismiss_container);
					relDismissContainer.setVisibility(View.INVISIBLE);
				}
			});
			btnOk.setTag(pos);
			btnOk.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					int position = (Integer) v.getTag();
					showLoading(true);
					mAsyncAccessService.dismissRecommendation(
							mListViewAdapter.getTvdbIdOfShow(position), new ServiceResponseListener() {
								@Override public void onProgress(int bytesWritten, int totalSize) { }

								@Override
								public void onSuccess(Object o) {
									if(o instanceof String) {
										loadDynamicContent();
									} else {
										onFailure(new WrongObjectReturnedException(String.class));
									}
								}

								@Override
								public void onFailure(Throwable throwable) {
									showError(throwable);
								}
							});
				}
			});
			return true;
		}
	};

	private void setupFilter() {
		// Filter
		mRelFilterContainer = (RelativeLayout) mRootView.findViewById(R.id.recommendations_rel_filter_container);
		mRelFilterHlContainer = (RelativeLayout) mRootView.findViewById(R.id.recommendations_rel_filter_hl_container);
		mTxtFilterSubHl = (TextView) mRootView.findViewById(R.id.recommendations_txt_filter_sub_hl);
		mImgExpCol = (ImageView) mRootView.findViewById(R.id.recommendations_img_exp_col);
		mRelFilterContent = (RelativeLayout) mRootView.findViewById(R.id.recommendations_rel_filter_content_container);
		mRelFilterButtons = (LinearLayout) mRootView.findViewById(R.id.recommendations_lin_filter_button_container);

		mRelFilterHlContainer.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if(mRelFilterContent.getVisibility() == View.GONE) {
					expandFilter(true);
				} else {
					expandFilter(false);
				}
			}
		});

		// Genre Spinner
		mGenreList.add(getString(R.string.recommendations_all_genres));
		mGenreListAdapter = 
				new ArrayAdapter<String>(mMainActivity.getApplicationContext(), R.layout.spinner_item, mGenreList);
		mGenreListAdapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
		mSpinGenres = (Spinner) mRootView.findViewById(R.id.recommendations_spin_genres);
		mSpinGenres.setAdapter(mGenreListAdapter);
		mSpinGenres.setOnItemSelectedListener(new OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parent, View v, int position, long id) {
				setButtonsEnabledOrDisabled();
			}
			@Override public void onNothingSelected(AdapterView<?> arg0) { }
		});

		// YearStart Spinner
		mStartYearList.add(getString(R.string.recommendations_year_default));
		mStartYearListAdapter = 
				new ArrayAdapter<String>(mMainActivity.getApplicationContext(), R.layout.spinner_item, mStartYearList);
		mStartYearListAdapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
		mSpinStartYear = (Spinner) mRootView.findViewById(R.id.recommendations_spin_year_start);
		mSpinStartYear.setAdapter(mStartYearListAdapter);
		mSpinStartYear.setOnItemSelectedListener(new OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parent, View v, int position, long id) {
				setButtonsEnabledOrDisabled();
				if(position == 0) { return; }
				if(mSpinEndYear.getSelectedItemPosition() > position && mSpinEndYear.getSelectedItemPosition() != 0) {
					mSpinEndYear.setSelection(position);
				}
			}
			@Override public void onNothingSelected(AdapterView<?> arg0) { }
		});

		// YearEnd Spinner
		mEndYearList.add(getString(R.string.recommendations_year_default));
		mEndYearListAdapter = 
				new ArrayAdapter<String>(mMainActivity.getApplicationContext(), R.layout.spinner_item, mEndYearList);
		mEndYearListAdapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
		mSpinEndYear = (Spinner) mRootView.findViewById(R.id.recommendations_spin_year_end);
		mSpinEndYear.setAdapter(mEndYearListAdapter);
		mSpinEndYear.setOnItemSelectedListener(new OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parent, View v, int position, long id) {
				setButtonsEnabledOrDisabled();
				if(position == 0) { return; }
				if(mSpinStartYear.getSelectedItemPosition() < position 
						&& mSpinStartYear.getSelectedItemPosition() != 0) {
					mSpinStartYear.setSelection(position);
				}
			}
			@Override public void onNothingSelected(AdapterView<?> arg0) { }
		});

		// Fill Spinners with years
		int thisYear = Calendar.getInstance().get(Calendar.YEAR);
		int minYear = 1930;
		int tempYear = thisYear + 3;
		while (tempYear >= minYear) {
			mStartYearList.add(Integer.toString(tempYear));
			mEndYearList.add(Integer.toString(tempYear));
			tempYear--;
		}
		mStartYearListAdapter.notifyDataSetChanged();
		mEndYearListAdapter.notifyDataSetChanged();

		mBtnReset = (Button) mRootView.findViewById(R.id.recommendations_btn_reset);
		mBtnReset.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				mSpinGenres.setSelection(0);
				mSpinStartYear.setSelection(0);
				mSpinEndYear.setSelection(0);
				setButtonsEnabledOrDisabled();
				if(!mNoFilterSet) {
					setFilterSubHl();
					expandFilter(false);
					loadDynamicContent();
				}
			}
		});
		mBtnApply = (Button) mRootView.findViewById(R.id.recommendations_btn_apply);
		mBtnApply.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				setFilterSubHl();
				expandFilter(false);
				loadDynamicContent();
			}
		});

	}

	private void expandFilter(boolean b) {
		int bottom = 0;
		if(b) {
			mImgExpCol.setImageDrawable(getResources().getDrawable(R.drawable.ic_collapse));
			mRelFilterContent.setVisibility(View.VISIBLE);
			mRelFilterButtons.setVisibility(View.VISIBLE);
		} else {
			mImgExpCol.setImageDrawable(getResources().getDrawable(R.drawable.ic_expand));
			mRelFilterContent.setVisibility(View.GONE);
			mRelFilterButtons.setVisibility(View.GONE);
			bottom = getResources().getDimensionPixelSize(R.dimen.margin_vertical_half);
		}
		mRelFilterContainer.setPadding(mRelFilterContainer.getPaddingLeft(), mRelFilterContainer.getPaddingTop(), 
				mRelFilterContainer.getPaddingRight(), bottom);
	}

	private void setFilterSubHl() {
		if(mSpinGenres.getSelectedItemPosition()==0 && mSpinStartYear.getSelectedItemPosition()==0 
				&& mSpinEndYear.getSelectedItemPosition()==0) {
			mTxtFilterSubHl.setText(getString(R.string.recommendations_no_filter));
			mTxtFilterSubHl.setTextColor(getResources().getColor(R.color.dark_gray));
		} else {
			String filterText = 
					(String) mSpinGenres.getSelectedItem() + " " + getString(R.string.recommendations_between) + " ";
			filterText += 
					(String) mSpinStartYear.getSelectedItem() + " " + getString(R.string.recommendations_and) + " "; 
			filterText += (String) mSpinEndYear.getSelectedItem();
			mTxtFilterSubHl.setText(filterText);
			mTxtFilterSubHl.setTextColor(getResources().getColor(R.color.blue_dark));
		}
	}

	private void setButtonsEnabledOrDisabled() {
		boolean enabled = true;
		int typeface = Typeface.BOLD;
		if(mSpinGenres.getSelectedItemPosition()==0 && mSpinStartYear.getSelectedItemPosition()==0 
				&& mSpinEndYear.getSelectedItemPosition()==0) {
			mBtnReset.setTypeface(null, Typeface.NORMAL);
			enabled = false;
		}
		mBtnReset.setEnabled(enabled);
		mBtnApply.setEnabled(enabled);
		mBtnApply.setTypeface(null, typeface);
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// ----- LOAD_DYNAMIC_CONTENT ----------------------------------------------------------------------------------- //

	/**
	 * Makes REST-call and fills {@link #mListViewAdapter} respectively {@link #mListView}
	 */
	private void loadDynamicContent() {
		showLoading(true);
		if(mGenreList.size() == 1) {
			mAsyncAccessService.getGenreStringList(new ServiceResponseListener() {
				@Override 
				public void onProgress(int bytesWritten, int totalSize) { }

				@Override @SuppressWarnings("unchecked")
				public void onSuccess(Object o) {
					if(o != null) {
						List<String> genres = (List<String>) o;
						mGenreList.addAll(genres);
						mGenreListAdapter.notifyDataSetChanged();
					}
				}

				@Override
				public void onFailure(Throwable throwable) {
					showError(throwable);
				}
			});
		}

		String genre = null;
		if(mSpinGenres.getSelectedItemPosition() != 0) {
			String slug = (String) mSpinGenres.getSelectedItem();
			slug = slug.toLowerCase(Locale.getDefault());
			slug = slug.replace(" ", "-");
			genre = slug;
		}
		Integer startYear = null;
		if(mSpinStartYear.getSelectedItemPosition() != 0) {
			try {
				startYear = Integer.parseInt((String) mSpinStartYear.getSelectedItem());
			} catch(NumberFormatException e) { mSpinStartYear.setSelection(0);
			}
		}
		Integer endYear = null;
		if(mSpinEndYear.getSelectedItemPosition() != 0) {
			try {
				endYear = Integer.parseInt((String) mSpinEndYear.getSelectedItem());
			} catch(NumberFormatException e) { mSpinEndYear.setSelection(0);
			}
		}
		if(genre == null && startYear == null && endYear == null) {
			mNoFilterSet = true;
		} else {
			mNoFilterSet = false;
		}
		mAsyncAccessService.getRecommendations(genre, startYear, endYear, new ServiceResponseListener() {

			@Override public void onProgress(int bytesWritten, int totalSize) { }

			@Override @SuppressWarnings("unchecked")
			public void onSuccess(Object o) {
				if(o != null) {
					List<RecommendationShowDTO> shows = (List<RecommendationShowDTO>) o;
					mListViewAdapter.clear();
					mListViewAdapter.addAll(shows);
					// Scrolls to the top of the ListView
					mListView.setSelectionAfterHeaderView();
					showLoading(false);
				}

			}

			@Override
			public void onFailure(Throwable throwable) {
				showLoading(false);
				showError(throwable);
			}
		});
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// ----- LOADING ------------------------------------------------------------------------------------------------ //

	private void showLoading(boolean b) {
		if(b) {
			mUpdateRunning = true;
			MenuHelper.updateStarted(mRefreshMenuItem);
			mListView.setVisibility(View.INVISIBLE);
			mRelLoading.setVisibility(View.VISIBLE);
		} else {
			mUpdateRunning = false;
			MenuHelper.updateEnded(mRefreshMenuItem);
			mListView.setVisibility(View.VISIBLE);
			mRelLoading.setVisibility(View.GONE);
		}
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// ----- DOWNLOAD_SERVICE---------------------------------------------------------------------------------------- //

	private BroadcastReceiver receiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			Bundle bundle = intent.getExtras();
			if (bundle != null) {
				int resultCode = bundle.getInt(Constants.EXTRA_RESULT);
				if(resultCode == Activity.RESULT_OK) {
					DownloadResponseType responseType = 
							(DownloadResponseType) intent.getSerializableExtra(Constants.EXTRA_RESPONSE_TYPE);
					switch (responseType) {
					case ONE_SHOW_UPDATE:
						int tvdbId = intent.getIntExtra(Constants.EXTRA_SHOW_TVDB_ID, -1);
						ShowDownloadStatus status = 
								(ShowDownloadStatus) intent.getSerializableExtra(Constants.EXTRA_SHOW_DOWNLOAD_STATUS);
						if(tvdbId != -1 && status != null) {
							if(status.equals(ShowDownloadStatus.DONE)) {
								mListViewAdapter.notifyDataSetChanged();
							}
							//						
							//							Log.i(LOGTAG, "tvdbId:"+tvdbId+" status:"+status);
							//							updateShowItem(tvdbId, status);
						}
						break;
					default: /* DO NOTHING */ break;
					}
				}
			}
		}
	};


	/**
	 * Could be useful after rebuild
	 * @param tvdbId
	 * @param status
	 */
	@SuppressWarnings("unused")
	private void updateShowItem(int tvdbId, ShowDownloadStatus status) {
		RecommendationShowDTO recShow = new RecommendationShowDTO();
		recShow.setTvdb_id(tvdbId);
		Log.e(LOGTAG, "pos="+mListViewAdapter.getPosition(recShow));
		RelativeLayout relListItem = (RelativeLayout) mListView.getChildAt(mListViewAdapter.getPosition(recShow));
		if(relListItem == null) { return; }
		ImageView imgFollow = (ImageView) relListItem.findViewById(R.id.li_recomendations_img_follow);
		ProgressBar pbarLoading = (ProgressBar) relListItem.findViewById(R.id.li_recomendations_pbar_download_loading);
		TextView txtLoading = (TextView) relListItem.findViewById(R.id.li_recomendations_txt_download);

		switch (status) {
		case DOWNLOADING:
			pbarLoading.setVisibility(View.VISIBLE);
			txtLoading.setVisibility(View.VISIBLE);
			imgFollow.setVisibility(View.GONE);
			txtLoading.setText(getString(R.string.setup_download_downloading));
			txtLoading.setTextColor(getResources().getColor(R.color.black));
			break;
		case SAVING:
			txtLoading.setText(getString(R.string.setup_download_saving));
			txtLoading.setTextColor(getResources().getColor(R.color.black));
			break;
		case DONE:
			pbarLoading.setVisibility(View.GONE);
			txtLoading.setVisibility(View.GONE);
			imgFollow.setOnClickListener(null);
			imgFollow.setImageDrawable(getResources().getDrawable(R.drawable.ic_follow_blue));
			imgFollow.setVisibility(View.VISIBLE);
			break;
		default:
			txtLoading.setText(getString(R.string.setup_download_waiting));
			break;
		}
	}

	@Override
	public void onResume() {
		super.onResume();
		loadDynamicContent();
		mMainActivity.registerReceiver(receiver, new IntentFilter(Constants.EXTRA_DOWNLOAD_SERVICE_NOTIFICATION));
	}


	@Override
	public void onStop() {
		super.onStop();
		mMainActivity.unregisterReceiver(receiver);
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// ----- MENU --------------------------------------------------------------------------------------------------- //

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		inflater.inflate(R.menu.recommendations, menu);
		mRefreshMenuItem = menu.findItem(R.id.menu_refresh);
		if(mUpdateRunning) {
			MenuHelper.updateStarted(mRefreshMenuItem);
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.menu_search:
			mMainActivity.changeFragment(
					new SearchFragment(), getString(R.string.navigation_item_search), MainActivity.NAVI_POS_SEARCH);
			break;
		case R.id.menu_refresh:
			loadDynamicContent();
			break;
		default:
			break;
		}
		return super.onOptionsItemSelected(item);
	}


	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// ----- ERROR -------------------------------------------------------------------------------------------------- //

	public void showError(Throwable throwable) {
		if(DialogHelper.isInternetError(throwable)) {
			showLoading(false);
			if(mNoInternetDialog == null) {
				mNoInternetDialog = DialogHelper.getNoInternetErrorDialogBuilder(getActivity())
						.setPositiveButton(getString(R.string.error_btn_retry), new AlertDialog.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog, int id) {
								loadDynamicContent();
							}
						}).create();
			}
			if(!mNoInternetDialog.isShowing()) {
				mNoInternetDialog.show();
			}
			return;
		}
		DialogHelper.handleDialogErrors(throwable, mMainActivity);
	}
}
