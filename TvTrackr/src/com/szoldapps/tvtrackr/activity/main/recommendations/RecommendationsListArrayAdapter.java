package com.szoldapps.tvtrackr.activity.main.recommendations;

import java.util.List;

import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.szoldapps.tvtrackr.R;
import com.szoldapps.tvtrackr.dto.show.MinShowDTO;
import com.szoldapps.tvtrackr.dto.show.RecommendationShowDTO;
import com.szoldapps.tvtrackr.exception.WrongObjectReturnedException;
import com.szoldapps.tvtrackr.helper.DateHelper;
import com.szoldapps.tvtrackr.helper.DialogHelper;
import com.szoldapps.tvtrackr.helper.ImageLoaderHelper;
import com.szoldapps.tvtrackr.helper.ImageURLHelper;
import com.szoldapps.tvtrackr.helper.LinkingHelper;
import com.szoldapps.tvtrackr.helper.LogCatHelper;
import com.szoldapps.tvtrackr.helper.DateHelper.DateHelperFormat;
import com.szoldapps.tvtrackr.service.async.AsyncAccessService;
import com.szoldapps.tvtrackr.service.async.AsyncAccessServiceImpl;
import com.szoldapps.tvtrackr.service.listener.ServiceResponseListener;

public class RecommendationsListArrayAdapter extends ArrayAdapter<RecommendationShowDTO> {
	public static final String LOGTAG = LogCatHelper.getAppTag()+RecommendationsListArrayAdapter.class.getSimpleName().toString();

	private AsyncAccessService mAsyncAccessService;
	private final Context mContext;
	private RecommendationsFragment mRecommendationsFragment;

	public RecommendationsListArrayAdapter(RecommendationsFragment recommendationsFrag, Context context, int resource, List<RecommendationShowDTO> shows) {
		super(context, resource, shows);
		mRecommendationsFragment = recommendationsFrag;
		mContext = context;
		mAsyncAccessService = AsyncAccessServiceImpl.getInstance(mContext.getApplicationContext());
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater) mRecommendationsFragment.getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		RelativeLayout relListItem = null;
		if(getRealCount() == 0) {
			relListItem = (RelativeLayout) inflater.inflate(R.layout.li_recommendations_info, parent, false);
		} else {
			relListItem = (RelativeLayout) inflater.inflate(R.layout.li_recommendations, parent, false);

			final RecommendationShowDTO show = getItem(position);

			relListItem.setTag(show);
			relListItem.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					LinkingHelper.goToShow((MinShowDTO) v.getTag(), mRecommendationsFragment.getActivity(), false);
				}
			});

			// Poster
			ImageView imgPoster = (ImageView) relListItem.findViewById(R.id.li_recommendations_img_poster);
			String imgUrl = ImageURLHelper.getImageURLByImageType(show.getImages().getPoster(), ImageURLHelper.POSTER_SMALL);
			ImageLoader.getInstance().displayImage(imgUrl, imgPoster, ImageLoaderHelper.getDIOPosterOrHeadShot(mContext));

			// Title
			TextView txtTitle = (TextView) relListItem.findViewById(R.id.li_recommendations_txt_title);
			txtTitle.setText(show.getTitle());

			// Time and Network
			TextView txtTimeNetwork = (TextView) relListItem.findViewById(R.id.li_recommendations_txt_time_network);
			String weekDay = DateHelper.getDateStrWithTimeInS(show.getFirst_aired(), DateHelperFormat.WEEKDAY_SHORT, mContext);
			String time = DateHelper.getDateStrWithTimeInS(show.getFirst_aired(), DateHelperFormat.TIME, mContext);
			String timeAndNetwork = String.format(mContext.getString(R.string.recommendations_time_network), weekDay, time, show.getNetwork());
			txtTimeNetwork.setText(timeAndNetwork);

			// Genres
			TextView txtGenres = (TextView) relListItem.findViewById(R.id.li_recommendations_txt_genres);
			txtGenres.setText(getGenresStr(show.getGenres()));

			// Premiered
			TextView txtPremiered = (TextView) relListItem.findViewById(R.id.li_recommendations_txt_premiered);
			txtPremiered.setText(DateHelper.getDateStrWithTimeInS(show.getFirst_aired(), DateHelperFormat.DATE_ONLY, mContext));

			// Follow Img
			final ImageView imgFollow = (ImageView) relListItem.findViewById(R.id.li_recomendations_img_follow);
			//			final ProgressBar pbarLoading = (ProgressBar) relListItem.findViewById(R.id.li_recomendations_pbar_download_loading);
			//			final TextView txtLoading = (TextView) relListItem.findViewById(R.id.li_recomendations_txt_download);
			imgFollow.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					followShow(imgFollow, show);
				}
			});
			// Check if Show followed (can only happen, if followed and then scrolled)
			mAsyncAccessService.isShowFollowedByCurrentUser(show.getTvdb_id(), new ServiceResponseListener() {
				@Override
				public void onProgress(int bytesWritten, int totalSize) { }

				@Override
				public void onSuccess(Object o) {
					if(o instanceof Boolean) {
						if((Boolean)o) {
							imgFollow.setOnClickListener(null);
						}
						int icon = ((Boolean)o) ? R.drawable.ic_follow_blue : R.drawable.ic_follow_gray;
						imgFollow.setImageDrawable(mContext.getResources().getDrawable(icon));
					} else {
						onFailure(new WrongObjectReturnedException(Boolean.class));
					}
				}

				@Override
				public void onFailure(Throwable throwable) {
					mRecommendationsFragment.showError(throwable);
				}
			});

		}

		return relListItem;
	}

	private void followShow(final ImageView imgFollow, final RecommendationShowDTO show) {
		//		imgFollow.setVisibility(View.INVISIBLE);
		//					pbarLoading.setVisibility(View.VISIBLE);
		//					txtLoading.setVisibility(View.VISIBLE);
		// Follow show
		mAsyncAccessService.uploadFollowAndSaveShow(show, new ServiceResponseListener() {
			@Override public void onProgress(int bytesWritten, int totalSize) { }

			@Override
			public void onSuccess(Object o) {
				if(o instanceof Boolean) {
					if((Boolean) o) {
						imgFollow.setImageDrawable(
								mContext.getResources().getDrawable(R.drawable.ic_follow_blue));
						String msg = String.format(mContext.getString(R.string.followed_show), show.getTitle());
						DialogHelper.showInfoShort(msg, mRecommendationsFragment.getActivity());
						imgFollow.setOnClickListener(null);
					} else {
						DialogHelper.getFollowErrorDialogBuilder(mRecommendationsFragment.getActivity())
						.setPositiveButton(mContext.getString(R.string.error_btn_retry), 
								new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,int id) {
								followShow(imgFollow, show);
							}
						}).create().show();
					}
				} else {
					onFailure(new WrongObjectReturnedException(Boolean.class));
				}
			}
			@Override
			public void onFailure(Throwable throwable) {
				mRecommendationsFragment.showError(throwable);
			}
		});
	}

	public CharSequence getGenresStr(List<String> genres) {
		String genreStr = "";
		boolean first = true;
		for (String gs : genres) {
			if(first) {
				genreStr += gs;
				first = false;
			} else {
				genreStr += ", " + gs;
			}
		}
		return genreStr;
	}


	@Override
	public int getCount() {
		if(super.getCount() == 0) {
			return 1;
		}
		return super.getCount();
	}

	public int getRealCount() {
		return super.getCount();
	}

	//	public void setShows(List<RecommendationShowDTO> shows) {
	//		mShows = shows;
	//		notifyDataSetChanged();
	//	}

	public int getTvdbIdOfShow(int position) {
		RecommendationShowDTO show = getItem(position);
		if(show != null) {
			return show.getTvdb_id();
		}
		return -1;
	}

	//	public int getPositionOfShow(int tvdbId) {
	//		int position = 0;
	//		getPosition(item)
	//		for (RecommendationShowDTO show : mShows) {
	//			if(show.getTvdb_id() == tvdbId) {
	//				return position;
	//			}
	//			position++;
	//		}
	//		return -1;
	//	}

}