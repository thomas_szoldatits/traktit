package com.szoldapps.tvtrackr.activity.main.calendar;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.util.SparseArray;

import com.szoldapps.tvtrackr.helper.Constants;
import com.szoldapps.tvtrackr.helper.DateHelper;
import com.szoldapps.tvtrackr.helper.LogCatHelper;

public class CalendarTabPageAdapter extends FragmentPagerAdapter {
	
	public static final String LOGTAG = LogCatHelper.getAppTag()+CalendarTabPageAdapter.class.getSimpleName().toString();
	SparseArray<CalendarDayFragment> mFragments = new SparseArray<CalendarDayFragment>();
	private int mPageCount;
	private Context mContext;
	
    
    public CalendarTabPageAdapter(FragmentManager fm, int pageCount, Context context) {
        super(fm);
        mPageCount = pageCount;
        mContext = context;
    }

    @Override 
    public Fragment getItem(int position) {
    	CalendarDayFragment fragment = new CalendarDayFragment();
    	Bundle args = new Bundle();
        args.putInt(Constants.EXTRA_CALENDAR_OFFSET, getDayOffset(position));
        fragment.setArguments(args);
        mFragments.put(position, fragment);
        return fragment;
    }

    @Override
    public int getCount() {
    	return mPageCount;
    }
    
    private int getDayOffset(int position) {
    	return (int) (position-Math.round(mPageCount*1f/2*1f));
    }
    
    @Override
    public CharSequence getPageTitle(int position) {
        return DateHelper.getCalendarTabsDate(getDayOffset(position), mContext);
    }
}

