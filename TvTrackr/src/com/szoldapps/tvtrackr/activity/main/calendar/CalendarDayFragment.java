package com.szoldapps.tvtrackr.activity.main.calendar;

import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.szoldapps.tvtrackr.R;
import com.szoldapps.tvtrackr.activity.main.MainActivity;
import com.szoldapps.tvtrackr.activity.show.ShowActivity;
import com.szoldapps.tvtrackr.db.model.Episode;
import com.szoldapps.tvtrackr.db.model.Show;
import com.szoldapps.tvtrackr.exception.WrongObjectReturnedException;
import com.szoldapps.tvtrackr.helper.Constants;
import com.szoldapps.tvtrackr.helper.LogCatHelper;
import com.szoldapps.tvtrackr.service.async.AsyncAccessService;
import com.szoldapps.tvtrackr.service.async.AsyncAccessServiceImpl;
import com.szoldapps.tvtrackr.service.listener.ServiceResponseListener;

public class CalendarDayFragment extends Fragment{

	public static final String LOGTAG = LogCatHelper.getAppTag()+CalendarDayFragment.class.getSimpleName().toString();
	
	private View mRootView;
	private MainActivity mMainActivity;
	private AsyncAccessService mAsyncAccessService;
	private int mOffset;

	private ListView mListView;
	private CalendarDayListArrayAdapter mListViewAdapter;


	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		mMainActivity = (MainActivity) getActivity();
		mRootView = inflater.inflate(R.layout.fragment_calendar_day, container, false);
		
		if(getArguments() != null) {
			mOffset = getArguments().getInt(Constants.EXTRA_CALENDAR_OFFSET);
			mAsyncAccessService = AsyncAccessServiceImpl.getInstance(mMainActivity.getApplicationContext());
			buildUI();
		}

		return mRootView;
	}
	
	@Override
	public void onResume() {
		loadDynamicContent();
		super.onResume();
	}
	
	private void buildUI() {
		mListView = (ListView) mRootView.findViewById(R.id.calendar_day_list);
		mListViewAdapter = new CalendarDayListArrayAdapter(this, mMainActivity.getApplicationContext(), R.id.calendar_day_list, null, mOffset);
		mListView.setAdapter(mListViewAdapter);
	}

	private void loadDynamicContent() {
		mAsyncAccessService.getEpisodesOfDay(mOffset, new ServiceResponseListener() {
			@Override public void onProgress(int bytesWritten, int totalSize) { }
			
			@Override @SuppressWarnings("unchecked")
			public void onSuccess(Object o) {
				if(o != null) {
					List<Episode> episodes = (List<Episode>) o;
					mListViewAdapter.setEpisodes(episodes);
				} else {
					onFailure(new WrongObjectReturnedException("List<Episode>"));
				}
			}
			
			@Override
			public void onFailure(Throwable throwable) {
				showError(throwable);
			}
		});
	}

	// SQLException
	// WrongObjectReturnedException
	public void showError(Throwable throwable) {
		Log.e(LOGTAG, throwable.getMessage());
	}
	
	public void goToEpisode(Episode episode, Show show) {
		Class<?> cls = ShowActivity.class;
		Intent intent = new Intent(getActivity().getApplicationContext(), cls);
		intent.putExtra(Constants.EXTRA_SHOW_TVDB_ID, show.getTvdbID());
		intent.putExtra(Constants.EXTRA_SHOW_TITLE, show.getTitle());
		intent.putExtra(Constants.EXTRA_EPISODE_ID, episode.getId());
		getActivity().startActivity(intent);
		getActivity().overridePendingTransition(R.anim.enter_right_slide, R.anim.exit_left_slide);
	}
}
