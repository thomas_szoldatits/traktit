package com.szoldapps.tvtrackr.activity.main.calendar;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.szoldapps.tvtrackr.R;
import com.szoldapps.tvtrackr.db.model.Episode;
import com.szoldapps.tvtrackr.db.model.Show;
import com.szoldapps.tvtrackr.dto.internal.InternalEpisodeWatchedDTO;
import com.szoldapps.tvtrackr.exception.WrongObjectReturnedException;
import com.szoldapps.tvtrackr.helper.DateHelper;
import com.szoldapps.tvtrackr.helper.ImageLoaderHelper;
import com.szoldapps.tvtrackr.helper.LinkingHelper;
import com.szoldapps.tvtrackr.helper.DateHelper.DateHelperFormat;
import com.szoldapps.tvtrackr.service.async.AsyncAccessService;
import com.szoldapps.tvtrackr.service.async.AsyncAccessServiceImpl;
import com.szoldapps.tvtrackr.service.listener.ServiceResponseListener;

public class CalendarDayListArrayAdapter extends ArrayAdapter<Episode> {
	private final Context mContext;
	private CalendarDayFragment mCalendarDayFragment;
	private List<Episode> mEpisodes;
	private int mOffset;
	private AsyncAccessService mAsyncAccessService;

	public CalendarDayListArrayAdapter(CalendarDayFragment calFragment, Context context, int resource, 
			List<Episode> episodes, int offset) {
		super(context, resource, episodes);
		mCalendarDayFragment = calFragment;
		mContext = context;
		mEpisodes = episodes;
		mOffset = offset;
		mAsyncAccessService = AsyncAccessServiceImpl.getInstance(mContext);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = 
				(LayoutInflater) mCalendarDayFragment.getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		if(mEpisodes == null) {
			RelativeLayout relListItem = 
					(RelativeLayout) inflater.inflate(R.layout.li_calendar_day_info, parent, false);
			ProgressBar pbarLoading = (ProgressBar) relListItem.findViewById(R.id.li_calendar_day_info_pbar_loading);
			TextView txtInfo = (TextView) relListItem.findViewById(R.id.li_calendar_day_info_txt_hl);
			pbarLoading.setVisibility(View.VISIBLE);
			String text = String.format(mContext.getString(R.string.calendar_loading_episodes), 
					DateHelper.getCalendarTabsDate(mOffset, mContext));
			txtInfo.setText(text);
			return relListItem;
		} else {
			if(mEpisodes.isEmpty()) {
				RelativeLayout relListItem = 
						(RelativeLayout) inflater.inflate(R.layout.li_calendar_day_info, parent, false);
				ProgressBar pbarLoading = 
						(ProgressBar) relListItem.findViewById(R.id.li_calendar_day_info_pbar_loading);
				TextView txtInfo = (TextView) relListItem.findViewById(R.id.li_calendar_day_info_txt_hl);
				pbarLoading.setVisibility(View.INVISIBLE);
				String text = String.format(mContext.getString(R.string.calendar_no_episodes), 
						DateHelper.getCalendarNoEpisodeDateStr(mOffset, mContext));
				txtInfo.setText(text);
				return relListItem;
			} else {
				// Load Show and Episode
				final Episode episode = mEpisodes.get(position);
				final Show show = episode.getSeason().getShow();
				// RelListItem View
				final RelativeLayout relListItem = 
						(RelativeLayout) inflater.inflate(R.layout.li_calendar_day, parent, false);

				// imgBanner
				ImageView imgBanner = (ImageView) relListItem.findViewById(R.id.li_calendar_day_img_banner);
				ImageLoader.getInstance().displayImage(show.getBannerURL(), imgBanner, 
						ImageLoaderHelper.getDIOBanner(mContext));

				// txtSeText
				TextView txtSeText = (TextView) relListItem.findViewById(R.id.li_calendar_day_txt_se_text);
				txtSeText.setText(episode.getSeasonEpisodeText());

				// txtHl
				TextView txtHl = (TextView) relListItem.findViewById(R.id.li_calendar_day_txt_title);
				txtHl.setText(episode.getTitle());

				// txtTime
				TextView txtTime = (TextView) relListItem.findViewById(R.id.li_calendar_day_txt_time);
				String timeNetwork = 
						DateHelper.getDateStrWithTimeInS(episode.getFirstAiredUTC(), DateHelperFormat.TIME, mContext);
				timeNetwork = String.format(mContext.getString(R.string.calendar_time_and_location), 
						timeNetwork, show.getNetwork());
				txtTime.setText(timeNetwork);

				// imgSeen
				final ImageView imgSeen = (ImageView) relListItem.findViewById(R.id.li_calendar_day_img_btn_seen);
				final ProgressBar pbarSeenLoading = 
						(ProgressBar) relListItem.findViewById(R.id.li_calendar_day_pbar_seen_loading);

				mAsyncAccessService.isEpisodeSeen(episode, new ServiceResponseListener() {
					@Override public void onProgress(int bytesWritten, int totalSize) { }

					@Override
					public void onSuccess(Object o) {
						if(o instanceof Boolean) {
							setImgFollow((Boolean) o, imgSeen, pbarSeenLoading);
						} else {
							onFailure(new WrongObjectReturnedException(Boolean.class));
						}
					}

					@Override
					public void onFailure(Throwable throwable) {
						mCalendarDayFragment.showError(throwable);
					}
				});

				imgSeen.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						imgSeen.setVisibility(View.INVISIBLE);
						pbarSeenLoading.setVisibility(View.VISIBLE);
						mAsyncAccessService.uploadEpisodeSeenToggle(episode, new ServiceResponseListener() {
							@Override public void onProgress(int bytesWritten, int totalSize) { }

							@Override
							public void onSuccess(Object o) {
								if(o instanceof InternalEpisodeWatchedDTO) {
									InternalEpisodeWatchedDTO dto = (InternalEpisodeWatchedDTO) o;
									setImgFollow(dto.isWatched(), imgSeen, pbarSeenLoading);
								} else {
									onFailure(
											new WrongObjectReturnedException(InternalEpisodeWatchedDTO.class));
								}
							}

							@Override
							public void onFailure(Throwable throwable) {
								mCalendarDayFragment.showError(throwable);
							}
						});
					}
				});
				relListItem.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						LinkingHelper.goToEpisode(episode.getId(), episode.getShowTvdbId(), show.getTitle(), 
								mCalendarDayFragment.getActivity());
					}
				});
				return relListItem;
			}
		}
	}

	private void setImgFollow(boolean isSeen, ImageView imgSeen, ProgressBar pbarSeenLoading) {
		if(isSeen) {
			imgSeen.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_seen_blue));
		} else {
			imgSeen.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_seen_gray));
		}
		imgSeen.setVisibility(View.VISIBLE);
		pbarSeenLoading.setVisibility(View.INVISIBLE);
	}

	@Override
	public int getCount() {
		if(mEpisodes == null) {
			return 1;
		}
		if(mEpisodes.isEmpty()) {
			return 1;
		}
		return mEpisodes.size();
	}

	public void setEpisodes(List<Episode> episodes) {
		this.mEpisodes = episodes;
		this.notifyDataSetChanged();
	}


}