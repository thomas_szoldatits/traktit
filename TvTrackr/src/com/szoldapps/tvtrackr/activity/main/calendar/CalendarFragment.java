package com.szoldapps.tvtrackr.activity.main.calendar;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.astuetz.PagerSlidingTabStrip;
import com.szoldapps.tvtrackr.R;
import com.szoldapps.tvtrackr.activity.main.MainActivity;
import com.szoldapps.tvtrackr.activity.main.search.SearchFragment;
import com.szoldapps.tvtrackr.helper.LogCatHelper;

/**
 * Displays Calendar.
 * 
 * @author thomasszoldatits
 *
 */
public class CalendarFragment extends Fragment {

	public static final String LOGTAG = LogCatHelper.getAppTag()+CalendarFragment.class.getSimpleName().toString();

	private Activity mActivity;
	private View mRootView;
	private ViewPager mPager;
	private CalendarTabPageAdapter mPageAdapter;
	private int mStartPos;

	public CalendarFragment() { /* Empty constructor required for fragment subclasses */ }

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		mActivity = getActivity();
		setHasOptionsMenu(true);
		mRootView = inflater.inflate(R.layout.fragment_calendar, container, false);
		buildUI();
		return mRootView;
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// ----- BUILD_UI ----------------------------------------------------------------------------------------------- //

	private void buildUI() {
		mPager = (ViewPager) mRootView.findViewById(R.id.calendar_pag);
		mPager.removeAllViews();
		int pageCount = 101;	// 50 to the left and 50 to the right
		mPageAdapter = new CalendarTabPageAdapter(getChildFragmentManager(), pageCount, mActivity.getApplicationContext());
		mPager.setAdapter(mPageAdapter);
		mStartPos = Math.round(pageCount*1f/2*1f);
		//mStartPos++;	//DEBUG
		mPager.setCurrentItem(mStartPos);

		// Bind the tabs to the ViewPager
		PagerSlidingTabStrip tabs = (PagerSlidingTabStrip) mRootView.findViewById(R.id.calendar_tabs);
		tabs.setViewPager(mPager);
	}


	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// ----- MENU --------------------------------------------------------------------------------------------------- //

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		inflater.inflate(R.menu.calendar, menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.menu_search:
			((MainActivity) getActivity()).changeFragment(
					new SearchFragment(), getString(R.string.navigation_item_search), MainActivity.NAVI_POS_SEARCH);
			break;
		case R.id.menu_today:
			goToToday();
			break;
		default:
			break;
		}
		return super.onOptionsItemSelected(item);
	}

	private void goToToday() {
		mPager.setCurrentItem(mStartPos);
	}

}