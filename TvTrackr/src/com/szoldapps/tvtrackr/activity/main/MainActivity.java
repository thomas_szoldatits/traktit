package com.szoldapps.tvtrackr.activity.main;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnChildClickListener;
import android.widget.ExpandableListView.OnGroupClickListener;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.szoldapps.tvtrackr.R;
import com.szoldapps.tvtrackr.activity.main.calendar.CalendarFragment;
import com.szoldapps.tvtrackr.activity.main.recommendations.RecommendationsFragment;
import com.szoldapps.tvtrackr.activity.main.search.SearchFragment;
import com.szoldapps.tvtrackr.activity.main.towatch.ToWatchFragment;
import com.szoldapps.tvtrackr.activity.main.trending.TrendingShowsFragment;
import com.szoldapps.tvtrackr.activity.main.yourshows.YourShowsFragment;
import com.szoldapps.tvtrackr.db.model.User;
import com.szoldapps.tvtrackr.exception.WrongObjectReturnedException;
import com.szoldapps.tvtrackr.helper.Constants;
import com.szoldapps.tvtrackr.helper.DateHelper;
import com.szoldapps.tvtrackr.helper.DialogHelper;
import com.szoldapps.tvtrackr.helper.ImageLoaderHelper;
import com.szoldapps.tvtrackr.helper.KeyBoardHelper;
import com.szoldapps.tvtrackr.helper.LinkingHelper;
import com.szoldapps.tvtrackr.helper.LogCatHelper;
import com.szoldapps.tvtrackr.helper.Constants.DownloadResponseType;
import com.szoldapps.tvtrackr.service.async.AsyncAccessService;
import com.szoldapps.tvtrackr.service.async.AsyncAccessServiceImpl;
import com.szoldapps.tvtrackr.service.intent.DownloadService;
import com.szoldapps.tvtrackr.service.listener.ServiceResponseListener;

/**
 * <b>Optional Intent Extras:</b>
 * <ul>
 * 		<li>{@link Constants#EXTRA_PEEK_NAV_DRAWER} ... if TRUE shows the user a quick NavDrawer peek.</li>
 * </ul>
 * 
 * @author thomasszoldatits
 *
 */
public class MainActivity extends FragmentActivity {

	public static final String LOGTAG = LogCatHelper.getAppTag()+MainActivity.class.getSimpleName().toString();

	private AsyncAccessService mAsyncAccessService;

	// UI
	private DrawerLayout mDrawerLayout;
	private RelativeLayout mDrawerNavigation;
	private ExpandableListView mDrawerNavigationExpList;
	private NavigationExpandableListAdapter mDrawerNavigationExpListAdapter;
	private ActionBarDrawerToggle mDrawerToggle;
	// UI User
	private ImageView mImgUserAvatar;
	private TextView mTxtUsername;
	private ImageView mImgUserRefresh;
	// UI Update
	private RelativeLayout mRelUpdate;
	private ImageView mImgUpdate;
	private ProgressBar mPbarUpdate;
	private TextView mTxtUpdate;
	private ProgressBar mPbarUserRefresh;

	// Helper
	private CharSequence mDrawerTitle;
	private CharSequence mTitle;

	private boolean mIsCurrentlyUpdating = false;
	private boolean mOnStartChangeFragment = true;
	// Used in onPrepareOptionsMenu to open Keyboard for Search
	private int mCurrentFragmentPosition;
	// Error
	private Dialog mNoInternetDialog;

	// Navi Positions
	public final static int NAVI_POS_HEADER_TRACK = 0;
	public final static int NAVI_POS_CALENDAR = 1;
	public final static int NAVI_POS_TO_WATCH = 2;
	public final static int NAVI_POS_HEADER_DISCOVERY = 3;
	public final static int NAVI_POS_SEARCH = 4;
	public final static int NAVI_POS_RECOMMENDATIONS = 5;
	public final static int NAVI_POS_TRENDING = 6;
	public final static int NAVI_POS_HEADER_OTHER = 7;
	public final static int NAVI_POS_YOUR_SHOWS = 8;
	public final static int NAVI_POS_SETTINGS = 9;
	public final static int NAVI_POS_SETUP = 10;
	public final static int NAVI_POS_LOG_OUT = 11;

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// ----- ON_CREATE ---------------------------------------------------------------------------------------------- //

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		mAsyncAccessService = AsyncAccessServiceImpl.getInstance(getApplicationContext());
		buildUI();

		if (savedInstanceState == null) {
			selectItem(NAVI_POS_CALENDAR);	// : Debug
		}

		//				mDrawerLayout.openDrawer(mDrawerNavigation);	// Debug
		// Check whether to show a quick peek at NavDrawer 
		boolean showPeekDrawer = getIntent().getBooleanExtra(Constants.EXTRA_PEEK_NAV_DRAWER, false);
		if(showPeekDrawer) {
			peekDrawer();
			mLastUpdateLaunched = DateHelper.getCurrentTime();	// prevent another update
		}
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// ----- BUILD_UI ----------------------------------------------------------------------------------------------- //

	private void buildUI() {
		mTitle = mDrawerTitle = getTitle();
		mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
		mDrawerNavigation = (RelativeLayout) findViewById(R.id.navigation);
		mDrawerNavigationExpList = (ExpandableListView) findViewById(R.id.navigation_explist);

		// ExpList HEADERs
		List<String> headerList = new ArrayList<String>();
		headerList.add(getString(R.string.navigation_header_track));
		headerList.add(getString(R.string.navigation_header_discovery));
		headerList.add(getString(R.string.navigation_header_other));

		// ExpList CHILDREN
		// Track Children
		List<NavigationListItem> trackChildren = new ArrayList<NavigationListItem>();
		trackChildren.add(new NavigationListItem(
				getString(R.string.navigation_item_calendar), R.drawable.ic_calendar_gray));
		trackChildren.add(new NavigationListItem(
				getString(R.string.navigation_item_to_watch), R.drawable.ic_seen_gray));
		// Discovery Children
		List<NavigationListItem> discoveryChildren = new ArrayList<NavigationListItem>();
		discoveryChildren.add(new NavigationListItem(
				getString(R.string.navigation_item_search), R.drawable.ic_search_gray));
		discoveryChildren.add(new NavigationListItem(
				getString(R.string.navigation_item_recommended_shows), R.drawable.ic_recommendations_gray));
		discoveryChildren.add(new NavigationListItem(
				getString(R.string.navigation_item_trending_shows), R.drawable.ic_trending_gray));
		// Other Children
		List<NavigationListItem> otherChildren = new ArrayList<NavigationListItem>();
		otherChildren.add(new NavigationListItem(
				getString(R.string.navigation_item_your_shows), R.drawable.ic_your_shows));
		otherChildren.add(new NavigationListItem(
				getString(R.string.navigation_item_settings), R.drawable.ic_settings_gray));
		otherChildren.add(new NavigationListItem(
				getString(R.string.navigation_item_setup), R.drawable.ic_setup_gray));
		otherChildren.add(new NavigationListItem(
				getString(R.string.navigation_item_log_out), R.drawable.ic_logout));
		// Add all to childList
		HashMap<String, List<NavigationListItem>> childList = new HashMap<String, List<NavigationListItem>>();
		childList.put(getString(R.string.navigation_header_track), trackChildren);
		childList.put(getString(R.string.navigation_header_discovery), discoveryChildren);
		childList.put(getString(R.string.navigation_header_other), otherChildren);

		mDrawerNavigationExpListAdapter = 
				new NavigationExpandableListAdapter(getApplicationContext(), headerList, childList, this);
		mDrawerNavigationExpList.setAdapter(mDrawerNavigationExpListAdapter);

		// Expand all Groups
		for (int i = 0; i < mDrawerNavigationExpListAdapter.getGroupCount(); i++) {
			mDrawerNavigationExpList.expandGroup(i);
		}
		// Keeps the Groups expanded
		mDrawerNavigationExpList.setOnGroupClickListener(new OnGroupClickListener() {
			@Override 
			public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
				return true;
			}
		});
		mDrawerNavigationExpList.setGroupIndicator(null);
		mDrawerNavigationExpList.setOnChildClickListener(new OnChildClickListener() {
			@Override
			public boolean onChildClick(
					ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
				int index = parent.getFlatListPosition(
						ExpandableListView.getPackedPositionForChild(groupPosition, childPosition));
				// only change if necessary
				if(index != parent.getCheckedItemPosition()) {
					selectItem(index);
				} else {
					mDrawerLayout.closeDrawer(mDrawerNavigation);
				}
				return true;
			}
		});

		// Shadow
		mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);

		// enable ic_drawer
		getActionBar().setDisplayHomeAsUpEnabled(true);
		getActionBar().setHomeButtonEnabled(true);

		// ActionBarDrawerToggle ties together the the proper interactions between the sliding drawer 
		// and the action bar app icon
		mDrawerToggle = new ActionBarDrawerToggle(
				this,                  /* host Activity */
				mDrawerLayout,         /* DrawerLayout object */
				R.drawable.ic_drawer,  /* nav drawer image to replace 'Up' caret */
				R.string.navigation_drawer_open,  /* "open drawer" description for accessibility */
				R.string.navigation_drawer_close  /* "close drawer" description for accessibility */
				) {
			public void onDrawerClosed(View view) {
				getActionBar().setTitle(mTitle);
				invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
			}

			public void onDrawerOpened(View drawerView) {
				getActionBar().setTitle(mDrawerTitle);
				invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
				refreshAllDynamicParts();
				KeyBoardHelper.hideKeyboard(drawerView, getApplicationContext());
			}
		};
		mDrawerLayout.setDrawerListener(mDrawerToggle);


		// Update
		mRelUpdate = (RelativeLayout) findViewById(R.id.navigation_rel_update);
		mImgUpdate = (ImageView) findViewById(R.id.navigation_img_update);
		mPbarUpdate = (ProgressBar) findViewById(R.id.navigation_pbar_update);
		mTxtUpdate = (TextView) findViewById(R.id.navigation_txt_update);
		mRelUpdate.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if(!mIsCurrentlyUpdating) {
					mIsCurrentlyUpdating = true;
					showUpdateLoading(true);
					startUpdate(0);
				}
			}
		});
		refreshAllDynamicParts();

		// User
		mImgUserAvatar = (ImageView) findViewById(R.id.navigation_img_user);
		mTxtUsername = (TextView) findViewById(R.id.navigation_txt_username);
		mImgUserRefresh = (ImageView) findViewById(R.id.navigation_img_user_refresh);
		mPbarUserRefresh = (ProgressBar) findViewById(R.id.navigation_pbar_user_refresh);
		mImgUserRefresh.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				refreshUser(true);
			}
		});
		refreshUser(false);

	}

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// ----- PEEK_DRAWER -------------------------------------------------------------------------------------------- //
	/**
	 * Opens the NavigationDrawer just a little (for a short time) to show the user that there is a NavigationDrawer.
	 * 
	 * @category PEEK_DRAWER
	 */
	private void peekDrawer() {
		new Handler().postDelayed(new Runnable() {
			@Override
			public void run() {
				final int metaState = 0;
				final float x = 0.0f;
				final float y = 100.0f;
				MotionEvent motionEvent = MotionEvent.obtain(SystemClock.uptimeMillis(), SystemClock.uptimeMillis()+100, 
						MotionEvent.ACTION_DOWN, x, y, metaState);
				mDrawerLayout.dispatchTouchEvent(motionEvent);
				motionEvent.recycle();

				new Handler().postDelayed(new Runnable() {
					@Override
					public void run() {
						MotionEvent motionEvent = MotionEvent.obtain(SystemClock.uptimeMillis(), SystemClock.uptimeMillis()+100, 
								MotionEvent.ACTION_UP, x, y, metaState);
						mDrawerLayout.dispatchTouchEvent(motionEvent);
						motionEvent.recycle();
					}
				}, 2000);	// defines how long Drawer is visible
			}
		}, 500);	// wait 500 ms to show peek, so everything is loaded
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// ----- DYNAMIC_CONTENT ---------------------------------------------------------------------------------------- //
	/**
	 * Refreshes all dynamic parts, like calendarCount, toWatchCount & lastUpdateTime.
	 * @category DYNAMIC_CONTENT
	 */
	public void refreshAllDynamicParts() {
		mDrawerNavigationExpListAdapter.refreshCalendarCount();
		mDrawerNavigationExpListAdapter.refreshToWatchCount();
		refreshLastUpdateTime();
	}

	/**
	 * Refreshes User Info
	 * @param forceUpdate	if false, only updated if not yet fully loaded
	 * @category DYNAMIC_CONTENT
	 */
	private void refreshUser(boolean forceUpdate) {
		// show the user that userInfo is loading
		mImgUserRefresh.setVisibility(View.INVISIBLE);
		mPbarUserRefresh.setVisibility(View.VISIBLE);
		mAsyncAccessService.getCurrentUser(forceUpdate, new ServiceResponseListener() {
			@Override public void onProgress(int bytesWritten, int totalSize) { }

			@Override
			public void onSuccess(Object o) {
				if(o instanceof User) {
					User user = (User) o;
					ImageLoader.getInstance().displayImage(user.getAvatar(), mImgUserAvatar, 
							ImageLoaderHelper.getDIOAvatar(getApplicationContext()));

					mTxtUsername.setText(user.getUsername());

					mImgUserRefresh.setVisibility(View.VISIBLE);
					mPbarUserRefresh.setVisibility(View.GONE);
				}
			}

			@Override
			public void onFailure(Throwable throwable) {
				showError(throwable);
			}
		});
	}


	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// ----- DOWNLOAD_SERVICE --------------------------------------------------------------------------------------- //

	private void refreshLastUpdateTime() {
		mAsyncAccessService.getLastCompleteUpdateString(new ServiceResponseListener() {
			@Override public void onProgress(int bytesWritten, int totalSize) { }

			@Override
			public void onSuccess(Object o) {
				if(o instanceof String) {
					String txt = (String) o;
					Log.w(LOGTAG, "UPDATING"+txt);
					if(txt.isEmpty()) {
						mTxtUpdate.setText(getString(R.string.navigation_click_to_update));
					} else {
						mTxtUpdate.setText(txt);
					}
				} else {
					onFailure(new WrongObjectReturnedException(String.class));
				}
			}

			@Override
			public void onFailure(Throwable throwable) {
				showError(throwable);
			}
		});

	}

	private void showUpdateLoading(boolean b) {
		if(b) {
			mImgUpdate.setVisibility(View.INVISIBLE);
			mPbarUpdate.setVisibility(View.VISIBLE);
			mTxtUpdate.setText(getString(R.string.navigation_updating));
		} else {
			mImgUpdate.setVisibility(View.VISIBLE);
			mPbarUpdate.setVisibility(View.GONE);
			refreshAllDynamicParts();
		}

	}

	private BroadcastReceiver mReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			Bundle bundle = intent.getExtras();
			if (bundle != null) {
				int resultCode = bundle.getInt(Constants.EXTRA_RESULT);
				if(resultCode == Activity.RESULT_OK) {
					DownloadResponseType responseType = 
							(DownloadResponseType) intent.getSerializableExtra(Constants.EXTRA_RESPONSE_TYPE);
					switch (responseType) {
					case COMPLETE_UPDATE_FINAL_RESULT:
						showUpdateLoading(false);
						mIsCurrentlyUpdating = false;
						break;
					default: /* DO NOTHING */ break;
					}
				}
			}
		}
	};

	private long mLastUpdateLaunched = -1;

	@Override
	public void onResume() {
		super.onResume();
		registerReceiver(mReceiver, new IntentFilter(Constants.EXTRA_DOWNLOAD_SERVICE_NOTIFICATION));
		startUpdate(5 * DateUtils.MINUTE_IN_MILLIS);
	}

	private void startUpdate(long threshold) {
		long currentTime = DateHelper.getCurrentTime();
		//		Log.e(LOGTAG, "x=" + (currentTime - mLastUpdateLaunched) + ", th="+threshold);
		if(currentTime - mLastUpdateLaunched > threshold) {
			mLastUpdateLaunched = currentTime;
			DownloadService.requestCompleteUpdateInclShowRefresh(getApplicationContext());
		}
	}

	@Override
	public void onStop() {
		super.onStop();
		unregisterReceiver(mReceiver);
	}


	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// ----- MENU --------------------------------------------------------------------------------------------------- //
	// Called whenever we call invalidateOptionsMenu()

	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		// If the nav drawer is open, hide ALL menu items related to the content view
		boolean drawerOpen = mDrawerLayout.isDrawerOpen(mDrawerNavigation);
		for (int i = 0; i < menu.size(); i++) {
			menu.getItem(i).setVisible(!drawerOpen);
		}
		// If SearchFrag is displayed, showKeyboard again.
		//		Log.e(LOGTAG, "Main.onPrepareOptionsMenu " + drawerOpen + ", cur frag pos="+mCurrentFragmentPosition);
		if(!drawerOpen && mCurrentFragmentPosition == NAVI_POS_SEARCH) {
			KeyBoardHelper.showKeyboard(mDrawerLayout, this);
		}

		return super.onPrepareOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// The action bar home/up action should open or close the drawer.
		// ActionBarDrawerToggle will take care of this.
		if (mDrawerToggle.onOptionsItemSelected(item)) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// ----- CHANGE_FRAGMENT ---------------------------------------------------------------------------------------- //

	private void selectItem(int position) {
		// update the main content by replacing fragments
		Fragment fragment = null;
		String fragmentTitle = null;
		switch (position) {
		case NAVI_POS_HEADER_TRACK:
			return;	// TRACK HEADER
		case NAVI_POS_CALENDAR:
			fragment = new CalendarFragment();
			fragmentTitle = getString(R.string.navigation_item_calendar);
			break;
		case NAVI_POS_TO_WATCH:
			fragment = new ToWatchFragment();
			fragmentTitle = getString(R.string.navigation_item_to_watch);
			break;
		case NAVI_POS_HEADER_DISCOVERY:
			return;	// DISCOVERY HEADER
		case NAVI_POS_SEARCH:
			fragment = new SearchFragment();
			fragmentTitle = getString(R.string.navigation_item_search);
			break;
		case NAVI_POS_RECOMMENDATIONS:
			fragment = new RecommendationsFragment();
			fragmentTitle = getString(R.string.navigation_item_recommended_shows);
			break;
		case NAVI_POS_TRENDING:
			fragment = new TrendingShowsFragment();
			fragmentTitle = getString(R.string.navigation_item_trending_shows);
			break;
		case NAVI_POS_HEADER_OTHER:
			return;	// OTHER HEADER
		case NAVI_POS_YOUR_SHOWS:
			fragment = new YourShowsFragment();
			fragmentTitle = getString(R.string.navigation_item_your_shows);
			break;
		case NAVI_POS_SETTINGS:
			LinkingHelper.goToSettings(this);
			break;
		case NAVI_POS_SETUP:
			LinkingHelper.goToSetup(this);
			break;	
		case NAVI_POS_LOG_OUT:
			askForLogout();
			break;
		default:
			return;
		}
		if(fragment!=null) {
			this.changeFragment(fragment, fragmentTitle, position);
		}
	}

	public void changeFragment(Fragment fragment, String fragmentTitle, int position) {
		String fragmentTag = fragmentTitle+position; //mNavTextItems[position];

		FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
		transaction.replace(R.id.content_frame, fragment, fragmentTag);
		if(mOnStartChangeFragment) {
			mOnStartChangeFragment = false;
		} else {
			transaction.addToBackStack(null);
		}
		transaction.commit();
		// needed for keyboard handling in onPrepareOptionsMenu
		mCurrentFragmentPosition = position;

		// update selected item and title, then close the drawer
		mDrawerNavigationExpList.setItemChecked(position, true);
		setTitle(fragmentTitle);
		mDrawerLayout.closeDrawer(mDrawerNavigation);
	}

	@Override
	public void setTitle(CharSequence title) {
		mTitle = title;
		getActionBar().setTitle(mTitle);
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// ----- ACTION_BAR_TOGGLE -------------------------------------------------------------------------------------- //
	// When using the ActionBarDrawerToggle, you must call it during onPostCreate() and onConfigurationChanged()...   //

	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);
		// Sync the toggle state after onRestoreInstanceState has occurred.
		mDrawerToggle.syncState();
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		// Pass any configuration change to the drawer toggls
		mDrawerToggle.onConfigurationChanged(newConfig);
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// ----- LOGOUT ------------------------------------------------------------------------------------------------- //

	private void askForLogout() {
		new AlertDialog.Builder(this)
		.setTitle(getString(R.string.logout_alert_title))	// Title
		.setMessage(getString(R.string.logout_alert_msg))	// Message
		.setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog,int id) {
				dialog.cancel();
			}
		})
		.setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog,int id) {
				logout();
			}
		}).create().show();
	}

	private void logout() {
		DialogHelper.logout(this);
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// ----- ERROR -------------------------------------------------------------------------------------------------- //

	/**
	 * Redirects to {@link DialogHelper#handleDialogErrors(Throwable, Activity)}
	 * @param throwable
	 */
	public void showError(Throwable throwable) {
		if(DialogHelper.isInternetError(throwable)) {
			if(mNoInternetDialog == null) {
				mNoInternetDialog = DialogHelper.getNoInternetErrorDialogBuilder(this)
						.setPositiveButton(getString(R.string.error_btn_retry), new AlertDialog.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog, int id) {
								refreshUser(true);
							}
						}).create();
			}
			if(!mNoInternetDialog.isShowing()) {
				mNoInternetDialog.show();
			}
			return;
		}
		DialogHelper.handleDialogErrors(throwable, this);
	}
}
