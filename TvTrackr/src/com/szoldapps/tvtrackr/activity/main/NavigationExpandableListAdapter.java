package com.szoldapps.tvtrackr.activity.main;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.szoldapps.tvtrackr.R;
import com.szoldapps.tvtrackr.exception.WrongObjectReturnedException;
import com.szoldapps.tvtrackr.helper.LogCatHelper;
import com.szoldapps.tvtrackr.service.async.AsyncAccessService;
import com.szoldapps.tvtrackr.service.async.AsyncAccessServiceImpl;
import com.szoldapps.tvtrackr.service.listener.ServiceResponseListener;

public class NavigationExpandableListAdapter extends BaseExpandableListAdapter {

	public static final String LOGTAG = LogCatHelper.getAppTag()+NavigationExpandableListAdapter.class.getSimpleName().toString();

	private Context mContext;
	private List<String> mListDataHeader; // header titles
	private HashMap<String, List<NavigationListItem>> mListDataChild;	// child data in format of header title, child title
	private AsyncAccessService mAsyncAccessService;
	private MainActivity mMainActivity;

	private TextView mTxtToWatchCounter;
	private TextView mTxtCalendarCounter;


	public NavigationExpandableListAdapter(Context context, List<String> listDataHeader, 
			HashMap<String, List<NavigationListItem>> listChildData, MainActivity mainActivity) {
		mContext = context;
		mListDataHeader = listDataHeader;
		mListDataChild = listChildData;
		mAsyncAccessService = AsyncAccessServiceImpl.getInstance(context);
		mMainActivity = mainActivity;
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// ----- GROUP_METHODS ------------------------------------------------------------------------------------------ //

	@Override
	public Object getGroup(int groupPosition) {
		return mListDataHeader.get(groupPosition);
	}

	@Override
	public long getGroupId(int groupPosition) {
		return groupPosition;
	}

	@Override
	public int getGroupCount() {
		return mListDataHeader.size();
	}

	@Override
	public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
		if (convertView == null) {
			LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater.inflate(R.layout.li_navigation_group, null);
		}
		String name = (String) getGroup(groupPosition);

		// name
		TextView txtName = (TextView) convertView.findViewById(R.id.li_navigation_group_txt_name);
		txtName.setText(name.toUpperCase(Locale.getDefault()));
		return convertView;
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// ----- CHILD_METHODS ------------------------------------------------------------------------------------------ //


	@Override
	public Object getChild(int groupPosition, int childPosititon) {
		return this.mListDataChild.get(this.mListDataHeader.get(groupPosition)).get(childPosititon);
	}

	@Override
	public long getChildId(int groupPosition, int childPosition) {
		return childPosition;
	}

	@Override
	public int getChildrenCount(int groupPosition) {
		return mListDataChild.get(mListDataHeader.get(groupPosition)).size();
	}

	@Override
	public View getChildView(int groupPosition, final int childPosition, boolean isLastChild, View convertView, 
			ViewGroup parent) {

		if (convertView == null) {
			LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater.inflate(R.layout.li_navigation, null);
		}
		NavigationListItem navLi = (NavigationListItem) getChild(groupPosition, childPosition);

		// icon
		ImageView imgIcon = (ImageView) convertView.findViewById(R.id.li_navigation_img_icon);
		imgIcon.setImageDrawable(mContext.getResources().getDrawable(navLi.getDrawable()));

		// name
		TextView txtName = (TextView) convertView.findViewById(R.id.li_navigation_txt_name);
		txtName.setText(navLi.getName());

		// counter (only on calendar and to watch)
		if(navLi.getName().equals(mContext.getString(R.string.navigation_item_calendar))) {
			setCalendarCount((TextView) convertView.findViewById(R.id.li_navigation_txt_counter));
		} else if(navLi.getName().equals(mContext.getString(R.string.navigation_item_to_watch))) {
			setToWatchCount((TextView) convertView.findViewById(R.id.li_navigation_txt_counter));
		}
		return convertView;
	}

	public void refreshCalendarCount() {
		if(mTxtCalendarCounter != null) {
			//mTxtCalendarCounter.setVisibility(View.INVISIBLE);	// Looks stupid, if calculations are fast enough
			setCalendarCount(mTxtCalendarCounter);
		}
	}

	private void setCalendarCount(final TextView txtCounter) {
		mTxtCalendarCounter = txtCounter;
		mAsyncAccessService.getUnwatchedEpisodeCountOfDay(0, new ServiceResponseListener() {
			@Override
			public void onProgress(int bytesWritten, int totalSize) { }

			@Override
			public void onSuccess(Object o) {
				if(o instanceof Long) {
					Long count = (Long) o;
					mTxtCalendarCounter.setText(count > 0 ? count.toString() : "");
					mTxtCalendarCounter.setVisibility(View.VISIBLE);
				} else {
					onFailure(new WrongObjectReturnedException(Long.class));
				}
			}

			@Override
			public void onFailure(Throwable throwable) {
				mMainActivity.showError(throwable);
			}
		});
	}

	public void refreshToWatchCount() {
		if(mTxtToWatchCounter != null) {
			//mTxtToWatchCounter.setVisibility(View.INVISIBLE);		// Looks stupid, if calculations are fast enough
			setToWatchCount(mTxtToWatchCounter);
		}
	}

	private void setToWatchCount(final TextView txtCounter) {
		mTxtToWatchCounter = txtCounter;
		mAsyncAccessService.getAllEpisodesToWatchCount(new ServiceResponseListener() {
			@Override
			public void onProgress(int bytesWritten, int totalSize) { }

			@Override
			public void onSuccess(Object o) {
				if(o instanceof Long) {
					Long count = (Long) o;
					mTxtToWatchCounter.setText(count > 0 ? count.toString() : "");
					mTxtToWatchCounter.setVisibility(View.VISIBLE);
				} else {
					onFailure(new WrongObjectReturnedException(Long.class));
				}
			}

			@Override
			public void onFailure(Throwable throwable) {
				mMainActivity.showError(throwable);
			}
		});
	}


	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// ----- OTHER_METHODS ------------------------------------------------------------------------------------------ //

	@Override
	public boolean hasStableIds() {
		return false;
	}

	@Override
	public boolean isChildSelectable(int groupPosition, int childPosition) {
		return true;
	}
}
