package com.szoldapps.tvtrackr.activity.main.search;

import java.util.List;

import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.szoldapps.tvtrackr.R;
import com.szoldapps.tvtrackr.db.model.Show;
import com.szoldapps.tvtrackr.dto.show.SearchShowDTO;
import com.szoldapps.tvtrackr.exception.WrongObjectReturnedException;
import com.szoldapps.tvtrackr.helper.DialogHelper;
import com.szoldapps.tvtrackr.helper.ImageLoaderHelper;
import com.szoldapps.tvtrackr.helper.ImageURLHelper;
import com.szoldapps.tvtrackr.helper.KeyBoardHelper;
import com.szoldapps.tvtrackr.helper.LinkingHelper;
import com.szoldapps.tvtrackr.helper.LogCatHelper;
import com.szoldapps.tvtrackr.helper.ScaleImageView;
import com.szoldapps.tvtrackr.service.async.AsyncAccessService;
import com.szoldapps.tvtrackr.service.async.AsyncAccessServiceImpl;
import com.szoldapps.tvtrackr.service.listener.ServiceResponseListener;

public class SearchListArrayAdapter extends ArrayAdapter<SearchShowDTO> {

	public static final String LOGTAG = LogCatHelper.getAppTag()+SearchListArrayAdapter.class.getSimpleName().toString();

	private final Context mContext;
	//		private List<ListItemCache> mListItemsCache = new ArrayList<ListItemCache>();
	private SearchFragment mSearchFragment;
	private List<SearchShowDTO> mShows;
	// AsyncAccess
	private AsyncAccessService mAsyncAccessService;

	public SearchListArrayAdapter(SearchFragment searchFragment, Context context, int resource, List<SearchShowDTO> shows) {
		super(context, resource, shows);
		mSearchFragment = searchFragment;
		mContext = context;
		mShows = shows;
		mAsyncAccessService = AsyncAccessServiceImpl.getInstance(context);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater) mSearchFragment.getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		if(mShows == null || mShows.isEmpty()) {
			RelativeLayout relListItem = (RelativeLayout) inflater.inflate(R.layout.li_search_info, parent, false);
			ProgressBar pbarLoading = (ProgressBar) relListItem.findViewById(R.id.li_search_info_pbar_loading);
			TextView txtLoading = (TextView) relListItem.findViewById(R.id.li_search_info_txt_hl);

			if(mShows == null) {	// LOADING
				pbarLoading.setVisibility(View.VISIBLE);
				txtLoading.setText(mContext.getString(R.string.search_loading_searching_for_shows));
			} else {
				pbarLoading.setVisibility(View.GONE);	// No Shows found
				txtLoading.setText(mContext.getString(R.string.search_no_shows_found));
			}
			return relListItem;
		}


		final RelativeLayout relListItem = (RelativeLayout) inflater.inflate(R.layout.li_search, parent, false);
		final SearchShowDTO show = mShows.get(position);

		// Poster
		ScaleImageView imgPoster = (ScaleImageView) relListItem.findViewById(R.id.li_search_img_poster);
		String imgUrl = ImageURLHelper.getImageURLByImageType(show.getImages().getPoster(), ImageURLHelper.POSTER_SMALL);
		ImageLoader.getInstance().displayImage(imgUrl, imgPoster, ImageLoaderHelper.getDIOPosterOrHeadShot(mContext));

		// Hl
		TextView txtHl = (TextView) relListItem.findViewById(R.id.li_search_txt_hl);
		txtHl.setText(show.getTitle());

		// SubHl
		TextView txtSubHl = (TextView) relListItem.findViewById(R.id.li_search_txt_subhl);
		txtSubHl.setText(String.format(mSearchFragment.getString(R.string.show_year_squared_brackets), show.getYear()));

		// imgFollow
		final ProgressBar pbarDownload = (ProgressBar) relListItem.findViewById(R.id.li_search_pbar_download_loading);
		final TextView txtDownload = (TextView) relListItem.findViewById(R.id.li_search_txt_download);
		final ImageView imgFollow = (ImageView) relListItem.findViewById(R.id.li_search_img_follow);
		if(show.isFollowedByUser()) {
			imgFollow.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_follow_blue));
		} else {
			imgFollow.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_follow_gray));
		}

		imgFollow.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if(!show.isFollowedByUser()) {
					followShow(imgFollow, pbarDownload, txtDownload, show);
				} // else do nothing, unfollow only works in AddFragment, YourShowsActivity & ShowFragmentActivity
			}
		});

		relListItem.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				KeyBoardHelper.hideKeyboard(relListItem, mContext);
				LinkingHelper.goToShow(show, mSearchFragment.getActivity(), false);
			}
		});

		return relListItem;
	}

	private void followShow(final ImageView imgFollow, final ProgressBar pbarDownload, final TextView txtDownload, 
			final SearchShowDTO show) {
		showDownloading(true, pbarDownload, txtDownload, imgFollow);
		mAsyncAccessService.uploadFollowAndSaveShow(show, new ServiceResponseListener() {
			@Override 
			public void onProgress(int bytesWritten, int totalSize) { }

			@Override
			public void onSuccess(Object o) {
				if(o instanceof Boolean) {
					if((Boolean) o) {
						imgFollow.setImageDrawable(
								mContext.getResources().getDrawable(R.drawable.ic_follow_blue));
						String msg = String.format(mContext.getString(R.string.followed_show), show.getTitle());
						DialogHelper.showInfoShort(msg, mSearchFragment.getActivity());
						showDownloading(false, pbarDownload, txtDownload, imgFollow);
					} else {
						DialogHelper.getFollowErrorDialogBuilder(mSearchFragment.getActivity())
						.setPositiveButton(mContext.getString(R.string.error_btn_retry), 
								new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,int id) {
								followShow(imgFollow, pbarDownload, txtDownload, show);
							}
						}).create().show();
						showDownloading(false, pbarDownload, txtDownload, imgFollow);
					}
				} else {
					onFailure(new WrongObjectReturnedException(Boolean.class));
				}
			}

			@Override
			public void onFailure(Throwable throwable) {
				mSearchFragment.showError(throwable);
			}
		});
	}

	private void showDownloading(boolean b, View pbarDownload, View txtDownload, View imgFollow) {
		if(b) {
			pbarDownload.setVisibility(View.VISIBLE);
			txtDownload.setVisibility(View.VISIBLE);
			imgFollow.setVisibility(View.GONE);
		} else {
			pbarDownload.setVisibility(View.GONE);
			txtDownload.setVisibility(View.GONE);
			imgFollow.setVisibility(View.VISIBLE);
		}
	}

	@Override
	public int getCount() {
		if(mShows == null) {	// LOADING
			return 1;
		}
		if(mShows.isEmpty()) {	// No Shows found
			return 1;
		}
		return mShows.size();
	}

	public void setShows(List<SearchShowDTO> shows) {
		this.mShows = shows;
		this.notifyDataSetChanged();
	}


	public class ShowListItemCache {
		ProgressBar pbarDownload;
		TextView txtDownload;
		ImageView imgFollow;
		Show show;
		public ShowListItemCache(ProgressBar pbarDownload, TextView txtDownload, ImageView imgFollow, Show show) {
			super();
			this.txtDownload = txtDownload;
			this.imgFollow = imgFollow;
			this.show = show;
			this.pbarDownload = pbarDownload;
		}
	}

}