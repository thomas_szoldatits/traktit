package com.szoldapps.tvtrackr.activity.main.search;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.SearchView;
import android.widget.SearchView.OnQueryTextListener;

import com.szoldapps.tvtrackr.R;
import com.szoldapps.tvtrackr.dto.show.SearchShowDTO;
import com.szoldapps.tvtrackr.exception.WrongObjectReturnedException;
import com.szoldapps.tvtrackr.helper.Constants;
import com.szoldapps.tvtrackr.helper.DialogHelper;
import com.szoldapps.tvtrackr.helper.KeyBoardHelper;
import com.szoldapps.tvtrackr.helper.LogCatHelper;
import com.szoldapps.tvtrackr.service.async.AsyncAccessService;
import com.szoldapps.tvtrackr.service.async.AsyncAccessServiceImpl;
import com.szoldapps.tvtrackr.service.listener.ServiceResponseListener;

/**
 * Searches Trakt for Shows.
 * 
 * @author thomasszoldatits
 *
 */
public class SearchFragment extends Fragment {

	public static final String LOGTAG = LogCatHelper.getAppTag()+SearchFragment.class.getSimpleName().toString();

	private LayoutInflater mInflater;
	private Activity mActivity;
	private View mRootView;

	// AsyncAccess
	private AsyncAccessService mAsyncAccessService;

	// UI-Elements
	private SearchView mSearchView;
	private ListView mListShows;
	private SearchListArrayAdapter mListShowsAdapter;
	private RelativeLayout mRelSearchHint;
	// Error
	private AlertDialog mNoInternetDialog;

	public SearchFragment() { /* Empty constructor required for fragment subclasses */ }

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		mActivity = (Activity) getActivity();
		mInflater = inflater;
		mAsyncAccessService = AsyncAccessServiceImpl.getInstance(mActivity.getApplicationContext());

		mRootView = inflater.inflate(R.layout.fragment_search, container, false);

		buildUI();
		return mRootView;
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// ----- BUILD_UI ----------------------------------------------------------------------------------------------- //

	private void buildUI() {
		setHasOptionsMenu(true);
		mRelSearchHint = (RelativeLayout) mRootView.findViewById(R.id.search_rel_hint);

		// ListView
		mListShows = (ListView) mRootView.findViewById(R.id.search_list);
		mListShowsAdapter = new SearchListArrayAdapter(this, mActivity.getApplicationContext(), R.id.search_list, null);
		mListShows.setAdapter(mListShowsAdapter);
		// ListView: Header and Footer
		RelativeLayout relHeader = (RelativeLayout) mInflater.inflate(R.layout.li_search_header, mListShows, false);
		RelativeLayout relFooter = (RelativeLayout) mInflater.inflate(R.layout.li_footer_with_card_shadow, mListShows, false);
		mListShows.addHeaderView(relHeader);
		mListShows.addFooterView(relFooter);
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// ----- MENU --------------------------------------------------------------------------------------------------- //

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		inflater.inflate(R.menu.search, menu);

		// Get the SearchView and set the searchable configuration
		SearchManager searchManager = (SearchManager) getActivity().getSystemService(Context.SEARCH_SERVICE);
		mSearchView = (SearchView) menu.findItem(R.id.menu_search).getActionView();
		// Assumes current activity is the searchable activity
		mSearchView.setSearchableInfo(searchManager.getSearchableInfo(getActivity().getComponentName()));
		mSearchView.setIconifiedByDefault(false); // Do not iconify the widget; expand it by default
		mSearchView.setOnQueryTextListener(new OnQueryTextListener() {

			private Timer timer=new Timer();
			@Override
			public boolean onQueryTextSubmit(String query) {
				onQueryTextChange(query);
				KeyBoardHelper.hideKeyboard(mSearchView, mActivity.getApplicationContext());
				return false;
			}

			@Override
			public boolean onQueryTextChange(final String newText) {
				timer.cancel();
				timer = new Timer();
				timer.schedule(new TimerTask() {
					@Override
					public void run() {
						mCurrentQuery = newText;
						mSearchHandler.sendEmptyMessage(Constants.SEARCH_DELAY);
					}
				}, Constants.SEARCH_DELAY);
				return false;
			}
		});

		mSearchView.requestFocus();
	}

	@SuppressLint("HandlerLeak")
	private Handler mSearchHandler = new Handler(){
		public void handleMessage(android.os.Message msg) {
			switch (msg.what) {
			case Constants.SEARCH_DELAY:
				search(mCurrentQuery);
			}

		};
	};

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// ----- SEARCH ------------------------------------------------------------------------------------------------- //

	private String mCurrentQuery;
	private String mLastSearch;

	protected void search(String query) {
		
		// used for NoInternetError
		if(query == null) {
			if(mCurrentQuery != null) {
				query = mCurrentQuery;
			} else {
				query = "";
			}
		}
		query = query.trim();

		if(query.length() < 1) {
			showListShows(false);
			return;
		}
		// Just to prevent unnecessary queries (e.g. 1st query = "lost", 2nd query = "lost " -> no query necessary)
		if(mLastSearch != null) {	
			if(mLastSearch.equals(query)) {
				return;
			}
		}

		// Show Loading
		mListShowsAdapter.setShows(null);
		showListShows(true);

		mLastSearch = query;
		mAsyncAccessService.search(query, 10, new ServiceResponseListener() {
			@Override public void onProgress(int bytesWritten, int totalSize) { }

			@Override @SuppressWarnings("unchecked")
			public void onSuccess(Object o) {
				if(o != null) {
					List<SearchShowDTO> searchResults = (List<SearchShowDTO>) o;
					mListShowsAdapter.setShows(searchResults);
				} else {
					onFailure(new WrongObjectReturnedException("Expected: List<SearchShowDTO>"));
				}
			}

			@Override
			public void onFailure(Throwable throwable) {
				showError(throwable);
			}
		});
	}

	private void showListShows(boolean b) {
		if(b) {
			mRelSearchHint.setVisibility(View.INVISIBLE);
			mListShows.setVisibility(View.VISIBLE);
		} else {
			mRelSearchHint.setVisibility(View.VISIBLE);
			mListShows.setVisibility(View.INVISIBLE);
		}
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// ----- ERROR -------------------------------------------------------------------------------------------------- //

	protected void showError(Throwable throwable) {
		if(DialogHelper.isInternetError(throwable)) {
			// hide Search Loading
			mListShowsAdapter.setShows(new ArrayList<SearchShowDTO>());
			// Reset mLastSearch so the research after internet access regained, isn't blocked by same search request
			mLastSearch = "";
			if(mNoInternetDialog == null) {
				mNoInternetDialog = DialogHelper.getNoInternetErrorDialogBuilder(getActivity())
						.setPositiveButton(getString(R.string.error_btn_retry), new AlertDialog.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog, int id) {
								search(null);
							}
						}).create();
			}
			if(!mNoInternetDialog.isShowing()) {
				mNoInternetDialog.show();
			}
			return;
		}
		DialogHelper.handleDialogErrors(throwable, mActivity);
	}

}
