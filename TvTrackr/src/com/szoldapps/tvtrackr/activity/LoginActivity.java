package com.szoldapps.tvtrackr.activity;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

import org.apache.http.client.HttpResponseException;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.StyleSpan;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;

import com.szoldapps.tvtrackr.R;
import com.szoldapps.tvtrackr.activity.main.MainActivity;
import com.szoldapps.tvtrackr.activity.setup.SetupActivity;
import com.szoldapps.tvtrackr.db.model.User;
import com.szoldapps.tvtrackr.exception.FailedAuthentificationException;
import com.szoldapps.tvtrackr.exception.UserNotFoundException;
import com.szoldapps.tvtrackr.helper.Constants;
import com.szoldapps.tvtrackr.helper.DialogHelper;
import com.szoldapps.tvtrackr.helper.FormValidationHelper;
import com.szoldapps.tvtrackr.helper.KeyBoardHelper;
import com.szoldapps.tvtrackr.helper.LinkingHelper;
import com.szoldapps.tvtrackr.helper.LogCatHelper;
import com.szoldapps.tvtrackr.service.async.AsyncAccessService;
import com.szoldapps.tvtrackr.service.async.AsyncAccessServiceImpl;
import com.szoldapps.tvtrackr.service.listener.ServiceResponseListener;


public class LoginActivity extends Activity {

	public static final String LOGTAG = LogCatHelper.getAppTag()+LoginActivity.class.getSimpleName().toString();

	private AsyncAccessService mAsyncAccessService;

	private boolean mIsLoggingOut;
	private boolean mAreListenersSet;

	//	private UserService mUserService;
	private User mUser;

	/** UI-Elements **/
	// Loading
	private RelativeLayout mRelLoading;
	private TextView mTxtLoadingMsg;
	// Logo Hl
	private TextView mTxtHl;
	// Form
	private LinearLayout mLinForm;
	private EditText mEtxtUsername;
	private EditText mEtxtPassword;
	private Button mBtnLogin;
	// OpenRegisterLink
	private TextView mTxtJoinTrakt;

	private boolean mCurrentlyLoggingIn = false;



	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// ----- ON_CREATE ---------------------------------------------------------------------------------------------- //

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);

		Log.d(LOGTAG, "------------------------------------------------------------------------------\n" + 
				"------------------------------------------------------------------------------\n" + 
				"------------------------------ TVTRACKR STARTED ------------------------------");

		mAsyncAccessService = AsyncAccessServiceImpl.getInstance(getApplicationContext());

		/* Calling this during onCreate() ensures that your application is properly initialized with default settings, 
		 * which your application might need to read in order to determine some behaviors 
		 * (such as whether to download data while on a cellular network).
		 * When false, the system sets the default values only if this method has never been called in the past 
		 * (or the KEY_HAS_SET_DEFAULT_VALUES in the default value shared preferences file is false).
		 */
		PreferenceManager.setDefaultValues(this, R.xml.preferences, false);

		// Extras
		mIsLoggingOut = getIntent().getBooleanExtra(Constants.EXTRA_LOGOUT_BOOL, false);

		// Define UI Elements
		buildUI();
		// Load Services
		if(mIsLoggingOut) {
			Log.d(LOGTAG, "User Service succesfully loaded -> logout()");
			logout();
		} else {
			Log.d(LOGTAG, "User Service succesfully loaded -> loadCurrentUser()");
			loadCurrentUser();
		}

	}

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// ----- BUILD_UI ----------------------------------------------------------------------------------------------- //

	/**
	 * @category BUILD_UI
	 */
	private void buildUI() {
		// Loading
		mRelLoading = (RelativeLayout) findViewById(R.id.login_rel_loading);
		mTxtLoadingMsg = (TextView) findViewById(R.id.login_txt_loading);

		// Logo HL
		mTxtHl = (TextView) findViewById(R.id.login_txt_logo);
		SpannableString spannablecontent=new SpannableString(getString(R.string.app_name));
		spannablecontent.setSpan(new StyleSpan(android.graphics.Typeface.BOLD), 2, getString(R.string.app_name).length(), 0);
		mTxtHl.setText(spannablecontent);

		// Form
		mLinForm = (LinearLayout) findViewById(R.id.login_lin_form);
		mEtxtUsername = (EditText) findViewById(R.id.login_etxt_username);
		mEtxtPassword = (EditText)findViewById(R.id.login_etxt_password);
		mBtnLogin = (Button) findViewById(R.id.login_btn_login);
		// OpenRegisterLink
		mTxtJoinTrakt = (TextView) findViewById(R.id.login_txt_open_register);

		SpannableString spanString = new SpannableString(mTxtJoinTrakt.getText());
		spanString.setSpan(new UnderlineSpan(), 0, spanString.length(), 0);
		mTxtJoinTrakt.setText(spanString);

	}

	private void loadCurrentUser() {
		mAsyncAccessService.getCurrentUser(false, new ServiceResponseListener() {
			@Override
			public void onSuccess(Object o) {
				if(o instanceof User) {
					mUser = (User) o;
					//login();
					goToNextActivity();
				}
			}
			@Override
			public void onFailure(Throwable throwable) {
				if(throwable instanceof UserNotFoundException) {
					Log.d(LOGTAG, "No current User set in DB -> Show Login Form");
					mUser = null;
					showLoading(false);
				} else {
					showError(throwable);
				}
			}
			@Override public void onProgress(int bytesWritten, int totalSize) { }
		});
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// ----- LOADING ------------------------------------------------------------------------------------------------ //

	/**
	 * Hides or shows loading depending on <code>b</code>.
	 * 
	 * @param b
	 * @category LOADING
	 */
	private void showLoading(boolean b) {
		if(b) {
			mRelLoading.setVisibility(View.VISIBLE);
			mLinForm.setVisibility(View.GONE);
		} else {
			setListeners();
			mRelLoading.setVisibility(View.GONE);
			mLinForm.setVisibility(View.VISIBLE);
			mEtxtUsername.clearFocus();
			mLinForm.requestFocus();
		}
	}


	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// ----- LISTENERS ---------------------------------------------------------------------------------------------- //

	/**
	 * Sets Listeners on form elements.
	 * @category LISTENERS
	 */
	private void setListeners() {
		// Listener for the "Sign in"-Button on the SoftKeyboard
		if(!mAreListenersSet) {
			mEtxtPassword.setOnEditorActionListener(new OnEditorActionListener() {        
				@Override
				public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
					if(actionId == R.id.login || actionId == EditorInfo.IME_NULL) { 
						loginViaForm();
					}
					return false;
				}
			});

			// Normal Button Listener
			mBtnLogin.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View view) {
					loginViaForm();
				}
			});

			mTxtJoinTrakt.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					LinkingHelper.goToRegister(LoginActivity.this);
				}
			});
			mAreListenersSet = true;


			/** VALIDATION LISTENERS **/
			// USERNAME
			mEtxtUsername.addTextChangedListener(
					FormValidationHelper.getUsernameTextWatcher(mEtxtUsername, this));
			mEtxtUsername.setOnFocusChangeListener(
					FormValidationHelper.getUsernameFocusChangeListener(mEtxtUsername, this));
			// PASSWORD
			mEtxtPassword.setOnFocusChangeListener(
					FormValidationHelper.getPasswordFocusChangeListener(mEtxtPassword, this));

		}
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// ----- FORM_VALIDATION ---------------------------------------------------------------------------------------- //

	/**
	 * Checks if form is valid. Calls {@link FormValidationHelper} methods to achieve that.
	 * 
	 * @return
	 * @category FORM_VALIDATION
	 */
	private boolean isFormValid() {
		EditText firstErrorView = null;

		FormValidationHelper.validateUsername(mEtxtUsername, this);
		FormValidationHelper.validatePassword(mEtxtPassword, this);

		if(!TextUtils.isEmpty(mEtxtUsername.getError())) {
			firstErrorView = mEtxtUsername;
		} else if(!TextUtils.isEmpty(mEtxtPassword.getError())) {
			firstErrorView = mEtxtPassword;
		}

		// SHOW ERROR if present
		if(firstErrorView != null) {
			firstErrorView.requestFocus();
			KeyBoardHelper.showKeyboard(firstErrorView, getApplicationContext());
			return false;
		}
		return true;
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// ----- LOGIN -------------------------------------------------------------------------------------------------- //

	/**
	 * Instantiates and fills {@link #mUser} according to the form and calls {@link #login()}.
	 * 
	 * @category LOGIN
	 */
	private void loginViaForm() {
		if(mCurrentlyLoggingIn) {
			Log.w(LOGTAG, "BLOCKED second LoginRequest");
			return;
		}

		if(isFormValid()) {
			// Store values at the time of the login attempt.
			if(mUser == null) {
				mUser = new User();
			}
			try {
				mUser.setUsername(mEtxtUsername.getText().toString());
				mUser.setPassword(mEtxtPassword.getText().toString());
			} catch (UnsupportedEncodingException e) {
				showError(e);
			}
			login();
		}
	}

	/**
	 * Logs in {@link #mUser}.
	 * 
	 * @category LOGIN
	 */
	private void login() {
		if(!mUser.getUsername().isEmpty() && !mUser.getPassword().isEmpty()) {
			mTxtLoadingMsg.setText(String.format(getString(R.string.login_loading_user), mUser.getUsername()));
			showLoading(true);
			Log.d(LOGTAG, "login: username="+mUser.getUsername() + ", curUser="+mUser.isCurrentUser());
			mCurrentlyLoggingIn = true;
			mAsyncAccessService.login(mUser, new ServiceResponseListener() {
				@Override
				public void onProgress(int bytesWritten, int totalSize) { }

				@Override
				public void onSuccess(Object o) {
					mCurrentlyLoggingIn = false;
					if(o instanceof User) {
						mUser = (User) o;
						Log.d(LOGTAG, "LOGIN:DONE:USER="+mUser.getUsername());
						mTxtLoadingMsg.setText(
								String.format(getString(R.string.login_loading_user_successful), mUser.getUsername()));
						goToNextActivity();
					}
				}

				@Override
				public void onFailure(Throwable throwable) {
					mCurrentlyLoggingIn = false;
					showError(throwable);
				}
			});
		} else {
			showLoading(false);
		}
	}


	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// ----- GO_TO_NEXTACTIVITY ------------------------------------------------------------------------------------- //

	/**
	 * Depending on whether this is the first time, the user opened the app on this device, {@link SetupActivity} or 
	 * {@link MainActivity} are opened.
	 * 
	 * @category GO_TO_NEXTACTIVITY
	 */
	private void goToNextActivity() {
		Class<?> cls = null;
		if(mUser.isFirstTimeUserOnDevice()) {
			cls = SetupActivity.class;
		} else {
			cls = MainActivity.class;
		}

		// DEBUG
		//		cls = ShowActivity.class;
		//		cls = EpisodesActivity.class;
		//		cls = ActorActivity.class;
		//		cls = SettingsActivity.class;
		//		cls = SetupActivity.class;
		// DEBUG

		Intent intent = new Intent(getApplicationContext(), cls);
		intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

		// DEBUG
		intent.putExtra(Constants.EXTRA_SHOW_TVDB_ID, 260586);//259054); //260586);    //DEBUG
		intent.putExtra(Constants.EXTRA_SHOW_TITLE, "Cosmos: A Spacetime Odyssey");    //DEBUG

		//		intent.putExtra(Constants.EXTRA_SHOW_TVDB_ID, 71256);    //DEBUG
		//		intent.putExtra(Constants.EXTRA_SHOW_TITLE, "The Daily Show with Jon Stewart");    //DEBUG

		//		intent.putExtra(Constants.EXTRA_SHOW_TVDB_ID, 81189);    //DEBUG
		//		intent.putExtra(Constants.EXTRA_SHOW_TITLE, "Breaking Bad");    //DEBUG

		//		intent.putExtra(Constants.EXTRA_ACTOR_SLUG, "evangeline-lilly");
		// DEBUG
		startActivity(intent);
		overridePendingTransition(R.anim.enter_right_slide, R.anim.exit_left_slide);
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// ----- LOGOUT ------------------------------------------------------------------------------------------------- //

	/**
	 * Logs the current User out. Displays short Info of success and calls {@link #showLoginForm()}.
	 * @category LOGOUT
	 */
	private void logout() {
		mTxtLoadingMsg.setText(getString(R.string.login_loading_logging_out));
		mAsyncAccessService.logout(new ServiceResponseListener() {
			@Override public void onProgress(int bytesWritten, int totalSize) { }
			@Override
			public void onSuccess(Object o) {
				mUser = (User) o;
				Log.i(LOGTAG, "LOGGED OUT (username="+mUser.getUsername() + ", curUser="+mUser.isCurrentUser() + ")");
				DialogHelper.showInfoShort(
						String.format(getString(R.string.logout_success), mUser.getUsername()), LoginActivity.this);
				showLoading(false);
			}
			@Override
			public void onFailure(Throwable throwable) {
				showError(throwable);
			}
		});
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// ----- ERROR -------------------------------------------------------------------------------------------------- //

	/**
	 * Displays check Credentials Dialog.
	 * @category ERROR
	 */
	private void showCheckCredentialsDialog() {
		new AlertDialog.Builder(this)
		.setTitle(getString(R.string.error_wrong_credentials_title))
		.setMessage(getString(R.string.error_wrong_credentials_login))
		.setNeutralButton(getString(R.string.continue_str), new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog,int id) {
				dialog.cancel();
				mEtxtUsername.setError(getString(R.string.error_wrong_credentials_login));
				mEtxtPassword.setError(getString(R.string.error_wrong_credentials_login));
			}
		}).create().show();
	}

	/**
	 * Handles {@link HttpResponseException} & {@link IOException}. Everything else is passed on to 
	 * {@link DialogHelper#getGeneralErrorDialog(Throwable, Activity)}
	 * 
	 * @param throwable
	 * @category ERROR
	 */
	private void showError(Throwable throwable) {
		if(throwable instanceof FailedAuthentificationException || throwable instanceof HttpResponseException) {
			mEtxtUsername.setText(mUser.getUsername());
			showLoading(false);
			showCheckCredentialsDialog();
		} else if(DialogHelper.isInternetError(throwable)) {
			showLoading(false);
			DialogHelper.getNoInternetErrorDialogBuilder(this)
			.setPositiveButton(getString(R.string.error_btn_retry), new AlertDialog.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int id) {
					login();
				}
			}).create().show();
		} else {
			DialogHelper.getGeneralErrorDialog(throwable, this).show();
		}
	}

}
