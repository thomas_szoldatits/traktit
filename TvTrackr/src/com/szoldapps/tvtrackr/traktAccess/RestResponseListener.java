package com.szoldapps.tvtrackr.traktAccess;

import org.apache.http.Header;
import org.json.JSONObject;

/**
 * Used for communication between ServiceLayer and RestRequestLayer
 * @author thomasszoldatits
 *
 */
public interface RestResponseListener {
    public void onDownloadSuccess(int statusCode, Header[] headers, String jsonStr);
    public void onDownloadFailure(Throwable throwable, JSONObject jsonObject);
    public void onProgress(int bytesWritten, int totalSize);
}