package com.szoldapps.tvtrackr.traktAccess;

import org.apache.http.HttpEntity;

import android.content.Context;
import android.util.Log;

import com.szoldapps.tvtrackr.helper.Constants;
import com.szoldapps.tvtrackr.helper.LogCatHelper;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.SyncHttpClient;

public class RestClient {

    public static final String LOGTAG = LogCatHelper.getAppTag()+RestClient.class.getSimpleName().toString();
    
    private static AsyncHttpClient CLIENT = new AsyncHttpClient();
    private static SyncHttpClient SYNC_CLIENT = new SyncHttpClient();
    
    public static final String BASE_URL = "http://api.trakt.tv/";
    private static String APP_JSON = "application/json";

    public class RestURLs {
        public static final String ACCOUNT_CREATE = "account/create/"+Constants.DEVKEY;
        public static final String ACCOUNT_TEST = "account/test/"+Constants.DEVKEY;
        public static final String ACCOUNT_SETTINGS = "account/settings/"+Constants.DEVKEY;

        public static final String SEARCH_SHOW = "search/shows.json/"+Constants.DEVKEY+"/";
        public static final String SEARCH_PEOPLE = "search/people.json/"+Constants.DEVKEY+"/";

        public static final String COMMENT_SHOW = "comment/show/"+Constants.DEVKEY;
        public static final String COMMENT_EPISODE = "comment/episode/"+Constants.DEVKEY;

        public static final String GENRES_SHOWS = "genres/shows.json/"+Constants.DEVKEY;

        public static final String RECOMMENDATIONS_SHOW = "recommendations/shows/"+Constants.DEVKEY;
        public static final String RECOMMENDATIONS_SHOW_DISMISS = "recommendations/shows/dismiss/"+Constants.DEVKEY;

        public static final String SHOW_SUMMARY = "show/summary.json/"+Constants.DEVKEY+"/";
        public static final String SHOW_SUMMARIES = "show/summaries.json/"+Constants.DEVKEY+"/";
        public static final String SHOW_SEASONS = "show/seasons.json/"+Constants.DEVKEY+"/";
        public static final String SHOW_SEASON = "show/season.json/"+Constants.DEVKEY+"/";
        public static final String SHOW_SEASON_SEEN = "show/season/seen/"+Constants.DEVKEY;
        public static final String SHOW_COMMENTS = "show/comments.json/"+Constants.DEVKEY+"/";
        public static final String SHOW_SEEN = "show/seen/"+Constants.DEVKEY;
        public static final String SHOW_STATS = "show/stats.json/"+Constants.DEVKEY+"/";
        public static final String SHOW_WATCHLIST = "show/watchlist/"+Constants.DEVKEY;
        public static final String SHOW_UNWATCHLIST = "show/unwatchlist/"+Constants.DEVKEY;

        public static final String SHOW_EPISODE_SEEN = "show/episode/seen/"+Constants.DEVKEY;
        public static final String SHOW_EPISODE_UNSEEN = "show/episode/unseen/"+Constants.DEVKEY;
        public static final String SHOW_EPISODE_STATS = "show/episode/stats.json/"+Constants.DEVKEY+"/";
        public static final String SHOW_EPISODE_SUMMARY = "show/episode/summary.json/"+Constants.DEVKEY+"/";
        public static final String SHOW_EPISODE_COMMENTS = "show/episode/comments.json/"+Constants.DEVKEY+"/";

        public static final String SHOWS_TRENDING = "shows/trending.json/"+Constants.DEVKEY;
        
        public static final String RATE_EPISODE = "rate/episode/"+Constants.DEVKEY;
        public static final String RATE_SHOW = "rate/show/"+Constants.DEVKEY;

        public static final String USER_LIBRARY_SHOWS_WATCHED = "user/library/shows/watched.json/"+Constants.DEVKEY+"/";
        public static final String USER_WATCHLIST_SHOWS = "user/watchlist/shows.json/"+Constants.DEVKEY+"/";

    }

    public static void getAsync(String relativeUrl, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        CLIENT.get(getAbsoluteUrl(relativeUrl), params, responseHandler);
    }

    public static void postAsync(String relativeUrl, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        CLIENT.post(getAbsoluteUrl(relativeUrl), params, responseHandler);
    }

    public static void postAsyncWithJSON(Context context, String relativeUrl, HttpEntity entity, AsyncHttpResponseHandler responseHandler) {
        CLIENT.post(context, getAbsoluteUrl(relativeUrl), entity, APP_JSON, responseHandler);
    }

    public static String getAbsoluteUrl(String relativeUrl) {
        Log.d("RestClient", "REST-Call = " + relativeUrl);
        return BASE_URL + relativeUrl;
    }
    
    /** ----------------------------------- SYNC METHODS ------------------------------------ */
    
    public static void postSync(String relativeUrl, RequestParams params, AsyncHttpResponseHandler responseHandler) {
    	SYNC_CLIENT.post(getAbsoluteUrl(relativeUrl), params, responseHandler);
    }
    
    public static void postSyncWithJSON(Context context, String relativeUrl, HttpEntity entity, AsyncHttpResponseHandler responseHandler) {
    	SYNC_CLIENT.post(context, getAbsoluteUrl(relativeUrl), entity, APP_JSON, responseHandler);
    }

}
