package com.szoldapps.tvtrackr.traktAccess;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONObject;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.szoldapps.tvtrackr.helper.LogCatHelper;

public class DefaultRequestsHandler extends JsonHttpResponseHandler {

	public static final String LOGTAG = LogCatHelper.getAppTag()+DefaultRequestsHandler.class.getSimpleName().toString();
	RestResponseListener mListener;

	public DefaultRequestsHandler(RestResponseListener listener) {
		this.mListener = listener;
	}

	@Override
	public void onProgress(int bytesWritten, int totalSize) {
		mListener.onProgress(bytesWritten, totalSize);
	}

	@Override
	public void onSuccess(int statusCode, Header[] headers, String responseBody) {
		//    	Log.w(LOGTAG, responseBody);
		mListener.onDownloadSuccess(statusCode, headers, responseBody);
	}

	@Override
	public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
		//		Log.w(LOGTAG, response.toString());
		mListener.onDownloadSuccess(statusCode, headers, response.toString());
	}

	@Override
	public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
		//		Log.w(LOGTAG, response.toString());
		mListener.onDownloadSuccess(statusCode, headers, response.toString());
	}


	//    @Override
	//    public void onFailure(Throwable throwable, JSONObject jsonObject) {
	//    	Log.w(LOGTAG, "FUCKIN FAILURE");
	//    	mListener.onDownloadFailure(throwable, jsonObject);
	//    }

	@Override
	public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject jsonObject) {
		mListener.onDownloadFailure(throwable, jsonObject);
	}


}