package com.szoldapps.tvtrackr.helper;

public class ImageURLHelper {

	public static final String DEFAULT_TRAKT_IMAGE1 = "poster-dark.jpg";
	public static final String DEFAULT_TRAKT_IMAGE2 = "avatar-large.jpg";
    public static final String POSTER_SMALL = "-138";
    public static final String POSTER_LARGE = "-300";

    public static final String FANART_SMALL = "-218";
    public static final String FANART_LARGE = "-940";

    public static final String EPISODES_SMALL = "-218";

    public static String getImageURLByImageType(String url, String imageType) {
    	if(url == null) {
    		return null;
    	}
    	
        if(imageType.equals(EPISODES_SMALL)) {
            if(!url.contains("/episodes/")) {
                return url;
            }
        }
        if(url.contains(DEFAULT_TRAKT_IMAGE1)) {
            return url;
        } else {
            return new StringBuilder(url).insert(url.length()-".jpg".length(), imageType).toString();
        } 
    }

    public static boolean isTraktDefaultImgURL(String url) {
    	if(url.contains(DEFAULT_TRAKT_IMAGE1) || url.contains(DEFAULT_TRAKT_IMAGE2)) {
    		return true;
    	}
    	return false;
    }
}
