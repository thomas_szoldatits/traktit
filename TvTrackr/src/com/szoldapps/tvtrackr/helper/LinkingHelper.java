package com.szoldapps.tvtrackr.helper;

import android.app.Activity;
import android.content.Intent;

import com.szoldapps.tvtrackr.R;
import com.szoldapps.tvtrackr.activity.ActorActivity;
import com.szoldapps.tvtrackr.activity.RegisterActivity;
import com.szoldapps.tvtrackr.activity.main.MainActivity;
import com.szoldapps.tvtrackr.activity.settings.SettingsActivity;
import com.szoldapps.tvtrackr.activity.setup.SetupActivity;
import com.szoldapps.tvtrackr.activity.show.ShowActivity;
import com.szoldapps.tvtrackr.activity.show.episodes.EpisodesActivity;
import com.szoldapps.tvtrackr.db.model.Actor;
import com.szoldapps.tvtrackr.db.model.Show;
import com.szoldapps.tvtrackr.dto.show.MinShowDTO;

public class LinkingHelper {

	public static void goToShow(MinShowDTO minShow, Activity activity, boolean clearHistory) {
		goToShow(minShow.getTvdb_id(), minShow.getTitle(), activity, clearHistory);
	}
	
	public static void goToShow(Show show, Activity activity, boolean clearHistory) {
		goToShow(show.getTvdbID(), show.getTitle(), activity, clearHistory);
	}

	public static void goToShow(int tvdbId, String title, Activity activity, boolean clearHistory) {
		Intent intent = new Intent(activity, ShowActivity.class);
		if(clearHistory) {
			intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
		}
		intent.putExtra(Constants.EXTRA_SHOW_TVDB_ID, tvdbId);
		intent.putExtra(Constants.EXTRA_SHOW_TITLE, title);

		activity.startActivity(intent);
		animation(activity, ANIM_ENTER_RIGHT_EXIT_LEFT);
	}
	
	public static void goToShowFromEpisodes(int tvdbId, String title, Activity activity) {
		Intent intent = new Intent(activity, ShowActivity.class);
		intent.putExtra(Constants.EXTRA_SHOW_TVDB_ID, tvdbId);
		intent.putExtra(Constants.EXTRA_SHOW_TITLE, title);

		activity.startActivity(intent);
		animation(activity, ANIM_ENTER_LEFT_EXIT_RIGHT);
	}
	
	public static void goToMain(Activity activity) {
		Intent intent = new Intent(activity, MainActivity.class);

		activity.startActivity(intent);
		animation(activity, ANIM_ENTER_LEFT_EXIT_RIGHT);
	}
	
	public static void goToMainFromSetup(Activity activity) {
		Class<?> cls = MainActivity.class;
		Intent intent = new Intent(activity, cls );
		intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

		intent.putExtra(Constants.EXTRA_PEEK_NAV_DRAWER, true);
		
		activity.startActivity(intent);
		animation(activity, ANIM_ENTER_RIGHT_EXIT_LEFT);
	}
	
	public final static int ANIM_ENTER_RIGHT_EXIT_LEFT = 0;
	public final static int ANIM_ENTER_LEFT_EXIT_RIGHT = 1;
	public final static int ANIM_ENTER_BOTTOM_EXIT_TOP = 2;
	
	private static void animation(Activity activity, int animation) {
		switch (animation) {
		case ANIM_ENTER_RIGHT_EXIT_LEFT:
			activity.overridePendingTransition(R.anim.enter_right_slide, R.anim.exit_left_slide);
			break;
		case ANIM_ENTER_LEFT_EXIT_RIGHT:
			activity.overridePendingTransition(R.anim.enter_left_slide, R.anim.exit_right_slide);
			break;
		case ANIM_ENTER_BOTTOM_EXIT_TOP:
			activity.overridePendingTransition(R.anim.slide_bottom_up, R.anim.slide_top_up);
			break;
		default:
			break;
		}
	}

	public static void goToEpisode(int episodeId, int showTvdbId, String showTitle, Activity activity) {
		Intent intent = new Intent(activity, EpisodesActivity.class);

		intent.putExtra(Constants.EXTRA_EPISODE_ID, episodeId);
		intent.putExtra(Constants.EXTRA_SHOW_TVDB_ID, showTvdbId);
		intent.putExtra(Constants.EXTRA_SHOW_TITLE, showTitle);

		activity.startActivity(intent);
		animation(activity, ANIM_ENTER_RIGHT_EXIT_LEFT);
	}
	
	public static void goToRegister(Activity activity) {
		Intent intent = new Intent(activity, RegisterActivity.class);
		activity.startActivity(intent);
		animation(activity, ANIM_ENTER_BOTTOM_EXIT_TOP);
	}

	public static void goBack(Activity activity) {
		activity.onBackPressed();
		animation(activity, ANIM_ENTER_LEFT_EXIT_RIGHT);
	}

	public static void goToActor(Actor actor, Show show, Activity activity) {
		Intent intent = new Intent(activity, ActorActivity.class);
		intent.putExtra(Constants.EXTRA_ACTOR_SLUG, actor.getSlug());
		intent.putExtra(Constants.EXTRA_SHOW_TITLE, show.getTitle());

		activity.startActivity(intent);
		animation(activity, ANIM_ENTER_RIGHT_EXIT_LEFT);
	}
	
	/**
	 * Opens {@link SettingsActivity} with {@link #ANIM_ENTER_RIGHT_EXIT_LEFT}.
	 * 
	 * @param activity
	 */
	public static void goToSettings(Activity activity) {
		activity.startActivity(new Intent(activity, SettingsActivity.class));
		animation(activity, ANIM_ENTER_RIGHT_EXIT_LEFT);
	}
	
	/**
	 * Opens {@link SetupActivity} with {@link #ANIM_ENTER_LEFT_EXIT_RIGHT}.
	 * 
	 * @param activity
	 */
	public static void goToSetup(Activity activity) {
		activity.startActivity(new Intent(activity, SetupActivity.class));
		animation(activity, ANIM_ENTER_LEFT_EXIT_RIGHT);
	}
}
