package com.szoldapps.tvtrackr.helper;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;

import com.nostra13.universalimageloader.cache.disc.impl.UnlimitedDiscCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;
import com.szoldapps.tvtrackr.R;

public class ImageLoaderHelper {

	private static boolean alreadyRun = false;
	private static DisplayImageOptions sDIOAvatar = null;
	private static DisplayImageOptions sDIOBanner = null;
	private static DisplayImageOptions sDIOFanart = null;
	private static DisplayImageOptions sDIOPosterOrHeadshot = null;
	private static DisplayImageOptions sDIOScreen = null;


	public static void runImageConfig(Context context) {
		if(!alreadyRun) {
			ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(context)
			.discCache(new UnlimitedDiscCache(context.getFilesDir()))
			.build();
			ImageLoader.getInstance().init(config);
			alreadyRun = true;
		}
	}

	public static DisplayImageOptions getDIOAvatar(Context context) {
		ImageLoaderHelper.runImageConfig(context);
		if(sDIOAvatar == null) {
			sDIOAvatar = new DisplayImageOptions.Builder()
            .showImageForEmptyUri(R.drawable.default_avatar).showImageOnFail(R.drawable.default_avatar)//.showImageOnLoading(R.drawable.default_avatar)
            .cacheOnDisc(true)
            .imageScaleType(ImageScaleType.EXACTLY)
            .displayer(new RoundedBitmapDisplayer(context.getResources().getDimensionPixelSize(R.dimen.card_corner_radius)))
            .build();
		}
		return sDIOAvatar;
	}
	
	public static DisplayImageOptions getDIOBanner(Context context) {
		ImageLoaderHelper.runImageConfig(context);
		if(sDIOBanner == null) {
			sDIOBanner = new DisplayImageOptions.Builder()
			.showImageForEmptyUri(R.drawable.default_banner)
			.showImageOnFail(R.drawable.default_banner)
			//.showImageOnLoading(R.drawable.default_banner)
			.cacheOnDisc(true)
			.build();
		}
		return sDIOBanner;
	}

	public static DisplayImageOptions getDIOFanart(Context context) {
		ImageLoaderHelper.runImageConfig(context);
		if(sDIOFanart == null) {
			sDIOFanart = new DisplayImageOptions.Builder()
			.showImageForEmptyUri(R.drawable.default_fanart)
			.showImageOnFail(R.drawable.default_fanart)
			//.showImageOnLoading(R.drawable.default_fanart)
			.cacheOnDisc(true)
			.build();
		}
		return sDIOFanart;
	}

	public static DisplayImageOptions getDIOPosterOrHeadShot(Context context) {
		ImageLoaderHelper.runImageConfig(context);
		if(sDIOPosterOrHeadshot == null) {
			sDIOPosterOrHeadshot = new DisplayImageOptions.Builder()
			.showImageForEmptyUri(R.drawable.default_poster_or_headshot)
			.showImageOnFail(R.drawable.default_poster_or_headshot)
			//.showImageOnLoading(R.drawable.default_poster)
			.cacheOnDisc(true)
			.build();
		}
		return sDIOPosterOrHeadshot;
	}

	
	
	public static DisplayImageOptions getDIOScreen(Context context) {
		ImageLoaderHelper.runImageConfig(context);
		if(sDIOScreen == null) {
			sDIOScreen = new DisplayImageOptions.Builder()
			.showImageForEmptyUri(R.drawable.default_screen)
			.showImageOnFail(R.drawable.default_screen)
			//.showImageOnLoading(R.drawable.default_screen)
			.cacheOnDisc(true)
			.build();
		}
		return sDIOScreen;
	}

	public static Bitmap getRoundedCornerBitmap(Bitmap bitmap, int pixels) {
		Bitmap output = Bitmap.createBitmap(bitmap.getWidth(), bitmap
				.getHeight(), Config.ARGB_8888);
		Canvas canvas = new Canvas(output);

		final int color = 0xff424242;
		final Paint paint = new Paint();
		final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
		final RectF rectF = new RectF(rect);
		final float roundPx = pixels;

		paint.setAntiAlias(true);
		canvas.drawARGB(0, 0, 0, 0);
		paint.setColor(color);
		canvas.drawRoundRect(rectF, roundPx, roundPx, paint);

		paint.setXfermode(new PorterDuffXfermode(Mode.SRC_IN));
		canvas.drawBitmap(bitmap, rect, rect, paint);

		return output;
	}
	
	public static void clearDiskCache(Context context) {
		ImageLoaderHelper.runImageConfig(context);
		ImageLoader.getInstance().clearDiscCache();
	}

}
