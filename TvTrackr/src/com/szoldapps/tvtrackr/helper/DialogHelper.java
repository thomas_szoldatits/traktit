package com.szoldapps.tvtrackr.helper;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.sql.SQLException;

import org.apache.http.client.HttpResponseException;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.widget.Toast;

import com.szoldapps.tvtrackr.R;
import com.szoldapps.tvtrackr.activity.LoginActivity;
import com.szoldapps.tvtrackr.exception.WrongObjectReturnedException;
import com.szoldapps.tvtrackr.helper.Constants.DownloadErrorType;

public class DialogHelper {

	/**
	 * Prepares AlertDialog that displays general errors like "No Internet", "DB" or "Hashing" Errors.
	 * @param throwable
	 * @param activity
	 * @return prepared AlertDialog (do not forget to call .show())
	 */
	public static AlertDialog getGeneralErrorDialog(Throwable throwable, Activity activity) {
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(activity);
		// set title
		alertDialogBuilder.setTitle(activity.getString(R.string.error_title));
		// set button
		alertDialogBuilder
		.setNeutralButton(activity.getString(android.R.string.ok), new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog,int id) {
				dialog.cancel();
			}
		});

		// set message
		if(throwable instanceof IOException) {
			alertDialogBuilder.setTitle(activity.getString(R.string.error_no_internet_connection_title));
			alertDialogBuilder.setMessage(activity.getString(R.string.error_no_internet_connection));
		} else if(throwable instanceof SQLException) {
			alertDialogBuilder.setTitle(activity.getString(R.string.error_sql_title));
			alertDialogBuilder.setMessage(activity.getString(R.string.error_sql));
		} else if(throwable instanceof UnsupportedEncodingException) {
			alertDialogBuilder.setTitle(activity.getString(R.string.error_hashing_title));
			alertDialogBuilder.setMessage(activity.getString(R.string.error_hashing));
		} else {
			alertDialogBuilder.setTitle(activity.getString(R.string.error_unknown_title));
			alertDialogBuilder.setMessage(activity.getString(R.string.error_unknown));
			throwable.printStackTrace();
		}

		// create alert dialog
		AlertDialog alertDialog = alertDialogBuilder.create();
		return alertDialog;
	}

	/**
	 * Converts <code>throwable</code> to {@link DownloadErrorType}s and executes 
	 * {@link DialogHelper#handleDialogErrors(DownloadErrorType, Activity)} <br><br>
	 * Please refer to {@link DialogHelper#handleDialogErrors(DownloadErrorType, Activity)} for more info
	 * 
	 * @param throwable
	 * @param activity
	 */
	public static void handleDialogErrors(Throwable throwable, final Activity activity) {
		if(throwable instanceof WrongObjectReturnedException) { 
			handleDialogErrors(DownloadErrorType.WRONG_OBJECT_RETURNED_ERROR, activity);
		} else if(throwable instanceof SQLException) {
			handleDialogErrors(DownloadErrorType.SQL_ERROR, activity);
		} else if(throwable instanceof HttpResponseException) {
			handleDialogErrors(DownloadErrorType.WRONG_CREDENTIALS_ERROR, activity);
		} else {
			handleDialogErrors(DownloadErrorType.UNKNOWN_ERROR, activity);
		}
	}

	/**
	 * Handles the following {@link DownloadErrorType}s / {@link Throwable}s:
	 * <ul>
	 * 	<li>{@link DownloadErrorType#SQL_ERROR} / {@link SQLException}</li>
	 * 	<li>{@link DownloadErrorType#WRONG_CREDENTIALS_ERROR} / {@link HttpResponseException}</li>
	 * 	<li>Default: {@link DownloadErrorType#UNKNOWN_ERROR} / {@link UnknownError}</li>
	 * </ul>
	 * 
	 * @param errorType
	 * @param activity
	 */
	public static void handleDialogErrors(DownloadErrorType errorType, final Activity activity) {
		switch (errorType) {
		case WRONG_OBJECT_RETURNED_ERROR:
			break;	// DO NOTHING, that's just for Development
		case SQL_ERROR:
			showSQLErrorDialog(activity);
			break;
		case WRONG_CREDENTIALS_ERROR:
			showWrongCredentialsErrorDialog(activity);
			break;
		default: // UNKNOWN_ERROR
			showUnknownErrorDialog(activity);
			break;
		}
	}

	private static void showSQLErrorDialog(final Activity activity) {
		new AlertDialog.Builder(activity)
		.setTitle(activity.getString(R.string.error_sql_title))
		.setMessage(activity.getString(R.string.error_sql))
		.setNegativeButton(activity.getString(R.string.error_btn_close_app), new OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int id) {
				DialogHelper.closeApp(activity);
			}
		}).setPositiveButton(activity.getString(R.string.error_btn_restart), new OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int id) {
				DialogHelper.restartApp(activity);
			}
		}).create().show();
	}

	private static void showWrongCredentialsErrorDialog(final Activity activity) {
		new AlertDialog.Builder(activity)
		.setTitle(activity.getString(R.string.error_wrong_credentials_title))
		.setMessage(activity.getString(R.string.error_wrong_credentials))
		.setPositiveButton(activity.getString(R.string.error_btn_relogin), new OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int id) {
				dialog.dismiss();
				logout(activity);
			}
		}).create().show();
	}

	private static void showUnknownErrorDialog(final Activity activity) {
		new AlertDialog.Builder(activity)
		.setTitle(activity.getString(R.string.error_unknown_title))
		.setMessage(activity.getString(R.string.error_unknown))
		.setNegativeButton(activity.getString(R.string.error_btn_close_app), new OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int id) {
				DialogHelper.closeApp(activity);
			}
		}).setPositiveButton(activity.getString(R.string.error_btn_restart), new OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int id) {
				DialogHelper.restartApp(activity);
			}
		}).create().show();
	}

	public static OnClickListener getDefaultCancelDialogListener() {
		return new OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int id) {
				dialog.cancel();
			}
		};
	}


	// ------------------ USEFUL METHODS -----------------------

	public static void restartApp(Context context) {
		Intent i = context.getPackageManager().getLaunchIntentForPackage(context.getPackageName());
		context.startActivity(i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
	}

	public static void closeApp(Activity activity) {
		activity.finish();
	}

	public static void logout(final Activity activity) {
		Intent intent = new Intent(activity, LoginActivity.class);
		intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
		intent.putExtra(Constants.EXTRA_LOGOUT_BOOL, true);
		activity.startActivity(intent);
	}

	public static boolean isInternetError(Throwable throwable) {
		if(throwable instanceof IOException && !(throwable instanceof HttpResponseException)) {
			return true;
		}
		return false;
	}

	/**
	 * Simply displays a Toast with msg as content (duration=SHORT).
	 * 
	 * @param msg
	 * @param activity
	 */
	public static void showInfoShort(String msg, Context context) {
		Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
	}

	/**
	 * Simply displays a Toast with msg as content (duration=LONG).
	 * 
	 * @param msg
	 * @param activity
	 */
	public static void showInfoLong(String msg, Context context) {
		Toast.makeText(context, msg, Toast.LENGTH_LONG).show();
	}

	/**
	 * Returns a Builder with set Title, Message & NegativeButton (Cancel).
	 * 
	 * @param activity
	 * @return
	 */
	public static AlertDialog.Builder getNoInternetErrorDialogBuilder(final Activity activity) {
		return new AlertDialog.Builder(activity)
		.setTitle(activity.getString(R.string.error_no_internet_connection_title))
		.setMessage(activity.getString(R.string.error_no_internet_connection))
		.setNegativeButton(activity.getString(R.string.cancel), getDefaultCancelDialogListener());
	}

	/**
	 * Returns a Builder with set Title, Message & NegativeButton (Cancel).
	 * 
	 * @param showTitle
	 * @param activity
	 * @return
	 */
	public static AlertDialog.Builder getUnfollowErrorDialogBuilder(String showTitle, final Activity activity) {
		return new AlertDialog.Builder(activity)
		.setTitle(activity.getString(R.string.show_unfollow_dialog_title))
		.setMessage(String.format(activity.getString(R.string.show_unfollow_dialog_msg), showTitle))
		.setNegativeButton(activity.getString(R.string.cancel), getDefaultCancelDialogListener());
	}

	/**
	 * Returns a Builder with set Title, Message & NegativeButton (Cancel).
	 * 
	 * @param activity
	 * @return
	 */
	public static AlertDialog.Builder getFollowErrorDialogBuilder(final Activity activity) {
		return new AlertDialog.Builder(activity)
		.setTitle(activity.getString(R.string.show_follow_dialog_title))
		.setMessage(R.string.show_follow_dialog)
		.setNegativeButton(activity.getString(R.string.cancel), getDefaultCancelDialogListener());
	}

}
