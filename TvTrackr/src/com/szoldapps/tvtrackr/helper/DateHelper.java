package com.szoldapps.tvtrackr.helper;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import android.annotation.SuppressLint;
import android.content.Context;
import android.text.format.DateFormat;
import android.text.format.DateUtils;

import com.szoldapps.tvtrackr.R;

public class DateHelper {

	public static enum DateHelperFormat {

		/** "hh:mm" ... e.g. "09:00" **/
		TIME,
		/** "EEEE" ... e.g. "Monday" **/
		WEEKDAY,
		/** "EEE" ... e.g. "Monday" **/
		WEEKDAY_SHORT,
		/** "YYYY/MM/DD hh:mm" ... e.g. "2012/07/16 03:00" **/
		DATE_AND_TIME,
		/** "YYYY/MM/DD hh:mm:ss.mmm" ... e.g. "2012/07/16 03:00:00.000" **/
		DATE_AND_TIME2,
		/** "EEE. MMM DD, YYYY <at> hh:mm" ... e.g. "Mon. Jul 16, 2012 at 03:00" **/
		MEDIUM_DATE_AND_TIME_WITH_WEEKDAY,
		/** "MMM DD, YYYY <at> hh:mm" ... e.g. "Jul 16, 2012 at 03:00" **/
		MEDIUM_DATE_AND_TIME,
		/** "MMM DD, YYYY" ... e.g. "Jul 16, 2012" **/
		MEDIUM_DATE_FORMAT,
		/** "YYYY-MM-DDTTT:" ... e.g. "2012-09-30T21:00:00-05:00" **/
		DATE_FOR_TRAKT,
		/** "MMM DD, YYYY" ... e.g. "Jul 16, 2012" **/
		DATE_ONLY,
		/** "MM/DD" ... e.g. "01/15" **/
		DAY_MONTH_ONLY

	}

	@SuppressLint("SimpleDateFormat")
	public static String getDateStrFromStr(String dateStr, String format, Context context) {
		String ret = "";
		if(dateStr == null) {
			return ret;
		}
		try {  
			SimpleDateFormat sdf = new SimpleDateFormat(format);  
			Date date = sdf.parse(dateStr);
			ret = getDateStr(date, DateHelperFormat.DATE_ONLY, context);
		} catch (ParseException e) {  
			return ret;
		}
		return ret;
	}

	public static String getDateStr(Date date, DateHelperFormat dhf, Context context) {
		String ret = "";

//		java.text.DateFormat lf = DateFormat.getLongDateFormat(context);
		java.text.DateFormat mf = DateFormat.getMediumDateFormat(context);
		java.text.DateFormat df = DateFormat.getDateFormat(context);
		java.text.DateFormat tf = DateFormat.getTimeFormat(context);

		switch (dhf) {
		case TIME:
			ret = tf.format(date).toString();
			break;
		case WEEKDAY:
			ret = android.text.format.DateFormat.format("EEEE", date).toString();
			break;
		case WEEKDAY_SHORT:
			ret = android.text.format.DateFormat.format("EEE", date).toString();
			break;
		case DATE_AND_TIME:
			ret = df.format(date) + " " + tf.format(date);
			break;
		case DATE_AND_TIME2:
			ret = df.format(date) + " " + tf.format(date)+":"+android.text.format.DateFormat.format("ss", date);
			break;
		case MEDIUM_DATE_AND_TIME_WITH_WEEKDAY:
			ret = android.text.format.DateFormat.format("EEE", date) + ". " + mf.format(date) + " "+context.getString(R.string.general_at)+" " + tf.format(date);
			break;
		case MEDIUM_DATE_AND_TIME:
			ret = mf.format(date) + " "+context.getString(R.string.general_at)+" " + tf.format(date);
			break;
		case MEDIUM_DATE_FORMAT:
			ret = mf.format(date);
			break;
		case DATE_FOR_TRAKT:
			ret = android.text.format.DateFormat.format("yyyy-MM-dd'T'HH:mm:ssz", date) + "";
			break;
		case DATE_ONLY:
			ret = mf.format(date).toString();
			break;
		case DAY_MONTH_ONLY:
			ret = android.text.format.DateFormat.format("dd.MM.", date).toString();
			break;
		default:
			break;
		}

		return ret;
	}
	
	public static String getDateStrWithTimeInS(long timeInS, DateHelperFormat dhf, Context context) {

		if(timeInS > 0) {
			Date date = new Date(getTimeInMs(timeInS));
			return getDateStr(date, dhf, context);
		}

		return "-";
	}

	public static String getDateStr(long timeInMs, DateHelperFormat dhf, Context context) {

		if(timeInMs > 0) {
			Date date = new Date(timeInMs);
			return getDateStr(date, dhf, context);
		}

		return "";
	}

	public static String getTimeAgoInS(long timeInS, Context context) {
		return getTimeAgo(getTimeInMs(timeInS), context);
	}

	public static String getTimeAgo(long ms, Context context) {
		if(ms > 0) {
			long minResolution = DateUtils.SECOND_IN_MILLIS;
			long transitionResolution = DateUtils.SECOND_IN_MILLIS;
			return DateUtils.getRelativeDateTimeString(context, ms, minResolution, transitionResolution, 0).toString();
		}
		return "";
	}
	
	/**
	 * As trakt returns a unix timestamp with only 10 digits (seconds) and 13 (ms) are needed, this method converts timeInS to timeInMs
	 * @param timeInS
	 * @return
	 */
	public static long getTimeInMs(long timeInS) {
		return timeInS * 1000;
	}

	public static long getCurrentTime() {
		return new Date().getTime();
	}
	
	public static long getCurrentTimeInS() {
		return getCurrentTime()/1000;
	}


	public static String getCalendarTabsDate(int offset, Context context) {
		String ret = "";
		switch (offset) {
		case -1:
			ret = context.getString(R.string.calendar_yesterday);
			break;
		case 0:
			ret = context.getString(R.string.calendar_today);
			break;
		case 1:
			ret = context.getString(R.string.calendar_tomorrow);
			break;
		default:
			Calendar now = Calendar.getInstance();
			now.setTime(new Date());
			now.add(Calendar.DAY_OF_MONTH, offset);
			
			ret = getDateStr(now.getTimeInMillis(), DateHelperFormat.WEEKDAY_SHORT, context) + " " + getDateStr(now.getTimeInMillis(), DateHelperFormat.DAY_MONTH_ONLY, context);
			break;
		}

		return ret;
	}
	
	public static String getCalendarNoEpisodeDateStr(int offset, Context context) {
		String ret = "";
		Calendar now = Calendar.getInstance();
		now.setTime(new Date());
		now.add(Calendar.DAY_OF_MONTH, offset);
		ret = getDateStr(now.getTimeInMillis(), DateHelperFormat.DATE_ONLY, context);
		switch (offset) {
		case -1:
			ret += " ("+context.getString(R.string.calendar_yesterday)+")";
			break;
		case 0:
			ret += " ("+context.getString(R.string.calendar_today)+")";
			break;
		case 1:
			ret += " ("+context.getString(R.string.calendar_tomorrow)+")";
			break;
		}
		return ret;
	}

	/**
	 * Compares <code>firstAiredUTC</code> to {@link DateHelper#getCurrentTimeInS()}.
	 * 
	 * @param firstAiredUTC
	 * @return	TRUE, if firstAiredUTC < getCurrentTimeInS(), otherwise FALSE
	 */
	public static boolean hasBeenAired(long firstAiredUTC) {
		if(firstAiredUTC < getCurrentTimeInS()) {
			return true;
		}
		return false;
	}

}
