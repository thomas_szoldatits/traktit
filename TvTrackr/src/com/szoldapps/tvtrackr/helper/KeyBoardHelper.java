package com.szoldapps.tvtrackr.helper;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;

public class KeyBoardHelper {

	public static final String LOGTAG = LogCatHelper.getAppTag()+KeyBoardHelper.class.getSimpleName().toString();

	public static void hideKeyboard(View v, Context context) {
		InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
		if(imm != null) {	// only will trigger it if no physical keyboard is open
			imm.hideSoftInputFromWindow(v.getWindowToken(), 0); 
		}
	}

	public static void showKeyboard(View v, Context context) {
		InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
		if (imm != null) {	// only will trigger it if no physical keyboard is open
			imm.showSoftInput(v, 0);
		}
	}

	public static void showKeyboard(Activity activity) {
		Log.i(LOGTAG, "showKeyboard");
		activity.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
	}

	public static void hideKeyboard(Activity activity) {
		Log.i(LOGTAG, "hideKeyboard");
		activity.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
	}
}
