package com.szoldapps.tvtrackr.helper;

import com.szoldapps.tvtrackr.R;

import android.view.MenuItem;

public class MenuHelper {

	public static void updateStarted(MenuItem refreshMenuItem) {
		if(refreshMenuItem != null) {
			refreshMenuItem.setActionView(R.layout.menu_progress_wheel);
		}
	}
	
	public static void updateEnded(MenuItem refreshMenuItem) {
		if(refreshMenuItem != null) {
			refreshMenuItem.setActionView(null);
		}
	}
}
