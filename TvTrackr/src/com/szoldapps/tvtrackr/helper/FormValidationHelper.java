package com.szoldapps.tvtrackr.helper;

import java.util.regex.Pattern;

import android.app.Activity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.View.OnFocusChangeListener;
import android.widget.EditText;

import com.szoldapps.tvtrackr.R;

public class FormValidationHelper {

	// TextWatcher
	public static TextWatcher getUsernameTextWatcher(EditText etxt, Activity activity) {
		return new TextWatcher() {
			@Override
			public void afterTextChanged(Editable s) {
				String text = s.toString();
				int length = text.length();
				if(!Pattern.matches("^[a-zA-Z0-9-_.]*$", text)) {
					if(length > 0) {     
						s.delete(length - 1, length);
					}
				}
			}
			@Override public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) { }
			@Override public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) { }
		};
	}

	// OnFocusChangeListener
	public static OnFocusChangeListener getUsernameFocusChangeListener(final EditText etxt, final Activity activity) {
		return new OnFocusChangeListener() {
			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				if(!hasFocus) { FormValidationHelper.validateUsername(etxt, activity); }
			}
		};
	}

	public static OnFocusChangeListener getEmailFocusChangeListener(final EditText etxt, final Activity activity) {
		return new OnFocusChangeListener() {
			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				if(!hasFocus) { FormValidationHelper.validateEmail(etxt, activity); }
			}
		};
	}

	public static OnFocusChangeListener getPasswordFocusChangeListener(final EditText etxt, final Activity activity) {
		return new OnFocusChangeListener() {
			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				if(!hasFocus) { FormValidationHelper.validatePassword(etxt, activity); }
			}
		};
	}
	public static OnFocusChangeListener getPasswordRetypeFocusChangeListener(final EditText etxtPwRetype, final EditText etxtPw, final Activity activity) {
		return new OnFocusChangeListener() {
			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				if(!hasFocus) { FormValidationHelper.validatePasswordRetype(etxtPwRetype, etxtPw, activity); }
			}
		};
	}

	// Actual validation
	public static void validateUsername(EditText etxt, Activity activity) {
		String errorStr = null;
		if (etxt.getText().toString().isEmpty()) {
			errorStr  = String.format(activity.getString(R.string.error_empty_field), activity.getString(R.string.login_label_username));
		} else if (etxt.getText().length() < 3 || etxt.getText().length() > 20) {
			errorStr = activity.getString(R.string.error_username_length);
		} 
		if(errorStr != null) { etxt.setError(errorStr); }
	}

	public static void validateEmail(EditText etxt, Activity activity) {
		String errorStr = null;
		if(etxt.getText().toString().isEmpty()) {
			errorStr = String.format(activity.getString(R.string.error_empty_field), activity.getString(R.string.register_label_email));
		} else if(!android.util.Patterns.EMAIL_ADDRESS.matcher(etxt.getText()).matches()) {
			errorStr = activity.getString(R.string.error_email_invalid);
		} 
		if(errorStr != null) { etxt.setError(errorStr); }
	}

	public static void validatePassword(EditText etxt, Activity activity) {
		String errorStr = null;
		if(etxt.getText().toString().isEmpty()) {
			errorStr = String.format(activity.getString(R.string.error_empty_field), activity.getString(R.string.login_label_password));
		}
		if(errorStr != null) { etxt.setError(errorStr); }
	}

	public static void validatePasswordRetype(EditText etxtPwRetype, EditText etxtPw, Activity activity) {
		String errorStr = null;
		if(!etxtPw.getText().toString().equals(etxtPwRetype.getText().toString())) {
			errorStr = activity.getString(R.string.error_password_no_match);
		}
		if(errorStr != null) { etxtPwRetype.setError(errorStr); }
	}


}
