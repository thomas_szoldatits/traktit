package com.szoldapps.tvtrackr.helper;

import com.szoldapps.tvtrackr.service.intent.DownloadService;

public class Constants {
	public static final String DEVKEY = "1ad45a94fdb20d8bc7170f0d5209cb10d0b824dd";

	// EXTRAs
	private static final String INTENT = ".intent.";

	public static final String EXTRA_LOGOUT_BOOL = Constants.class.getPackage() + INTENT + "logout";
	public static final String EXTRA_SHOWACTIVITY_BOOL = Constants.class.getPackage() + INTENT + "showfragment";
	
	public static final String EXTRA_SHOW_TVDB_ID = Constants.class.getPackage() + INTENT + "tvdb_id";
	public static final String EXTRA_SHOW_TITLE = Constants.class.getPackage() + INTENT + "show_name";

	public static final String EXTRA_EPISODE_ID = Constants.class.getPackage() + INTENT + "episode_nr";
	public static final String EXTRA_EPISODE_MAP_POSITION = Constants.class.getPackage() + INTENT + "episode_map_position";

	public static final String EXTRA_ACTOR_SLUG = Constants.class.getPackage() + INTENT + "actor_id";

	public static final String EXTRA_RESULT = Constants.class.getPackage() + INTENT + "result";
	public static final String EXTRA_IS_SHOWCACHE = Constants.class.getPackage() + INTENT + "is_showcache";

	public static final String EXTRA_ERROR_TYPE = Constants.class.getPackage() + INTENT + "error_type";
	public static final String EXTRA_REQUEST_TYPE = Constants.class.getPackage() + INTENT + "request_type";
	public static final String EXTRA_RESPONSE_TYPE = Constants.class.getPackage() + INTENT + "response_type";
	public static final String EXTRA_SHOW_DOWNLOAD_STATUS = Constants.class.getPackage() + INTENT + "status";
	public static final String EXTRA_UP2DATE_STATUS = Constants.class.getPackage() + INTENT + "up2date_status";
	/**
	 * Used to tell {@link DownloadService}.loadShow to ignore checks, whether to update and simply re-download
	 */
	public static final String EXTRA_FORCE_DOWNLOAD = Constants.class.getPackage() + INTENT + "force_download";
	/**
	 * Used to tell {@link DownloadService}.loadShow to ignore checks, whether to update and simply re-download
	 */
	public static final String EXTRA_PEEK_NAV_DRAWER = Constants.class.getPackage() + INTENT + "peek_nav_drawer";
	
	// DownloadService
	public static final String EXTRA_DOWNLOAD_SERVICE_NOTIFICATION = Constants.class.getPackage() + INTENT + "download_service_notification";

	public enum DownloadErrorType {
		UNKNOWN_ERROR, NO_VALID_REQUEST_TYPE_ERROR, SQL_ERROR, NO_INTERNET_ERROR, WRONG_CREDENTIALS_ERROR, WRONG_OBJECT_RETURNED_ERROR;
	}
	
	public enum DownloadRequestType {
		SHOW_DOWNLOAD, COMPLETE_UPDATE_INCL_SHOWLIST_REFRESH, COMPLETE_UPDATE_NO_SHOWLIST_REFRESH, LOAD_SHOW;
	}
	
	public enum DownloadResponseType {
		UPDATE_ALL_SHOWS, ONE_SHOW_UPDATE, COMPLETE_UPDATE_INTERIM_RESULT, COMPLETE_UPDATE_FINAL_RESULT;
	}
	
	public static final String EXTRA_CALENDAR_OFFSET = Constants.class.getPackage() + INTENT + "calendar_offset";

	// Search Delay + used for SearchHandler to get back to UI Thread
	public static final int SEARCH_DELAY = 500;

	
}