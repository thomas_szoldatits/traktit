package com.szoldapps.tvtrackr.dto.request;


public class RequestShowDTO {

	private Integer tvdb_id;

	public RequestShowDTO(int tvdbId) {
		super();
		this.tvdb_id = tvdbId;
	}

	public Integer getTvdb_id() {
		return tvdb_id;
	}

	public void setTvdb_id(Integer tvdb_id) {
		this.tvdb_id = tvdb_id;
	}

}
