package com.szoldapps.tvtrackr.dto.request;

import com.szoldapps.tvtrackr.db.model.User;

public class RequestAccountCreateDTO {

	String username;
	String password;
	String email;

	public RequestAccountCreateDTO(User u) {
		super();
		this.username = u.getUsername();
		this.password = u.getPassword();
		this.email = u.getEmail();
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	//    {
	//    "username": "justin",
	//    "password": "sha1hash",
	//    "email": "username@gmail.com"
	//	  }
}
