package com.szoldapps.tvtrackr.dto.request;

import com.szoldapps.tvtrackr.db.model.Episode;
import com.szoldapps.tvtrackr.db.model.Show;
import com.szoldapps.tvtrackr.db.model.User;

public class RequestRateShowEpisodeDTO {

    String username;
    String password;
    String imdb_id;
    Integer tvdb_id;
    String title;
    Integer year;
    Integer season;
    Integer episode;
    Integer rating;
    
    public RequestRateShowEpisodeDTO(User u, Show show, Integer rating) {
        super();
        this.username = u.getUsername();
        this.password = u.getPassword();
        this.imdb_id = show.getImdbID();
        this.tvdb_id = show.getTvdbID();
        this.title = show.getTitle();
        this.year = show.getYear();
        this.rating = rating;
    }
    
    public RequestRateShowEpisodeDTO(User u, Episode e, Integer rating) {
        this(u, e.getSeason().getShow(), rating);
        this.season = e.getSeason().getSeasonNr();
        this.episode = e.getNumber();
    }
    
    


    // That's how it should look like, according to trakt
    //    {
    //        "username": "username",
    //        "password": "sha1hash",
    //        "tvdb_id": 213221,
    //        "title": "Portlandia",
    //        "year": 2011,
    //        "season": 1,
    //        "episode": 1,
    //        "rating": "love"
    //    }

}
