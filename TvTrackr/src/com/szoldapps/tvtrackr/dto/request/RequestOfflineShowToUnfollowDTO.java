package com.szoldapps.tvtrackr.dto.request;

import java.util.ArrayList;
import java.util.List;

import com.szoldapps.tvtrackr.db.model.OfflineEpisodeToUnfollow;
import com.szoldapps.tvtrackr.db.model.OfflineShowToUnfollow;
import com.szoldapps.tvtrackr.db.model.User;

public class RequestOfflineShowToUnfollowDTO {
    String username;
    String password;
    Integer tvdb_id;
    List<RequestOfflineEpisodeToUnfollowDTO> episodes = new ArrayList<RequestOfflineShowToUnfollowDTO.RequestOfflineEpisodeToUnfollowDTO>();

    public RequestOfflineShowToUnfollowDTO(User user, OfflineShowToUnfollow oShow) {
        super();
        this.username = user.getUsername();
        this.password = user.getPassword();
        this.tvdb_id = oShow.getTvdbID();
        for (OfflineEpisodeToUnfollow oEpisode : oShow.getEpisodes()) {
            addEpisode(oEpisode);
        }
    }
    
    public void addEpisode(OfflineEpisodeToUnfollow oEpisode) {
        episodes.add(new RequestOfflineEpisodeToUnfollowDTO(oEpisode));
    }
    
    public class RequestOfflineEpisodeToUnfollowDTO {

        Integer season;
        Integer episode;

        public RequestOfflineEpisodeToUnfollowDTO(OfflineEpisodeToUnfollow oEpisode) {
            this.season = oEpisode.getSeason();
            this.episode = oEpisode.getEpisode();
        }

    }

}
