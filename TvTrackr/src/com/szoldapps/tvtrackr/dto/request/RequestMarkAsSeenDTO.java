package com.szoldapps.tvtrackr.dto.request;

import java.util.ArrayList;
import java.util.List;

import com.j256.ormlite.dao.ForeignCollection;
import com.szoldapps.tvtrackr.db.model.Episode;
import com.szoldapps.tvtrackr.db.model.Season;
import com.szoldapps.tvtrackr.db.model.Show;
import com.szoldapps.tvtrackr.db.model.User;
import com.szoldapps.tvtrackr.traktAccess.RestClient.RestURLs;

public class RequestMarkAsSeenDTO {
	String username;
	String password;
	String imdb_id;
	Integer tvdb_id;
	String title;
	Integer year;
	Integer season;
	List<MarkAsSeenEpisodeDetailsDTO> episodes = new ArrayList<RequestMarkAsSeenDTO.MarkAsSeenEpisodeDetailsDTO>();

	public RequestMarkAsSeenDTO(User user, Show show) {
		super();
		this.username = user.getUsername();
		this.password = user.getPassword();
		this.tvdb_id = show.getTvdbID();
		this.imdb_id = show.getImdbID();
		this.title = show.getTitle();
		this.year = show.getYear();
	}

	/**
	 * Use for {@link RestURLs#SHOW_SEASON_SEEN}
	 * @param user
	 * @param show
	 * @param seasonNr
	 */
	public RequestMarkAsSeenDTO(User user, Show show, int seasonNr) {
		super();
		this.username = user.getUsername();
		this.password = user.getPassword();
		this.tvdb_id = show.getTvdbID();
		this.imdb_id = show.getImdbID();
		this.title = show.getTitle();
		this.year = show.getYear();
		this.season = seasonNr;
	}

	public RequestMarkAsSeenDTO(User user, int tvdbId) {
		super();
		this.username = user.getUsername();
		this.password = user.getPassword();
		this.tvdb_id = tvdbId;
	}

	public RequestMarkAsSeenDTO(User user, Show show, Integer season) {
		this(user, show);
		this.season = season;
	}


	/**
	 * Adds all Episodes
	 * @param episodes
	 * @param lastPlayed
	 */
	public void addEpisodes(List<Episode> episodes, String lastPlayed) {
		for (Episode episode : episodes) {
			addEpisode(episode.getSeason(), episode, lastPlayed);
		}
	}


	/**
	 * Adds all Episodes of show to {@link RequestMarkAsSeenDTO} (except season 0)
	 * @param show
	 * @param lastPlayed
	 */
	public void addEpisodesOfShow(Show show, String lastPlayed) {
		ForeignCollection<Season> seasons = show.getSeasons();
		if(seasons == null) { return; }
		for (Season season : seasons) {
			if(season.getSeasonNr().equals(0)) { continue; }	// Season 0 does not count
			ForeignCollection<Episode> episodes = season.getEpisodes();
			if(episodes != null) {
				addEpisodes(season, episodes, lastPlayed);
			}
		}
	}

	/**
	 * Adds all Episodes of season to {@link RequestMarkAsSeenDTO}
	 * @param show
	 * @param lastPlayed
	 */
	public void addEpisodesOfSeason(Season season, String lastPlayed) {
		ForeignCollection<Episode> episodes = season.getEpisodes();
		if(episodes != null) {
			addEpisodes(season, episodes, lastPlayed);
		}
	}

	private void addEpisodes(Season season, ForeignCollection<Episode> episodes, String lastPlayed) {
		for (Episode episode : episodes) {
			addEpisode(season, episode, lastPlayed);
		}
	}

	private void addEpisode(Season season, Episode episode, String lastPlayed) {
		episodes.add(new MarkAsSeenEpisodeDetailsDTO(season.getSeasonNr(), episode.getNumber(), lastPlayed));
	}

	/**
	 * lastPlayed is optional
	 * @author thomasszoldatits
	 *
	 */
	public class MarkAsSeenEpisodeDetailsDTO {

		Integer season;
		Integer episode;
		String last_played;

		public MarkAsSeenEpisodeDetailsDTO(Integer season, Integer episode, String lastPlayed) {
			this.season = season;
			this.episode = episode;
			this.last_played = lastPlayed;
		}

	}

}
