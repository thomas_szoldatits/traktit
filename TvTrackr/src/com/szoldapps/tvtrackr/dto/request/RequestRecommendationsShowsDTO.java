package com.szoldapps.tvtrackr.dto.request;

import com.szoldapps.tvtrackr.db.model.User;

public class RequestRecommendationsShowsDTO {
    String username;
    String password;
    String genre;
    boolean hide_collected = false;
    boolean hide_watchlisted = true;
	Integer start_year;
	Integer end_year;
    
    public RequestRecommendationsShowsDTO(User user, String genre, Integer startYear, Integer endYear) {
        super();
        this.username = user.getUsername();
        this.password = user.getPassword();
        this.genre = genre;
        this.start_year = startYear;
        this.end_year = endYear;
    }

//    {
//        "username": "username",
//        "password": "sha1hash"
//        "genre": "science-fiction",
//        "hide_collected": false,
//        "hide_watchlisted": true
//    }
}
