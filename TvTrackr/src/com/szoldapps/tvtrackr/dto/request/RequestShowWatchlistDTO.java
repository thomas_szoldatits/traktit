package com.szoldapps.tvtrackr.dto.request;

import java.util.List;

import com.szoldapps.tvtrackr.db.model.User;

public class RequestShowWatchlistDTO {

	String username;
	String password;
	List<RequestShowDTO> shows;
	
	public RequestShowWatchlistDTO(User u, List<RequestShowDTO> shows) {
		super();
		this.username = u.getUsername();
		this.password = u.getPassword();
		this.shows = shows;
	}

	// That's how it should look like, according to trakt
	//    {
	//        "username": "username",
	//        "password": "sha1hash",
	//        "shows": [
	//            {
	//                "tvdb_id": 153021, 
	//                "title": "The Walking Dead", 
	//                "year": 2010
	//            }
	//        ]
	//    }

}
