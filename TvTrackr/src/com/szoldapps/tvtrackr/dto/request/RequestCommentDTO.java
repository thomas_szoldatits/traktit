package com.szoldapps.tvtrackr.dto.request;

import com.szoldapps.tvtrackr.db.model.Episode;
import com.szoldapps.tvtrackr.db.model.Show;
import com.szoldapps.tvtrackr.db.model.User;

public class RequestCommentDTO {

    String username;
    String password;
    Integer tvdb_id;
    String title;
    Integer year;
    Integer season;
    Integer episode;
    String comment;
    boolean spoiler;
    boolean review;

    public RequestCommentDTO(User u, Show show, String comment, boolean spoiler, boolean review) {
        super();
        this.username = u.getUsername();
        this.password = u.getPassword();

        this.tvdb_id = show.getTvdbID();
        this.title = show.getTitle();
        this.year = show.getYear();
        
        this.comment = comment;
        this.spoiler = spoiler;
        this.review = review;
    }
    
    public RequestCommentDTO(User u, Show show, String comment, boolean spoiler) {
        this(u, show, comment, spoiler, hasMoreThan200words(comment));
    }
    

    public RequestCommentDTO(User u, Episode e, String comment, boolean spoiler, boolean review) {
        this(u, e.getSeason().getShow(), comment, spoiler, review);
        this.season = e.getSeason().getSeasonNr();
        this.episode = e.getNumber();
    }
    public RequestCommentDTO(User u, Episode e, String comment, boolean spoiler) {
        this(u, e, comment, spoiler, hasMoreThan200words(comment));
    }
    
    private static boolean hasMoreThan200words(String comment) {
        return false;
    }
    
    @Override
    public String toString() {
        return "RequestCommentDTO [username=" + username + ", password=" + password + ", tvdb_id=" + tvdb_id + ", title="
                + title + ", year=" + year + ", season=" + season + ", episode=" + episode + ", comment=" + comment
                + ", spoiler=" + spoiler + ", review=" + review + "]";
    }
//    {
//        "username": "username",
//        "password": "sha1hash",
//        "tvdb_id": 213221,
//        "title": "Portlandia",
//        "year": 2011,
//        "season": 1,
//        "episode": 1,
//        "comment": "The opening musical number is just superb!",
//        "spoiler": false,
//        "review": false
//    }
}
