package com.szoldapps.tvtrackr.dto.request;

import com.szoldapps.tvtrackr.db.model.User;

public class RequestRecommendationsDismissDTO {
    String username;
    String password;
    Integer tvdb_id;

    public RequestRecommendationsDismissDTO(User user, int tvdbId) {
        super();
        this.username = user.getUsername();
        this.password = user.getPassword();
        this.tvdb_id = tvdbId;
    }

}
