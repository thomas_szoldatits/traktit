package com.szoldapps.tvtrackr.dto.response;


public class InWatchlistShowDTO {

    private boolean in_watchlist;

    public boolean getIn_watchlist() {
        return in_watchlist;
    }

    public void setIn_watchlist(boolean in_watchlist) {
        this.in_watchlist = in_watchlist;
    }

}
