package com.szoldapps.tvtrackr.dto.response;

import com.szoldapps.tvtrackr.dto.show.RatingsDTO;

public class ResponseRateShowEpisodeDTO {
    String status;
    String message;
    String type;
    public String rating;
    public Integer ratingAdvanced; // is not returned from trakt, just need it afterwards
    public RatingsDTO ratings;
    boolean facebook;
    boolean twitter;
    boolean tumblr;
    boolean path;
    @Override
    public String toString() {
        return "RateEpisodeResponseDTO [status=" + status + ", message=" + message + ", type=" + type + ", rating="
                + rating + ", ratings=" + ratings + ", facebook=" + facebook + ", twitter=" + twitter + ", tumblr="
                + tumblr + ", path=" + path + "]";
    }
}
