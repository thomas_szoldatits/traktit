package com.szoldapps.tvtrackr.dto.response;

public class ResponseStatsDTO {
    private long plays;
    private long watchers;
    private ResponseStatsCollection collection;
    private ResponseStatsLists lists;
    
	public long getPlays() {
		return plays;
	}

	public void setPlays(long plays) {
		this.plays = plays;
	}

	public long getWatchers() {
		return watchers;
	}

	public void setWatchers(long watchers) {
		this.watchers = watchers;
	}

	public ResponseStatsCollection getCollection() {
		return collection;
	}

	public void setCollection(ResponseStatsCollection collection) {
		this.collection = collection;
	}

	public ResponseStatsLists getLists() {
		return lists;
	}

	public void setLists(ResponseStatsLists lists) {
		this.lists = lists;
	}
	
    public class ResponseStatsCollection {
        private long all;
        private long users;
		public long getAll() {
			return all;
		}
		public void setAll(long all) {
			this.all = all;
		}
		public long getUsers() {
			return users;
		}
		public void setUsers(long users) {
			this.users = users;
		}
    }
    
    public class ResponseStatsLists {
        private long all;
        private long watchlist;
        private long custom;
		public long getAll() {
			return all;
		}
		public void setAll(long all) {
			this.all = all;
		}
		public long getWatchlist() {
			return watchlist;
		}
		public void setWatchlist(long watchlist) {
			this.watchlist = watchlist;
		}
		public long getCustom() {
			return custom;
		}
		public void setCustom(long custom) {
			this.custom = custom;
		}
    }
}
