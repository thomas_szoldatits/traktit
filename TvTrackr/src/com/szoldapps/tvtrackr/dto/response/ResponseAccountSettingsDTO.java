package com.szoldapps.tvtrackr.dto.response;

import com.google.gson.annotations.SerializedName;


public class ResponseAccountSettingsDTO {

	private String status;
	private String message;
	private ResponseUserProfileDTO profile;
	private ResponseUserAccountDTO account;

	

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public ResponseUserProfileDTO getProfile() {
		return profile;
	}

	public void setProfile(ResponseUserProfileDTO profile) {
		this.profile = profile;
	}

	public ResponseUserAccountDTO getAccount() {
		return account;
	}

	public void setAccount(ResponseUserAccountDTO account) {
		this.account = account;
	}

	public class ResponseUserProfileDTO {
		private String username;
		private String full_name;
		private String gender;
		private Integer age;
		private String location;
		private String about;
		private long joined;
		private long last_login;
		private String avatar;
		private String url;
		private Boolean vip;
		
		public String getUsername() {
			return username;
		}
		public void setUsername(String username) {
			this.username = username;
		}
		public String getFull_name() {
			return full_name;
		}
		public void setFull_name(String full_name) {
			this.full_name = full_name;
		}
		public String getGender() {
			return gender;
		}
		public void setGender(String gender) {
			this.gender = gender;
		}
		public Integer getAge() {
			return age;
		}
		public void setAge(Integer age) {
			this.age = age;
		}
		public String getLocation() {
			return location;
		}
		public void setLocation(String location) {
			this.location = location;
		}
		public String getAbout() {
			return about;
		}
		public void setAbout(String about) {
			this.about = about;
		}
		public long getJoined() {
			return joined;
		}
		public void setJoined(long joined) {
			this.joined = joined;
		}
		public long getLast_login() {
			return last_login;
		}
		public void setLast_login(long last_login) {
			this.last_login = last_login;
		}
		public String getAvatar() {
			return avatar;
		}
		public void setAvatar(String avatar) {
			this.avatar = avatar;
		}
		public String getUrl() {
			return url;
		}
		public void setUrl(String url) {
			this.url = url;
		}
		public Boolean getVip() {
			return vip;
		}
		public void setVip(Boolean vip) {
			this.vip = vip;
		}
	}
	
	public class ResponseUserAccountDTO {
		private String timezone;
		private Boolean use_24hr;
		@SerializedName("protected")
		private Boolean isProtected;
		public String getTimezone() {
			return timezone;
		}
		public void setTimezone(String timezone) {
			this.timezone = timezone;
		}
		public Boolean getUse_24hr() {
			return use_24hr;
		}
		public void setUse_24hr(Boolean use_24hr) {
			this.use_24hr = use_24hr;
		}
		public Boolean getIsProtected() {
			return isProtected;
		}
		public void setIsProtected(Boolean isProtected) {
			this.isProtected = isProtected;
		}
	}
	
	
	
	//    	{
	//    	   "status":"success",
	//    	   "message":"All settings for justin",
	//    	   "profile":{
	//    	      "username":"justin",
	//    	      "full_name":"Justin",
	//    	      "gender":"male",
	//    	      "age":30,
	//    	      "location":"San Diego, CA",
	//    	      "about":"Co-founder of trakt. Please use the feedback tab on the right side to leave us suggestions or bugs you come across. Hope you're enjoying the site!",
	//    	      "joined":1285436965,
	//    	      "last_login":1334901801,
	//    	      "avatar":"http://trakt.us/images/avatars/1.4.jpg",
	//    	      "url":"http://trakt.tv/user/justin",
	//    	      "vip":true
	//    	   },
	//    	   "account":{
	//    	      "timezone":"UM8", //go.trakt.tv/HX7SfQ
	//    	      "use_24hr":false,
	//    	      "protected":false
	//    	   },
	/** --------------- ADD MORE STUFF IF NEEDED --------------- **/
	//    	   "viewing":{
	//    	      "ratings":{
	//    	         "mode":"advanced" //simple,advanced
	//    	      },
	//    	      "shouts":{
	//    	         "show_badges":false,
	//    	         "show_spoilers":false
	//    	      }
	//    	   },
	//    	   "connections":{
	//    	      "facebook":{
	//    	         "connected":true
	//    	      },
	//    	      "twitter":{
	//    	         "connected":true
	//    	      },
	//    	      "tumblr":{
	//    	         "connected":true
	//    	      },
	//    	      "path":{
	//    	         "connected":true
	//    	      },
	//    	      "prowl":{
	//    	         "connected":true
	//    	      }
	//    	   },
	//    	   "sharing_text":{
	//    	      "watching":"I'm watching [item]",
	//    	      "watched":"I just watched [item]"
	//    	   }
	//    	}
}
