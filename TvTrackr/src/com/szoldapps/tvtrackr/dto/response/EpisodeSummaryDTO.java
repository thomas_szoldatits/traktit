package com.szoldapps.tvtrackr.dto.response;

import com.szoldapps.tvtrackr.dto.show.EpisodeDTO;



public class EpisodeSummaryDTO {

//	private MediumShowDTO show;	// would be returned, but is not needed
    private EpisodeDTO episode;

	public EpisodeDTO getEpisode() {
		return episode;
	}

	public void setEpisode(EpisodeDTO episode) {
		this.episode = episode;
	}

}
