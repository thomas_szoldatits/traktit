package com.szoldapps.tvtrackr.dto.response;

import com.google.gson.annotations.SerializedName;

public class ResponseCommentsDTO {

    @Override
    public String toString() {
        return "ResponseCommentsDTO [id=" + id + ", inserted=" + inserted + ", text=" + text + ", text_html=" + text_html
                + ", spoiler=" + spoiler + ", type=" + type + ", likes=" + likes + ", replies=" + replies + ", user=" + user
                + ", user_ratings=" + user_ratings + "]";
    }

    public Integer id;
    public long inserted;
    public String text;
    public String text_html;
    public boolean spoiler;
    public String type;
    public Integer likes;
    public Integer replies;
    public UserDTO user;
    public UserRatingsDTO user_ratings;


    public class UserDTO {
        public String username;
        @SerializedName("protected")
        public boolean userProtected;
        public String avatar;
        public String full_name;
        public String gender;
        public String age;
        public String location;
        public String about;
        public long joined;
        public String url;
    }

    public class UserRatingsDTO {
        public String rating;
        public Integer rating_advanced;
    }

    //    "id":3554,
    //    "inserted":1354833317,
    //    "text":"awesome show, but I have only watched season 1 so far. But I am really excited for the rest :)",
    //    "text_html":"awesome show, but I have only watched season 1 so far. But I am really excited for the rest :)",
    //    "spoiler":false,
    //    "type":"shout",
    //    "likes":0,
    //    "replies":0,
    //    "user":{
    //       "username":"freeman",
    //       "protected":false,
    //       "avatar":"http://gravatar.com/avatar/e8487393dc5e1c87d1624776ee2c200d.jpg?s=140&r=pg&d=http%3A%2F%2Ftrakt.tv%2Fimages%2Favatar-large.jpg",
    //       "full_name":"",
    //       "gender":"male",
    //       "age":"",
    //       "location":"Germany",
    //       "about":"",
    //       "joined":1347795808,
    //       "url":"http://trakt.tv/user/freeman"
    //    },
    //    "user_ratings":{
    //       "rating":"love",
    //       "rating_advanced":10
    //    }
}
