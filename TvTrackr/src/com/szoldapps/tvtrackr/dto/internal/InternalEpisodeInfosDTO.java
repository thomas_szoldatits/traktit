package com.szoldapps.tvtrackr.dto.internal;

import java.util.List;

/**
 * Contains {@link #episodeCount}, {@link #position} & {@link #tabNameList}
 * 
 * @author thomasszoldatits
 *
 */
public class InternalEpisodeInfosDTO {
	private int episodeCount;
	private int position;
	private List<String> tabNameList;
	
	public InternalEpisodeInfosDTO(List<String> tabNameList, int position) {
		super();
		this.tabNameList = tabNameList;
		this.episodeCount = tabNameList.size();
		this.position = position;
	}
	public int getEpisodeCount() {
		return episodeCount;
	}
	public void setEpisodeCount(int episodeCount) {
		this.episodeCount = episodeCount;
	}
	public int getPosition() {
		return position;
	}
	public void setPosition(int position) {
		this.position = position;
	}
	public List<String> getTabNameList() {
		return tabNameList;
	}
	public void setTabNameList(List<String> tabNameList) {
		this.tabNameList = tabNameList;
	}

}
