package com.szoldapps.tvtrackr.dto.internal;

import com.szoldapps.tvtrackr.db.model.Show;

public class InternalShowDownloadResponseDTO {

	private boolean isShowCache = false;
	private Show show;
	
	public InternalShowDownloadResponseDTO(Show show, boolean isShowCache) {
		this.show = show;
		this.isShowCache = isShowCache;
	}
	
	public boolean isShowCache() {
		return isShowCache;
	}

	public Show getShow() {
		return show;
	}
}
