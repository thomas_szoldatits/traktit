package com.szoldapps.tvtrackr.dto.internal;

import java.util.List;

import com.szoldapps.tvtrackr.dto.response.ResponseCommentsDTO;

public class InternalCommentsDTO {
	private String username;
    private List<ResponseCommentsDTO> comments;

    public InternalCommentsDTO(String username, List<ResponseCommentsDTO> comments) {
    	super();
    	this.username = username;
    	this.comments = comments;
    }
    
    public String getUsername() {
    	return username;
    }
    public void setUsername(String username) {
    	this.username = username;
    }
	public List<ResponseCommentsDTO> getComments() {
		return comments;
	}
	public void setComments(List<ResponseCommentsDTO> comments) {
		this.comments = comments;
	}
}
