package com.szoldapps.tvtrackr.dto.internal;

import com.szoldapps.tvtrackr.db.model.Show.ShowDownloadStatus;

public class InternalRecommendationStatusDTO {
	private boolean followed;
	private ShowDownloadStatus status;
	
	public boolean isFollowed() {
		return followed;
	}
	public void setFollowed(boolean followed) {
		this.followed = followed;
	}
	public ShowDownloadStatus getStatus() {
		return status;
	}
	public void setStatus(ShowDownloadStatus status) {
		this.status = status;
	}

}
