package com.szoldapps.tvtrackr.dto.internal;


public class InternalEpisodeWatchedDTO {
	
	boolean watched;
	boolean offlineMarked;
	
	public InternalEpisodeWatchedDTO(boolean watched, boolean offlineMarked) {
		super();
		this.watched = watched;
		this.offlineMarked = offlineMarked;
	}
	public boolean isWatched() {
		return watched;
	}
	public void setWatched(boolean watched) {
		this.watched = watched;
	}
	public boolean isOfflineMarked() {
		return offlineMarked;
	}
	public void setOfflineMarked(boolean offlineMarked) {
		this.offlineMarked = offlineMarked;
	}
	
}
