package com.szoldapps.tvtrackr.dto.internal;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.szoldapps.tvtrackr.db.model.Episode;
import com.szoldapps.tvtrackr.db.model.Season;


public class InternalSeasonsFragmentDTO {
	private boolean fullyDownloaded;
	private ArrayList<Season> headerList;
	private HashMap<Season, List<Episode>> childMap;

	/**
	 * Creates {@link InternalSeasonsFragmentDTO} and automatically sets {@link #fullyDownloaded} to <code>FALSE</code>;<br/>
	 * {@link #headerList} stays <code>null</code><br/>
	 * {@link #childMap} stays <code>null</code><br/>
	 * @param fullyDownloaded
	 */
	public InternalSeasonsFragmentDTO(boolean fullyDownloaded) {
		super();
		this.fullyDownloaded = false;
	}
	/**
	 * Creates {@link InternalSeasonsFragmentDTO} and automatically sets {@link #fullyDownloaded} to <code>TRUE</code>;
	 * @param headerList
	 * @param childMap
	 */
	public InternalSeasonsFragmentDTO(ArrayList<Season> headerList, HashMap<Season, List<Episode>> childMap) {
		super();
		this.headerList = headerList;
		this.childMap = childMap;
		this.fullyDownloaded = true;
	}

	public boolean isFullyDownloaded() {
		return fullyDownloaded;
	}
	public void setFullyDownloaded(boolean fullyDownloaded) {
		this.fullyDownloaded = fullyDownloaded;
	}
	public ArrayList<Season> getHeaderList() {
		return headerList;
	}
	public void setHeaderList(ArrayList<Season> headerList) {
		this.headerList = headerList;
	}
	public HashMap<Season, List<Episode>> getChildMap() {
		return childMap;
	}
	public void setChildMap(HashMap<Season, List<Episode>> childMap) {
		this.childMap = childMap;
	}

}
