package com.szoldapps.tvtrackr.dto.internal;

import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.szoldapps.tvtrackr.db.model.Episode;

public class InternalToWatchDTO {
	private RelativeLayout relListItem;
	private TextView txtHl;
	private TextView txtTime;
	private TextView txtSeText;
	private ImageView imgSeen;
	private ProgressBar pbarSeenLoding;
	private Episode episode;
	private Integer episodesLeft;
	private TextView txtEpisodesLeft;
	private TextView txtEpisodesLeftSubLine;
	public InternalToWatchDTO(RelativeLayout relListItem, TextView txtHl, TextView txtTime, TextView txtSeText, 
			ImageView imgSeen, ProgressBar pbarSeenLoding, TextView txtEpisodesLeft, 
			TextView txtEpisodesLeftSubLine) {
		super();
		this.relListItem = relListItem;
		this.txtHl = txtHl;
		this.txtTime = txtTime;
		this.txtSeText = txtSeText;
		this.imgSeen = imgSeen;
		this.pbarSeenLoding = pbarSeenLoding;
		this.txtEpisodesLeft = txtEpisodesLeft;
		this.txtEpisodesLeftSubLine = txtEpisodesLeftSubLine;
	}
	public Episode getEpisode() {
		return episode;
	}
	public TextView getTxtHl() {
		return txtHl;
	}
	public void setTxtHl(TextView txtHl) {
		this.txtHl = txtHl;
	}
	public TextView getTxtTime() {
		return txtTime;
	}
	public void setTxtTime(TextView txtTime) {
		this.txtTime = txtTime;
	}
	public TextView getTxtSeText() {
		return txtSeText;
	}
	public void setTxtSeText(TextView txtSeText) {
		this.txtSeText = txtSeText;
	}
	public ImageView getImgSeen() {
		return imgSeen;
	}
	public void setImgSeen(ImageView imgSeen) {
		this.imgSeen = imgSeen;
	}
	public void setEpisode(Episode episode) {
		this.episode = episode;
	}
	public ProgressBar getPbarSeenLoding() {
		return pbarSeenLoding;
	}
	public void setPbarSeenLoding(ProgressBar pbarSeenLoding) {
		this.pbarSeenLoding = pbarSeenLoding;
	}
	public TextView getTxtEpisodesLeft() {
		return txtEpisodesLeft;
	}
	public void setTxtEpisodesLeft(TextView txtEpisodesLeft) {
		this.txtEpisodesLeft = txtEpisodesLeft;
	}
	public Integer getEpisodesLeft() {
		return episodesLeft;
	}
	public void setEpisodesLeft(Integer episodesLeft) {
		this.episodesLeft = episodesLeft;
	}
	public TextView getTxtEpisodesLeftSubLine() {
		return txtEpisodesLeftSubLine;
	}
	public void setTxtEpisodesLeftSubLine(TextView txtEpisodesLeftSubLine) {
		this.txtEpisodesLeftSubLine = txtEpisodesLeftSubLine;
	}
	public RelativeLayout getRelListItem() {
		return relListItem;
	}
	public void setRelListItem(RelativeLayout relListItem) {
		this.relListItem = relListItem;
	}
}
