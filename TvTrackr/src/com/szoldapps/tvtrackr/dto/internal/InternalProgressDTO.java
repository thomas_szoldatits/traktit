package com.szoldapps.tvtrackr.dto.internal;

import com.szoldapps.tvtrackr.helper.NumbersHelper;

public class InternalProgressDTO {
    private int percent;
    private long watchedEpisodeCount;
    private long airedEpisodeCount;
    private long episodeCount;
	private Integer runtime;
	private int completeLeftTimeToWatchInMin;
	private int hoursLeftToWatch;
	private int minLeftToWatch;
	private int episodesLeftToWatch;
    
    public InternalProgressDTO(long watchedEpisodeCount, long airedEpisodeCount, long episodeCount, Integer runtime) {
        super();
        this.watchedEpisodeCount = watchedEpisodeCount;
        this.airedEpisodeCount = airedEpisodeCount;
        this.episodeCount = episodeCount;
        this.runtime = runtime;
        this.setDynamicVars();
    }

	private void setDynamicVars() {
		percent = NumbersHelper.getPercentage(watchedEpisodeCount, airedEpisodeCount);
		episodesLeftToWatch = (int) (airedEpisodeCount - watchedEpisodeCount);
		if(watchedEpisodeCount == airedEpisodeCount) {
			completeLeftTimeToWatchInMin = 0;
		} else {
			completeLeftTimeToWatchInMin = episodesLeftToWatch*runtime;
		}
		hoursLeftToWatch = completeLeftTimeToWatchInMin / 60;
		minLeftToWatch = completeLeftTimeToWatchInMin % 60;
	}

	public void setWatchedEpisodeCount(long watchedEpisodeCount) {
		this.watchedEpisodeCount = watchedEpisodeCount;
		this.setDynamicVars();
	}

	public void setEpisodeCount(long episodeCount) {
		this.episodeCount = episodeCount;
		this.setDynamicVars();
	}
	
	public void setRuntime(Integer runtime) {
		this.runtime = runtime;
		this.setDynamicVars();
	}
	
	// GETTERS
	
	public int getPercent() {
		return percent;
	}

	public Integer getRuntime() {
		return runtime;
	}

	public int getCompleteLeftTimeToWatchInMin() {
		return completeLeftTimeToWatchInMin;
	}

	public int getHoursLeftToWatch() {
		return hoursLeftToWatch;
	}

	public int getMinLeftToWatch() {
		return minLeftToWatch;
	}

	public long getWatchedEpisodeCount() {
		return watchedEpisodeCount;
	}

	public long getEpisodeCount() {
		return episodeCount;
	}
	
	public int getEpisodesLeftToWatch() {
		return episodesLeftToWatch;
	}

	public long getAiredEpisodeCount() {
		return airedEpisodeCount;
	}

	public void setAiredEpisodeCount(long airedEpisodeCount) {
		this.airedEpisodeCount = airedEpisodeCount;
	}
    
}
