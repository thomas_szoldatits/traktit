package com.szoldapps.tvtrackr.dto.show;


public class SearchShowDTO extends MediumShowDTO {

	boolean ended;
	boolean followedByUser;

	public boolean isEnded() {
		return ended;
	}
	public void setEnded(boolean ended) {
		this.ended = ended;
	}
	public boolean isFollowedByUser() {
		return followedByUser;
	}
	public void setFollowedByUser(boolean followedByUser) {
		this.followedByUser = followedByUser;
	}
	
}
