package com.szoldapps.tvtrackr.dto.show;

public class ImagesDTO {
    
    String poster;
    String fanart;
    String banner;

    public String getPoster() {
        return poster;
    }
    public void setPoster(String poster) {
        this.poster = poster;
    }
    public String getFanart() {
        return fanart;
    }
    public void setFanart(String fanart) {
        this.fanart = fanart;
    }
    public String getBanner() {
        return banner;
    }
    public void setBanner(String banner) {
        this.banner = banner;
    }
    
    @Override
    public String toString() {
        return "ImagesDTO [poster=" + poster + ", fanart=" + fanart + ", banner=" + banner + "]";
    }
}
