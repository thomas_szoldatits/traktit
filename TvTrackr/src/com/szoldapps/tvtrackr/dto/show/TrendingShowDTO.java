package com.szoldapps.tvtrackr.dto.show;


public class TrendingShowDTO extends MediumShowDTO {

	String status;
	long watchers;
	long plays;
	boolean watched;
	boolean in_watchlist;
	String rating;
	int rating_advanced;
	
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public long getWatchers() {
		return watchers;
	}
	public void setWatchers(long watchers) {
		this.watchers = watchers;
	}
	public long getPlays() {
		return plays;
	}
	public void setPlays(long plays) {
		this.plays = plays;
	}
	public boolean isWatched() {
		return watched;
	}
	public void setWatched(boolean watched) {
		this.watched = watched;
	}
	public boolean isIn_watchlist() {
		return in_watchlist;
	}
	public void setIn_watchlist(boolean in_watchlist) {
		this.in_watchlist = in_watchlist;
	}
	public String getRating() {
		return rating;
	}
	public void setRating(String rating) {
		this.rating = rating;
	}
	public int getRating_advanced() {
		return rating_advanced;
	}
	public void setRating_advanced(int rating_advanced) {
		this.rating_advanced = rating_advanced;
	}
	
}
