package com.szoldapps.tvtrackr.dto.show;

public class StatsDTO {
	
	long watchers;
	long plays;
	long scrobbles;
	long scrobbles_unique;
	long checkins;
	long checkins_unique;
	long collection;
	long collection_unique;
	
	public long getWatchers() {
		return watchers;
	}
	public void setWatchers(long watchers) {
		this.watchers = watchers;
	}
	public long getPlays() {
		return plays;
	}
	public void setPlays(long plays) {
		this.plays = plays;
	}
	public long getScrobbles() {
		return scrobbles;
	}
	public void setScrobbles(long scrobbles) {
		this.scrobbles = scrobbles;
	}
	public long getScrobbles_unique() {
		return scrobbles_unique;
	}
	public void setScrobbles_unique(long scrobbles_unique) {
		this.scrobbles_unique = scrobbles_unique;
	}
	public long getCheckins() {
		return checkins;
	}
	public void setCheckins(long checkins) {
		this.checkins = checkins;
	}
	public long getCheckins_unique() {
		return checkins_unique;
	}
	public void setCheckins_unique(long checkins_unique) {
		this.checkins_unique = checkins_unique;
	}
	public long getCollection() {
		return collection;
	}
	public void setCollection(long collection) {
		this.collection = collection;
	}
	public long getCollection_unique() {
		return collection_unique;
	}
	public void setCollection_unique(long collection_unique) {
		this.collection_unique = collection_unique;
	}
	
}
