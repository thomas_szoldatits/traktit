package com.szoldapps.tvtrackr.dto.show;


public class WatchlistShowDTO extends MediumShowDTO {

	long inserted;
	long plays;
	boolean watched;
	boolean in_watchlist;
	String rating;
	
	public long getInserted() {
		return inserted;
	}
	public void setInserted(long inserted) {
		this.inserted = inserted;
	}
	public long getPlays() {
		return plays;
	}
	public void setPlays(long plays) {
		this.plays = plays;
	}
	public boolean isWatched() {
		return watched;
	}
	public void setWatched(boolean watched) {
		this.watched = watched;
	}
	public boolean isIn_watchlist() {
		return in_watchlist;
	}
	public void setIn_watchlist(boolean in_watchlist) {
		this.in_watchlist = in_watchlist;
	}
	public String getRating() {
		return rating;
	}
	public void setRating(String rating) {
		this.rating = rating;
	}
	
}
