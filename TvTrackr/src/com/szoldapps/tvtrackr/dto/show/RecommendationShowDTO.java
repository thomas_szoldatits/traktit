package com.szoldapps.tvtrackr.dto.show;


public class RecommendationShowDTO extends MediumShowDTO {

	boolean in_watchlist;

	public boolean isIn_watchlist() {
		return in_watchlist;
	}
	public void setIn_watchlist(boolean in_watchlist) {
		this.in_watchlist = in_watchlist;
	}
	
}
