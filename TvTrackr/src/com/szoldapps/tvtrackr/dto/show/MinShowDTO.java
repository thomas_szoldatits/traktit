package com.szoldapps.tvtrackr.dto.show;

import java.util.List;

import com.szoldapps.tvtrackr.db.model.Show;


public class MinShowDTO {

	String title;
	int year;
	String imdb_id;
	int tvdb_id;
	String tvrage_id;
	String url;
	ImagesDTO images;
	List<String> genres;

	@Override
	public boolean equals(Object o) {
		if(o instanceof MinShowDTO) {
			MinShowDTO minShow = (MinShowDTO) o;
			return (this.getTvdb_id() == minShow.getTvdb_id());
		}
		return super.equals(o);
	}
	
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public int getYear() {
		return year;
	}
	public void setYear(int year) {
		this.year = year;
	}
	public String getImdb_id() {
		return imdb_id;
	}
	public void setImdb_id(String imdb_id) {
		this.imdb_id = imdb_id;
	}
	public int getTvdb_id() {
		return tvdb_id;
	}
	public void setTvdb_id(int tvdb_id) {
		this.tvdb_id = tvdb_id;
	}
	public String getTvrage_id() {
		return tvrage_id;
	}
	public void setTvrage_id(String tvrage_id) {
		this.tvrage_id = tvrage_id;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public ImagesDTO getImages() {
		return images;
	}
	public void setImages(ImagesDTO images) {
		this.images = images;
	}
	public List<String> getGenres() {
		return genres;
	}
	public void setGenres(List<String> genres) {
		this.genres = genres;
	}
	
	/**
	 * Creates {@link Show} object with all {@link MinShowDTO} variables,
	 * <b>EXCEPT: {@link MinShowDTO#images} & {@link MinShowDTO#genres} </b>
	 * @return
	 */
	public Show getShow() {
		Show show = new Show(tvdb_id);
		show.setTitle(title);
		show.setYear(year);
		show.setImdbID(imdb_id);
		show.setTvrageID(tvrage_id);
		show.setUrl(url);
		
		return show;
	}

}
