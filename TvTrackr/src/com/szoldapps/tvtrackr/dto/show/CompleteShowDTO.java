package com.szoldapps.tvtrackr.dto.show;

import java.util.List;


public class CompleteShowDTO extends MediumShowDTO {

	String first_aired_iso;
	long first_aired_utc;
	String status;
	String air_day_utc;
	String air_time_utc;
	long last_updated;
	StatsDTO stats;
	PeopleDTO people;
	String rating;
	int rating_advanced;
	boolean in_watchlist;
	List<SeasonDTO> seasons;
	
	public String getFirst_aired_iso() {
		return first_aired_iso;
	}
	public void setFirst_aired_iso(String first_aired_iso) {
		this.first_aired_iso = first_aired_iso;
	}
	public long getFirst_aired_utc() {
		return first_aired_utc;
	}
	public void setFirst_aired_utc(long first_aired_utc) {
		this.first_aired_utc = first_aired_utc;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getAir_day_utc() {
		return air_day_utc;
	}
	public void setAir_day_utc(String air_day_utc) {
		this.air_day_utc = air_day_utc;
	}
	public String getAir_time_utc() {
		return air_time_utc;
	}
	public void setAir_time_utc(String air_time_utc) {
		this.air_time_utc = air_time_utc;
	}
	public long getLast_updated() {
		return last_updated;
	}
	public void setLast_updated(long last_updated) {
		this.last_updated = last_updated;
	}
	public StatsDTO getStats() {
		return stats;
	}
	public void setStats(StatsDTO stats) {
		this.stats = stats;
	}
	public PeopleDTO getPeople() {
		return people;
	}
	public void setPeople(PeopleDTO people) {
		this.people = people;
	}
	public String getRating() {
		return rating;
	}
	public void setRating(String rating) {
		this.rating = rating;
	}
	public int getRating_advanced() {
		return rating_advanced;
	}
	public void setRating_advanced(int rating_advanced) {
		this.rating_advanced = rating_advanced;
	}
	public boolean isIn_watchlist() {
		return in_watchlist;
	}
	public void setIn_watchlist(boolean in_watchlist) {
		this.in_watchlist = in_watchlist;
	}
	public List<SeasonDTO> getSeasons() {
		return seasons;
	}
	public void setSeasons(List<SeasonDTO> seasons) {
		this.seasons = seasons;
	}
	
}
