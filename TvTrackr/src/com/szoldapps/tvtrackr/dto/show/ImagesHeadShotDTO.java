package com.szoldapps.tvtrackr.dto.show;

public class ImagesHeadShotDTO {
    
	String headshot;

    public String getHeadshot() {
        return headshot;
    }
    public void setHeadshot(String headshot) {
        this.headshot = headshot;
    }
    
    @Override
    public String toString() {
        return "ImagesHeadShotDTO [headshot=" + headshot + "]";
    }
}
