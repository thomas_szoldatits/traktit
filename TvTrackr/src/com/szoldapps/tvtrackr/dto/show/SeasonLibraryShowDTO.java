package com.szoldapps.tvtrackr.dto.show;

import java.util.List;

public class SeasonLibraryShowDTO {

	int season;
	List<Integer> episodes;
	
	public int getSeason() {
		return season;
	}
	public void setSeason(int season) {
		this.season = season;
	}
	public List<Integer> getEpisodes() {
		return episodes;
	}
	public void setEpisodes(List<Integer> episodes) {
		this.episodes = episodes;
	}
	
}
