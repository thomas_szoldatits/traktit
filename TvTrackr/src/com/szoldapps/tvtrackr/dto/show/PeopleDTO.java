package com.szoldapps.tvtrackr.dto.show;

import java.util.List;


public class PeopleDTO {

	List<ActorDTO> actors;

	public List<ActorDTO> getActors() {
		return actors;
	}
	public void setActors(List<ActorDTO> actors) {
		this.actors = actors;
	}

	@Override
	public String toString() {
		return "PeopleDTO [actors=" + actors + "]";
	}


}
