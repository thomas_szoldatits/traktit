package com.szoldapps.tvtrackr.dto.show;


public class ShowSummariesDTO extends MinShowDTO {

	long last_updated;

	public long getLast_updated() {
		return last_updated;
	}
	public void setLast_updated(long last_updated) {
		this.last_updated = last_updated;
	}
	
}
