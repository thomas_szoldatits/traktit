package com.szoldapps.tvtrackr.dto.show;

public class RatingsDTO {
	
	int percentage;
	long votes;
	long loved;
	long hated;

	public int getPercentage() {
		return percentage;
	}
	public void setPercentage(int percentage) {
		this.percentage = percentage;
	}
	public long getVotes() {
		return votes;
	}
	public void setVotes(long votes) {
		this.votes = votes;
	}
	public long getLoved() {
		return loved;
	}
	public void setLoved(long loved) {
		this.loved = loved;
	}
	public long getHated() {
		return hated;
	}
	public void setHated(long hated) {
		this.hated = hated;
	}

	@Override
	public String toString() {
		return "RatingsDTO [percentage=" + percentage + ", votes=" + votes + ", loved=" + loved  + ", hated=" + hated + "]";
	}
}
