package com.szoldapps.tvtrackr.dto.show;


public class ActorDTO {
    String name;
    String character;
    ImagesHeadShotDTO images;
    
    String url;
    String biography;
    String birthday;
    String birthplace;
    Integer tmdb_id;

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getCharacter() {
        return character;
    }
    public void setCharacter(String character) {
        this.character = character;
    }
    public String getUrl() {
        return url;
    }
    public void setUrl(String url) {
        this.url = url;
    }
    public String getBiography() {
        return biography;
    }
    public void setBiography(String biography) {
        this.biography = biography;
    }
    public String getBirthday() {
        return birthday;
    }
    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }
    public String getBirthplace() {
        return birthplace;
    }
    public void setBirthplace(String birthplace) {
        this.birthplace = birthplace;
    }
    public Integer getTmdb_id() {
        return tmdb_id;
    }
    public void setTmdb_id(Integer tmdb_id) {
        this.tmdb_id = tmdb_id;
    }
    public ImagesHeadShotDTO getImages() {
        return images;
    }
    public void setImages(ImagesHeadShotDTO images) {
        this.images = images;
    }

    @Override
    public String toString() {
        return "ResponseActorDTO [name=" + name + ", character=" + character + ", url=" + url + ", biography=" + biography
                + ", birthday=" + birthday + ", birthplace=" + birthplace + ", tmdb_id=" + tmdb_id + ", images=" + images
                + "]";
    }

}
