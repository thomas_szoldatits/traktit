package com.szoldapps.tvtrackr.dto.show;

import java.util.List;


public class LibraryShowDTO extends MinShowDTO {

	List<SeasonLibraryShowDTO> seasons;

	public List<SeasonLibraryShowDTO> getSeasons() {
		return seasons;
	}

	public void setSeasons(List<SeasonLibraryShowDTO> seasons) {
		this.seasons = seasons;
	}
	
}
