package com.szoldapps.tvtrackr.dto.show;


public abstract class MediumShowDTO extends MinShowDTO {

	long first_aired;
	String country;
	String overview;
	int runtime;
	String network;
	String air_day;
	String air_time;
	String certification;
	RatingsDTO ratings;

	public long getFirst_aired() {
		return first_aired;
	}
	public void setFirst_aired(long first_aired) {
		this.first_aired = first_aired;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getOverview() {
		return overview;
	}
	public void setOverview(String overview) {
		this.overview = overview;
	}
	public int getRuntime() {
		return runtime;
	}
	public void setRuntime(int runtime) {
		this.runtime = runtime;
	}
	public String getNetwork() {
		return network;
	}
	public void setNetwork(String network) {
		this.network = network;
	}
	public String getAir_day() {
		return air_day;
	}
	public void setAir_day(String air_day) {
		this.air_day = air_day;
	}
	public String getAir_time() {
		return air_time;
	}
	public void setAir_time(String air_time) {
		this.air_time = air_time;
	}
	public String getCertification() {
		return certification;
	}
	public void setCertification(String certification) {
		this.certification = certification;
	}
	public RatingsDTO getRatings() {
		return ratings;
	}
	public void setRatings(RatingsDTO ratings) {
		this.ratings = ratings;
	}

}
