package com.szoldapps.tvtrackr.dto.show;

import java.util.List;

import com.google.gson.annotations.SerializedName;

public class SeasonDTO {
	
	@SerializedName(value = "season")
	int seasonNr;
	List<EpisodeDTO> episodes;
	String url;
	String poster;
	
	public int getSeasonNr() {
		return seasonNr;
	}
	public void setSeasonNr(int seasonNr) {
		this.seasonNr = seasonNr;
	}
	public List<EpisodeDTO> getEpisodes() {
		return episodes;
	}
	public void setEpisodes(List<EpisodeDTO> episodes) {
		this.episodes = episodes;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getPoster() {
		return poster;
	}
	public void setPoster(String poster) {
		this.poster = poster;
	}
	
}
