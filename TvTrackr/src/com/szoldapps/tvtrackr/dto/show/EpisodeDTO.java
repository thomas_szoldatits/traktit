package com.szoldapps.tvtrackr.dto.show;


public class EpisodeDTO {

	int season;
	int episode;
	int number;
	int tvdb_id;
	String title;
	String overview;
	long first_aired;
	String first_aired_iso;
	long first_aired_utc;
	String url;
	String screen;
	RatingsDTO ratings;
	boolean watched;
	boolean in_collection;
	boolean in_watchlist;
	String rating;
	int rating_advanced;
	
	public int getSeason() {
		return season;
	}
	public void setSeason(int season) {
		this.season = season;
	}
	public int getEpisode() {
		return episode;
	}
	public void setEpisode(int episode) {
		this.episode = episode;
	}
	public int getNumber() {
		return number;
	}
	public void setNumber(int number) {
		this.number = number;
	}
	public int getTvdb_id() {
		return tvdb_id;
	}
	public void setTvdb_id(int tvdb_id) {
		this.tvdb_id = tvdb_id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getOverview() {
		return overview;
	}
	public void setOverview(String overview) {
		this.overview = overview;
	}
	public long getFirst_aired() {
		return first_aired;
	}
	public void setFirst_aired(long first_aired) {
		this.first_aired = first_aired;
	}
	public String getFirst_aired_iso() {
		return first_aired_iso;
	}
	public void setFirst_aired_iso(String first_aired_iso) {
		this.first_aired_iso = first_aired_iso;
	}
	public long getFirst_aired_utc() {
		return first_aired_utc;
	}
	public void setFirst_aired_utc(long first_aired_utc) {
		this.first_aired_utc = first_aired_utc;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getScreen() {
		return screen;
	}
	public void setScreen(String screen) {
		this.screen = screen;
	}
	public RatingsDTO getRatings() {
		return ratings;
	}
	public void setRatings(RatingsDTO ratings) {
		this.ratings = ratings;
	}
	public boolean isWatched() {
		return watched;
	}
	public void setWatched(boolean watched) {
		this.watched = watched;
	}
	public boolean isIn_collection() {
		return in_collection;
	}
	public void setIn_collection(boolean in_collection) {
		this.in_collection = in_collection;
	}
	public boolean isIn_watchlist() {
		return in_watchlist;
	}
	public void setIn_watchlist(boolean in_watchlist) {
		this.in_watchlist = in_watchlist;
	}
	public String getRating() {
		return rating;
	}
	public void setRating(String rating) {
		this.rating = rating;
	}
	public int getRating_advanced() {
		return rating_advanced;
	}
	public void setRating_advanced(int rating_advanced) {
		this.rating_advanced = rating_advanced;
	}
	
}
