package com.szoldapps.tvtrackr.exception;

public class FailedAuthentificationException extends Exception {

	private static final long serialVersionUID = 9101895775185458783L;

	public FailedAuthentificationException(String message) { 
        super(message);
    }

}
