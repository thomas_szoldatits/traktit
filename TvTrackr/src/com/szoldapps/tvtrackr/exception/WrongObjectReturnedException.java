package com.szoldapps.tvtrackr.exception;

public class WrongObjectReturnedException extends Exception {

	private static final long serialVersionUID = 9101895775185458783L;

	/**
	 * Creates {@link WrongObjectReturnedException} -> message 'Expected: cls.getName()'
	 * @param cls ... expected Class
	 */
	public WrongObjectReturnedException(Class<?> cls) { 
        super("Expected: "+cls.getName());
    }

	/**
	 * Creates {@link WrongObjectReturnedException} -> message 'Expected: msg'<br/>
	 * ONLY USE for List<?> and similar things where you can't get the class
	 * @param msg
	 */
	public WrongObjectReturnedException(String msg) {
		super("Expected: "+msg);
	}
	
	

}
