package com.szoldapps.tvtrackr.exception;

public class IntentExtraException extends Exception {

	private static final long serialVersionUID = 9101895775185458783L;

	public IntentExtraException(String message) { 
        super(message);
    }

}
