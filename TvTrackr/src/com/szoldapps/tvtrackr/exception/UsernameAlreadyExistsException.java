package com.szoldapps.tvtrackr.exception;

public class UsernameAlreadyExistsException extends Exception {

	private static final long serialVersionUID = 9101895775185458783L;

	public UsernameAlreadyExistsException(String message) { 
        super(message);
    }

}
