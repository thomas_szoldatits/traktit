package com.szoldapps.tvtrackr.exception;

public class EmailAlreadyExistsException extends Exception {

	private static final long serialVersionUID = 9101895775185458783L;

	public EmailAlreadyExistsException(String message) { 
        super(message);
    }

}
