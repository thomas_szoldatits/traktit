package com.szoldapps.tvtrackr.exception;

public class RestException extends Exception {

    private static final long serialVersionUID = -6568300698557568529L;

    public RestException(String message) { 
        super(message);
    }
    
}
