package com.szoldapps.tvtrackr.exception;

import com.szoldapps.tvtrackr.db.model.Episode;
import com.szoldapps.tvtrackr.db.model.User;

public class EpisodeUserNotFoundException extends Exception {

	private static final long serialVersionUID = 9101895775185458783L;

	public EpisodeUserNotFoundException(String message) { 
        super(message);
    }
	
	public EpisodeUserNotFoundException(Episode episode, User user) {
		super("EpisodeUser (EpisodeId:"+episode.getId()+", User:"+user.getUsername()+" not found");
	}
}
