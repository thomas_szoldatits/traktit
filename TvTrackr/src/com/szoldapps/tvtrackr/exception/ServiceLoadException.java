package com.szoldapps.tvtrackr.exception;

public class ServiceLoadException extends Exception {

    private static final long serialVersionUID = -6568300698557568529L;

    public ServiceLoadException(String message) { 
        super(message);
    }
    
}
