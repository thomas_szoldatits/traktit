package com.szoldapps.tvtrackr.exception;


public class NotFoundInDbException extends Exception {

	private static final long serialVersionUID = 9101895775185458783L;

	/**
	 * Creates {@link NotFoundInDbException} -> message 'The following object was NOT found in the DB: msg'<br/>
	 * @param msg
	 */
	public NotFoundInDbException(String msg) {
		super("The following object was NOT found in the DB: "+msg);
	}
	
}
