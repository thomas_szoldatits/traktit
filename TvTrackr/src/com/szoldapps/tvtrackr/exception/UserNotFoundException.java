package com.szoldapps.tvtrackr.exception;

public class UserNotFoundException extends Exception {

    private static final long serialVersionUID = -3705406412607379194L;
    
    public UserNotFoundException(String message) { 
        super(message);
    }

}
