package com.szoldapps.tvtrackr.exception;

public class NoValidDownloadRequestTypeException extends Exception {

	private static final long serialVersionUID = 9101895775185458783L;

	public NoValidDownloadRequestTypeException(String message) { 
        super(message);
    }

}
