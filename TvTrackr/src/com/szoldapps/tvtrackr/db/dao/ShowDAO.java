package com.szoldapps.tvtrackr.db.dao;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Callable;

import android.util.Log;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.stmt.PreparedQuery;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.support.ConnectionSource;
import com.szoldapps.tvtrackr.db.model.Actor;
import com.szoldapps.tvtrackr.db.model.ActorShow;
import com.szoldapps.tvtrackr.db.model.Episode;
import com.szoldapps.tvtrackr.db.model.EpisodeUser;
import com.szoldapps.tvtrackr.db.model.Genre;
import com.szoldapps.tvtrackr.db.model.GenreShow;
import com.szoldapps.tvtrackr.db.model.Rating;
import com.szoldapps.tvtrackr.db.model.Season;
import com.szoldapps.tvtrackr.db.model.Show;
import com.szoldapps.tvtrackr.db.model.Show.ShowDownloadStatus;
import com.szoldapps.tvtrackr.db.model.ShowUser;
import com.szoldapps.tvtrackr.db.model.User;
import com.szoldapps.tvtrackr.dto.show.ActorDTO;
import com.szoldapps.tvtrackr.dto.show.CompleteShowDTO;
import com.szoldapps.tvtrackr.dto.show.EpisodeDTO;
import com.szoldapps.tvtrackr.dto.show.LibraryShowDTO;
import com.szoldapps.tvtrackr.dto.show.MinShowDTO;
import com.szoldapps.tvtrackr.dto.show.SearchShowDTO;
import com.szoldapps.tvtrackr.dto.show.SeasonDTO;
import com.szoldapps.tvtrackr.dto.show.SeasonLibraryShowDTO;
import com.szoldapps.tvtrackr.dto.show.WatchlistShowDTO;
import com.szoldapps.tvtrackr.helper.DateHelper;
import com.szoldapps.tvtrackr.helper.LogCatHelper;

public class ShowDAO extends BaseDaoImpl<Show, Integer>{

	public static final String LOGTAG = LogCatHelper.getAppTag()+ShowDAO.class.getSimpleName().toString();

	// needed for BaseDaoImpl
	public ShowDAO(ConnectionSource connectionSource, Class<Show> dataClass) throws SQLException {
		super(connectionSource, dataClass);
	}

	public void deleteAll() throws SQLException {
		this.delete(this.queryForAll());
	}

	public Show getShowBySlug(String slug) throws SQLException {
		List<Show> showList = this.queryForEq("slug", slug);
		if(!showList.isEmpty()) {
			return showList.get(0);
		}
		return null;
	}


	public List<Show> getAllShowsOfUser(User mUser, ShowUserDAO showUserDAO) throws SQLException {
		QueryBuilder<ShowUser, Integer> jqb = showUserDAO.queryBuilder();
		jqb.where().eq(User.USERNAME, mUser.getUsername());
		PreparedQuery<Show> pq = queryBuilder().join(jqb).orderBy(Show.TITLE, true).prepare();
		return query(pq);
	}

	public Show createOrUpdateCompleteShow(
			CompleteShowDTO showDTO, boolean followedByUser, User user, ActorDAO actorDAO, ActorShowDAO actorShowDAO,
			GenreDAO genreDAO, GenreShowDAO genreShowDAO, RatingDAO ratingDAO, SeasonDAO seasonDAO, 
			EpisodeDAO episodeDAO, EpisodeUserDAO episodeUserDAO, ShowUserDAO showUserDAO) throws SQLException {
		List<CompleteShowDTO> showDTOs = new ArrayList<CompleteShowDTO>();
		showDTOs.add(showDTO);
		ArrayList<Show> shows = this.createOrUpdateCompleteShows(showDTOs, followedByUser, user, actorDAO, actorShowDAO, genreDAO, genreShowDAO, ratingDAO, seasonDAO, episodeDAO, episodeUserDAO, showUserDAO);
		if(!shows.isEmpty()) {
			return shows.get(0);
		}
		return null;
	}

	private Show mShow;
	private ArrayList<Show> mShows;
	public ArrayList<Show> createOrUpdateCompleteShows(
			final List<CompleteShowDTO> showDTOs, final boolean followedByUser, final User user, final ActorDAO actorDAO, final ActorShowDAO actorShowDAO,
			final GenreDAO genreDAO, final GenreShowDAO genreShowDAO, final RatingDAO ratingDAO, final SeasonDAO seasonDAO, 
			final EpisodeDAO episodeDAO, final EpisodeUserDAO episodeUserDAO, final ShowUserDAO showUserDAO) throws SQLException {
		callBatchTasks(
				new Callable<Void>() {
					public Void call() throws SQLException {
						mShows = new ArrayList<Show>();
						for (CompleteShowDTO showDTO : showDTOs) {
							createOrUpdate(Show.getShow(showDTO));
							mShow = queryForId(showDTO.getTvdb_id());

							// Actors
							if(showDTO.getPeople() != null) {
								List<ActorDTO> actorDTOs = showDTO.getPeople().getActors();
								for (ActorDTO actorDTO : actorDTOs) {
									Actor actor = new Actor().getActor(actorDTO);
									actorDAO.createOrUpdate(actor);
									ActorShow actorShow = actorShowDAO.getActorShow(actor, mShow);
									if(actorShow == null) {
										actorShow = new ActorShow(actor, mShow);
									}
									actorShow.setCharacter(actorDTO.getCharacter());
									actorShowDAO.createOrUpdate(actorShow);
								}
							}
							// Genres
							if(showDTO.getGenres() != null) {
								for (String genreName : showDTO.getGenres()) {
									Genre genre = genreDAO.createIfNotExists(new Genre(genreName));
									GenreShow genreShow = genreShowDAO.getGenreShow(genre, mShow);
									if(genreShow == null) {
										genreShow = new GenreShow(genre, mShow);
									}
									genreShowDAO.createOrUpdate(genreShow);
								}
							}

							// Create a showUser relation, only if show is followed by User
							if(followedByUser) {
								//- ShowUser
								showUserDAO.createOrUpdateShowUser(user, mShow, showDTO.isIn_watchlist());
							} else {
								showUserDAO.unfollowShowInDB(user, mShow.getTvdbID(), episodeUserDAO, episodeDAO);
							}
							
							//- personal ratings
							if(showDTO.getRating() != null) {
								Rating rating = ratingDAO.getShowRating(mShow, user);
								if(!showDTO.getRating().equals("false")) {
									if(rating == null) {
										rating = new Rating();
										rating.setShow(mShow);
										rating.setUser(user);
									}
									rating.setRating(showDTO.getRating());
									rating.setRatingAdvanced(showDTO.getRating_advanced());
									ratingDAO.createOrUpdate(rating);
								} else {
									if(rating != null) {
										ratingDAO.delete(rating);
									}
								}
							}
							
							if(showDTO.getSeasons() != null) {
								for (SeasonDTO seasonDTO : showDTO.getSeasons()) {
									Season season = seasonDAO.getSeason(seasonDTO.getSeasonNr(), mShow);
									if(season == null) {
										season = new Season().getSeason(seasonDTO, mShow);
									}
									seasonDAO.createOrUpdate(season);
									for (EpisodeDTO eDTO : seasonDTO.getEpisodes()) {
										createOrUpdateEpisode(eDTO, season, followedByUser, user, episodeDAO, episodeUserDAO, ratingDAO);
									}
								}
								// All Seasons and Episodes have been saved, so isFullyDownloaded = true
								mShow.setFullyDownloaded(true);
							}
							Log.e(LOGTAG, "show isfullyloaded="+mShow.isFullyDownloaded());
							mShow.setDownloadStatus(ShowDownloadStatus.DONE);
							mShow.setLastDBSave(DateHelper.getCurrentTime());
							update(mShow);
							mShows.add(mShow);
						}
						return null;
					}
				});
		return mShows;
	}
	
	public void createOrUpdateEpisode(EpisodeDTO eDTO, Season season, boolean followedByUser, User user, 
			EpisodeDAO episodeDAO, EpisodeUserDAO episodeUserDAO, RatingDAO ratingDAO) throws SQLException {
		Episode episode = episodeDAO.getEpisode(eDTO, season);
		if(episode == null) {
			episode = new Episode().getEpisode(eDTO, season);
		} 

		episodeDAO.createOrUpdate(episode);

		// Create EpisodeUser relation only, if followed by User
		if(followedByUser) {
			//- episodeUsers
			EpisodeUser eu = episodeUserDAO.getEpisodeUser(episode, user);
			if(eu == null) {
				eu = new EpisodeUser();
				eu.setEpisode(episode);
				eu.setUser(user);
			}
			if(eDTO.isWatched()) {
				eu.setWatched(eDTO.isWatched());
			} else {
				eu.setWatched(false);
			}
			eu.setOfflineMarked(false);
			episodeUserDAO.createOrUpdate(eu);

			//- personal ratings
			Rating rating = ratingDAO.getEpisodeRating(episode, user);
			if(!eDTO.getRating().equals("false")) {
				if(rating == null) {
					rating = new Rating();
					rating.setEpisode(episode);
					rating.setShow(mShow);
					rating.setUser(user);
				}
				rating.setRating(eDTO.getRating());
				rating.setRatingAdvanced(eDTO.getRating_advanced());
				ratingDAO.createOrUpdate(rating);
			} else {
				if(rating != null) {
					ratingDAO.delete(rating);
				}
			}
		} // else -> Unfollowing is already taken care of in ShowUser part
	}

	public Show createOrUpdate(MinShowDTO minShowDTO, User user, ShowUserDAO showUserDAO, EpisodeUserDAO episodeUserDAO) throws SQLException {
		Show show = createIfNotExists(new Show(minShowDTO.getTvdb_id()));
		show.setTitle(minShowDTO.getTitle());
		show.setYear(minShowDTO.getYear());
		show.setImdbID(minShowDTO.getImdb_id());
		show.setTvrageID(minShowDTO.getTvrage_id());
		show.setUrl(minShowDTO.getUrl());
		// Images
		show.setPosterURL(minShowDTO.getImages().getPoster());
		show.setFanartURL(minShowDTO.getImages().getFanart());
		show.setBannerURL(minShowDTO.getImages().getBanner());
		// Genres are missing, but not needed anyway. They are saved during completeShowSave

		boolean inWatchlist = false;
		if(minShowDTO instanceof WatchlistShowDTO || minShowDTO instanceof SearchShowDTO) {
			inWatchlist = true;
		}

		// ShowUser
		showUserDAO.createOrUpdateShowUser(user, show, inWatchlist);

		show.setDownloadStatus(ShowDownloadStatus.WAITING);

		update(show);
		return show;
	}

	public List<Show> createOrUpdate(final List<MinShowDTO> minShowDTOs, final User user, final ShowUserDAO showUserDAO, final EpisodeUserDAO episodeUserDAO) throws SQLException {
		final ArrayList<Show> shows = new ArrayList<Show>();
		callBatchTasks(
				new Callable<Void>() {
					public Void call() throws SQLException {
						for (MinShowDTO minShowDTO : minShowDTOs) {
							shows.add(createOrUpdate(minShowDTO, user, showUserDAO, episodeUserDAO));
						}
						return null;
					}
				});
		return shows;
	}

	public ArrayList<Show> updateAllShowUserData(final List<LibraryShowDTO> libShowDTOs, final User user, final ShowUserDAO showUserDAO, final EpisodeUserDAO episodeUserDAO) throws SQLException {
		callBatchTasks(
				new Callable<Void>() {
					public Void call() throws SQLException {
						mShows = new ArrayList<Show>();
						for (LibraryShowDTO libShowDTO : libShowDTOs) {
							mShow = queryForId(libShowDTO.getTvdb_id());
							if(mShow != null) {
								// Seasons 
								if(mShow.getSeasons() != null && libShowDTO.getSeasons() != null) {
									for (Season season : mShow.getSeasons()) {
										SeasonLibraryShowDTO tempLibSeason = null;
										for (SeasonLibraryShowDTO libSeason : libShowDTO.getSeasons()) {
											if(season.getSeasonNr().equals(libSeason.getSeason())) {
												tempLibSeason = libSeason;
												break;
											}
										}
										if(tempLibSeason != null) {
											for (Episode episode : season.getEpisodes()) {
												EpisodeUser eu = episodeUserDAO.getEpisodeUser(episode, user);
												if(eu == null) {
													eu = new EpisodeUser(episode, user, false);
												}
												if(tempLibSeason.getEpisodes().contains(episode.getNumber())) {
													eu.setWatched(true);
												} else {
													eu.setWatched(false);
												}
												eu.setOfflineMarked(false);
												episodeUserDAO.createOrUpdate(eu);
											}
											libShowDTO.getSeasons().remove(tempLibSeason);
										}
									}
								}
								update(mShow);
								mShows.add(mShow);
							}
						}
						return null;
					}
				});
		return mShows;
	}


	public List<Show> getToWatchShows(User user, EpisodeUserDAO episodeUserDAO, SeasonDAO seasonDAO, EpisodeDAO episodeDAO) throws SQLException {

		QueryBuilder<EpisodeUser, Integer> joinedQueryBuilder = episodeUserDAO.queryBuilder();
		joinedQueryBuilder.where().eq(User.USERNAME, user.getUsername()).and().eq(EpisodeUser.WATCHED, false);

		Calendar now = Calendar.getInstance();
		now.setTime(new Date());
		long timeNow = now.getTimeInMillis()/1000;
		QueryBuilder<Episode, Integer> jqbEpisode = episodeDAO.queryBuilder();
		jqbEpisode.join(joinedQueryBuilder).orderBy(Show.TVDB_ID, true).orderBy(Episode.FIRST_AIRED_UTC, true).where().le(Episode.FIRST_AIRED_UTC, timeNow);

		QueryBuilder<Season, Integer> jqbSeason = seasonDAO.queryBuilder();
		jqbSeason.join(jqbEpisode).where().ne(Season.SEASON_NR, 0);
		return query(queryBuilder().distinct().join(jqbSeason).prepare());
	}

	/**
	 * Uses {@link EpisodeDAO#getAllEpisodesOrderedBySeasonDesc(int, SeasonDAO)} to get an Episode List.
	 * Then runs through this list to create new String List (which is not very efficient -> SHOULD BE OPTIMIZED)
	 * @param tvdbId
	 * @param seasonDAO
	 * @param episodeDAO
	 * @return
	 * @throws SQLException
	 */
	public List<String> getEpisodesTabNameList(int tvdbId, SeasonDAO seasonDAO, EpisodeDAO episodeDAO) throws SQLException {
		List<Episode> episodes = episodeDAO.getAllEpisodesOrderedBySeasonDesc(tvdbId, seasonDAO);
		List<String> tabNameList = new ArrayList<String>();
		for (Episode episode : episodes) {
			tabNameList.add(episode.getSeasonEpisodeText());
		}
		return tabNameList;
	}

}

// once needed, maybe still useful

//public long getEpisodeCount(int tvdbId, SeasonDAO seasonDAO, EpisodeDAO episodeDAO) throws SQLException {
//QueryBuilder<Season, Integer> jqbSeason = seasonDAO.queryBuilder();
//jqbSeason.join(this.queryBuilder()).where().eq(Show.TVDB_ID, tvdbId);
//PreparedQuery<Episode> query =  episodeDAO.queryBuilder().setCountOf(true).join(jqbSeason).prepare();
//long count = episodeDAO.countOf(query);
//return count;
//}



