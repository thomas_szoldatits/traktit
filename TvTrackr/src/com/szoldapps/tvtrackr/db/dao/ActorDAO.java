package com.szoldapps.tvtrackr.db.dao;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.j256.ormlite.support.ConnectionSource;
import com.szoldapps.tvtrackr.db.model.Actor;
import com.szoldapps.tvtrackr.db.model.ActorShow;

public class ActorDAO extends BaseDaoImpl<Actor, String>{

	// needed for BaseDaoImpl
	public ActorDAO(ConnectionSource connectionSource, Class<Actor> dataClass) throws SQLException {
		super(connectionSource, dataClass);
	}

	public void deleteUnused(ActorShowDAO actorShowDAO) throws SQLException {
		List<ActorShow> allActorShows = actorShowDAO.queryForAll();
		List<String> allSlugs = new ArrayList<String>();
		for (ActorShow actorShow : allActorShows) {
			allSlugs.add(actorShow.getActor().getSlug());
		}
		DeleteBuilder<Actor, String> deleteBuilder = deleteBuilder();
		deleteBuilder.where().notIn(Actor.SLUG, allSlugs);
		deleteBuilder.delete();
	}

}
