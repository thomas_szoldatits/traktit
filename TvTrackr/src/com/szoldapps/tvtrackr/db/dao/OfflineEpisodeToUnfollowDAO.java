package com.szoldapps.tvtrackr.db.dao;

import java.sql.SQLException;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.support.ConnectionSource;
import com.szoldapps.tvtrackr.db.model.OfflineEpisodeToUnfollow;
import com.szoldapps.tvtrackr.helper.LogCatHelper;

public class OfflineEpisodeToUnfollowDAO extends BaseDaoImpl<OfflineEpisodeToUnfollow, Integer>{

	public static final String LOGTAG = LogCatHelper.getAppTag()+OfflineEpisodeToUnfollowDAO.class.getSimpleName().toString();

	// needed for BaseDaoImpl
	public OfflineEpisodeToUnfollowDAO(ConnectionSource connectionSource, Class<OfflineEpisodeToUnfollow> dataClass) throws SQLException {
		super(connectionSource, dataClass);
	}

}
