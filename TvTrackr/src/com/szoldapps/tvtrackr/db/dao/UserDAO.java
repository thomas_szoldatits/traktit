package com.szoldapps.tvtrackr.db.dao;

import java.sql.SQLException;
import java.util.List;

import com.szoldapps.tvtrackr.db.model.User;
import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.support.ConnectionSource;

public class UserDAO extends BaseDaoImpl<User, String>{

    // needed for BaseDaoImpl
    public UserDAO(ConnectionSource connectionSource, Class<User> dataClass) throws SQLException {
        super(connectionSource, dataClass);
    }

    /**
     * Returns the User with the given username;
     * @param username
     * @return
     * @throws SQLException
     */
    public User getUserByUsername(String username) throws SQLException {
        List<User> userList = this.queryForEq("username", username);
        if(!userList.isEmpty()) {
            return userList.get(0);
        }
        return null;
    }

    /**
     * Checks whether @User already exists in DB. <br>
     * If yes -> moves flag to the desired {@link User} and updates Password.  <br>
     * If no -> creates new {@link User} in DB and sets currentUser true
     * @param user
     * @return updated user
     * @throws SQLException
     */
    public User setCurrentUser(User u) throws SQLException {
        User user = this.getUserByUsername(u.getUsername());
        if(user == null) {
            user = new User(u.getUsername(), u.getPassword(), true);
            this.create(user);
        } else if(!user.isCurrentUser()) {
            this.resetCurrentUserFlag();
            user.setPasswordHashed(u.getPassword());
            user.setCurrentUser(true);
            this.update(user);
        }
        return user;
    }
    
    /**
     * Returns the currentUser from the DB.
     * @return
     * @throws SQLException - if more than 1 currentUser exists or other SQLExceptions
     */
    public User getCurrentUser() throws SQLException {
        return queryForFirst(queryBuilder().where().eq("isCurrentUser", true).prepare());
    }
    
    public User resetCurrentUserFlag() throws SQLException {
        User user = this.getCurrentUser();   
        if(user != null) {
            user.setCurrentUser(false);
            this.update(user);
        }
        return user;
    }

}
