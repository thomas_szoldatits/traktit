package com.szoldapps.tvtrackr.db.dao;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.j256.ormlite.support.ConnectionSource;
import com.szoldapps.tvtrackr.db.model.Genre;
import com.szoldapps.tvtrackr.db.model.GenreShow;

public class GenreDAO extends BaseDaoImpl<Genre, String>{

    // needed for BaseDaoImpl
    public GenreDAO(ConnectionSource connectionSource, Class<Genre> dataClass) throws SQLException {
        super(connectionSource, dataClass);
    }

    public Genre getGenreByName(String name) throws SQLException {
        List<Genre> genreList = this.queryForEq("name", name);
        if(!genreList.isEmpty()) {
            return genreList.get(0);
        }
        return null;
    }

	public void deleteUnused(GenreShowDAO genreShowDAO) throws SQLException {
		List<GenreShow> allGenreShows = genreShowDAO.queryForAll();
		List<String> allSlugs = new ArrayList<String>();
		for (GenreShow genreShow : allGenreShows) {
			allSlugs.add(genreShow.getGenre().getSlug());
		}
		DeleteBuilder<Genre, String> deleteBuilder = deleteBuilder();
		deleteBuilder.where().notIn(Genre.SLUG, allSlugs);
		deleteBuilder.delete();
	}

}
