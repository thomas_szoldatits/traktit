package com.szoldapps.tvtrackr.db.dao;

import java.sql.SQLException;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.j256.ormlite.support.ConnectionSource;
import com.szoldapps.tvtrackr.db.model.Season;
import com.szoldapps.tvtrackr.db.model.Show;

public class SeasonDAO extends BaseDaoImpl<Season, Integer>{

    // needed for BaseDaoImpl
    public SeasonDAO(ConnectionSource connectionSource, Class<Season> dataClass) throws SQLException {
        super(connectionSource, dataClass);
    }

    public Season getSeason(Integer seasonNr, int showTvdbId) throws SQLException {
        return queryForFirst(queryBuilder().where().eq(Show.TVDB_ID, showTvdbId).and().eq(Season.SEASON_NR, seasonNr).prepare());
    }
    
    public Season getSeason(Integer seasonNr, Show show) throws SQLException {
        return getSeason(seasonNr, show.getTvdbID());
    }

	public void deleteForShow(int tvdbId) throws SQLException {
		DeleteBuilder<Season, Integer> deleteBuilder = deleteBuilder();
		deleteBuilder.where().eq(Show.TVDB_ID, tvdbId);
		deleteBuilder.delete();
	}


    //    public User getUserByUsername(String username) throws SQLException {
    //        List<User> userList = this.queryForEq("username", username);
    //        if(!userList.isEmpty()) {
    //            return userList.get(0);
    //        }
    //        return null;
    //    }
    
}
