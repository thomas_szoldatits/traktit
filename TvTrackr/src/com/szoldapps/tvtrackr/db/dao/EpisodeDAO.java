package com.szoldapps.tvtrackr.db.dao;

import java.sql.SQLException;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.j256.ormlite.stmt.PreparedQuery;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.support.ConnectionSource;
import com.szoldapps.tvtrackr.db.model.Episode;
import com.szoldapps.tvtrackr.db.model.EpisodeUser;
import com.szoldapps.tvtrackr.db.model.Season;
import com.szoldapps.tvtrackr.db.model.Show;
import com.szoldapps.tvtrackr.db.model.User;
import com.szoldapps.tvtrackr.dto.show.EpisodeDTO;
import com.szoldapps.tvtrackr.helper.DateHelper;
import com.szoldapps.tvtrackr.helper.LogCatHelper;

public class EpisodeDAO extends BaseDaoImpl<Episode, Integer> {

	public static final String LOGTAG = LogCatHelper.getAppTag()+EpisodeDAO.class.getSimpleName().toString();

	// needed for BaseDaoImpl
	public EpisodeDAO(ConnectionSource connectionSource, Class<Episode> dataClass) throws SQLException {
		super(connectionSource, dataClass);
	}

	public void deleteAllEpisodesOfShow(int tvdbId) throws SQLException {
		DeleteBuilder<Episode, Integer> deleteBuilder = deleteBuilder();
		deleteBuilder.where().eq(Show.TVDB_ID, tvdbId);
		deleteBuilder.delete();
	}

	public Episode getEpisode(EpisodeDTO episodeDTO, Season season) throws SQLException {
		return queryForFirst(queryBuilder().where().eq(Show.TVDB_ID, season.getShow().getTvdbID()).and().eq(Season.SEASON_ID, season.getId()).and().eq(Episode.NUMBER, episodeDTO.getNumber()).prepare());
	}

	public List<Episode> getEpisodesSeenByUser(Integer seasonID, User u, EpisodeUserDAO episodeUserDAO) throws SQLException {
		QueryBuilder<EpisodeUser, Integer> joinedQueryBuilder = episodeUserDAO.queryBuilder();
		joinedQueryBuilder.where().eq(User.USERNAME, u.getUsername()).and().eq(EpisodeUser.WATCHED, true);
		PreparedQuery<Episode> pq = queryBuilder().join(joinedQueryBuilder).where().eq(Season.SEASON_ID, seasonID).prepare();

		return query(pq);
	}

	public List<Episode> getAllEpisodesOrderedBySeasonDesc(int showTvdbId, SeasonDAO seasonDAO) throws SQLException {
		QueryBuilder<Season, Integer> joinedQueryBuilder = seasonDAO.queryBuilder();
		joinedQueryBuilder.where().eq(Show.TVDB_ID, showTvdbId);
		PreparedQuery<Episode> pq = queryBuilder().orderByRaw("'"+Season.TABLE_NAME+"'.'"+Season.SEASON_NR+"' DESC, '"+Episode.TABLE_NAME+"'.'"+Episode.NUMBER+"' DESC").join(joinedQueryBuilder).prepare();
		//SELECT `Episode`.* FROM `Episode` INNER JOIN `Season` ON `Episode`.`season_id` = `Season`.`id` WHERE `Season`.`show_id` = 1 ORDER BY `Episode`.`firstAiredUTC` DESC ,`Season`.`season` DESC 
		//        List<Episode> episodes = this.query(pq);
		//        for (Episode episode : episodes) {
		//            Log.v(LOGTAG, episode.getSeasonEpisodeText() + ", " + episode.getFirstAiredUTC());
		//        }
		return this.query(pq); //episodes;
	}

	public Episode getEpisodeByPosition(int position, int showTvdbId, SeasonDAO seasonDAO) throws SQLException {
		List<Episode> episodes = this.getAllEpisodesOrderedBySeasonDesc(showTvdbId, seasonDAO);
		if(episodes.size() > position){
			return episodes.get(position);
		}
		return null;
	}

	public int getPositionByEpisodeId(int episodeId, int showTvdbId, SeasonDAO seasonDAO) throws SQLException {
		List<Episode> episodes = this.getAllEpisodesOrderedBySeasonDesc(showTvdbId, seasonDAO);
		int counter = 0;
		for (Episode episode : episodes) {
			if(episode.getId().equals(episodeId)) {
				return counter;
			}
			counter++;
		}
		return -1;
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// ----- EPISODE COUNT ------------------------------------------------------------------------------------------ //
	/**
	 * Returns the number of Episodes related to the <code>season</code>.
	 * @param season
	 * @return
	 * @throws SQLException
	 */
	public long getEpisodeCount(Season season) throws SQLException {
		PreparedQuery<Episode> query = 
				queryBuilder().setCountOf(true).where().eq(Season.SEASON_ID, season.getId()).prepare();
		return countOf(query);
	}
	
	/**
	 * Returns episode count of <code>show</code>.
	 * 
	 * @param show
	 * @param seasonDAO
	 * @return
	 * @throws SQLException
	 */
	public long getEpisodeCountWithoutSpecialSeason(Show show, SeasonDAO seasonDAO) throws SQLException {
		QueryBuilder<Season, Integer> jqbSeason = seasonDAO.queryBuilder();
		jqbSeason.where().eq(Show.TVDB_ID, show.getTvdbID()).and().ne(Season.SEASON_NR, 0);
		return countOf(queryBuilder().setCountOf(true).join(jqbSeason).prepare());
	}

	public long getEpisodeSeenCountOfSeason(Season season, User user, EpisodeUserDAO episodeUserDAO) throws SQLException {
		QueryBuilder<EpisodeUser, Integer> joinedQueryBuilder = episodeUserDAO.queryBuilder();
		joinedQueryBuilder.where().eq(User.USERNAME, user.getUsername()).and().eq(EpisodeUser.WATCHED, true);
		return countOf(queryBuilder().setCountOf(true).join(joinedQueryBuilder).where().eq(Season.SEASON_ID, season.getId()).prepare());
	}

	public long getEpisodeSeenCountOfShow(Show show, User user, EpisodeUserDAO episodeUserDAO) throws SQLException {
		QueryBuilder<EpisodeUser, Integer> joinedQueryBuilder = episodeUserDAO.queryBuilder();
		joinedQueryBuilder.where().eq(User.USERNAME, user.getUsername()).and().eq(EpisodeUser.WATCHED, true);
		return countOf(queryBuilder().setCountOf(true).join(joinedQueryBuilder).where().eq(Show.TVDB_ID, show.getTvdbID()).prepare());
	}

	/**
	 * Returns <b>aired</b> episode count of <code>show</code>.
	 * 
	 * @param show
	 * @param seasonDAO
	 * @return
	 * @throws SQLException
	 */
	public long getAiredEpisodeCountWithoutSpecialSeason(Show show, SeasonDAO seasonDAO) throws SQLException {
		QueryBuilder<Season, Integer> jqbSeason = seasonDAO.queryBuilder();
		jqbSeason.where().eq(Show.TVDB_ID, show.getTvdbID()).and().ne(Season.SEASON_NR, 0);
		PreparedQuery<Episode> query = queryBuilder().setCountOf(true)
				.join(jqbSeason).where().le(Episode.FIRST_AIRED_UTC, DateHelper.getCurrentTimeInS()).prepare();
		return countOf(query);
	}

	/**
	 * Returns <b>aired</b> episode count of <code>season</code>.
	 * 
	 * @param show
	 * @param seasonDAO
	 * @return
	 * @throws SQLException
	 */
	public long getAiredEpisodeCount(Season season, SeasonDAO seasonDAO) throws SQLException {
		QueryBuilder<Season, Integer> jqbSeason = seasonDAO.queryBuilder();
		jqbSeason.where().eq(Show.TVDB_ID, season.getShow().getTvdbID()).and().eq(Season.SEASON_NR, season.getSeasonNr());
		PreparedQuery<Episode> query = queryBuilder().setCountOf(true)
				.join(jqbSeason).where().le(Episode.FIRST_AIRED_UTC, DateHelper.getCurrentTimeInS()).prepare();
		return countOf(query);
	}

	/**
	 * Returns <b>aired</b> episode count of <code>show</code> which the <code>user</code> has already seen.
	 * 
	 * @param show
	 * @param user
	 * @param seasonDAO
	 * @param episodeUserDAO
	 * @return
	 * @throws SQLException
	 */
	public long getSeenAiredEpisodeCountWithoutSpecialSeason(Show show, User user, SeasonDAO seasonDAO, EpisodeUserDAO episodeUserDAO) throws SQLException {
		QueryBuilder<Season, Integer> jqbSeason = seasonDAO.queryBuilder();
		jqbSeason.where().eq(Show.TVDB_ID, show.getTvdbID()).and().ne(Season.SEASON_NR, 0);

		QueryBuilder<EpisodeUser, Integer> jqbEpisodeUser = episodeUserDAO.queryBuilder();
		jqbEpisodeUser.where().eq(User.USERNAME, user.getUsername()).and().eq("watched", true);

		QueryBuilder<Episode, Integer> jqbEpisode = queryBuilder().join(jqbEpisodeUser);

		PreparedQuery<Episode> query = jqbEpisode.setCountOf(true).join(jqbSeason)
				.where().le(Episode.FIRST_AIRED_UTC, DateHelper.getCurrentTimeInS()).prepare();
		return countOf(query);
	}

	/**
	 * Returns <b>aired</b> episode count of <code>show</code> which the <code>user</code> has already seen.
	 * 
	 * @param show
	 * @param user
	 * @param seasonDAO
	 * @param episodeUserDAO
	 * @return
	 * @throws SQLException
	 */
	public long getSeenAiredEpisodeCount(Season season, User user, SeasonDAO seasonDAO, EpisodeUserDAO episodeUserDAO) throws SQLException {
		QueryBuilder<Season, Integer> jqbSeason = seasonDAO.queryBuilder();
		jqbSeason.where().eq(Show.TVDB_ID, season.getShow().getTvdbID())
		.and().eq(Season.SEASON_NR, season.getSeasonNr());

		QueryBuilder<EpisodeUser, Integer> jqbEpisodeUser = episodeUserDAO.queryBuilder();
		jqbEpisodeUser.where().eq(User.USERNAME, user.getUsername()).and().eq("watched", true);

		QueryBuilder<Episode, Integer> jqbEpisode = queryBuilder().join(jqbEpisodeUser);

		PreparedQuery<Episode> query = jqbEpisode.setCountOf(true).join(jqbSeason)
				.where().le(Episode.FIRST_AIRED_UTC, DateHelper.getCurrentTimeInS()).prepare();
		return countOf(query);
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// ----- CALENDAR ----------------------------------------------------------------------------------------------- //

	/**
	 * TODO: JAVADOC
	 * 
	 * @param offset
	 * @param user
	 * @param episodeUserDAO
	 * @return
	 * @throws SQLException
	 */
	public List<Episode> getEpisodesOfDay(int offset, User user, EpisodeUserDAO episodeUserDAO) throws SQLException {
		long timeStart = getTimeStart(offset);
		long timeEnd = getTimeEnd(offset);
		QueryBuilder<EpisodeUser, Integer> jqb = episodeUserDAO.queryBuilder();
		jqb.where().eq(User.USERNAME, user.getUsername());
		return query(queryBuilder().join(jqb).orderBy(Episode.FIRST_AIRED_UTC, false).where().ge(Episode.FIRST_AIRED_UTC, timeStart).and().le(Episode.FIRST_AIRED_UTC, timeEnd).prepare());
	}

	/**
	 * Returns a count ({@link Long}) of all unwatched Episodes of the day (offset=0 equals today, offset=1 equals tomorrow, etc.)
	 * of <code>user</code>
	 * @param offset
	 * @param user
	 * @param episodeUserDAO
	 * @return
	 * @throws SQLException
	 */
	public Long getUnwatchedEpisodeCountOfDay(int offset, User user, EpisodeUserDAO episodeUserDAO) throws SQLException {
		long timeStart = getTimeStart(offset);
		long timeEnd = getTimeEnd(offset);
		QueryBuilder<EpisodeUser, Integer> jqb = episodeUserDAO.queryBuilder();
		jqb.where().eq(User.USERNAME, user.getUsername()).and().eq(EpisodeUser.WATCHED, false);
		PreparedQuery<Episode> qb = queryBuilder().setCountOf(true)
				.join(jqb)
				.orderBy(Episode.FIRST_AIRED_UTC, false)
				.where().ge(Episode.FIRST_AIRED_UTC, timeStart)
				.and().le(Episode.FIRST_AIRED_UTC, timeEnd)
				.prepare();
		return countOf(qb);
	}

	/**
	 * Returns {@link Long} of 00:00:00 in seconds of today (if offset=0, tomorrow if offset=1, etc.)
	 * @param offset
	 * @return
	 */
	private long getTimeStart(int offset) {
		Calendar now = Calendar.getInstance();
		now.setTime(new Date());
		now.add(Calendar.DAY_OF_MONTH, offset);
		now.set(Calendar.HOUR_OF_DAY, 0);
		now.set(Calendar.MINUTE, 0);
		now.set(Calendar.SECOND, 0);
		return now.getTimeInMillis()/1000;	// because Trakt.tv saves only 10 digits (no ms)
	}
	/**
	 * Returns {@link Long} of 23:59:59 in seconds of today (if offset=0, tomorrow if offset=1, etc.)
	 * @param offset
	 * @return
	 */
	private long getTimeEnd(int offset) {
		Calendar now = Calendar.getInstance();
		now.setTime(new Date());
		now.add(Calendar.DAY_OF_MONTH, offset);
		now.set(Calendar.HOUR_OF_DAY, 23);
		now.set(Calendar.MINUTE, 59);
		now.set(Calendar.SECOND, 59);
		return now.getTimeInMillis()/1000;	// because Trakt.tv saves only 10 digits (no ms)
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// TO WATCH ----------------------------------------------------------------------------------------------------------------------------------------------------- //

	/**
	 * Returns {@link Integer} count of all Episodes left to watch for <code>user</code> of a specific {@link Show} with <code>showTvdbId</code>
	 * @param showTvdbId
	 * @param user
	 * @param episodeUserDAO
	 * @param seasonDAO
	 * @return
	 * @throws SQLException
	 */
	public Integer getEpisodesToWatchCountOfShow(int showTvdbId, User user, EpisodeUserDAO episodeUserDAO, SeasonDAO seasonDAO) throws SQLException {
		QueryBuilder<Season, Integer> jqbSeason = seasonDAO.queryBuilder();
		jqbSeason.where().ne(Season.SEASON_NR, 0);

		QueryBuilder<EpisodeUser, Integer> joinedQueryBuilder = episodeUserDAO.queryBuilder();
		joinedQueryBuilder.where().eq(User.USERNAME, user.getUsername()).and().eq(EpisodeUser.WATCHED, false);

		Calendar now = Calendar.getInstance();
		now.setTime(new Date());
		long timeNow = now.getTimeInMillis()/1000;
		PreparedQuery<Episode> query = queryBuilder().setCountOf(true).join(joinedQueryBuilder).join(jqbSeason).where().eq(Show.TVDB_ID, showTvdbId).and().le(Episode.FIRST_AIRED_UTC, timeNow).prepare();
		return (int) countOf(query);
	}

	/**
	 * Returns {@link Long} count of all Episodes left to watch for <code>user</code>
	 * @param user
	 * @param episodeUserDAO
	 * @param seasonDAO
	 * @return
	 * @throws SQLException
	 */
	public Long getAllEpisodesToWatchCount(User user, EpisodeUserDAO episodeUserDAO, SeasonDAO seasonDAO) throws SQLException {
		QueryBuilder<Season, Integer> jqbSeason = seasonDAO.queryBuilder();
		jqbSeason.where().ne(Season.SEASON_NR, 0);

		QueryBuilder<EpisodeUser, Integer> joinedQueryBuilder = episodeUserDAO.queryBuilder();
		joinedQueryBuilder.where().eq(User.USERNAME, user.getUsername()).and().eq(EpisodeUser.WATCHED, false);

		Calendar now = Calendar.getInstance();
		now.setTime(new Date());
		long timeNow = now.getTimeInMillis()/1000;
		PreparedQuery<Episode> query = queryBuilder().setCountOf(true).join(joinedQueryBuilder).join(jqbSeason).where().le(Episode.FIRST_AIRED_UTC, timeNow).prepare();
		return countOf(query);
	}

	public Episode getNextEpisodeToWatch(int showTvdbId, User user, EpisodeUserDAO episodeUserDAO, SeasonDAO seasonDAO) throws SQLException {
		QueryBuilder<Season, Integer> jqbSeason = seasonDAO.queryBuilder();
		jqbSeason.where().ne(Season.SEASON_NR, 0);

		QueryBuilder<EpisodeUser, Integer> joinedQueryBuilder = episodeUserDAO.queryBuilder();
		joinedQueryBuilder.where().eq(User.USERNAME, user.getUsername()).and().eq(EpisodeUser.WATCHED, false);

		Calendar now = Calendar.getInstance();
		now.setTime(new Date());
		long timeNow = now.getTimeInMillis()/1000;
		return queryForFirst(queryBuilder().join(joinedQueryBuilder).join(jqbSeason).orderBy(Show.TVDB_ID, true).orderBy(Episode.FIRST_AIRED_UTC, true).where().eq(Show.TVDB_ID, showTvdbId).and().le(Episode.FIRST_AIRED_UTC, timeNow).prepare());
	}

	public List<Episode> getAllEpisodesSeenOrUnseenByUser(boolean isSeen, User u, EpisodeUserDAO episodeUserDAO, SeasonDAO seasonDAO, ShowDAO showDAO) throws SQLException {
		QueryBuilder<Season, Integer> jqbSeason = seasonDAO.queryBuilder();
		jqbSeason.where().ne(Season.SEASON_NR, 0);

		QueryBuilder<EpisodeUser, Integer> joinedQueryBuilder = episodeUserDAO.queryBuilder();
		joinedQueryBuilder.where().eq(User.USERNAME, u.getUsername()).and().eq(EpisodeUser.WATCHED, isSeen);

		Calendar now = Calendar.getInstance();
		now.setTime(new Date());
		long timeNow = now.getTimeInMillis()/1000;
		return query(queryBuilder().join(joinedQueryBuilder).join(jqbSeason).orderBy(Show.TVDB_ID, true).orderBy(Episode.FIRST_AIRED_UTC, true).where().le(Episode.FIRST_AIRED_UTC, timeNow).prepare());
	}



}
