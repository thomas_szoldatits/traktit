package com.szoldapps.tvtrackr.db.dao;

import java.sql.SQLException;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.j256.ormlite.stmt.PreparedQuery;
import com.j256.ormlite.support.ConnectionSource;
import com.szoldapps.tvtrackr.db.model.Episode;
import com.szoldapps.tvtrackr.db.model.Rating;
import com.szoldapps.tvtrackr.db.model.Show;
import com.szoldapps.tvtrackr.db.model.User;

public class RatingDAO extends BaseDaoImpl<Rating, Integer>{

	// needed for BaseDaoImpl
	public RatingDAO(ConnectionSource connectionSource, Class<Rating> dataClass) throws SQLException {
		super(connectionSource, dataClass);
	}

    public Rating getEpisodeRating(Episode episode, User user) throws SQLException {
        PreparedQuery<Rating> pq = queryBuilder().where().eq(Episode.EPISODE_ID, episode.getId()).and().eq(User.USERNAME, user.getUsername()).prepare();
        return this.queryForFirst(pq);
    }

    public Rating getShowRating(Show show, User user) throws SQLException {
        PreparedQuery<Rating> pq = queryBuilder().where().eq(Show.TVDB_ID, show.getTvdbID()).and().eq(User.USERNAME, user.getUsername()).and().isNull(Episode.EPISODE_ID).prepare();
        return this.queryForFirst(pq);
    }

	public void deleteForUser(User user, int tvdbId) throws SQLException {
		DeleteBuilder<Rating, Integer> deleteBuilder = deleteBuilder();
		deleteBuilder.where().eq(Show.TVDB_ID, tvdbId).and().eq(User.USERNAME, user.getUsername());
		deleteBuilder.delete();
	}

}
