package com.szoldapps.tvtrackr.db.dao;

import java.sql.SQLException;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.j256.ormlite.support.ConnectionSource;
import com.szoldapps.tvtrackr.db.model.Genre;
import com.szoldapps.tvtrackr.db.model.GenreShow;
import com.szoldapps.tvtrackr.db.model.Show;

public class GenreShowDAO extends BaseDaoImpl<GenreShow, Integer>{

	// needed for BaseDaoImpl
	public GenreShowDAO(ConnectionSource connectionSource, Class<GenreShow> dataClass) throws SQLException {
		super(connectionSource, dataClass);
	}

	public GenreShow getGenreShow(Genre genre, Show show) throws SQLException {
		return queryForFirst(queryBuilder().where().eq(Genre.SLUG, genre.getSlug()).and().eq(Show.TVDB_ID, show.getTvdbID()).prepare());
	}

	public void deleteForShow(int tvdbId) throws SQLException {
		DeleteBuilder<GenreShow, Integer> deleteBuilder = deleteBuilder();
		deleteBuilder.where().eq(Show.TVDB_ID, tvdbId);
		deleteBuilder.delete();
	}

}
