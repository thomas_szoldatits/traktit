package com.szoldapps.tvtrackr.db.dao;

import java.sql.SQLException;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.support.ConnectionSource;
import com.szoldapps.tvtrackr.db.model.OfflineShowToUnfollow;
import com.szoldapps.tvtrackr.helper.LogCatHelper;

public class OfflineShowToUnfollowDAO extends BaseDaoImpl<OfflineShowToUnfollow, Integer>{

	public static final String LOGTAG = LogCatHelper.getAppTag()+OfflineShowToUnfollowDAO.class.getSimpleName().toString();

	// needed for BaseDaoImpl
	public OfflineShowToUnfollowDAO(ConnectionSource connectionSource, Class<OfflineShowToUnfollow> dataClass) throws SQLException {
		super(connectionSource, dataClass);
	}

}
