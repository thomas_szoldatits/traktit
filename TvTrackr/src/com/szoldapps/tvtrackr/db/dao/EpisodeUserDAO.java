package com.szoldapps.tvtrackr.db.dao;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.Callable;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.dao.ForeignCollection;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.support.ConnectionSource;
import com.szoldapps.tvtrackr.db.model.Episode;
import com.szoldapps.tvtrackr.db.model.EpisodeUser;
import com.szoldapps.tvtrackr.db.model.Season;
import com.szoldapps.tvtrackr.db.model.Show;
import com.szoldapps.tvtrackr.db.model.User;
import com.szoldapps.tvtrackr.exception.EpisodeUserNotFoundException;
import com.szoldapps.tvtrackr.helper.DateHelper;

public class EpisodeUserDAO extends BaseDaoImpl<EpisodeUser, Integer>{

	// needed for BaseDaoImpl
	public EpisodeUserDAO(ConnectionSource connectionSource, Class<EpisodeUser> dataClass) throws SQLException {
		super(connectionSource, dataClass);
	}

	public EpisodeUser getEpisodeUser(Episode episode, User user) throws SQLException {
		return queryForFirst(queryBuilder().where().eq(Episode.EPISODE_ID, episode.getId()).and().eq(User.USERNAME, user.getUsername()).prepare());
	}

	protected void deleteForUser(int showTvdbId, User user, EpisodeDAO episodeDAO) throws SQLException {
		QueryBuilder<Episode, Integer> episodeQueryBuilder = episodeDAO.queryBuilder();
		episodeQueryBuilder.where().eq(Show.TVDB_ID, showTvdbId);
		DeleteBuilder<EpisodeUser, Integer> deleteBuilder = deleteBuilder();
		deleteBuilder.where().eq(User.USERNAME, user.getUsername()).and().in(Episode.EPISODE_ID, episodeQueryBuilder.selectColumns(Episode.EPISODE_ID));
		deleteBuilder.delete();
	}

	/**
	 * returns all Offline Marked Episodes of {@link User} <code>user</code>
	 * @param user
	 * @param episodeDAO
	 * @return
	 * @throws SQLException
	 */
	public List<Episode> getAllOfflineMarkedEpisodes(User user, EpisodeDAO episodeDAO) throws SQLException {
		QueryBuilder<EpisodeUser, Integer> euQB = queryBuilder();
		euQB.where().eq(User.USERNAME, user.getUsername()).and().eq(EpisodeUser.OFFLINE_MARKED, true);
		List<Episode> list = episodeDAO.query(episodeDAO.queryBuilder().join(euQB.orderBy(EpisodeUser.WATCHED, true)).orderBy(Show.TVDB_ID, true).prepare());
		//		for (Episode episode : list) {
		//			EpisodeUser eu = getEpisodeUser(episode, user);
		//			Log.e("xxxx", episode.getSeasonEpisodeText() + ": " + episode.getTitle() + ", watched="+eu.isWatched());
		//		}
		return list;
	}

	public HashMap<Show, List<Episode>> getAllOfflineMarkedEpisodesShowMap(User user, EpisodeDAO episodeDAO, ShowDAO showDAO) throws SQLException {
		HashMap<Show, List<Episode>> retMap = new HashMap<Show, List<Episode>>();
		List<Episode> offlineMarkedEpisodes = getAllOfflineMarkedEpisodes(user, episodeDAO);
		int offlineMarkedEpisodesSize = offlineMarkedEpisodes.size();
		if(offlineMarkedEpisodes == null || offlineMarkedEpisodesSize == 0) { 
			return retMap; 
		}

		Show tempShow = null;
		Episode tempEpisode = offlineMarkedEpisodes.get(0);
		Integer curShowTvdbId = tempEpisode.getShowTvdbId();
		List<Episode> tempEpisodeList = new ArrayList<Episode>();
		boolean tempWatched = getEpisodeUser(tempEpisode, user).isWatched();
		for (int i = 0; i < offlineMarkedEpisodesSize; i++) {
			Episode episode = offlineMarkedEpisodes.get(i);
			EpisodeUser eu = getEpisodeUser(episode, user);

			if(!episode.getShowTvdbId().equals(curShowTvdbId) || tempWatched != eu.isWatched()) {	// Show change -> store Show + EpisodeList in Map
				tempShow = showDAO.queryForId(curShowTvdbId);
				retMap.put(tempShow, tempEpisodeList);

				// reset tempEpisodeList
				tempEpisodeList = new ArrayList<Episode>();
				tempWatched = eu.isWatched();
				curShowTvdbId = episode.getShowTvdbId();

			}

			tempEpisodeList.add(episode);
			if(i == (offlineMarkedEpisodesSize-1)) {	// LAST Episode -> store Show + EpisodeList in Map
				tempShow = showDAO.queryForId(curShowTvdbId);
				retMap.put(tempShow, tempEpisodeList);
			}
		}
		return retMap;
	}



	/**
	 * Creates (if they do not already exist) {@link EpisodeUser} relations for all {@link Episode} of 
	 * <code>show</code> and <code>user</code>.
	 * 
	 * @param user
	 * @param show
	 * @throws SQLException
	 */
	public void createOrUpdateEpisodeUsers(final User user, final Show show) throws SQLException {
		callBatchTasks(
				new Callable<Void>() {
					public Void call() throws SQLException, EpisodeUserNotFoundException {
						ForeignCollection<Season> seasons = show.getSeasons();
						if(seasons == null) { return null; }

						for (Season season : seasons) {
							ForeignCollection<Episode> episodes = season.getEpisodes();
							if(episodes == null) { continue; }

							for (Episode episode : episodes) {
								EpisodeUser eu = getEpisodeUser(episode, user);
								if(eu == null) {
									create(new EpisodeUser(episode, user, false));
								}
							}
						}
						return null;
					}
				});
	}

	/**
	 * If <code>markAsSeen</code>=TRUE -> Marks all <b>aired</b> Episodes of <code>show</code> seen for <code>user</code>.
	 * <b>(except Season 0, like on trakt.tv)</b><br>
	 * If <code>markAsSeen</code>=FALSE -> Marks all Episodes of <code>show</code> unseen for <code>user</code>.
	 * 
	 * @param markAsSeen
	 * @param show
	 * @param user
	 * @throws SQLException
	 */
	public void markShowAsSeenIfAiredOrUnseen(final boolean markAsSeen, final boolean offlineMarked, final Show show, final User user) throws SQLException {
		callBatchTasks(
				new Callable<Void>() {
					public Void call() throws SQLException, EpisodeUserNotFoundException {
						ForeignCollection<Season> seasons = show.getSeasons();
						if(seasons == null) { return null; }

						for (Season season : seasons) {
							if(season.getSeasonNr().equals(0)) { continue; }	// Season 0 is also left out by trakt.

							ForeignCollection<Episode> episodes = season.getEpisodes();
							if(episodes == null) { continue; }

							for (Episode episode : episodes) {
								markEpisodeAsSeenIfAiredOrUnseen(markAsSeen, offlineMarked, episode, user);
							}
						}
						return null;
					}
				});
	}

	/**
	 * If <code>markAsSeen</code>=TRUE -> Marks all <b>aired</b> Episodes of <code>season</code> seen for <code>user</code>.
	 * If <code>markAsSeen</code>=FALSE -> Marks all Episodes of <code>season</code> unseen for <code>user</code>.
	 * 
	 * @param markAsSeen
	 * @param show
	 * @param user
	 * @throws SQLException
	 */
	public void markSeasonAsSeenIfAiredOrUnseen(final boolean markAsSeen, final boolean offlineMarked, final Season season, final User user) throws SQLException {
		callBatchTasks(
				new Callable<Void>() {
					public Void call() throws SQLException, EpisodeUserNotFoundException {

						ForeignCollection<Episode> episodes = season.getEpisodes();
						if(episodes == null) { return null; }

						for (Episode episode : episodes) {
							markEpisodeAsSeenIfAiredOrUnseen(markAsSeen, offlineMarked, episode, user);
						}
						return null;
					}
				});
	}

	/**
	 * If <code>markAsSeen</code>=TRUE -> Marks Episode as seen for <code>user</code> (only if already <b>aired</b>).
	 * If <code>markAsSeen</code>=FALSE -> Marks Episode as unseen for <code>user</code>.
	 * 
	 * @param markAsSeen
	 * @param episode
	 * @param user
	 * @throws SQLException
	 */
	private void markEpisodeAsSeenIfAiredOrUnseen(boolean markAsSeen, final boolean offlineMarked, Episode episode, User user) throws SQLException {
		if(markAsSeen) {
			if(DateHelper.hasBeenAired(episode.getFirstAiredUTC())) {
				markEpisodeAsSeenOrUnseen(markAsSeen, offlineMarked, episode, user);
			} 
		} else {
			markEpisodeAsSeenOrUnseen(markAsSeen, offlineMarked, episode, user);
		}
	}

	/**
	 * Marks Episode as seen or unseen (according to <code>markAsSeen</code>) for <code>user</code> and marks Episode
	 * offline or not.
	 * 
	 * @param markAsSeen
	 * @param episode
	 * @param user
	 * @throws SQLException
	 */
	private void markEpisodeAsSeenOrUnseen(boolean markAsSeen, final boolean offlineMarked, Episode episode, User user) throws SQLException {
		EpisodeUser eu = getEpisodeUser(episode, user);
		if(eu != null) {
			eu.setWatched(markAsSeen);
			eu.setOfflineMarked(offlineMarked);
			update(eu);
		}
	}


	/**
	 * Marks Episodes as seen or unseen (according to <code>markAsSeen</code>) for <code>user</code> and marks Episodes
	 * offline or not.
	 * 
	 * @param episodes
	 * @param markAsSeen
	 * @param offlineMarked
	 * @param user
	 * @throws SQLException
	 */
	public void markEpisodesAsSeenOrUnseen(final List<Episode> episodes, final boolean markAsSeen, 
			final boolean offlineMarked, final User user) throws SQLException {
		callBatchTasks(
				new Callable<Void>() {
					public Void call() throws SQLException {
						for (Episode episode : episodes) {
							markEpisodeAsSeenOrUnseen(markAsSeen, offlineMarked, episode, user);
						}
						return null;
					}
				});
	}


}
