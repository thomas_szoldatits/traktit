package com.szoldapps.tvtrackr.db.dao;

import java.sql.SQLException;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.j256.ormlite.support.ConnectionSource;
import com.szoldapps.tvtrackr.db.model.Actor;
import com.szoldapps.tvtrackr.db.model.ActorShow;
import com.szoldapps.tvtrackr.db.model.Show;

public class ActorShowDAO extends BaseDaoImpl<ActorShow, Integer>{

	// needed for BaseDaoImpl
	public ActorShowDAO(ConnectionSource connectionSource, Class<ActorShow> dataClass) throws SQLException {
		super(connectionSource, dataClass);
	}

	public ActorShow getActorShow(Actor actor, Show show) throws SQLException {
		return queryForFirst(queryBuilder().where().eq(Actor.SLUG, actor.getSlug()).and().eq(Show.TVDB_ID, show.getTvdbID()).prepare());
	}

	public void deleteForShow(int tvdbId) throws SQLException {
		DeleteBuilder<ActorShow, Integer> deleteBuilder = deleteBuilder();
		deleteBuilder.where().eq(Show.TVDB_ID, tvdbId);
		deleteBuilder.delete();
	}

}
