package com.szoldapps.tvtrackr.db.dao;

import java.sql.SQLException;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.j256.ormlite.stmt.PreparedQuery;
import com.j256.ormlite.support.ConnectionSource;
import com.szoldapps.tvtrackr.db.model.EpisodeUser;
import com.szoldapps.tvtrackr.db.model.Show;
import com.szoldapps.tvtrackr.db.model.ShowUser;
import com.szoldapps.tvtrackr.db.model.User;

public class ShowUserDAO extends BaseDaoImpl<ShowUser, Integer>{

	// needed for BaseDaoImpl
	public ShowUserDAO(ConnectionSource connectionSource, Class<ShowUser> dataClass) throws SQLException {
		super(connectionSource, dataClass);
	}

	public ShowUser getShowUser(User user, int tvdbId) throws SQLException {
		PreparedQuery<ShowUser> query = queryBuilder().where().eq(Show.TVDB_ID, tvdbId).and().eq(User.USERNAME, user.getUsername()).prepare();
		return queryForFirst(query);
	}
	
	public ShowUser getShowUser(User user, Show show) throws SQLException {
		return this.getShowUser(user, show.getTvdbID());
	}
	
	public ShowUser createOrUpdateShowUser(User user, Show show, boolean isInWatchlist) throws SQLException {
		ShowUser showUser = getShowUser(user, show);
		if(showUser == null) {
			showUser = new ShowUser(show, user, isInWatchlist);
			create(showUser);
		} else {
			showUser.setInWatchlist(isInWatchlist);
			update(showUser);
		}
		return showUser;
	}
	
	private void deleteForUser(User user, int tvdbId) throws SQLException {
		DeleteBuilder<ShowUser, Integer> deleteBuilder = deleteBuilder();
		deleteBuilder.where().eq(Show.TVDB_ID, tvdbId).and().eq(User.USERNAME, user.getUsername());
		deleteBuilder.delete();
	}
	
	public boolean hasOtherUsers(int tvdbId) throws SQLException {
		long count = countOf(queryBuilder().setCountOf(true).where().eq(Show.TVDB_ID, tvdbId).prepare());
		if(count > 0) {
			return true;
		}
		return false;
	}

	/**
	 * Checks whether {@link ShowUser} object between <code>user</code> and <code>show</code> exists in DB
	 * @param user
	 * @param show
	 * @return ... TRUE if {@link ShowUser} exists; FALSE if {@link ShowUser} does NOT exist
	 * @throws SQLException
	 */
	public boolean isShowFollowedByUser(User user, Show show) throws SQLException {
		return isShowFollowedByUser(user, show.getTvdbID());
	}
	
	/**
	 * Checks whether {@link ShowUser} object between <code>user</code> and <code>show</code> exists in DB
	 * @param user
	 * @param tvdbId
	 * @return ... TRUE if {@link ShowUser} exists; FALSE if {@link ShowUser} does NOT exist
	 * @throws SQLException
	 */
	public boolean isShowFollowedByUser(User user, int tvdbId) throws SQLException {
		if(getShowUser(user, tvdbId) != null) {
			return true;
		}
		return false;
	}

	/**
	 * Unfollows Show (with <code>tvdbId</code>). Removes all {@link EpisodeUser} & {@link ShowUser} relations from DB
	 * @param user
	 * @param tvdbId
	 * @param episodeUserDAO
	 * @param episodeDAO
	 * @throws SQLException
	 */
	public void unfollowShowInDB(User user, int tvdbId, EpisodeUserDAO episodeUserDAO, EpisodeDAO episodeDAO) throws SQLException {
		// Delete all EpisodeUser relations
		episodeUserDAO.deleteForUser(tvdbId, user, episodeDAO);
		// Delete ShowUser relation
		deleteForUser(user, tvdbId);
	}
	
}
