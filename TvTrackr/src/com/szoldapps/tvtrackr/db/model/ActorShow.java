package com.szoldapps.tvtrackr.db.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import com.szoldapps.tvtrackr.db.dao.ActorShowDAO;

@DatabaseTable(tableName = ActorShow.TABLE_NAME, daoClass = ActorShowDAO.class)
public class ActorShow {

	public static final String TABLE_NAME = "ActorShow";
	
	@DatabaseField(generatedId = true)
	private Integer id;

	@DatabaseField(foreign = true, columnName = Actor.SLUG, foreignAutoRefresh = true)
	private Actor actor;
	
	@DatabaseField(foreign = true, columnName = Show.TVDB_ID, foreignAutoRefresh = true)
    private Show show;
	
	@DatabaseField
	private String character;

	/** ======================================================================================== **/
	
	public ActorShow() { /* ORMLite requirement */ }
	
	public ActorShow(Actor actor, Show show, String character) {
	    this.actor = actor;
	    this.show = show;
	    this.character = character;
	}
	
	public ActorShow(Actor actor, Show show) {
	    this.actor = actor;
	    this.show = show;
	}

	/// ======================================================================================== ///

	public Integer getId() {
	    return id;
	}

	public Actor getActor() {
        return actor;
    }

    public void setActor(Actor actor) {
        this.actor = actor;
    }

    public Show getShow() {
        return show;
    }

    public void setShow(Show show) {
        this.show = show;
    }

    public String getCharacter() {
        return character;
    }

    public void setCharacter(String character) {
        this.character = character;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////

    @Override
    public String toString() {
    	return "ActorShow [id=" + id + ", actor=" + actor.getSlug() + ", show=" + show.getTvdbID() + ", character=" + character + "]";
    }
}