package com.szoldapps.tvtrackr.db.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import com.szoldapps.tvtrackr.db.dao.RatingDAO;

@DatabaseTable(tableName = Rating.TABLE_NAME, daoClass = RatingDAO.class)
public class Rating {

	public static final String TABLE_NAME = "Rating";
	
    @DatabaseField(generatedId = true)
    private Integer id;

    @DatabaseField(foreign = true, columnName = User.USERNAME, foreignAutoRefresh = true)
    private User user;

    @DatabaseField(foreign = true, columnName = Show.TVDB_ID, foreignAutoRefresh = true, canBeNull = false)
    private Show show;

    @DatabaseField(foreign = true, columnName = Episode.EPISODE_ID, foreignAutoRefresh = true, canBeNull = true)
    private Episode episode;

    @DatabaseField
    private String rating;

    @DatabaseField
    private Integer ratingAdvanced;


    /** ======================================================================================== **/

    public Rating() { /* ORMLite requirement */ }

    public Rating(User user, Show show, Episode episode, String rating, Integer ratingAdvanced) {
        super();
        this.user = user;
        this.show = show;
        this.episode = episode;
        this.rating = rating;
        this.ratingAdvanced = ratingAdvanced;
    }

    /// ======================================================================================== ///

    public Integer getId() {
        return id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Show getShow() {
        return show;
    }

    public void setShow(Show show) {
        this.show = show;
    }

    public Episode getEpisode() {
        return episode;
    }

    public void setEpisode(Episode episode) {
        this.episode = episode;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public Integer getRatingAdvanced() {
        return ratingAdvanced;
    }

    public void setRatingAdvanced(Integer ratingAdvanced) {
        this.ratingAdvanced = ratingAdvanced;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////

    @Override
    public String toString() {
        return "Rating [id=" + id + ", username=" + user.getUsername() + ", tvdbID=" + show.getTvdbID() + ", episodeID=" + episode.getId()
                + ", rating=" + rating + ", ratingAdvanced=" + ratingAdvanced + "]";
    }

}