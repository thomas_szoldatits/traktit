package com.szoldapps.tvtrackr.db.model;

import com.j256.ormlite.dao.ForeignCollection;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;
import com.szoldapps.tvtrackr.db.dao.OfflineShowToUnfollowDAO;

@DatabaseTable(tableName = OfflineShowToUnfollow.TABLE_NAME, daoClass = OfflineShowToUnfollowDAO.class)
public class OfflineShowToUnfollow {

	public static final String TABLE_NAME = "Offline_Show_To_Unfollow";
	public static final String TVDB_ID = "show_tvdb_id";
	public static final String EPISODES = "episodes";

	@DatabaseField(id=true, columnName = TVDB_ID)
	private Integer tvdbID;

	@ForeignCollectionField (eager = true, columnName = EPISODES)
	ForeignCollection<OfflineEpisodeToUnfollow> episodes;

	public OfflineShowToUnfollow() { /* ORMLite requirement */ }

	public OfflineShowToUnfollow(Integer tvdbID) {
		super();
		this.tvdbID = tvdbID;
	}

	public Integer getTvdbID() {
		return tvdbID;
	}
	public void setTvdbID(Integer tvdbID) {
		this.tvdbID = tvdbID;
	}
	public ForeignCollection<OfflineEpisodeToUnfollow> getEpisodes() {
		return episodes;
	}
	public void setEpisodes(ForeignCollection<OfflineEpisodeToUnfollow> episodes) {
		this.episodes = episodes;
	}
}
