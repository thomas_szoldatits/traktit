package com.szoldapps.tvtrackr.db.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import com.szoldapps.tvtrackr.db.dao.GenreShowDAO;

@DatabaseTable(tableName = GenreShow.TABLE_NAME, daoClass = GenreShowDAO.class)
public class GenreShow {

	public static final String TABLE_NAME = "GenreShow";
	
	@DatabaseField(generatedId = true)
	private Integer id;

	@DatabaseField(foreign = true, columnName = Genre.SLUG, foreignAutoRefresh = true)
	private Genre genre;
	
	@DatabaseField(foreign = true, columnName = Show.TVDB_ID, foreignAutoRefresh = true)
    private Show show;

	/** ======================================================================================== **/
	
	public GenreShow() { /* ORMLite requirement */ }
	
	public GenreShow(Genre genre, Show show) {
	    this.genre = genre;
	    this.show = show;
	}

	/// ======================================================================================== ///

	public Integer getId() {
	    return id;
	}

	public Genre getGenre() {
        return genre;
    }

    public void setGenre(Genre genre) {
        this.genre = genre;
    }

    public Show getShow() {
        return show;
    }

    public void setShow(Show show) {
        this.show = show;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    
    @Override
    public String toString() {
        return "GenreShow[slug:"+genre.getSlug()+" | tvdbID:"+show.getTvdbID()+"]";
    }

}