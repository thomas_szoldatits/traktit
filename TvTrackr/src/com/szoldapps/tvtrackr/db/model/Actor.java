package com.szoldapps.tvtrackr.db.model;

import java.util.Locale;

import com.j256.ormlite.dao.ForeignCollection;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.misc.BaseDaoEnabled;
import com.j256.ormlite.table.DatabaseTable;

import com.szoldapps.tvtrackr.db.dao.ActorDAO;
import com.szoldapps.tvtrackr.dto.show.ActorDTO;

@DatabaseTable(tableName = Actor.TABLE_NAME, daoClass = ActorDAO.class)
public class Actor extends BaseDaoEnabled<Actor, Integer> {

	public static final String TABLE_NAME = "Actor";
	public static final String SLUG = "slug";
	
	@DatabaseField(id = true, columnName = SLUG)
	private String slug;
	
	@DatabaseField
	private String name;
	
	@DatabaseField
    private String url;
	
	@DatabaseField
    private String biography;
	
	@DatabaseField
    private String birthday;
	
	@DatabaseField
    private String birthplace;
	
	@DatabaseField
    private Integer tmd_id;
	
	@DatabaseField
    private String headshot;
	
	@DatabaseField
	private boolean isFullyLoaded = false;
	
    @ForeignCollectionField(eager = false)
    ForeignCollection<ActorShow> actorShows;

	/** ======================================================================================== **/
	
	public Actor() { /* ORMLite requirement */ }

	public Actor(String name, String url, String biography, String birthday, String birthplace, Integer tmd_id, String headshot) {
	    super();
	    this.setName(name);
	    this.url = url;
	    this.biography = biography;
	    this.birthday = birthday;
	    this.birthplace = birthplace;
	    this.tmd_id = tmd_id;
	    this.headshot = headshot;
	}

	/// ======================================================================================== ///

	public String getSlug() {
		return slug;
	}
	
    public String setSlug() {
        String slug = name.replace("'", "");
        slug = slug.toLowerCase(Locale.getDefault());
        slug = slug.replace(" ", "-");
        return slug;
    }
    
	public String getName() {
		return name;
	}

	public void setName(String name) {
		if(name == null) {
    		name = "Unknown Actor";
    	}
		this.name = name;
		this.slug = this.setSlug();
	}
	
	public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getBiography() {
        return biography;
    }

    public void setBiography(String biography) {
        this.biography = biography;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getBirthplace() {
        return birthplace;
    }

    public void setBirthplace(String birthplace) {
        this.birthplace = birthplace;
    }

    public Integer getTmd_id() {
        return tmd_id;
    }

    public void setTmd_id(Integer tmd_id) {
        this.tmd_id = tmd_id;
    }

    public String getHeadshot() {
        return headshot;
    }

    public void setHeadshot(String headshot) {
        this.headshot = headshot;
    }

    public ForeignCollection<ActorShow> getActorShows() {
        return actorShows;
    }

    public void setActorShows(ForeignCollection<ActorShow> actorShows) {
        this.actorShows = actorShows;
    }

    public boolean isFullyLoaded() {
        return isFullyLoaded;
    }

    public void setFullyLoaded(boolean isFullyLoaded) {
        this.isFullyLoaded = isFullyLoaded;
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////////
    
    @Override
    public String toString() {
        return "Actor [slug=" + slug + ", name=" + name + ", url=" + url + ", biography=" + biography
                + ", birthday=" + birthday + ", birthplace=" + birthplace + ", tmd_id=" + tmd_id
                + ", headshot=" + headshot + ", isFullyLoaded=" + isFullyLoaded + "]";
    }

    public Actor getActor(ActorDTO actorDTO) {
        this.setName(actorDTO.getName());
        this.setHeadshot(actorDTO.getImages().getHeadshot());
        return this;
    }

}