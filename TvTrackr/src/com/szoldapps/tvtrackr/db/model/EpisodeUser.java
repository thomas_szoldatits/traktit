package com.szoldapps.tvtrackr.db.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import com.szoldapps.tvtrackr.db.dao.EpisodeUserDAO;

@DatabaseTable(tableName = EpisodeUser.TABLE_NAME, daoClass = EpisodeUserDAO.class)
public class EpisodeUser {

	public static final String TABLE_NAME = "EpisodeUser";
	public static final String WATCHED = "watched";
	public static final String OFFLINE_MARKED = "offline_marked";
	
    @DatabaseField(generatedId = true)
    private Integer id;

    @DatabaseField(foreign = true, columnName = Episode.EPISODE_ID, foreignAutoRefresh = true)
    private Episode episode;

    @DatabaseField(foreign = true, columnName = User.USERNAME, foreignAutoRefresh = true)
    private User user;

    @DatabaseField(columnName = WATCHED)
    private Boolean watched;
    
    @DatabaseField(columnName = OFFLINE_MARKED)
    private boolean offlineMarked = false;
    

	/** ======================================================================================== **/

    public EpisodeUser() { /* ORMLite requirement */ }
    
    public EpisodeUser(Episode episode, User user, Boolean watched) {
        super();
        this.episode = episode;
        this.user = user;
        this.watched = watched;
    }

    /// ======================================================================================== ///

    public Integer getId() {
        return id;
    }


    public Episode getEpisode() {
        return episode;
    }


    public void setEpisode(Episode episode) {
        this.episode = episode;
    }


    public User getUser() {
        return user;
    }


    public void setUser(User user) {
        this.user = user;
    }


    public boolean isWatched() {
        return watched;
    }


    public void setWatched(boolean watched) {
        this.watched = watched;
    }
    
    public boolean isOfflineMarked() {
		return offlineMarked;
	}

	public void setOfflineMarked(boolean offlineMarked) {
		this.offlineMarked = offlineMarked;
	}
    
    ////////////////////////////////////////////////////////////////////////////////////////////////

    @Override
    public String toString() {
        return "EpisodeUser[episode_id:"+episode.getId()+" | username:"+user.getUsername()+" | watched:"+watched+"]";
    }
    
}