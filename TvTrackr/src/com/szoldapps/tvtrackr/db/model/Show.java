package com.szoldapps.tvtrackr.db.model;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;

import com.j256.ormlite.dao.ForeignCollection;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.misc.BaseDaoEnabled;
import com.j256.ormlite.table.DatabaseTable;
import com.szoldapps.tvtrackr.db.dao.ShowDAO;
import com.szoldapps.tvtrackr.dto.show.CompleteShowDTO;

@DatabaseTable(tableName = Show.TABLE_NAME, daoClass = ShowDAO.class)
public class Show extends BaseDaoEnabled<Show, Integer> {

	public static final String TABLE_NAME = "Show";
	public static final String TVDB_ID = "show_tvdb_id";
	public static final String TITLE = "title";

	@DatabaseField(id=true, columnName = TVDB_ID)
	private Integer tvdbID;

	@DatabaseField(columnName = TITLE)
	private String title;

	@DatabaseField
	private Integer year;

	@DatabaseField
	private String url;

	@DatabaseField
	private long firstAired;

	@DatabaseField
	private String firstAiredISO;

	@DatabaseField
	private long firstAiredUTC;

	@DatabaseField
	private String country;

	@DatabaseField
	private String overview;

	@DatabaseField
	private Integer runtime;

	@DatabaseField
	private String status;

	@DatabaseField
	private String network;

	@DatabaseField
	private String airDay;

	@DatabaseField
	private String airDayUTC;

	@DatabaseField
	private String airTime;

	@DatabaseField
	private String airTimeUTC;

	@DatabaseField
	private String certification;

	@DatabaseField
	private String imdbID;

	@DatabaseField
	private String tvrageID;

	@DatabaseField
	private long lastUpdated;
	
	@DatabaseField
	private long lastDBSave;

	@DatabaseField
	private String posterURL;

	@DatabaseField
	private String fanartURL;

	@DatabaseField
	private String bannerURL;

	@DatabaseField
	private int ratingsPercentage;

	@DatabaseField
	private long ratingsVotes;

	@DatabaseField
	private long ratingsLoved;

	@DatabaseField
	private long ratingsHated;

	@DatabaseField
	private long statsWatchers;

	@DatabaseField
	private long statsPlays;

	@DatabaseField
	private long statsLists;

	@DatabaseField
	private long statsCollected;
	
	@DatabaseField
	private ShowDownloadStatus downloadStatus = ShowDownloadStatus.WAITING;

	@DatabaseField
	private boolean isFullyDownloaded = false;
	
	@ForeignCollectionField(eager = false)
	ForeignCollection<Season> seasons;

	@ForeignCollectionField(eager = false)
	ForeignCollection<GenreShow> genreShows;

	@ForeignCollectionField(eager = false)
	ForeignCollection<ActorShow> actorShows;

	@ForeignCollectionField(eager = false)
	ForeignCollection<Rating> ratings;

	@ForeignCollectionField(eager = false)
	ForeignCollection<ShowUser> showUsers;

	
	public enum ShowDownloadStatus {
		WAITING, DOWNLOADING, SAVING, DONE, UPDATING;
	}
	
	
	/** ======================================================================================== **/

	public Show() { /* ORMLite requirement */ }

	public Show(int tvdbId){
		this.tvdbID = tvdbId;
	}

	/// ======================================================================================== ///

	public String getSlug() {
		if(url!=null) {
			int begin = url.lastIndexOf("/")+1;
			return url.substring(begin);
		}
		return null;
	}

	/// ======================================================================================== ///

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Integer getYear() {
		return year;
	}

	public void setYear(Integer year) {
		this.year = year;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public long getFirstAired() {
		return firstAired;
	}

	public void setFirstAired(long firstAired) {
		this.firstAired = firstAired;
	}

	public String getFirstAiredISO() {
		return firstAiredISO;
	}

	public void setFirstAiredISO(String firstAiredISO) {
		this.firstAiredISO = firstAiredISO;
	}

	public long getFirstAiredUTC() {
		return firstAiredUTC;
	}

	public void setFirstAiredUTC(long firstAiredUTC) {
		this.firstAiredUTC = firstAiredUTC;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getOverview() {
		return overview;
	}

	public void setOverview(String overview) {
		this.overview = overview;
	}

	public Integer getRuntime() {
		return runtime;
	}

	public void setRuntime(Integer runtime) {
		this.runtime = runtime;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getNetwork() {
		return network;
	}

	public void setNetwork(String network) {
		this.network = network;
	}

	public String getAirDay() {
		return airDay;
	}

	public void setAirDay(String airDay) {
		this.airDay = airDay;
	}

	public String getAirDayUTC() {
		return airDayUTC;
	}

	public void setAirDayUTC(String airDayUTC) {
		this.airDayUTC = airDayUTC;
	}

	public String getAirTime() {
		return airTime;
	}

	public void setAirTime(String airTime) {
		this.airTime = airTime;
	}

	public String getAirTimeUTC() {
		return airTimeUTC;
	}

	public void setAirTimeUTC(String airTimeUTC) {
		this.airTimeUTC = airTimeUTC;
	}

	public String getCertification() {
		return certification;
	}

	public void setCertification(String certification) {
		this.certification = certification;
	}

	public String getImdbID() {
		return imdbID;
	}

	public void setImdbID(String imdbID) {
		this.imdbID = imdbID;
	}

	public Integer getTvdbID() {
		return tvdbID;
	}

	public void setTvdbID(Integer tvdbID) {
		this.tvdbID = tvdbID;
	}

	public String getTvrageID() {
		return tvrageID;
	}

	public void setTvrageID(String tvrageID) {
		this.tvrageID = tvrageID;
	}

	public long getLastUpdated() {
		return lastUpdated;
	}

	public void setLastUpdated(long lastUpdated) {
		this.lastUpdated = lastUpdated;
	}

	public long getLastDBSave() {
		return lastDBSave;
	}

	public void setLastDBSave(long lastDBSave) {
		this.lastDBSave = lastDBSave;
	}

	public String getPosterURL() {
		return posterURL;
	}

	public void setPosterURL(String posterURL) {
		this.posterURL = posterURL;
	}

	public String getFanartURL() {
		return fanartURL;
	}

	public void setFanartURL(String fanartURL) {
		this.fanartURL = fanartURL;
	}

	public String getBannerURL() {
		return bannerURL;
	}

	public void setBannerURL(String bannerURL) {
		this.bannerURL = bannerURL;
	}

	public int getRatingsPercentage() {
		return ratingsPercentage;
	}

	public void setRatingsPercentage(int ratingsPercentage) {
		this.ratingsPercentage = ratingsPercentage;
	}

	public long getRatingsVotes() {
		return ratingsVotes;
	}

	public void setRatingsVotes(long ratingsVotes) {
		this.ratingsVotes = ratingsVotes;
	}

	public long getRatingsLoved() {
		return ratingsLoved;
	}

	public void setRatingsLoved(long ratingsLoved) {
		this.ratingsLoved = ratingsLoved;
	}

	public long getRatingsHated() {
		return ratingsHated;
	}

	public void setRatingsHated(long ratingsHated) {
		this.ratingsHated = ratingsHated;
	}

	public long getStatsWatchers() {
		return statsWatchers;
	}

	public void setStatsWatchers(long statsWatchers) {
		this.statsWatchers = statsWatchers;
	}

	public long getStatsPlays() {
		return statsPlays;
	}

	public void setStatsPlays(long statsPlays) {
		this.statsPlays = statsPlays;
	}

	public long getStatsLists() {
		return statsLists;
	}

	public void setStatsLists(long statsLists) {
		this.statsLists = statsLists;
	}

	public long getStatsCollected() {
		return statsCollected;
	}

	public void setStatsCollected(long statsCollection) {
		this.statsCollected = statsCollection;
	}

	public ShowDownloadStatus getDownloadStatus() {
		return downloadStatus;
	}

	public void setDownloadStatus(ShowDownloadStatus downloadStatus) {
		this.downloadStatus = downloadStatus;
	}

	public boolean isFullyDownloaded() {
		return isFullyDownloaded;
	}

	public void setFullyDownloaded(boolean isFullyDownloaded) {
		this.isFullyDownloaded = isFullyDownloaded;
	}

	public ForeignCollection<Season> getSeasons() {
		return seasons;
	}

	public void setSeasons(ForeignCollection<Season> seasons) {
		this.seasons = seasons;
	}

	public ForeignCollection<GenreShow> getGenreShows() {
		return genreShows;
	}

	public void setGenreShows(ForeignCollection<GenreShow> genreShows) {
		this.genreShows = genreShows;
	}

	public ForeignCollection<ActorShow> getActorShows() {
		return actorShows;
	}

	public void setActorShows(ForeignCollection<ActorShow> actorShows) {
		this.actorShows = actorShows;
	}

	public ForeignCollection<Rating> getRatings() {
		return ratings;
	}

	public void setRatings(ForeignCollection<Rating> ratings) {
		this.ratings = ratings;
	}

	public ForeignCollection<ShowUser> getShowUsers() {
		return showUsers;
	}

	public void setShowUsers(ForeignCollection<ShowUser> showUsers) {
		this.showUsers = showUsers;
	}

	////////////////////////////////////////////////////////////////////////////////////////////////

	@Override
	public String toString() {
		return "Show [title=" + title + ", year=" + year + ", url=" + url + ", firstAired=" + firstAired
				+ ", firstAiredISO=" + firstAiredISO + ", firstAiredUTC=" + firstAiredUTC + ", country=" + country
				+ ", overview=" + overview + ", runtime=" + runtime + ", status=" + status + ", network=" + network
				+ ", airDay=" + airDay + ", airDayUTC=" + airDayUTC + ", airTime=" + airTime + ", airTimeUTC=" + airTimeUTC
				+ ", certification=" + certification + ", imdbID=" + imdbID + ", tvdbID=" + tvdbID + ", tvrageID="
				+ tvrageID + ", lastUpdated=" + lastUpdated + ", posterURL=" + posterURL + ", fanartURL=" + fanartURL
				+ ", bannerURL=" + bannerURL + ", ratingsPercentage=" + ratingsPercentage + ", ratingsVotes=" + ratingsVotes
				+ ", ratingsLoved=" + ratingsLoved + ", ratingsHated=" + ratingsHated + ", statsWatchers=" + statsWatchers
				+ ", statsPlays=" + statsPlays + ", statsScrobbles=" + statsLists + ", statsCollection="
				+ statsCollected + ", seasons=" + seasons + ", genreShows=" + genreShows + ", actorShows=" + actorShows
				+ ", ratings=" + ratings + ", showUsers=" + showUsers + "]";
	}

	@Override
	public boolean equals(Object o) {
		if(o instanceof Show) {
			Show show = (Show) o;
			if(this.getTvdbID() != null && show.getTvdbID() != null) {
				return this.getTvdbID().equals(show.getTvdbID());
			} else {
				if(this.getTitle() != null && show.getTitle() != null) {
					return this.getTitle().equals(show.getTitle());
				}
			}
		}
		return super.equals(o);
	}


	public CharSequence getGenresStr() {
		String genres = "";
		boolean first = true;
		for (GenreShow gs : this.getGenreShows()) {
			if(first) {
				genres += gs.getGenre().getName();
				first = false;
			} else {
				genres += ", " + gs.getGenre().getName();
			}
		}
		return genres;
	}

	public static Show getShow(CompleteShowDTO showDTO) {
		Show show = new Show();
		show.setTitle(showDTO.getTitle());
		show.setYear(showDTO.getYear());
		show.setUrl(showDTO.getUrl());
		show.setFirstAired(showDTO.getFirst_aired());
		show.setFirstAiredISO(showDTO.getFirst_aired_iso());
		show.setFirstAiredUTC(showDTO.getFirst_aired_utc());
		show.setCountry(showDTO.getCountry());
		show.setOverview(showDTO.getOverview());
		show.setRuntime(showDTO.getRuntime());
		show.setStatus(showDTO.getStatus());
		show.setNetwork(showDTO.getNetwork());
		show.setAirDay(showDTO.getAir_day());
		show.setAirDayUTC(showDTO.getAir_day_utc());
		show.setAirTime(showDTO.getAir_time());
		show.setAirTimeUTC(showDTO.getAir_time_utc());
		show.setCertification(showDTO.getCertification());
		show.setImdbID(showDTO.getImdb_id());
		show.setTvdbID(showDTO.getTvdb_id());
		show.setTvrageID(showDTO.getTvrage_id());
		show.setLastUpdated(showDTO.getLast_updated());
		// Images
		show.setPosterURL(showDTO.getImages().getPoster());
		show.setFanartURL(showDTO.getImages().getFanart());
		show.setBannerURL(showDTO.getImages().getBanner());
		// Ratings
		if(showDTO.getRatings() != null) {
			show.setRatingsPercentage(showDTO.getRatings().getPercentage());
			show.setRatingsVotes(showDTO.getRatings().getVotes());
			show.setRatingsLoved(showDTO.getRatings().getLoved());
			show.setRatingsHated(showDTO.getRatings().getHated());
		}
		// Stats
		if(showDTO.getStats() != null) {
			show.setStatsWatchers(showDTO.getStats().getWatchers());
			show.setStatsPlays(showDTO.getStats().getPlays());
			show.setStatsCollected(showDTO.getStats().getCollection());
		}
		return show;
	}

	public static List<Show> getShows(List<CompleteShowDTO> showDTOs) {
		List<Show> shows = new ArrayList<Show>();
		for (CompleteShowDTO showDTO : showDTOs) {
			shows.add(Show.getShow(showDTO));
		}
		return shows;
	}

	public static Comparator<Show> getComparator() {
		return new Comparator<Show>() {
			@Override
			public int compare(Show s1, Show s2) {
				Locale l = Locale.getDefault();
				return s1.getTitle().toUpperCase(l).compareTo(s2.getTitle().toUpperCase(l));	//ascending order
			}
		};
	}

	
}
