package com.szoldapps.tvtrackr.db.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import com.szoldapps.tvtrackr.db.dao.OfflineEpisodeToUnfollowDAO;

@DatabaseTable(tableName = OfflineEpisodeToUnfollow.TABLE_NAME, daoClass = OfflineEpisodeToUnfollowDAO.class)
public class OfflineEpisodeToUnfollow {

	public static final String TABLE_NAME = "Offline_Episode_To_Unfollow";

	public OfflineEpisodeToUnfollow() { /* ORMLite requirement */ }
	
	@DatabaseField (generatedId=true)
	private Integer id;

	@DatabaseField
	private int season;
	
	@DatabaseField
	private int episode;
	
	@DatabaseField (canBeNull = false, foreign = true, columnName = OfflineShowToUnfollow.EPISODES)
	private OfflineShowToUnfollow offlineShowToUnfollow;

	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public int getSeason() {
		return season;
	}
	public void setSeason(int season) {
		this.season = season;
	}
	public int getEpisode() {
		return episode;
	}
	public void setEpisode(int episode) {
		this.episode = episode;
	}
	public OfflineShowToUnfollow getOfflineShowToUnfollow() {
		return offlineShowToUnfollow;
	}
	public void setOfflineShowToUnfollow(OfflineShowToUnfollow offlineShowToUnfollow) {
		this.offlineShowToUnfollow = offlineShowToUnfollow;
	}
	
}
