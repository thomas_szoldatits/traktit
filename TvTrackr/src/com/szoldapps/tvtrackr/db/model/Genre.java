package com.szoldapps.tvtrackr.db.model;

import java.util.Locale;

import com.j256.ormlite.dao.ForeignCollection;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.misc.BaseDaoEnabled;
import com.j256.ormlite.table.DatabaseTable;
import com.szoldapps.tvtrackr.db.dao.GenreDAO;

@DatabaseTable(tableName = Genre.TABLE_NAME, daoClass = GenreDAO.class)
public class Genre extends BaseDaoEnabled<Genre, Integer> {

	public static final String TABLE_NAME = "Genre";
	public static final String SLUG = "slug";
	
	@DatabaseField(id = true, columnName = SLUG)
	private String slug;

	@DatabaseField
    private String name;

    @ForeignCollectionField(eager = false)
    ForeignCollection<GenreShow> genreShows;

    /** ======================================================================================== **/

    public Genre() { /* ORMLite requirement */ }

    public Genre(String name, String slug) {
        this.name = name;
        this.slug = slug;
    }

    public Genre(String name) {
        this.name = name;
        this.slug = this.generateSlug(name);
    }

    /// ======================================================================================== ///

    public String generateSlug(String name) {
        String slug = name.toLowerCase(Locale.US);
        slug.replace(" ", "-");
        return slug;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public ForeignCollection<GenreShow> getGenreShows() {
        return genreShows;
    }

    public void setGenreShows(ForeignCollection<GenreShow> genreShows) {
        this.genreShows = genreShows;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////

    @Override
    public String toString() {
        return "Genre [name=" + name + ", slug=" + slug + ", genreShows="
                + genreShows + "]";
    }

}