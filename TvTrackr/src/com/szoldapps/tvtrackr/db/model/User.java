package com.szoldapps.tvtrackr.db.model;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import com.j256.ormlite.dao.ForeignCollection;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.misc.BaseDaoEnabled;
import com.j256.ormlite.table.DatabaseTable;
import com.szoldapps.tvtrackr.db.dao.UserDAO;

@DatabaseTable(tableName = User.TABLE_NAME, daoClass = UserDAO.class)
public class User extends BaseDaoEnabled<User, Integer> {

	public static final String TABLE_NAME = "User";
	public static final String USERNAME = "username";
	
    @DatabaseField(unique = true, id = true, columnName=USERNAME)
    private String username;

    @DatabaseField
    private String password;

    @DatabaseField
    private Boolean isCurrentUser;

    @DatabaseField
    private String fullName;

    @DatabaseField
    private String gender;

    @DatabaseField
    private Integer age;

    @DatabaseField
    private String location;

    @DatabaseField
    private String about;

    @DatabaseField
    private long joined;

    @DatabaseField
    private String avatar;

    @DatabaseField
    private String url;
    
    @DatabaseField
    private boolean isFirstTimeUserOnDevice = true;
    
    @DatabaseField
    private boolean isFullyLoaded = false;
    
	@DatabaseField
    private long lastCompleteUpdate;

    @ForeignCollectionField(eager = false)
    ForeignCollection<EpisodeUser> episodeUsers;

    @ForeignCollectionField(eager = false)
    ForeignCollection<Rating> ratings;

    @ForeignCollectionField(eager = false)
    ForeignCollection<ShowUser> showUsers;

    // only here for register, trakt doesn't provide a user email via REST-API
    private String email;
    
    /** ======================================================================================== **/

    public User() { /* ORMLite requirement */ }

    public User(String username, String password, Boolean isCurrentUser) {
        this.username = username;
        this.password = password;
        this.isCurrentUser = isCurrentUser;
    }

    /// ======================================================================================== ///

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPasswordHashed(String hashedPassword) {
        this.password = hashedPassword;
    }
    
    public void setPassword(String password) throws UnsupportedEncodingException {
        this.password = getSHA1(password);
    }
    
    public String getSHA1(String text) throws UnsupportedEncodingException {

        MessageDigest md;
        try {
            md = MessageDigest.getInstance("SHA-1");

            byte[] sha1hash = new byte[40];

            md.update(text.getBytes("iso-8859-1"), 0, text.length());
            sha1hash = md.digest();
            return convertToHex(sha1hash);

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace(); // SHOULD NOT HAPPEN
        }
        return null;
    }
    
    private String convertToHex(byte[] data) {
        StringBuffer buffer = new StringBuffer();
        for (int i=0; i<data.length; i++) {
            if (i % 4 == 0 && i != 0)
                buffer.append("");
            int x = (int) data[i];
            if (x<0)
                x+=256;
            if (x<16)
                buffer.append("0");
            buffer.append(Integer.toString(x,16));
        }
        return buffer.toString();
    }

    public Boolean isCurrentUser() {
        return isCurrentUser;
    }

    public void setCurrentUser(Boolean currentUser) {
        this.isCurrentUser = currentUser;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getAbout() {
        return about;
    }

    public void setAbout(String about) {
        this.about = about;
    }

    public long getJoined() {
        return joined;
    }

    public void setJoined(long joined) {
        this.joined = joined;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public boolean isFirstTimeUserOnDevice() {
		return isFirstTimeUserOnDevice;
	}

	public void setFirstTimeUserOnDevice(boolean isFirstTimeUserOnDevice) {
		this.isFirstTimeUserOnDevice = isFirstTimeUserOnDevice;
	}

	public ForeignCollection<EpisodeUser> getEpisodeUsers() {
        return episodeUsers;
    }

    public void setEpisodeUsers(ForeignCollection<EpisodeUser> episodeUsers) {
        this.episodeUsers = episodeUsers;
    }

    public ForeignCollection<Rating> getRatings() {
        return ratings;
    }

    public void setRatings(ForeignCollection<Rating> ratings) {
        this.ratings = ratings;
    }

    public ForeignCollection<ShowUser> getShowUsers() {
        return showUsers;
    }

    public void setShowUsers(ForeignCollection<ShowUser> showUsers) {
        this.showUsers = showUsers;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////

    public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	public long getLastCompleteUpdate() {
		return lastCompleteUpdate;
	}

	public void setLastCompleteUpdate(long lastCompleteUpdate) {
		this.lastCompleteUpdate = lastCompleteUpdate;
	}
	
    public boolean isFullyLoaded() {
		return isFullyLoaded;
	}

	public void setFullyLoaded(boolean isFullyLoaded) {
		this.isFullyLoaded = isFullyLoaded;
	}

	@Override
    public String toString() {
        return "User [username=" + username + ", password="
                + password + ", currentUser=" + isCurrentUser + ", full_name=" + fullName + ", gender=" + gender
                + ", age=" + age + ", location=" + location + ", about=" + about + ", joined="
                + joined + ", avatar=" + avatar + ", url=" + url + "]";
    }

}