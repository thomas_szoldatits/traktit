package com.szoldapps.tvtrackr.db.model;

import com.j256.ormlite.dao.ForeignCollection;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;
import com.szoldapps.tvtrackr.db.dao.SeasonDAO;
import com.szoldapps.tvtrackr.dto.show.SeasonDTO;

@DatabaseTable(tableName = Season.TABLE_NAME, daoClass = SeasonDAO.class)
public class Season {

	public static final String TABLE_NAME = "Season";
	public static final String SEASON_ID = "season_id";
	public static final String SEASON_NR = "season_nr";
	
    @DatabaseField(generatedId = true, columnName = SEASON_ID)
    private Integer id;

    @DatabaseField(columnName = SEASON_NR, uniqueCombo = true)
    private Integer seasonNr;

    @DatabaseField
    private Integer episodeCount;

    @DatabaseField
    private String url;

    @DatabaseField
    private String poster;

    @DatabaseField(canBeNull = false, foreign = true, columnName = Show.TVDB_ID, uniqueCombo = true)
    private Show show;
    
    @ForeignCollectionField(eager = false, orderColumnName=Episode.NUMBER, orderAscending=false)
    ForeignCollection<Episode> episodes;

    /** ======================================================================================== **/

    public Season() { /* ORMLite requirement */ }

    public Season(Integer seasonNr, Integer episodeCount, String url, String poster, Show show) {
        this.seasonNr = seasonNr;
        this.episodeCount = episodeCount;
        this.url = url;
        this.poster = poster;
        this.show = show;
    }

    /// ======================================================================================== ///
    
    public Integer getId() {
        return id;
    }
    
    public Integer getSeasonNr() {
        return seasonNr;
    }

    public void setSeasonNr(Integer seasonNr) {
        this.seasonNr = seasonNr;
    }

    public Integer getEpisodeCount() {
        return episodeCount;
    }

    public void setEpisodeCount(Integer episodeCount) {
        this.episodeCount = episodeCount;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getPoster() {
        return poster;
    }

    public void setPoster(String poster) {
        this.poster = poster;
    }

    public Show getShow() {
        return show;
    }

    public void setShow(Show show) {
        this.show = show;
    }

    public ForeignCollection<Episode> getEpisodes() {
        return episodes;
    }

    public void setEpisodes(ForeignCollection<Episode> episodes) {
        this.episodes = episodes;
    }

    public String getKey() {
        return this.getSeasonNr().toString();
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////////
    

    @Override
    public String toString() {
        return "Season [id=" + id + ", seasonNr=" + seasonNr + ", episodeCount=" + episodeCount + ", url="
                + url + ", poster=" + poster + ", show=" + show.getTitle() + "]";
    }

    public boolean isFullyLoaded() {
        if(this.episodeCount == this.getEpisodes().size()) {
            return true;
        }
        return false;
    }

    public Season getSeason(SeasonDTO seasonDTO, Show s) {
        this.setSeasonNr(seasonDTO.getSeasonNr());
        this.setEpisodeCount(seasonDTO.getEpisodes().size());
        this.setUrl(seasonDTO.getUrl());
        this.setPoster(seasonDTO.getPoster());
        this.setShow(s);
        return this;
    }
}
