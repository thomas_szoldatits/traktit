package com.szoldapps.tvtrackr.db.model;

import com.j256.ormlite.dao.ForeignCollection;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.misc.BaseDaoEnabled;
import com.j256.ormlite.table.DatabaseTable;
import com.szoldapps.tvtrackr.db.dao.EpisodeDAO;
import com.szoldapps.tvtrackr.dto.show.EpisodeDTO;

@DatabaseTable(tableName = Episode.TABLE_NAME, daoClass = EpisodeDAO.class)
public class Episode extends BaseDaoEnabled<Episode, Integer> {

	public static final String TABLE_NAME = "Episode";
	public static final String EPISODE_ID = "episode_id";
    public static final String NUMBER = "number";
    public static final String FIRST_AIRED_UTC = "first_aired_utc";

	@DatabaseField(generatedId = true, columnName = EPISODE_ID)
    private Integer id;

    @DatabaseField(columnName = NUMBER)
    private Integer number;

    @DatabaseField
    private Integer tvdbID;
    
    @DatabaseField
    private String imdbID;

    @DatabaseField
    private String title;

    @DatabaseField
    private String overview;

    @DatabaseField
    private String url;

    @DatabaseField
    private long firstAired;

    @DatabaseField(columnName = FIRST_AIRED_UTC)
    private long firstAiredUTC;

    @DatabaseField
    private String screen;

    @DatabaseField
    private int ratingsPercentage;

    @DatabaseField
    private long ratingsVotes;

    @DatabaseField
    private long ratingsLoved;

    @DatabaseField
    private long ratingsHated;
    
    @DatabaseField
    private long statsPlays;
    
    @DatabaseField
    private long statsWatchers;
    
    @DatabaseField
    private long statsCollected;

    @DatabaseField
    private long statsLists;
    
    @DatabaseField(canBeNull = false, foreign = true, foreignAutoRefresh = true, columnName = Season.SEASON_ID)
    private Season season;
    
    @DatabaseField(canBeNull = false, columnName = Show.TVDB_ID)
    private Integer showTvdbId;

    @ForeignCollectionField(eager = false)
    ForeignCollection<EpisodeUser> episodeUsers;

    @ForeignCollectionField(eager = false)
    ForeignCollection<Rating> ratings;

    /** ======================================================================================== **/

    public Episode() { /* ORMLite requirement */ }

    public Episode(Integer number, String title, Season season) {
        this.number = number;
        this.title = title;
        this.season = season;
        this.showTvdbId = season.getShow().getTvdbID();
    }

    public Episode(Integer number, Integer tvdb_id, String title, String overview, String url,
            Integer firstAired, Integer firstAiredUTC, String screen, Integer ratingsPercentage, Integer ratingsVotes,
            Integer ratingsLoved, Integer ratingsHated, Season season) {
        super();
        this.number = number;
        this.tvdbID = tvdb_id;
        this.title = title;
        this.overview = overview;
        this.url = url;
        this.firstAired = firstAired;
        this.firstAiredUTC = firstAiredUTC;
        this.screen = screen;
        this.ratingsPercentage = ratingsPercentage;
        this.ratingsVotes = ratingsVotes;
        this.ratingsLoved = ratingsLoved;
        this.ratingsHated = ratingsHated;
        this.season = season;
        this.showTvdbId = season.getShow().getTvdbID();
    }

    @Override
    public boolean equals(Object o) {
        if(o instanceof Episode) {
            Episode e = (Episode) o;
            if(e.getId() == this.id) {
                return true;
            }
            return false;
        }
        return super.equals(o);
    }

    
    /// ======================================================================================== ///

    public Integer getId() {
        return id;
    }


    public Integer getNumber() {
        return number;
    }


    public void setNumber(Integer number) {
        this.number = number;
    }


    public Integer getTvdbID() {
        return tvdbID;
    }


    public void setTvdbID(Integer tvdbID) {
        this.tvdbID = tvdbID;
    }


    public String getImdbID() {
        return imdbID;
    }

    public void setImdbID(String imdbID) {
        this.imdbID = imdbID;
    }

    public String getTitle() {
        return title;
    }


    public void setTitle(String title) {
        this.title = title;
    }


    public String getOverview() {
        return overview;
    }


    public void setOverview(String overview) {
        this.overview = overview;
    }


    public String getUrl() {
        return url;
    }


    public void setUrl(String url) {
        this.url = url;
    }


    public long getFirstAired() {
        return firstAired;
    }


    public void setFirstAired(long firstAired) {
        this.firstAired = firstAired;
    }


    public long getFirstAiredUTC() {
        return firstAiredUTC;
    }

    public void setFirstAiredUTC(long firstAiredUTC) {
        this.firstAiredUTC = firstAiredUTC;
    }

    public String getScreen() {
        return screen;
    }


    public void setScreen(String screen) {
        this.screen = screen;
    }


    public int getRatingsPercentage() {
        return ratingsPercentage;
    }


    public void setRatingsPercentage(int ratingsPercentage) {
        this.ratingsPercentage = ratingsPercentage;
    }


    public long getRatingsVotes() {
        return ratingsVotes;
    }


    public void setRatingsVotes(long ratingsVotes) {
        this.ratingsVotes = ratingsVotes;
    }


    public long getRatingsLoved() {
        return ratingsLoved;
    }


    public void setRatingsLoved(long ratingsLoved) {
        this.ratingsLoved = ratingsLoved;
    }


    public long getRatingsHated() {
        return ratingsHated;
    }


    public void setRatingsHated(long ratingsHated) {
        this.ratingsHated = ratingsHated;
    }


    public long getStatsPlays() {
        return statsPlays;
    }

    public void setStatsPlays(long statsPlays) {
        this.statsPlays = statsPlays;
    }

    public long getStatsWatchers() {
        return statsWatchers;
    }

    public void setStatsWatchers(long statsWatchers) {
        this.statsWatchers = statsWatchers;
    }

    public long getStatsCollected() {
        return statsCollected;
    }

    public void setStatsCollected(long statsCollected) {
        this.statsCollected = statsCollected;
    }

    public long getStatsLists() {
        return statsLists;
    }

    public void setStatsLists(long statsLists) {
        this.statsLists = statsLists;
    }

    public Season getSeason() {
        return season;
    }


    public void setSeason(Season season) {
        this.season = season;
    }

    public Integer getShowTvdbId() {
		return showTvdbId;
	}

	public void setShowTvdbId(Integer showTvdbId) {
		this.showTvdbId = showTvdbId;
	}

	public ForeignCollection<EpisodeUser> getEpisodeUsers() {
        return episodeUsers;
    }

    public void setEpisodeUsers(ForeignCollection<EpisodeUser> episodeUsers) {
        this.episodeUsers = episodeUsers;
    }

    public ForeignCollection<Rating> getRatings() {
        return ratings;
    }

    public void setRatings(ForeignCollection<Rating> ratings) {
        this.ratings = ratings;
    }

    /** --------------------------------------- **/

    public String getKey() {
        return this.season.getSeasonNr() + "_" + this.getNumber();
    }

    public String getSeasonEpisodeText() {
        Integer seasonNr = this.getSeason().getSeasonNr();
        String ret = "S";
        if(seasonNr < 10) {
            ret += "0";
        }
        ret += seasonNr + "E";
        if(this.getNumber() < 10) {
            ret += "0";
        }
        ret += this.getNumber();
        return ret;
    }
    ////////////////////////////////////////////////////////////////////////////////////////////////

    @Override
    public String toString() {
        return "Episode [id=" + id + ", number=" + number + ", tvdb_id=" + tvdbID + ", title="
                + title + ", overview=" + overview + ", url=" + url + ", firstAired=" + firstAired + ", firstAiredUTC=" + firstAiredUTC
                + ", screen=" + screen + ", ratingsPercentage=" + ratingsPercentage
                + ", ratingsVotes=" + ratingsVotes + ", ratingsLoved=" + ratingsLoved
                + ", ratingsHated=" + ratingsHated + ", season=" + season.getSeasonNr() + "]";
    }

    public Episode getEpisode(EpisodeDTO eDTO, Season s) {
        this.setNumber(eDTO.getNumber());
        this.setTvdbID(eDTO.getTvdb_id());
        this.setTitle(eDTO.getTitle());
        this.setOverview(eDTO.getOverview());
        this.setUrl(eDTO.getUrl());
        this.setFirstAired(eDTO.getFirst_aired());
        this.setFirstAiredUTC(eDTO.getFirst_aired_utc());
        this.setScreen(eDTO.getScreen());
        this.setRatingsPercentage(eDTO.getRatings().getPercentage());
        this.setRatingsVotes(eDTO.getRatings().getVotes());
        this.setRatingsLoved(eDTO.getRatings().getLoved());
        this.setRatingsHated(eDTO.getRatings().getHated());
        this.setSeason(s);
        this.setShowTvdbId(s.getShow().getTvdbID());
        return this;
    }

}