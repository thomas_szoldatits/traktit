package com.szoldapps.tvtrackr.db.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import com.szoldapps.tvtrackr.db.dao.ShowUserDAO;

@DatabaseTable(tableName = ShowUser.TABLE_NAME, daoClass = ShowUserDAO.class)
public class ShowUser {

	public static final String TABLE_NAME = "ShowUser";
	
    @DatabaseField(generatedId = true)
    private Integer id;

    @DatabaseField(foreign = true, columnName = Show.TVDB_ID, foreignAutoRefresh = true)
    private Show show;

    @DatabaseField(foreign = true, columnName = User.USERNAME, foreignAutoRefresh = true)
    private User user;

    @DatabaseField
    private Boolean inWatchlist;
    
    @DatabaseField
    private boolean offlineMarked = false;
    
    /** ======================================================================================== **/

    public ShowUser() { /* ORMLite requirement */ }
    
    public ShowUser(Show show, User user, Boolean inWatchlist) {
        super();
        this.show = show;
        this.user = user;
        this.inWatchlist = inWatchlist;
    }

    /// ======================================================================================== ///

    public Integer getId() {
        return id;
    }


    public Show getShow() {
        return show;
    }


    public void setShow(Show show) {
        this.show = show;
    }


    public User getUser() {
        return user;
    }


    public void setUser(User user) {
        this.user = user;
    }

    public Boolean getInWatchlist() {
        return inWatchlist;
    }
    
    public void setInWatchlist(Boolean inWatchlist) {
        this.inWatchlist = inWatchlist;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////

    @Override
    public String toString() {
        return "ShowUser[tvdbID:"+show.getTvdbID()+" | username:"+user.getUsername()+" | inWatchlist:"+inWatchlist+"]";
    }

}