package com.szoldapps.tvtrackr.db.manager;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.util.Log;

import com.szoldapps.tvtrackr.db.dao.ActorDAO;
import com.szoldapps.tvtrackr.db.dao.ActorShowDAO;
import com.szoldapps.tvtrackr.db.dao.EpisodeDAO;
import com.szoldapps.tvtrackr.db.dao.EpisodeUserDAO;
import com.szoldapps.tvtrackr.db.dao.GenreDAO;
import com.szoldapps.tvtrackr.db.dao.GenreShowDAO;
import com.szoldapps.tvtrackr.db.dao.RatingDAO;
import com.szoldapps.tvtrackr.db.dao.SeasonDAO;
import com.szoldapps.tvtrackr.db.dao.ShowDAO;
import com.szoldapps.tvtrackr.db.dao.ShowUserDAO;
import com.szoldapps.tvtrackr.db.dao.UserDAO;
import com.szoldapps.tvtrackr.db.model.Actor;
import com.szoldapps.tvtrackr.db.model.ActorShow;
import com.szoldapps.tvtrackr.db.model.Episode;
import com.szoldapps.tvtrackr.db.model.EpisodeUser;
import com.szoldapps.tvtrackr.db.model.Genre;
import com.szoldapps.tvtrackr.db.model.GenreShow;
import com.szoldapps.tvtrackr.db.model.Rating;
import com.szoldapps.tvtrackr.db.model.Season;
import com.szoldapps.tvtrackr.db.model.Show;
import com.szoldapps.tvtrackr.db.model.ShowUser;
import com.szoldapps.tvtrackr.db.model.User;
import com.szoldapps.tvtrackr.helper.LogCatHelper;

public class DBTest extends Activity {

    public static final String LOGTAG = LogCatHelper.getAppTag()+DBTest.class.getSimpleName().toString();
    
    @SuppressWarnings("unused")
	private void dbTests() {


        try {

            DBHelper dbh = DBManager.getHelper(this);

            ShowDAO showDAO = dbh.getShowDAO();
            SeasonDAO seasonDAO = dbh.getSeasonDAO();
            GenreDAO genreDAO = dbh.getGenreDAO();
            GenreShowDAO genreShowDAO = dbh.getGenreShowDAO();
            ActorDAO actorDAO = dbh.getActorDAO();
            ActorShowDAO actorShowDAO = dbh.getActorShowDAO();
            EpisodeDAO episodeDAO = dbh.getEpisodeDAO();
            UserDAO userDAO = dbh.getUserDAO();
            EpisodeUserDAO episodeUserDAO = dbh.getEpisodeUserDAO();
            RatingDAO ratingDAO = dbh.getRatingDAO();
            ShowUserDAO showUserDAO = dbh.getShowUserDAO();

            showDAO.delete(showDAO.queryForAll());
            seasonDAO.delete(seasonDAO.queryForAll());
            genreDAO.delete(genreDAO.queryForAll());
            genreShowDAO.delete(genreShowDAO.queryForAll());
            actorDAO.delete(actorDAO.queryForAll());
            actorShowDAO.delete(actorShowDAO.queryForAll());
            episodeDAO.delete(episodeDAO.queryForAll());
            userDAO.delete(userDAO.queryForAll());
            episodeUserDAO.delete(episodeUserDAO.queryForAll());
            ratingDAO.delete(ratingDAO.queryForAll());
            showUserDAO.delete(showUserDAO.queryForAll());


            Show show1 = new Show();
            showDAO.create(show1);

            Season s1 = new Season(1, 10, "http://someurl.com/", "http://somePOSTERurl.com/", show1);
            seasonDAO.create(s1);

            Genre g1 = new Genre("Drama", "drama");
            Genre g2 = new Genre("Comedy", "comedy");
            genreDAO.create(g1);
            genreDAO.create(g2);


            GenreShow gs1 = new GenreShow(g1, show1);
            GenreShow gs2 = new GenreShow(g2, show1);
            genreShowDAO.create(gs1);
            genreShowDAO.create(gs2);

            Actor actor1 = new Actor("Courteney Cox", "http://courteneyCox.com", "bio", "01-01-1970", "home", 1234, "http://headshot.com");
            Actor actor2 = new Actor("Will Ferrell", "http://willFerrell.com", "bio", "01-01-1970", "home", 1234, "http://headshot.com");
            actorDAO.create(actor1);
            actorDAO.create(actor2);

            ActorShow as1 = new ActorShow(actor1, show1, "charatername");
            ActorShow as2 = new ActorShow(actor2, show1, "charatername");
            actorShowDAO.create(as1);
            actorShowDAO.create(as2);

            Episode e1 = new Episode(1, "First Episode", s1);
            Episode e2 = new Episode(2, "Second Episode", s1);
            Episode e3 = new Episode(3, "Third Episode", s1);
            episodeDAO.create(e1);
            episodeDAO.create(e2);
            episodeDAO.create(e3);



            User u1 = new User("soldmachinex", "password", true);
            userDAO.create(u1);
            User u2 = new User("soldmachine123", "password", true);
            userDAO.create(u2);

            EpisodeUser eu1 = new EpisodeUser(e1, u1, true);
            EpisodeUser eu2 = new EpisodeUser(e2, u1, true);
            EpisodeUser eu3 = new EpisodeUser(e3, u1, false);
            episodeUserDAO.create(eu1);
            episodeUserDAO.create(eu2);
            episodeUserDAO.create(eu3);


            Rating r1 = new Rating(u1, show1, e1, "love", null);
            Rating r2 = new Rating(u1, show1, null, null, 93);
            Rating r3 = new Rating(u1, show1, e3, "", 10);
            ratingDAO.create(r1);
            ratingDAO.create(r2);
            ratingDAO.create(r3);

            ShowUser w1 = new ShowUser(show1, u1, true);
            showUserDAO.create(w1);

            Log.d("SOME TAG", "-------------------------");
            Show x = showDAO.queryForId(show1.getTvdbID());
            Log.d("SOME TAG", x.toString());
            ArrayList<Season> seasons = new ArrayList<Season>(x.getSeasons());
            Log.d("SOME TAG", seasons.get(0).toString());
            ArrayList<GenreShow> genreShows = new ArrayList<GenreShow>(x.getGenreShows());

            for (GenreShow genreShow : genreShows) {
                Genre g = genreShow.getGenre();
                Log.d("GENRE SHOW", genreShow.toString() + ", " + g.toString());
            }
            Log.d("SOME TAG", "-------------------------");

            List<User> userList = userDAO.queryForEq("username", "soldmachine");

            Log.v(LOGTAG, userList.size() + " ||| " + userList.get(0).toString());

            DBManager.releaseHelper();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
