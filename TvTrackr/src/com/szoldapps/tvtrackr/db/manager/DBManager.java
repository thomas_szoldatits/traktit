package com.szoldapps.tvtrackr.db.manager;

import android.content.Context;

import com.j256.ormlite.android.apptools.OpenHelperManager;

public class DBManager {

	private static DBHelper DB_HELPER = null;

	public DBManager() { };
	
	//gets a helper once one is created ensures it doesnt create a new one
	public static DBHelper getHelper(Context context)
	{
		if (DB_HELPER == null) {
			DB_HELPER = OpenHelperManager.getHelper(context, DBHelper.class);
		}
		return DB_HELPER;
	}

	//releases the helper once usages has ended
	public static void releaseHelper()
	{
		if (DB_HELPER != null) {
			DB_HELPER.close();
			OpenHelperManager.releaseHelper();
			DB_HELPER = null;
		}
	}

}