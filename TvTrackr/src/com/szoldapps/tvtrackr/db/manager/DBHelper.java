package com.szoldapps.tvtrackr.db.manager;

import java.sql.SQLException;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;
import com.szoldapps.tvtrackr.db.dao.ActorDAO;
import com.szoldapps.tvtrackr.db.dao.ActorShowDAO;
import com.szoldapps.tvtrackr.db.dao.EpisodeDAO;
import com.szoldapps.tvtrackr.db.dao.EpisodeUserDAO;
import com.szoldapps.tvtrackr.db.dao.GenreDAO;
import com.szoldapps.tvtrackr.db.dao.GenreShowDAO;
import com.szoldapps.tvtrackr.db.dao.OfflineEpisodeToUnfollowDAO;
import com.szoldapps.tvtrackr.db.dao.OfflineShowToUnfollowDAO;
import com.szoldapps.tvtrackr.db.dao.RatingDAO;
import com.szoldapps.tvtrackr.db.dao.SeasonDAO;
import com.szoldapps.tvtrackr.db.dao.ShowDAO;
import com.szoldapps.tvtrackr.db.dao.ShowUserDAO;
import com.szoldapps.tvtrackr.db.dao.UserDAO;
import com.szoldapps.tvtrackr.db.model.Actor;
import com.szoldapps.tvtrackr.db.model.ActorShow;
import com.szoldapps.tvtrackr.db.model.Episode;
import com.szoldapps.tvtrackr.db.model.EpisodeUser;
import com.szoldapps.tvtrackr.db.model.Genre;
import com.szoldapps.tvtrackr.db.model.GenreShow;
import com.szoldapps.tvtrackr.db.model.OfflineEpisodeToUnfollow;
import com.szoldapps.tvtrackr.db.model.OfflineShowToUnfollow;
import com.szoldapps.tvtrackr.db.model.Rating;
import com.szoldapps.tvtrackr.db.model.Season;
import com.szoldapps.tvtrackr.db.model.Show;
import com.szoldapps.tvtrackr.db.model.ShowUser;
import com.szoldapps.tvtrackr.db.model.User;

public class DBHelper extends OrmLiteSqliteOpenHelper {

	// name of the database file for your application -- change to something appropriate for your app
	private static final String DATABASE_NAME = "TVTrackr.db";
	// any time you make changes to your database objects, you may have to increase the database version
	private static final int DATABASE_VERSION = 1;

	public DBHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	/**
	 * This is called when the database is first created. Usually you should call createTable statements here to create
	 * the tables that will store your data.
	 */
	@Override
	public void onCreate(SQLiteDatabase db, ConnectionSource connectionSource) {
		try {
			Log.i(DBHelper.class.getName(), "onCreate");
			TableUtils.createTable(connectionSource, Show.class);
			TableUtils.createTable(connectionSource, Season.class);
			TableUtils.createTable(connectionSource, Genre.class);
			TableUtils.createTable(connectionSource, GenreShow.class);
			TableUtils.createTable(connectionSource, Actor.class);
            TableUtils.createTable(connectionSource, ActorShow.class);
            TableUtils.createTable(connectionSource, Episode.class);
            TableUtils.createTable(connectionSource, User.class);
            TableUtils.createTable(connectionSource, EpisodeUser.class);
            TableUtils.createTable(connectionSource, Rating.class);
            TableUtils.createTable(connectionSource, ShowUser.class);
            TableUtils.createTable(connectionSource, OfflineShowToUnfollow.class);
            TableUtils.createTable(connectionSource, OfflineEpisodeToUnfollow.class);
		} catch (SQLException e) {
			Log.e(DBHelper.class.getName(), "Can't create database", e);
			throw new RuntimeException(e);
		}

	}

	/**
	 * This is called when your application is upgraded and it has a higher version number. This allows you to adjust
	 * the various data to match the new version number.
	 */
	@Override
	public void onUpgrade(SQLiteDatabase db, ConnectionSource connectionSource, int oldVersion, int newVersion) {
		try {
			Log.i(DBHelper.class.getName(), "onUpgrade");
			dropAllTables(connectionSource);
			// after we drop the old databases, we create the new ones
			onCreate(db, connectionSource);
		} catch (SQLException e) {
			Log.e(DBHelper.class.getName(), "Can't drop databases", e);
			throw new RuntimeException(e);
		}
	}


	private void dropAllTables(ConnectionSource connectionSource) throws SQLException {
		TableUtils.dropTable(connectionSource, Show.class, true);
		TableUtils.dropTable(connectionSource, Season.class, true);
		TableUtils.dropTable(connectionSource, Genre.class, true);
        TableUtils.dropTable(connectionSource, GenreShow.class, true);
        TableUtils.dropTable(connectionSource, Actor.class, true);
        TableUtils.dropTable(connectionSource, ActorShow.class, true);
        TableUtils.dropTable(connectionSource, Episode.class, true);
        TableUtils.dropTable(connectionSource, User.class, true);
        TableUtils.dropTable(connectionSource, EpisodeUser.class, true);
        TableUtils.dropTable(connectionSource, Rating.class, true);
        TableUtils.dropTable(connectionSource, ShowUser.class, true);
        TableUtils.dropTable(connectionSource, OfflineShowToUnfollow.class, true);
        TableUtils.dropTable(connectionSource, OfflineEpisodeToUnfollow.class, true);
	}

	/**
	 * Close the database connections and clear any cached DAOs.
	 */
	@Override
	public void close() {
		super.close();
		showDAO = null;
		seasonDAO = null;
		genreDAO = null;
		genreShowDAO = null;
		actorDAO = null;
        actorShowDAO = null;
        episodeDAO = null;
        userDAO = null;
        episodeUserDAO = null;
        ratingDAO = null;
        showUserDAO = null;
        offlineShowToUnfollowDAO = null;
        offlineEpisodeToUnfollowDAO = null;
	}
	
	/** ========================================================================================================== **/
	/** ========================================================================================================== **/
	/** =============================================DAOs========================================================= **/
	/**
     * Returns the Database Access Object (DAO). It will create it or just give the cached value.
     */
	
	private ShowDAO showDAO = null;
	private SeasonDAO seasonDAO = null;
	private GenreDAO genreDAO = null;
	private GenreShowDAO genreShowDAO = null;
	private ActorDAO actorDAO = null;
    private ActorShowDAO actorShowDAO = null;
    private EpisodeDAO episodeDAO = null;
    private UserDAO userDAO = null;
    private EpisodeUserDAO episodeUserDAO = null;
    private RatingDAO ratingDAO = null;
    private ShowUserDAO showUserDAO = null;
    private OfflineShowToUnfollowDAO offlineShowToUnfollowDAO = null;
    private OfflineEpisodeToUnfollowDAO offlineEpisodeToUnfollowDAO = null;
	
	
	public ShowDAO getShowDAO() throws SQLException {
		if (showDAO == null) { showDAO = getDao(Show.class); }
		return showDAO;
	}
	
	public SeasonDAO getSeasonDAO() throws SQLException {
		if (seasonDAO == null) { seasonDAO = getDao(Season.class); }
		return seasonDAO;
	}
	
	public GenreDAO getGenreDAO() throws SQLException {
        if (genreDAO == null) { genreDAO = getDao(Genre.class); }
        return genreDAO;
    }
	
	public GenreShowDAO getGenreShowDAO() throws SQLException {
        if (genreShowDAO == null) { genreShowDAO = getDao(GenreShow.class); }
        return genreShowDAO;
    }
	
	public ActorDAO getActorDAO() throws SQLException {
        if (actorDAO == null) { actorDAO = getDao(Actor.class); }
        return actorDAO;
    }
    
    public ActorShowDAO getActorShowDAO() throws SQLException {
        if (actorShowDAO == null) { actorShowDAO = getDao(ActorShow.class); }
        return actorShowDAO;
    }
    
    public EpisodeDAO getEpisodeDAO() throws SQLException {
        if (episodeDAO == null) { episodeDAO = getDao(Episode.class); }
        return episodeDAO;
    }
    
    public UserDAO getUserDAO() throws SQLException {
        if (userDAO == null) { userDAO = getDao(User.class); }
        return userDAO;
    }
    
    public EpisodeUserDAO getEpisodeUserDAO() throws SQLException {
        if (episodeUserDAO == null) { episodeUserDAO = getDao(EpisodeUser.class); }
        return episodeUserDAO;
    }
    
    public RatingDAO getRatingDAO() throws SQLException {
        if (ratingDAO == null) { ratingDAO = getDao(Rating.class); }
        return ratingDAO;
    }
    
    public ShowUserDAO getShowUserDAO() throws SQLException {
        if (showUserDAO == null) { showUserDAO = getDao(ShowUser.class); }
        return showUserDAO;
    }
    
    public OfflineShowToUnfollowDAO getOfflineShowToUnfollowDAO() throws SQLException {
        if (offlineShowToUnfollowDAO == null) { offlineShowToUnfollowDAO = getDao(OfflineShowToUnfollow.class); }
        return offlineShowToUnfollowDAO;
    }
    
    public OfflineEpisodeToUnfollowDAO getOfflineEpisodeToUnfollowDAO() throws SQLException {
        if (offlineEpisodeToUnfollowDAO == null) { offlineEpisodeToUnfollowDAO = getDao(OfflineEpisodeToUnfollow.class); }
        return offlineEpisodeToUnfollowDAO;
    }
    
	/** =============================================DAOs========================================================= **/
	/** ========================================================================================================== **/
	/** ========================================================================================================== **/
	
	
}