MOST IMPORTANT FEATURES
0. Tracking
1. Search
2. Calendar
3. Show Info
4. Episode Info
5. ToWatch











- to show that one has watched a show/episode use a stamp like animation (like DENIED/APPROVED)

- check whether FB provides API to read out liked TV-Shows!?! -> YES

Possible names
- TRACK IT
- TRAKT IT
- ShowTracker


logo ideas
- tv with eye in the middle (abstract or real)


after finishing a season -> ask user if he/she wants to post review/share



Colors
lighter-blue: #0066ff
darker-blue: #330099


Things that are cool, but probably won’t make it in v.1.0
- Parallax effect in showDetail (@fanart)
- Add a Statistics page (like Runtastic - http://www.runtastic.com/en/users/Thomas-Szoldatits/statistics)
- Reload animation like the one from Tumblr or Gmail (on iOS)


Settings
- Specials: also show specials